===============================================================================
Running with 1 support instances - 0.4 similarity.
Number of CQAs: 6

Binary query : ?answer0 <http://aims.fao.org/aos/agrontology#hasTaxonomicRank> ?answer1.
[<http://dbpedia.org/resource/Coffea>] <--> [<http://dbpedia.org/resource/Genus>]
Number of matched answers :1
No path found, similar answers : [<http://dbpedia.org/resource/Coffea>] <--> [<http://dbpedia.org/resource/Genus>]
Number of correspondences found: 0

Binary query : ?answer0 <http://aims.fao.org/aos/agrontology#hasTaxonomicRank> ?therank. {?answer0 <http://www.w3.org/2004/02/skos/core#prefLabel> ?answer1.} UNION{ ?answer0 <http://www.w3.org/2008/05/skos-xl#prefLabel> ?z. ?z <http://www.w3.org/2008/05/skos-xl#literalForm> ?answer1. } filter(lang(?answer1)="en")
[<http://dbpedia.org/resource/Manilkara_zapota>] <--> []
Number of matched answers :1
Number of correspondences found: 1
1.9444444444444444 <-> ?answer0 a <http://www.wikidata.org/entity/Q756>.  ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  

Unary query : ?answer <http://aims.fao.org/aos/agrontology#hasTaxonomicRank> <http://aims.fao.org/aos/agrovoc/c_11125>.
[<http://dbpedia.org/resource/Coffea>]
Number of matched answers :1
Number of correspondences found: 7
1.9 <-> ?answer <http://dbpedia.org/property/genus> ?someObject. 
2.0 <-> ?answer <http://dbpedia.org/ontology/order> ?someObject. 
2.5857142857142863 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.wikidata.org/entity/Q756>. 
3.014285714285715 <-> ?answer <http://dbpedia.org/ontology/family> ?y. ?y a <http://www.wikidata.org/entity/Q756>. 
4.287552521008404 <-> ?x <http://purl.org/linguistics/gold/hypernym> ?answer. ?x a <http://www.wikidata.org/entity/Q756>. 
5.989390756302521 <-> ?x <http://dbpedia.org/ontology/genus> ?answer. ?x a <http://www.wikidata.org/entity/Q756>. 
10.435714285714287 <-> ?answer <http://dbpedia.org/property/typeSpeciesAuthority> ?y. ?y a <http://www.wikidata.org/entity/Q5>. 

Binary query : ?answer0 <http://aims.fao.org/aos/agrontology#hasTaxonomicRank> ?rank1. ?answer1 <http://aims.fao.org/aos/agrontology#hasTaxonomicRank> ?rank2. ?answer0 <http://www.w3.org/2004/02/skos/core#broader> ?answer1.
[<http://dbpedia.org/resource/Pistacia_lentiscus>] <--> [<http://dbpedia.org/resource/Pistacia>]
Number of matched answers :1
Number of correspondences found: 1
0.5 <-> ?answer0 <http://dbpedia.org/ontology/genus> ?answer1.  

Unary query : ?answer <http://aims.fao.org/aos/agrontology#hasTaxonomicRank> ?y. ?y <http://www.w3.org/2004/02/skos/core#broader>+ <http://aims.fao.org/aos/agrovoc/c_7624>.
[<http://dbpedia.org/resource/Manilkara_zapota>]
Number of matched answers :1
Number of correspondences found: 7
0.8571428571428572 <-> ?answer <http://dbpedia.org/property/caption> ?someObject. 
1.0 <-> ?answer <http://dbpedia.org/ontology/order> ?someObject. 
5.271428571428571 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.wikidata.org/entity/Q756>. 
5.271428571428571 <-> ?answer <http://dbpedia.org/ontology/division> ?y. ?y a <http://www.wikidata.org/entity/Q756>. 
5.271428571428571 <-> ?answer <http://dbpedia.org/ontology/family> ?y. ?y a <http://www.wikidata.org/entity/Q756>. 
6.447899159663866 <-> ?answer <http://dbpedia.org/ontology/genus> ?y. ?y a <http://www.wikidata.org/entity/Q756>. 
7.057142857142859 <-> ?answer <http://dbpedia.org/ontology/binomialAuthority> ?y. ?y a <http://www.wikidata.org/entity/Q5>. 

Binary query : ?answer0 <http://www.w3.org/2004/02/skos/core#broader>+ ?answer1. ?answer1 <http://aims.fao.org/aos/agrontology#hasTaxonomicRank> <http://aims.fao.org/aos/agrovoc/c_330934>.
Number of matched answers :0
Looking for similar answers
[<http://dbpedia.org/resource/Bambusa_vulgaris>] <--> [<http://dbpedia.org/resource/Plantae>]
Number of similar answers :1
Number of correspondences found: 1
2.907738095238095 <-> ?answer0 a <http://www.wikidata.org/entity/Q756>.  ?answer0 <http://dbpedia.org/ontology/kingdom> ?answer1.  
Matching process ended
