===============================================================================
creating directory: ekaw-confOf
Running with 10 support instances - 0.1 similarity.
Number of CQAs: 65

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Proceedings_Publisher> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Rejected_Paper> .
Number of matched answers :10
Number of correspondences found: 3
0.646041123216204 <-> ?x <http://confOf#writes> ?answer. ?x a <http://confOf#Member_PC>. 
0.7369036430361623 <-> ?answer <http://confOf#writtenBy> ?y. ?y a <http://confOf#Scholar>. 
0.9285714285714285 <-> ?x <http://confOf#reviewes> ?answer. ?x a <http://confOf#Member_PC>. 
creating directory: rejected_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Track> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Participant> .   {?answer a <http://ekaw#Early-Registered_Participant> . } union{ ?answer a <http://ekaw#Late-Registered_Participant> . }
Number of matched answers :10
Number of correspondences found: 12
0.6189273689273689 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 
0.9165223665223665 <-> ?answer <http://confOf#hasSurname> ?someObject. 
1.0336099086099086 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar>. 
1.2093554593554594 <-> ?answer <http://confOf#studyAt> ?y. ?y a <http://confOf#Organization>. 
1.2317412217412218 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
1.3006780382687824 <-> ?answer <http://confOf#employedBy> ?someObject. 
1.4139971139971141 <-> ?answer <http://confOf#earlyRegistration> ?someObject. 
1.5093795093795095 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
1.5275372775372777 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant>. 
3.448474734152557 <-> ?answer <http://confOf#reviewes> ?someObject. 
3.8297859146709867 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
3.829785914670987 <-> ?answer <http://confOf#writes> ?someObject. 
creating directory: participant

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#OC_Member> .
Number of matched answers :10
Number of correspondences found: 5
0.31666666666666676 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
0.7777777777777779 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
0.8086368943008148 <-> ?answer <http://confOf#reviewes> ?someObject. 
0.8522382421917186 <-> ?answer <http://confOf#writes> ?someObject. 
0.8522382421917186 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
creating directory: oc_member

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Proceedings> .   ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer1 <http://ekaw#hasEvent> ?answer0 .  ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .  ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop> .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://ekaw#reviewOfPaper> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Tutorial> .  ?answer0 <http://ekaw#coversTopic> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
4.284090909090908 <-> ?answer0 a <http://confOf#Tutorial>.  ?answer0 <http://confOf#hasTopic> ?answer1.  ?answer1 a <http://confOf#Topic>.  
creating directory: tutorial_topic

Binary query : ?answer0 a <http://ekaw#Conference_Participant>.      bind( if (exists {?answer0 a <http://ekaw#Early-Registered_Participant>}, "true"^^<http://www.w3.org/2001/XMLSchema#boolean>,        if (exists {?answer0 a <http://ekaw#Late-Registered_Participant>}, "false"^^<http://www.w3.org/2001/XMLSchema#boolean>,"")) as ?answer1) filter(?answer1 != "")
Number of matched answers :10
Number of correspondences found: 2
3.8582498656028066 <-> ?answer0 a <http://confOf#Participant>.  ?answer0 <http://confOf#earlyRegistration> ?answer1.  
3.894385026737968 <-> ?answer0 a <http://confOf#Member_PC>.  ?answer0 <http://confOf#earlyRegistration> ?answer1.  
creating directory: early_registration

Binary query : ?answer1 <http://ekaw#hasEvent> ?answer0 .  ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .  ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Tutorial> .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 a <http://ekaw#Conference> . ?answer0 <http://ekaw#heldIn> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
3.9433908045977013 <-> ?answer0 a <http://confOf#Conference>.  ?answer0 <http://confOf#location> ?answer1.  
creating directory: conference_location

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Late-Registered_Participant> .
Number of matched answers :10
Number of correspondences found: 4
0.5555555555555556 <-> ?answer <http://confOf#earlyRegistration> ?someObject. 
1.3054541081631366 <-> ?answer <http://confOf#writes> ?someObject. 
1.3054541081631366 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
1.3123902497501292 <-> ?answer <http://confOf#reviewes> ?someObject. 
creating directory: late_registered_participant

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#University> .
Number of matched answers :10
Number of correspondences found: 2
0.6 <-> <http://confOf-instances#person274495320> <http://confOf#studyAt> ?answer. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#University>. 
creating directory: university

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Trip> .
Number of matched answers :5
Number of correspondences found: 2
0.6000000000000001 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Event>. 
0.8524137931034483 <-> ?answer <http://confOf#location> ?someObject. 
creating directory: trip

Binary query : ?answer0 a <http://ekaw#Invited_Speaker>. ?answer1 <http://ekaw#writtenBy> ?answer0. ?answer1 a <http://ekaw#Invited_Talk_Abstract>.
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 a <http://ekaw#Invited_Speaker>. ?o2 <http://ekaw#writtenBy> ?answer0. ?o2 <http://ekaw#paperPresentedAs> ?answer1. ?answer1 a <http://ekaw#Invited_Talk>.
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member> .   ?answer0 <http://ekaw#reviewerOfPaper> ?answer1 .
Number of matched answers :10
Number of correspondences found: 2
3.4111111111111114 <-> ?answer0 a <http://confOf#Member_PC>.  ?answer0 <http://confOf#reviewes> ?answer1.  ?answer1 a <http://confOf#Poster>.  
3.544444444444445 <-> ?answer0 a <http://confOf#Member_PC>.  ?answer0 <http://confOf#reviewes> ?answer1.  ?answer1 a <http://confOf#Paper>.  
creating directory: is_assigned_reviewer_of

Unary query : ?o3 <http://ekaw#reviewWrittenBy> ?answer. MINUS{ ?answer <http://ekaw#reviewerOfPaper> ?o2. }
Number of matched answers :10
Number of correspondences found: 5
0.7111111111111109 <-> ?answer <http://confOf#hasSurname> ?someObject. 
0.8592592592592593 <-> ?answer <http://confOf#employedBy> ?y. ?y a <http://confOf#University>. 
0.9599999999999997 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
2.213013068671753 <-> ?answer <http://confOf#writes> ?y. ?y a <http://confOf#Paper>. 
2.3839339370190924 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
creating directory: external_reviewer

Binary query : ?answer1 <http://ekaw#reviewWrittenBy> ?answer0 .   ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member> .  ?answer0 <http://ekaw#reviewerOfPaper> ?o3.
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Track> .   ?answer1 <http://ekaw#hasEvent> ?answer0 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop_Chair> .   ?answer0 <http://ekaw#organises> ?answer1 . ?answer1 a <http://ekaw#Workshop> .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .
Number of matched answers :5
Number of correspondences found: 9
0.6592592592592593 <-> ?answer <http://confOf#starts_on> ?someObject. 
0.7437037037037036 <-> ?answer <http://confOf#has_short_title> ?someObject. 
0.8962962962962961 <-> ?answer <http://confOf#ends_on> ?someObject. 
0.9547137138526225 <-> ?answer <http://confOf#has_title> "International Symposium on Languages in Biology and Medicine (LBM)". 
0.9592592592592594 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Event>. 
1.3703703703703702 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference>. 
1.430170535647209 <-> ?answer <http://confOf#location> ?someObject. 
1.5193504594820384 <-> ?answer <http://confOf#hasTopic> ?y. ?y a <http://confOf#Topic>. 
1.8576646673280144 <-> ?answer <http://confOf#hasAdministrativeEvent> ?y. ?y a <http://confOf#Reviewing_event>. 
creating directory: conference

Binary query : ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .   ?answer0 a <http://ekaw#Person> .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Person> .
Number of matched answers :10
Number of correspondences found: 5
0.6472222222222223 <-> ?answer <http://confOf#hasSurname> ?someObject. 
1.0015997832516184 <-> ?answer <http://confOf#reviewes> ?someObject. 
1.0596695778308833 <-> ?answer <http://confOf#writes> ?someObject. 
1.125 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 
1.170780688941994 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
creating directory: person

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Banquet> .   ?answer0 <http://ekaw#heldIn> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
2.6982758620689657 <-> ?answer0 a <http://confOf#Event>.  ?answer0 <http://confOf#location> ?answer1.  
creating directory: gala_dinner_location

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Proceedings> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer1 <http://ekaw#publisherOf> ?answer0 .  ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Proceedings> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?o3 <http://ekaw#reviewWrittenBy> ?answer0 .   ?o3 <http://ekaw#reviewWrittenBy> ?answer1 . ?answer1 <http://ekaw#reviewerOfPaper> ?o2 . ?o3 <http://ekaw#reviewOfPaper> ?o2 . FILTER(?o!=?answer0)
Number of matched answers :10
Number of correspondences found: 3
4.797435897435897 <-> ?v1 <http://confOf#writtenBy> ?answer0.  ?v1 a <http://confOf#Poster>.  ?answer1 <http://confOf#writes> ?v1.  ?answer1 a <http://confOf#Member_PC>.  
4.797435897435897 <-> ?answer0 <http://confOf#writes> ?v1.  ?v1 a <http://confOf#Poster>.  ?v1 <http://confOf#writtenBy> ?answer1.  ?answer1 a <http://confOf#Member_PC>.  
4.843589743589743 <-> ?v1 <http://confOf#writtenBy> ?answer0.  ?v1 a <http://confOf#Poster>.  ?v1 <http://confOf#writtenBy> ?answer1.  ?answer1 a <http://confOf#Member_PC>.  
creating directory: invite_reviewer

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Tutorial> .
Number of matched answers :10
Number of correspondences found: 2
0.875 <-> ?answer <http://confOf#hasTopic> ?y. ?y a <http://confOf#Topic>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Tutorial>. 
creating directory: tutorial

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Early-Registered_Participant> .
Number of matched answers :10
Number of correspondences found: 4
0.5714285714285714 <-> ?answer <http://confOf#earlyRegistration> ?someObject. 
1.1137516135909922 <-> ?answer <http://confOf#reviewes> ?someObject. 
1.1511313416129594 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
1.1511313416129596 <-> ?answer <http://confOf#writes> ?someObject. 
creating directory: early_registered_participant

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .  ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
2.8768518518518515 <-> ?answer0 a <http://confOf#Conference>.  ?answer0 <http://confOf#has_title> ?answer1.  
creating directory: conference_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member> .   ?answer <http://ekaw#reviewerOfPaper> ?o2.
Number of matched answers :10
Number of correspondences found: 7
0.6586973180076627 <-> ?answer <http://confOf#employedBy> ?someObject. 
0.788888888888889 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
0.8555555555555556 <-> ?answer <http://confOf#hasSurname> ?someObject. 
1.5676779433036345 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
1.577777777777778 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
1.6343446099703014 <-> ?answer <http://confOf#writes> ?someObject. 
1.8953201854647705 <-> ?answer <http://confOf#reviewes> ?someObject. 
creating directory: pc_member

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop> .
Number of matched answers :10
Number of correspondences found: 3
0.7 <-> ?answer <http://confOf#has_short_title> ?someObject. 
0.75 <-> ?answer <http://confOf#hasTopic> ?y. ?y a <http://confOf#Topic>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop>. 
creating directory: workshop

Binary query : ?answer0 <http://ekaw#coversTopic> ?answer1 .   ?answer0 a <http://ekaw#Conference> .
Number of matched answers :10
Number of correspondences found: 1
5.3888888888888875 <-> ?answer0 a <http://confOf#Conference>.  ?answer0 <http://confOf#hasTopic> ?answer1.  ?answer1 a <http://confOf#Topic>.  
creating directory: conference_topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member> .   ?answer <http://ekaw#reviewerOfPaper> ?o3.
Number of matched answers :10
Number of correspondences found: 7
0.6586973180076627 <-> ?answer <http://confOf#employedBy> ?someObject. 
0.788888888888889 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
0.8555555555555556 <-> ?answer <http://confOf#hasSurname> ?someObject. 
1.5676779433036345 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
1.577777777777778 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
1.6343446099703014 <-> ?answer <http://confOf#writes> ?someObject. 
1.8953201854647705 <-> ?answer <http://confOf#reviewes> ?someObject. 
creating directory: assigned_reviewer

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Accepted_Paper> .
Number of matched answers :10
Number of correspondences found: 4
0.6411568872243031 <-> ?answer <http://confOf#hasTitle> ?someObject. 
0.8487128251059453 <-> ?x <http://confOf#writes> ?answer. ?x a <http://confOf#Member_PC>. 
0.8698165869218502 <-> ?answer <http://confOf#writtenBy> ?someObject. 
0.9285714285714286 <-> ?x <http://confOf#reviewes> ?answer. ?x a <http://confOf#Member_PC>. 
creating directory: accepted_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Web_Site> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop_Chair> .
Number of matched answers :10
Number of correspondences found: 4
0.4285714285714287 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
0.8348006446513139 <-> ?answer <http://confOf#reviewes> ?someObject. 
0.9635373323206073 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
0.9635373323206075 <-> ?answer <http://confOf#writes> ?someObject. 
creating directory: workshop_chair

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Invited_Talk_Abstract> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Invited_Speaker> .
Number of matched answers :10
Number of correspondences found: 4
0.6738095238095239 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
1.1059896266392792 <-> ?answer <http://confOf#reviewes> ?someObject. 
1.114077415198772 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
1.1807440818654389 <-> ?answer <http://confOf#writes> ?someObject. 
creating directory: invited_speaker

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper_Author> . ?answer0 <http://ekaw#authorOf> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .
Number of matched answers :10
Number of correspondences found: 4
3.8888888888888884 <-> ?answer0 a <http://confOf#Author>.  ?answer1 <http://confOf#writtenBy> ?answer0.  ?answer1 a <http://confOf#Poster>.  
3.9999999999999996 <-> ?answer0 a <http://confOf#Author>.  ?answer0 <http://confOf#writes> ?answer1.  ?answer1 a <http://confOf#Poster>.  
4.555555555555556 <-> ?answer0 a <http://confOf#Author>.  ?answer1 <http://confOf#writtenBy> ?answer0.  ?answer1 a <http://confOf#Paper>.  
4.666666666666665 <-> ?answer0 a <http://confOf#Author>.  ?answer0 <http://confOf#writes> ?answer1.  ?answer1 a <http://confOf#Paper>.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Trip> .   ?answer0 <http://ekaw#heldIn> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
2.963793103448276 <-> ?answer0 a <http://confOf#Event>.  ?answer0 <http://confOf#location> ?answer1.  
creating directory: trip_location

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Research_Topic> .
Number of matched answers :10
Number of correspondences found: 2
0.9320315714659785 <-> ?x <http://confOf#hasTopic> ?answer. ?x a <http://confOf#Event>. 
1.1924898607922605 <-> ?someSubject <http://confOf#dealsWith> ?answer. 
creating directory: topic

Binary query : ?answer0 a <http://ekaw#Invited_Talk_Abstract>. ?answer0 <http://ekaw#paperPresentedAs> ?answer1. ?answer1 a <http://ekaw#Invited_Talk>.
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Student> .
Number of matched answers :10
Number of correspondences found: 4
0.6023809523809524 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
0.8222384219831087 <-> ?answer <http://confOf#writes> ?someObject. 
0.8753849708466475 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
0.9142857142857143 <-> ?answer <http://confOf#studyAt> ?y. ?y a <http://confOf#University>. 
creating directory: student

Unary query : ?o2 a <http://ekaw#Web_Site>. ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer. filter(regex(?s,"^http"))
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#SC_Member> .
Number of matched answers :10
Number of correspondences found: 5
0.2611111111111112 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
0.7539455716517017 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
0.7759889067734959 <-> ?answer <http://confOf#writes> ?someObject. 
0.7777777777777779 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
0.8442092693282675 <-> ?answer <http://confOf#reviewes> ?someObject. 
creating directory: sc_member

Binary query : ?answer1 <http://ekaw#reviewWrittenBy> ?answer0 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : {?answer <http://ekaw#reviewerOfPaper> ?o2 . } union{ ?o3 <http://ekaw#reviewOfPaper> ?o2 .  ?o3 <http://ekaw#reviewWrittenBy> ?answer .  }
Number of matched answers :10
Number of correspondences found: 8
0.7372549019607844 <-> ?answer <http://confOf#earlyRegistration> ?someObject. 
0.9732193732193731 <-> ?answer <http://confOf#hasSurname> ?someObject. 
1.1031806971008422 <-> ?answer <http://confOf#employedBy> ?y. ?y a <http://confOf#University>. 
1.2997435897435896 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
2.312820512820513 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
2.957440387506204 <-> ?answer <http://confOf#writes> ?someObject. 
3.003594233660051 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
3.649234002556873 <-> ?answer <http://confOf#reviewes> ?someObject. 
creating directory: reviewers_all

Binary query : ?answer0 <http://ekaw#coversTopic> ?answer1 . ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .
Number of matched answers :10
Number of correspondences found: 2
3.1292929292929292 <-> ?answer0 a <http://confOf#Poster>.  ?answer0 <http://confOf#dealsWith> ?answer1.  ?answer1 a <http://confOf#Topic>.  
3.538383838383838 <-> ?answer0 a <http://confOf#Paper>.  ?answer0 <http://confOf#dealsWith> ?answer1.  ?answer1 a <http://confOf#Topic>.  
creating directory: topic_of_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Review> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop> .  ?answer0 <http://ekaw#coversTopic> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
4.340909090909093 <-> ?answer0 a <http://confOf#Workshop>.  ?answer0 <http://confOf#hasTopic> ?answer1.  ?answer1 a <http://confOf#Topic>.  
creating directory: workshop_topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Banquet> .
Number of matched answers :5
Number of correspondences found: 1
0.7735632183908046 <-> ?answer <http://confOf#location> ?someObject. 
creating directory: gala_dinner

Binary query : ?answer1 a <http://ekaw#Conference> . ?o2 <http://ekaw#listsEvent> ?answer1 . ?o2 a <http://ekaw#Web_Site> . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer0 . filter(regex(?s,"^http"))
Number of matched answers :5
Number of correspondences found: 0

Unary query : ?answer a <http://ekaw#Invited_Talk> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper_Author> .
Number of matched answers :10
Number of correspondences found: 5
0.6249999999999999 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar>. 
0.6666666666666666 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author>. 
0.984156575448952 <-> ?answer <http://confOf#reviewes> ?someObject. 
1.1248102004157552 <-> ?x <http://confOf#writtenBy> ?answer. ?x a <http://confOf#Paper>. 
1.2976377586379317 <-> ?answer <http://confOf#writes> ?y. ?y a <http://confOf#Paper>. 
creating directory: author

Binary query : ?answer0 <http://ekaw#presentationOfPaper> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#OC_Chair> .
Number of matched answers :5
Number of correspondences found: 4
0.25 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?y. ?y a <http://www.w3.org/2002/07/owl#Class>. 
0.4617052881758764 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
0.6304812834224599 <-> ?answer <http://confOf#writes> <http://confOf-instances#paper-1180456823471114>. 
0.7874348881527542 <-> ?answer <http://confOf#reviewes> ?someObject. 
creating directory: oc_chair

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .
Number of matched answers :10
Number of correspondences found: 6
0.6222222222222223 <-> ?answer <http://confOf#dealsWith> <http://confOf-instances#topic3121>. 
0.7000000000000001 <-> ?answer <http://confOf#hasKeyword> "ar". 
0.7402777777777777 <-> ?someSubject <http://confOf#reviewes> ?answer. 
0.7515873015873016 <-> ?answer <http://confOf#writtenBy> ?someObject. 
0.7631345877997663 <-> ?x <http://confOf#writes> ?answer. ?x a <http://confOf#Member_PC>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Paper>. 
creating directory: paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Chair> .
Number of matched answers :5
Number of correspondences found: 4
0.2727272727272727 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant>. 
0.7608912945711943 <-> ?answer <http://confOf#reviewes> ?someObject. 
0.7790075677720092 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
0.7790075677720092 <-> ?answer <http://confOf#writes> ?someObject. 
creating directory: pc_chair

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Regular_Paper> .
Number of matched answers :10
Number of correspondences found: 3
0.6153846153846153 <-> ?answer <http://confOf#writtenBy> ?someObject. 
0.6937321937321936 <-> ?x <http://confOf#writes> ?answer. ?x a <http://confOf#Member_PC>. 
0.8461538461538461 <-> ?x <http://confOf#reviewes> ?answer. ?x a <http://confOf#Member_PC>. 
creating directory: regular_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .   ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.
Number of matched answers :10
Number of correspondences found: 2
1.7916666666666665 <-> ?answer0 a <http://confOf#Poster>.  ?answer0 <http://confOf#hasTitle> ?answer1.  
2.525 <-> ?answer0 a <http://confOf#Paper>.  ?answer0 <http://confOf#hasTitle> ?answer1.  
creating directory: paper_title

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Contributed_Talk> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 a <http://ekaw#Contributed_Talk>. ?answer1 a <http://ekaw#Conference> . ?answer0 <http://ekaw#partOfEvent> ?answer1.
Number of matched answers :10
Number of correspondences found: 0
Matching process ended
