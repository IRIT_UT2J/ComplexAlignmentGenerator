===============================================================================
Running with 1 support instances - 0.4 similarity.
Number of CQAs: 6

Binary query : ?answer0 a <http://dbpedia.org/ontology/Species>. bind( if (exists {[] <http://dbpedia.org/ontology/genus> ?answer0}, "genus",        if (exists {[] <http://dbpedia.org/ontology/kingdom> ?answer0}, "kingdom",       if (exists {[] <http://dbpedia.org/ontology/species> ?answer0}, "species",       if (exists {[] <http://dbpedia.org/ontology/phylum> ?answer0}, "phylum",       if (exists {[] <http://dbpedia.org/ontology/classis> ?answer0}, "classis",       if (exists {[] <http://dbpedia.org/ontology/order> ?answer0}, "order",       if (exists {[] <http://dbpedia.org/ontology/family> ?answer0}, "family",        if (exists {?answer0 <http://dbpedia.org/ontology/genus> []}, "species", 	"")))))))) as ?answer1 )
