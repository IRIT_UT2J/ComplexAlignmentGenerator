===============================================================================
creating directory: confOf-cmt
Running with 3 support instances - 0.4 similarity.
Number of CQAs: 54

Binary query : ?answer0 <http://confOf#hasSurname> ?answer1 .
Number of matched answers :3
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant> .
Number of matched answers :3
Number of correspondences found: 1
0.4444444444444444 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Co-author>. 
creating directory: participant

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Camera_Ready_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Registration_of_participants_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :3
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Tutorial> .  ?answer0 <http://confOf#hasTopic> ?answer1 .
Number of matched answers :3
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 a <http://confOf#Conference>. ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Reviewing_results_event> . ?o2 <http://confOf#starts_on> ?answer1 .
Number of matched answers :3
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#earlyRegistration> ?answer1 .
Number of matched answers :3
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#location> ?answer1 .   ?answer0 a <http://confOf#Conference> .
Number of matched answers :3
Number of correspondences found: 0

Unary query : ?answer <http://confOf#earlyRegistration> "false"^^<http://www.w3.org/2001/XMLSchema#boolean> .
Number of matched answers :3
Number of correspondences found: 2
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Author>. 
0.0 <-> ?answer <http://cmt#memberOfConference> <http://cmt-instances#conference1428971814>. 

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Submission_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#University> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#reviewes> ?answer1 .   ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .
Number of matched answers :3
Number of correspondences found: 2
2.375 <-> ?answer0 a <http://cmt#Reviewer>.  ?answer0 <http://cmt#hasBeenAssigned> ?answer1.  
2.375 <-> ?answer0 a <http://cmt#Reviewer>.  ?answer1 <http://cmt#assignedTo> ?answer0.  
creating directory: is_assigned_reviewer_of

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Administrator> .
Number of matched answers :3
Number of correspondences found: 2
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Administrator>. 
1.0091812513581733 <-> ?x <http://cmt#assignedByAdministrator> ?answer. ?x a <http://cmt#Co-author>. 
creating directory: administrator

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Camera_Ready_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :3
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> .
Number of matched answers :3
Number of correspondences found: 7
0.625 <-> ?answer <http://cmt#paperAssignmentFinalizedBy> ?y. ?y a <http://cmt#ConferenceMember>. 
0.625 <-> ?answer <http://cmt#paperAssignmentToolsRunBy> ?y. ?y a <http://cmt#ConferenceMember>. 
0.6931818181818182 <-> ?x <http://cmt#runPaperAssignmentTools> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.7007575757575758 <-> ?x <http://cmt#enterConferenceDetails> ?answer. ?x a <http://cmt#ConferenceMember>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Conference>. 
1.1517910614803328 <-> ?answer <http://cmt#hasConferenceMember> ?y. ?y a <http://cmt#ConferenceMember>. 
1.181189197190329 <-> ?x <http://cmt#memberOfConference> ?answer. ?x a <http://cmt#ConferenceMember>. 
creating directory: conference

Binary query : ?answer0 <http://confOf#hasFirstName> ?o1 .  ?answer0 <http://confOf#hasSurname> ?o2 .   BIND( concat(STR(?o1)," ", STR(?o2)) as ?answer1)
Number of matched answers :3
Number of correspondences found: 1
0.9 <-> ?answer0 <http://cmt#name> ?answer1.  
creating directory: person_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person> .
Number of matched answers :3
Number of correspondences found: 5
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person>. 
1.0 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#Person>. 
1.0 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#Person>. 
1.0 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#Person>. 
1.0 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#Person>. 
creating directory: person

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#starts_on> ?answer1 .   ?answer0 a <http://confOf#Conference> .
Number of matched answers :3
Number of correspondences found: 1
1.8999999999999997 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#date> ?answer1.  
creating directory: conference_start_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Tutorial> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://confOf#earlyRegistration> "true"^^<http://www.w3.org/2001/XMLSchema#boolean> .
Number of matched answers :3
Number of correspondences found: 3
0.0 <-> ?answer <http://cmt#co-writePaper> <http://cmt-instances#paper7495729491902198>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceMember>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person>. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#has_title> ?answer1 .
Number of matched answers :3
Number of correspondences found: 1
1.8999999999999997 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#name> ?answer1.  
creating directory: conference_name

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .
Number of matched answers :3
Number of correspondences found: 6
0.6153846153846154 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Meta-Reviewer>. 
0.75 <-> ?x <http://cmt#writtenBy> ?answer. ?x a <http://cmt#Review>. 
0.75 <-> ?answer <http://cmt#writeReview> ?y. ?y a <http://cmt#Review>. 
0.9444444444444444 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#ExternalReviewer>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer>. 
1.05672268907563 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#AuthorNotReviewer>. 
creating directory: pc_member

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Submission_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :3
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#hasTopic> ?answer1 .  ?answer0 a  <http://confOf#Conference> .
Number of matched answers :3
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .  ?answer <http://confOf#reviewes> ?o2 .
Number of matched answers :3
Number of correspondences found: 11
0.8823529411764706 <-> ?answer <http://cmt#addedBy> ?y. ?y a <http://cmt#AuthorNotReviewer>. 
0.9007352941176471 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#ExternalReviewer>. 
0.9099264705882353 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#ExternalReviewer>. 
0.9375 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#ExternalReviewer>. 
1.0714285714285718 <-> ?someSubject <http://cmt#readByReviewer> ?answer. 
1.1538461538461537 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Meta-Reviewer>. 
1.3819444444444444 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#ExternalReviewer>. 
1.5 <-> ?x <http://cmt#writtenBy> ?answer. ?x a <http://cmt#Review>. 
1.5 <-> ?answer <http://cmt#writeReview> ?y. ?y a <http://cmt#Review>. 
1.875 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer>. 
1.9721638655462186 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#AuthorNotReviewer>. 
creating directory: assigned_reviewer

Binary query : ?answer0 <http://confOf#writes> ?answer1 .
Number of matched answers :3
Number of correspondences found: 4
0.5 <-> ?answer0 <http://cmt#co-writePaper> ?answer1.  
0.5 <-> ?answer1 <http://cmt#hasCo-author> ?answer0.  
0.5 <-> ?answer1 <http://cmt#hasAuthor> ?answer0.  
1.0 <-> ?answer0 <http://cmt#writePaper> ?answer1.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :3
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Topic> .
Number of matched answers :3
Number of correspondences found: 1
0.0 <-> ?someSubject <http://cmt#hasSubjectArea> ?answer. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar> .
Number of matched answers :3
Number of correspondences found: 2
0.4444444444444444 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Co-author>. 
0.4444444444444444 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Co-author>. 
creating directory: student

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#dealsWith> ?answer1 .
Number of matched answers :3
Number of correspondences found: 1
0.5 <-> ?answer0 <http://cmt#hasSubjectArea> ?answer1.  
creating directory: topic_of_paper

Binary query : ?answer0 <http://confOf#hasFirstName> ?answer1 .
Number of matched answers :3
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .  ?answer0 <http://confOf#hasTopic> ?answer1 .
Number of matched answers :3
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author> .
Number of matched answers :3
Number of correspondences found: 7
0.6666666666666667 <-> ?someSubject <http://cmt#hasAuthor> ?answer. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Author>. 
1.0 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#Author>. 
1.0 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#Author>. 
1.1111111111111112 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#Co-author>. 
1.1111111111111112 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Co-author>. 
1.1111111111111112 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#Co-author>. 
creating directory: author

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Company> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Reviewing_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Contribution> .
Number of matched answers :3
Number of correspondences found: 15
0.8611111111111109 <-> ?x <http://cmt#markConflictOfInterest> ?answer. ?x a <http://cmt#Co-author>. 
0.861111111111111 <-> ?answer <http://cmt#hasCo-author> ?y. ?y a <http://cmt#Co-author>. 
0.861111111111111 <-> ?x <http://cmt#readPaper> ?answer. ?x a <http://cmt#Co-author>. 
0.861111111111111 <-> ?answer <http://cmt#readByReviewer> ?y. ?y a <http://cmt#Co-author>. 
0.861111111111111 <-> ?x <http://cmt#co-writePaper> ?answer. ?x a <http://cmt#Co-author>. 
0.861111111111111 <-> ?answer <http://cmt#assignedTo> ?y. ?y a <http://cmt#Co-author>. 
0.861111111111111 <-> ?answer <http://cmt#hasAuthor> ?y. ?y a <http://cmt#Co-author>. 
0.861111111111111 <-> ?x <http://cmt#hasBeenAssigned> ?answer. ?x a <http://cmt#Co-author>. 
0.861111111111111 <-> ?x <http://cmt#submitPaper> ?answer. ?x a <http://cmt#Co-author>. 
0.861111111111111 <-> ?answer <http://cmt#rejectedBy> ?y. ?y a <http://cmt#Co-author>. 
0.861111111111111 <-> ?x <http://cmt#rejectPaper> ?answer. ?x a <http://cmt#Co-author>. 
0.861111111111111 <-> ?answer <http://cmt#readByMeta-Reviewer> ?y. ?y a <http://cmt#Co-author>. 
0.861111111111111 <-> ?x <http://cmt#acceptPaper> ?answer. ?x a <http://cmt#Co-author>. 
0.861111111111111 <-> ?answer <http://cmt#acceptedBy> ?y. ?y a <http://cmt#Co-author>. 
1.076923076923077 <-> ?answer <http://cmt#hasSubjectArea> <http://cmt-instances#topic1932752118>. 
creating directory: paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Chair_PC> .
Number of matched answers :3
Number of correspondences found: 1
0.625 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Chairman>. 
creating directory: pc_chair

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Paper> .
Number of matched answers :3
Number of correspondences found: 7
0.7142857142857143 <-> ?answer <http://cmt#paperID> ?someObject. 
0.8545454545454546 <-> ?x <http://cmt#submitPaper> ?answer. ?x a <http://cmt#User>. 
0.8545454545454546 <-> ?x <http://cmt#acceptPaper> ?answer. ?x a <http://cmt#User>. 
0.8767676767676769 <-> ?x <http://cmt#rejectPaper> ?answer. ?x a <http://cmt#Co-author>. 
0.8772727272727273 <-> ?x <http://cmt#writePaper> ?answer. ?x a <http://cmt#User>. 
0.9703703703703704 <-> ?x <http://cmt#readPaper> ?answer. ?x a <http://cmt#Co-author>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Paper>. 
creating directory: regular_paper

Binary query : ?answer0 <http://confOf#hasTitle> ?answer1 .
Number of matched answers :3
Number of correspondences found: 1
1.125 <-> ?answer0 <http://cmt#title> ?answer1.  
creating directory: paper_title

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .
Number of matched answers :0
Number of correspondences found: 0
Matching process ended
