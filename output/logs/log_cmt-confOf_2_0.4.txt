===============================================================================
creating directory: cmt-confOf
Running with 2 support instances - 0.4 similarity.
Number of CQAs: 34

Unary query : ?o2 <http://cmt#hasAuthor> ?answer .
Number of matched answers :2
Number of correspondences found: 1
0.6666666666666667 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author>. 
creating directory: paper_1st_author

Unary query : { ?answer <http://cmt#hasDecision> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Rejection> . } UNION  { ?answer <http://cmt#rejectedBy> ?o2 . }
Number of matched answers :2
Number of correspondences found: 2
0.5 <-> ?answer <http://confOf#dealsWith> <http://confOf-instances#topic-367775188>. 
creating directory: rejected_paper

Binary query : ?answer0 <http://cmt#memberOfProgramCommittee> ?answer1 .
Number of matched answers :2
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceChair> .
Number of matched answers :2
Number of correspondences found: 2
0.0 <-> ?answer <http://confOf#earlyRegistration> "false". 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Co-author> .
Number of matched answers :2
Number of correspondences found: 4
0.861111111111111 <-> ?answer <http://confOf#reviewes> ?y. ?y a <http://confOf#Contribution>. 
1.1111111111111112 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author>. 
1.3055555555555554 <-> ?x <http://confOf#writtenBy> ?answer. ?x a <http://confOf#Contribution>. 
1.3055555555555554 <-> ?answer <http://confOf#writes> ?y. ?y a <http://confOf#Contribution>. 
creating directory: paper_co_author

Binary query : ?answer0 <http://cmt#hasBeenAssigned> ?answer1 .
Number of matched answers :2
Number of correspondences found: 1
0.5 <-> ?answer0 <http://confOf#reviewes> ?answer1.  
creating directory: is_assigned_reviewer_of

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Administrator> .
Number of matched answers :2
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Administrator>. 
creating directory: administrator

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommittee> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ExternalReviewer> .
Number of matched answers :2
Number of correspondences found: 2
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> <http://confOf-instances#paper8654177606562198> <http://confOf#writtenBy> ?answer. 

Binary query : ?answer0 <http://cmt#writeReview> ?answer1 .
Number of matched answers :2
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Conference> .
Number of matched answers :2
Number of correspondences found: 2
0.8051682692307692 <-> ?answer <http://confOf#hasTopic> ?y. ?y a <http://confOf#Topic>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference>. 
creating directory: conference

Binary query : ?answer0 <http://cmt#name> ?answer1 .   ?answer0 a <http://cmt#Person> .
Number of matched answers :2
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person> .
Number of matched answers :2
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 
creating directory: person

Binary query : ?answer0 <http://cmt#date> ?answer1 .
Number of matched answers :2
Number of correspondences found: 1
0.5 <-> ?answer0 <http://confOf#starts_on> ?answer1.  
creating directory: conference_start_date

Binary query : ?answer0 <http://cmt#assignExternalReviewer> ?answer1 .
Number of matched answers :2
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Conference> . ?answer0 <http://cmt#name> ?answer1 .
Number of matched answers :2
Number of correspondences found: 1
1.9 <-> ?answer0 a <http://confOf#Conference>.  ?answer0 <http://confOf#has_title> ?answer1.  
creating directory: conference_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperAbstract> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommitteeMember> .
Number of matched answers :2
Number of correspondences found: 1
0.0 <-> ?someSubject <http://confOf#writtenBy> ?answer. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer> .
Number of matched answers :2
Number of correspondences found: 2
0.875 <-> ?answer <http://confOf#reviewes> ?someObject. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
creating directory: assigned_reviewer

Unary query : { ?answer <http://cmt#hasDecision> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Acceptance> . } UNION  { ?answer <http://cmt#acceptedBy> ?o2 . }
Number of matched answers :2
Number of correspondences found: 1
0.4 <-> ?answer <http://confOf#writtenBy> ?someObject. 
creating directory: accepted_paper

Binary query : ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperFullVersion> .   { ?answer0 <http://cmt#writePaper> ?answer1 .   } union{ ?answer0 <http://cmt#co-writePaper> ?answer1 .   }
Number of matched answers :2
Number of correspondences found: 2
0.9 <-> ?answer1 <http://confOf#writtenBy> ?answer0.  
1.0 <-> ?answer0 <http://confOf#writes> ?answer1.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://cmt#memberOfConference> ?answer1 .
Number of matched answers :2
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#SubjectArea> .
Number of matched answers :2
Number of correspondences found: 2
0.0 <-> <http://confOf-instances#paper-16657167746872198> <http://confOf#dealsWith> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Topic>. 

Unary query : ?o2 <http://cmt#siteURL> ?answer .
Number of matched answers :0
Number of correspondences found: 0

Unary query : { ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer> .   } union{ ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ExternalReviewer> .   }
Number of matched answers :2
Number of correspondences found: 3
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
1.3125 <-> ?answer <http://confOf#reviewes> ?someObject. 
1.5 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
creating directory: reviewers_all

Binary query : ?answer0 a <http://cmt#PaperFullVersion> . ?answer0 <http://cmt#hasSubjectArea> ?answer1 .
Number of matched answers :2
Number of correspondences found: 1
0.5 <-> ?answer0 <http://confOf#dealsWith> ?answer1.  
creating directory: topic_of_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Review> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer1 <http://cmt#siteURL> ?answer0 .
Number of matched answers :2
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Author> .
Number of matched answers :2
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author>. 
creating directory: author

Binary query : ?answer0 a <http://cmt#PaperFullVersion> .   ?answer1 a <http://cmt#PaperAbstract> . ?answer0 <http://cmt#title> ?o2 . ?answer1 <http://cmt#title> ?o2 .
Number of matched answers :2
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperFullVersion> .
Number of matched answers :2
Number of correspondences found: 2
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Thing>. 
0.0 <-> <http://confOf-instances#person634161013> <http://confOf#writes> ?answer. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommitteeChair> .
Number of matched answers :2
Number of correspondences found: 1
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Chair_PC>. 

Binary query : ?answer0 <http://cmt#title> ?answer1 .   ?answer0 a <http://cmt#PaperFullVersion> .
Number of matched answers :2
Number of correspondences found: 1
1.125 <-> ?answer0 <http://confOf#hasTitle> ?answer1.  
creating directory: paper_title

Binary query : ?answer0 <http://cmt#title> ?answer1 .   ?answer0 a <http://cmt#PaperAbstract> .
Number of matched answers :0
Number of correspondences found: 0
Matching process ended
