===============================================================================
creating directory: confOf-edas
Running with 10 support instances - 0.2 similarity.
Number of CQAs: 54

Binary query : ?answer0 <http://confOf#hasSurname> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
1.1363636363636365 <-> ?answer0 <http://edas#hasLastName> ?answer1.  
creating directory: person_last_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant> .
Number of matched answers :10
Number of correspondences found: 4
0.2727272727272727 <-> ?answer <http://edas#hasLastName> "Horvat". 
0.6060606060606061 <-> ?answer <http://edas#hasRelatedDocument> ?y. ?y a <http://edas#Document>. 
0.6303030303030304 <-> ?answer <http://edas#hasRelatedPaper> ?y. ?y a <http://edas#ActivePaper>. 
0.7449494949494949 <-> ?answer <http://edas#hasFirstName> "Christine". 
creating directory: participant

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Camera_Ready_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :10
Number of correspondences found: 2
3.2464114832535884 <-> ?answer0 a <http://edas#Workshop>.  ?answer0 <http://edas#hasCall> ?v1.  ?v1 a <http://edas#CallForPapers>.  ?v1 <http://edas#hasSubmissionDeadline> ?answer1.  
4.102472089314196 <-> ?answer0 a <http://edas#Workshop>.  ?v1 <http://edas#forEvent> ?answer0.  ?v1 a <http://edas#CallForPapers>.  ?v1 <http://edas#hasSubmissionDeadline> ?answer1.  
creating directory: workshop_camera_date

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Registration_of_participants_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
4.54112996977447 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#registrationDueOn> ?answer1.  
creating directory: conference_regis_date

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
2.484126984126984 <-> ?answer0 a <http://edas#Reception>.  ?answer0 <http://edas#hasEndDateTime> ?answer1.  
creating directory: reception_end_date

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Tutorial> .  ?answer0 <http://confOf#hasTopic> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.380952380952381 <-> ?answer0 a <http://edas#Excursion>.  ?answer0 <http://edas#hasEndDateTime> ?answer1.  
creating directory: trip_end_date

Binary query : ?answer0 a <http://confOf#Conference>. ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Reviewing_results_event> . ?o2 <http://confOf#starts_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#earlyRegistration> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#location> ?answer1 .   ?answer0 a <http://confOf#Conference> .
Number of matched answers :5
Number of correspondences found: 1
3.275 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#location> ?answer1.  
creating directory: conference_location

Unary query : ?answer <http://confOf#earlyRegistration> "false"^^<http://www.w3.org/2001/XMLSchema#boolean> .
Number of matched answers :10
Number of correspondences found: 3
0.5441176470588235 <-> ?x <http://edas#relatesTo> ?answer. ?x a <http://edas#Document>. 
0.6458916900093371 <-> ?answer <http://edas#hasFirstName> ?someObject. 
0.69234360410831 <-> ?answer <http://edas#hasLastName> ?someObject. 
creating directory: late_registered_participant

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Submission_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :10
Number of correspondences found: 2
3.8441558441558437 <-> ?answer0 a <http://edas#Workshop>.  ?answer0 <http://edas#hasCall> ?v1.  ?v1 a <http://edas#CallForManuscripts>.  ?v1 <http://edas#hasSubmissionDeadline> ?answer1.  
4.741883116883116 <-> ?answer0 a <http://edas#Workshop>.  ?v1 <http://edas#forEvent> ?answer0.  ?v1 a <http://edas#CallForManuscripts>.  ?v1 <http://edas#hasSubmissionDeadline> ?answer1.  
creating directory: workshop_submission_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#University> .
Number of matched answers :10
Number of correspondences found: 2
0.07407407407407407 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> ?someObject. 
creating directory: university

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .
Number of matched answers :5
Number of correspondences found: 3
0.25 <-> ?answer <http://edas#hasLocation> <http://edas-instances#location-685742300>. 
creating directory: trip

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
2.2569444444444446 <-> ?answer0 a <http://edas#Reception>.  ?answer0 <http://edas#hasStartDateTime> ?answer1.  
creating directory: reception_start_date

Binary query : ?answer0 <http://confOf#reviewes> ?answer1 .   ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .
Number of matched answers :10
Number of correspondences found: 10
4.241666666666667 <-> ?answer0 a <http://edas#Reviewer>.  ?answer0 <http://edas#isMemberOf> ?v1.  ?v1 a <http://edas#Conference>.  ?v1 <http://edas#hasRelatedDocument> ?answer1.  
4.324494949494949 <-> ?answer0 a <http://edas#Reviewer>.  ?answer0 <http://edas#isReviewing> ?answer1.  ?answer1 a <http://edas#ActivePaper>.  
4.400252525252525 <-> ?answer0 a <http://edas#Reviewer>.  ?answer1 <http://edas#isReviewedBy> ?answer0.  ?answer1 a <http://edas#ActivePaper>.  
4.457362082362082 <-> ?answer0 a <http://edas#Reviewer>.  ?answer0 <http://edas#isReviewing> ?answer1.  ?answer1 a <http://edas#AcceptedPaper>.  
4.4638888888888895 <-> ?answer0 a <http://edas#Reviewer>.  ?answer0 <http://edas#isMemberOf> ?v1.  ?v1 a <http://edas#Conference>.  ?answer1 <http://edas#relatesTo> ?v1.  
4.533119658119658 <-> ?answer0 a <http://edas#Reviewer>.  ?answer1 <http://edas#isReviewedBy> ?answer0.  ?answer1 a <http://edas#AcceptedPaper>.  
4.611208236208236 <-> ?answer0 a <http://edas#Reviewer>.  ?answer0 <http://edas#isReviewing> ?answer1.  ?answer1 a <http://edas#RejectedPaper>.  
4.630555555555556 <-> ?answer0 a <http://edas#Reviewer>.  ?v1 <http://edas#hasMember> ?answer0.  ?v1 a <http://edas#Conference>.  ?v1 <http://edas#hasRelatedDocument> ?answer1.  
4.686965811965812 <-> ?answer0 a <http://edas#Reviewer>.  ?answer1 <http://edas#isReviewedBy> ?answer0.  ?answer1 a <http://edas#RejectedPaper>.  
4.852777777777778 <-> ?answer0 a <http://edas#Reviewer>.  ?v1 <http://edas#hasMember> ?answer0.  ?v1 a <http://edas#Conference>.  ?answer1 <http://edas#relatesTo> ?v1.  
creating directory: is_assigned_reviewer_of

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Administrator> .
Number of matched answers :5
Number of correspondences found: 2
0.6514170040485829 <-> ?answer <http://edas#isMemberOf> ?y. ?y a <http://edas#Conference>. 
0.6634615384615384 <-> ?answer <http://edas#hasFirstName> ?someObject. 
creating directory: administrator

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Camera_Ready_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
3.8096716149347727 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#paperDueOn> ?answer1.  
creating directory: conference_camera_date

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
0.8125 <-> ?answer0 <http://edas#hasStartDateTime> ?answer1.  
creating directory: gala_dinner_start_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> .
Number of matched answers :5
Number of correspondences found: 3
0.6222222222222223 <-> ?answer <http://edas#hasMember> ?y. ?y a <http://edas#ConferenceChair>. 
0.6277777777777778 <-> ?someSubject <http://edas#isMemberOf> ?answer. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Conference>. 
creating directory: conference

Binary query : ?answer0 <http://confOf#hasFirstName> ?o1 .  ?answer0 <http://confOf#hasSurname> ?o2 .   BIND( concat(STR(?o1)," ", STR(?o2)) as ?answer1)
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person> .
Number of matched answers :10
Number of correspondences found: 2
0.7857142857142857 <-> ?answer <http://edas#hasFirstName> "Anthony". 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 
creating directory: person

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :5
Number of correspondences found: 2
2.54376026272578 <-> ?answer0 a <http://edas#ConferenceEvent>.  ?answer0 <http://edas#hasLocation> ?v1.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
2.54376026272578 <-> ?answer0 a <http://edas#ConferenceEvent>.  ?v1 <http://edas#isLocationOf> ?answer0.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: gala_dinner_location

Binary query : ?answer0 <http://confOf#starts_on> ?answer1 .   ?answer0 a <http://confOf#Conference> .
Number of matched answers :5
Number of correspondences found: 1
2.7888888888888888 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#startDate> ?answer1.  
creating directory: conference_start_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Tutorial> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://confOf#earlyRegistration> "true"^^<http://www.w3.org/2001/XMLSchema#boolean> .
Number of matched answers :10
Number of correspondences found: 4
0.588235294117647 <-> ?answer <http://edas#hasFirstName> ?someObject. 
creating directory: early_registered_participant

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#has_title> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
3.094444444444444 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#hasName> ?answer1.  
creating directory: conference_name

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.3680555555555556 <-> ?answer0 a <http://edas#Excursion>.  ?answer0 <http://edas#hasStartDateTime> ?answer1.  
creating directory: trip_start_date

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
2.214285714285715 <-> ?answer0 a <http://edas#Workshop>.  ?answer0 <http://edas#hasEndDateTime> ?answer1.  
creating directory: workshop_end_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .
Number of matched answers :10
Number of correspondences found: 9
0.6153846153846153 <-> ?x <http://edas#isWrittenBy> ?answer. ?x a <http://edas#RejectedPaper>. 
0.8940619254074691 <-> ?answer <http://edas#hasRelatedPaper> ?y. ?y a <http://edas#Paper>. 
1.1220988581662925 <-> ?answer <http://edas#isReviewing> ?y. ?y a <http://edas#Paper>. 
1.154249905324759 <-> ?x <http://edas#isReviewedBy> ?answer. ?x a <http://edas#Paper>. 
1.1944444444444444 <-> ?answer <http://edas#hasRelatedDocument> ?y. ?y a <http://edas#Review>. 
1.2042105263157894 <-> ?answer <http://edas#isMemberOf> ?y. ?y a <http://edas#Conference>. 
1.2222222222222223 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reviewer>. 
1.3055555555555556 <-> ?x <http://edas#relatesTo> ?answer. ?x a <http://edas#Review>. 
1.3693957115009747 <-> ?x <http://edas#hasMember> ?answer. ?x a <http://edas#Conference>. 
creating directory: pc_member

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Workshop>. 
creating directory: workshop

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Submission_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
3.366161616161616 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#manuscriptDueOn> ?answer1.  
creating directory: conference_paper_date

Binary query : ?answer0 <http://confOf#hasTopic> ?answer1 .  ?answer0 a  <http://confOf#Conference> .
Number of matched answers :10
Number of correspondences found: 2
3.0805555555555557 <-> ?answer0 a <http://edas#Conference>.  ?answer1 <http://edas#isTopicOf> ?answer0.  ?answer1 a <http://edas#Topic>.  
3.5249999999999995 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#hasTopic> ?answer1.  ?answer1 a <http://edas#Topic>.  
creating directory: conference_topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .  ?answer <http://confOf#reviewes> ?o2 .
Number of matched answers :10
Number of correspondences found: 11
0.7777777777777778 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Presenter>. 
0.784444756969599 <-> ?x <http://edas#isWrittenBy> ?answer. ?x a <http://edas#AcceptedPaper>. 
1.0 <-> ?answer <http://edas#hasLastName> "Levine". 
1.3117138216311446 <-> ?answer <http://edas#hasRelatedPaper> ?y. ?y a <http://edas#AcceptedPaper>. 
1.4292397660818714 <-> ?answer <http://edas#isMemberOf> ?y. ?y a <http://edas#Conference>. 
1.682017543859649 <-> ?x <http://edas#hasMember> ?answer. ?x a <http://edas#Conference>. 
1.8888386245659452 <-> ?answer <http://edas#hasRelatedDocument> ?y. ?y a <http://edas#Review>. 
1.929111869981 <-> ?answer <http://edas#isReviewing> ?y. ?y a <http://edas#RatedPapers>. 
2.002354517136401 <-> ?x <http://edas#relatesTo> ?answer. ?x a <http://edas#Review>. 
2.0064083812420828 <-> ?x <http://edas#isReviewedBy> ?answer. ?x a <http://edas#RatedPapers>. 
2.0972222222222223 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reviewer>. 
creating directory: assigned_reviewer

Binary query : ?answer0 <http://confOf#writes> ?answer1 .
Number of matched answers :10
Number of correspondences found: 2
0.5 <-> ?answer0 <http://edas#hasRelatedPaper> ?answer1.  
0.9545454545454544 <-> ?answer1 <http://edas#isWrittenBy> ?answer0.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
2.6857142857142855 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#endDate> ?answer1.  
creating directory: conference_end_date

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :5
Number of correspondences found: 2
2.4166666666666665 <-> ?answer0 a <http://edas#Excursion>.  ?answer0 <http://edas#hasLocation> ?v1.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
2.4166666666666665 <-> ?answer0 a <http://edas#Excursion>.  ?v1 <http://edas#isLocationOf> ?answer0.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: trip_location

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Topic> .
Number of matched answers :10
Number of correspondences found: 3
0.6295977011494255 <-> ?someSubject <http://edas#hasTopic> ?answer. 
0.9555555555555556 <-> ?answer <http://edas#isTopicOf> ?y. ?y a <http://edas#Conference>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Topic>. 
creating directory: topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar> .
Number of matched answers :10
Number of correspondences found: 4
0.2857142857142857 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?y. ?y a <http://www.w3.org/2002/07/owl#Class>. 
0.4807692307692307 <-> ?answer <http://edas#isReviewing> ?y. ?y a <http://edas#RejectedPaper>. 
0.6222222222222222 <-> ?x <http://edas#hasMember> ?answer. ?x a <http://edas#Conference>. 
0.6799242424242423 <-> ?answer <http://edas#hasRelatedPaper> ?y. ?y a <http://edas#AcceptedPaper>. 
creating directory: student

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
2.395833333333333 <-> ?answer0 a <http://edas#Workshop>.  ?answer0 <http://edas#hasStartDateTime> ?answer1.  
creating directory: workshop_start_date

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.0 <-> ?answer0 <http://edas#hasEndDateTime> ?answer1.  
creating directory: gala_dinner_end_date

Binary query : ?answer0 <http://confOf#dealsWith> ?answer1 .
Number of matched answers :10
Number of correspondences found: 2
0.5 <-> ?answer0 <http://edas#hasTopic> ?answer1.  
0.5 <-> ?answer1 <http://edas#isTopicOf> ?answer0.  
creating directory: topic_of_paper

Binary query : ?answer0 <http://confOf#hasFirstName> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
1.75 <-> ?answer0 <http://edas#hasFirstName> ?answer1.  
creating directory: person_first_name

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .  ?answer0 <http://confOf#hasTopic> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .
Number of matched answers :5
Number of correspondences found: 1
0.33333333333333337 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#MealEvent>. 
creating directory: gala_dinner

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author> .
Number of matched answers :10
Number of correspondences found: 2
0.6222222222222222 <-> ?x <http://edas#hasMember> ?answer. ?x a <http://edas#Conference>. 
1.25 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Author>. 
creating directory: author

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Company> .
Number of matched answers :10
Number of correspondences found: 1
0.2857142857142857 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?y. ?y a <http://www.w3.org/2002/07/owl#Class>. 
creating directory: company

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :5
Number of correspondences found: 2
4.809386973180077 <-> ?answer0 a <http://edas#Reception>.  ?v1 <http://edas#isLocationOf> ?answer0.  ?v1 a <http://edas#ConferenceVenuePlace>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
4.839690003483106 <-> ?answer0 a <http://edas#Reception>.  ?answer0 <http://edas#hasLocation> ?v1.  ?v1 a <http://edas#ConferenceVenuePlace>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: reception_location

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Reviewing_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :10
Number of correspondences found: 2
2.272727272727273 <-> ?answer0 a <http://edas#Workshop>.  ?answer0 <http://edas#hasCall> ?v1.  ?v1 <http://edas#hasSubmissionDeadline> ?answer1.  
3.128787878787879 <-> ?answer0 a <http://edas#Workshop>.  ?v1 <http://edas#forEvent> ?answer0.  ?v1 <http://edas#hasSubmissionDeadline> ?answer1.  
creating directory: workshop_notif_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Contribution> .
Number of matched answers :10
Number of correspondences found: 11
0.6000000000000001 <-> ?answer <http://edas#relatedToEvent> ?y. ?y a <http://edas#ConferenceEvent>. 
0.6000000000000001 <-> ?x <http://edas#relatedToPaper> ?answer. ?x a <http://edas#ConferenceEvent>. 
0.6536911994339532 <-> ?someSubject <http://edas#hasRelatedDocument> ?answer. 
0.6728383838383838 <-> ?answer <http://edas#relatesTo> ?y. ?y a <http://edas#Conference>. 
0.7291666666666666 <-> ?answer <http://edas#isReviewedBy> ?someObject. 
0.75 <-> ?answer <http://edas#hasRating> ?y. ?y a <http://edas#AccpetIfRoomRating>. 
0.7552083333333334 <-> ?someSubject <http://edas#isReviewing> ?answer. 
0.8420370370370371 <-> ?someSubject <http://edas#hasRelatedPaper> ?answer. 
0.8450000000000001 <-> ?answer <http://edas#isWrittenBy> ?someObject. 
1.076923076923077 <-> ?answer <http://edas#hasTopic> <http://edas-instances#topic1932752118>. 
1.076923076923077 <-> <http://edas-instances#topic1932752118> <http://edas#isTopicOf> ?answer. 
creating directory: paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Chair_PC> .
Number of matched answers :5
Number of correspondences found: 2
0.7000000000000001 <-> ?x <http://edas#hasMember> ?answer. ?x a <http://edas#Conference>. 
0.7000000000000001 <-> ?answer <http://edas#isMemberOf> ?y. ?y a <http://edas#Conference>. 
creating directory: pc_chair

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Paper> .
Number of matched answers :10
Number of correspondences found: 9
0.6339285714285714 <-> ?answer <http://edas#isWrittenBy> ?someObject. 
0.6512605042016806 <-> ?x <http://edas#relatedToPaper> ?answer. ?x a <http://edas#PaperPresentation>. 
0.7115384615384616 <-> ?answer <http://edas#hasTopic> <http://edas-instances#topic1932752118>. 
0.725 <-> ?someSubject <http://edas#isReviewing> ?answer. 
0.7437500000000001 <-> ?answer <http://edas#isReviewedBy> ?someObject. 
0.7485521885521884 <-> ?answer <http://edas#relatesTo> ?y. ?y a <http://edas#Conference>. 
0.7636217948717952 <-> ?someSubject <http://edas#hasRelatedPaper> ?answer. 
0.7676767676767676 <-> <http://edas-instances#conference69622803> <http://edas#hasRelatedDocument> ?answer. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Paper>. 
creating directory: regular_paper

Binary query : ?answer0 <http://confOf#hasTitle> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
0.75 <-> ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: paper_title

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .
Number of matched answers :5
Number of correspondences found: 3
0.7654616338826865 <-> ?answer <http://edas#hasLocation> ?y. ?y a <http://edas#ConferenceVenuePlace>. 
0.8110712387028176 <-> ?someSubject <http://edas#isLocationOf> ?answer. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reception>. 
creating directory: reception
Matching process ended
