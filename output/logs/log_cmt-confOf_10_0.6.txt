===============================================================================
creating directory: cmt-confOf
Running with 10 support instances - 0.6 similarity.
Number of CQAs: 34

Unary query : ?o2 <http://cmt#hasAuthor> ?answer .
Number of matched answers :10
Number of correspondences found: 1
0.6666666666666667 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author>. 
creating directory: paper_1st_author

Unary query : { ?answer <http://cmt#hasDecision> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Rejection> . } UNION  { ?answer <http://cmt#rejectedBy> ?o2 . }
Number of matched answers :10
Number of correspondences found: 7
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Thing>. 
0.0 <-> <http://confOf-instances#person1214794500> <http://confOf#writes> ?answer. 
0.0 <-> <http://confOf-instances#person-1712757423> <http://confOf#reviewes> ?answer. 
0.0 <-> ?answer <http://confOf#dealsWith> ?someObject. 
0.0 <-> ?answer <http://confOf#hasTitle> "On-line separation of ^{163}Ta and ^{162}Ta". 
0.0 <-> ?answer <http://confOf#writtenBy> <http://confOf-instances#person-1049489004>. 
0.0 <-> ?answer <http://confOf#hasKeyword> "benchmarking,instance matching,ontology matching,semantic web data,ontologies,linked data". 

Binary query : ?answer0 <http://cmt#memberOfProgramCommittee> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceChair> .
Number of matched answers :5
Number of correspondences found: 5
0.0 <-> ?answer <http://confOf#earlyRegistration> "false". 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 
0.0 <-> ?answer <http://confOf#hasFirstName> "Manono". 
0.0 <-> ?answer <http://confOf#reviewes> <http://confOf-instances#paper18872461397322198>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Co-author> .
Number of matched answers :10
Number of correspondences found: 1
0.6666666666666667 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author>. 
creating directory: paper_co_author

Binary query : ?answer0 <http://cmt#hasBeenAssigned> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
0.5 <-> ?answer0 <http://confOf#reviewes> ?answer1.  
creating directory: is_assigned_reviewer_of

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Administrator> .
Number of matched answers :5
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Administrator>. 
creating directory: administrator

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommittee> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ExternalReviewer> .
Number of matched answers :10
Number of correspondences found: 8
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://confOf#employedBy> <http://confOf-instances#orga1699753905>. 
0.0 <-> ?answer <http://confOf#hasFirstName> "Dan". 
0.0 <-> ?answer <http://confOf#studyAt> <http://confOf-instances#orga1460571171>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 
0.0 <-> <http://confOf-instances#paper8654177606562198> <http://confOf#writtenBy> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Thing>. 

Binary query : ?answer0 <http://cmt#writeReview> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Conference> .
Number of matched answers :5
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference>. 
creating directory: conference

Binary query : ?answer0 <http://cmt#name> ?answer1 .   ?answer0 a <http://cmt#Person> .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person> .
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 
creating directory: person

Binary query : ?answer0 <http://cmt#date> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
0.5 <-> ?answer0 <http://confOf#starts_on> ?answer1.  
creating directory: conference_start_date

Binary query : ?answer0 <http://cmt#assignExternalReviewer> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Conference> . ?answer0 <http://cmt#name> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://confOf#Conference>.  ?answer0 <http://confOf#has_title> ?answer1.  
creating directory: conference_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperAbstract> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommitteeMember> .
Number of matched answers :10
Number of correspondences found: 4
0.0 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
0.0 <-> ?answer <http://confOf#employedBy> ?someObject. 
0.0 <-> ?answer <http://confOf#reviewes> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Thing>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer> .
Number of matched answers :10
Number of correspondences found: 2
0.875 <-> ?answer <http://confOf#reviewes> ?someObject. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
creating directory: assigned_reviewer

Unary query : { ?answer <http://cmt#hasDecision> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Acceptance> . } UNION  { ?answer <http://cmt#acceptedBy> ?o2 . }
Number of matched answers :10
Number of correspondences found: 4
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?someSubject <http://confOf#reviewes> ?answer. 
0.0 <-> ?someSubject <http://confOf#writes> ?answer. 
0.0 <-> ?answer <http://confOf#hasKeyword> "scalable machine learning". 

Binary query : ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperFullVersion> .   { ?answer0 <http://cmt#writePaper> ?answer1 .   } union{ ?answer0 <http://cmt#co-writePaper> ?answer1 .   }
Number of matched answers :10
Number of correspondences found: 2
0.5 <-> ?answer1 <http://confOf#writtenBy> ?answer0.  
0.5 <-> ?answer0 <http://confOf#writes> ?answer1.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://cmt#memberOfConference> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#SubjectArea> .
Number of matched answers :10
Number of correspondences found: 3
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Topic>. 
0.0 <-> ?someSubject <http://confOf#dealsWith> ?answer. 
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> ?someObject. 

Unary query : ?o2 <http://cmt#siteURL> ?answer .
Number of matched answers :0
Number of correspondences found: 0

Unary query : { ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer> .   } union{ ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ExternalReviewer> .   }
Number of matched answers :10
Number of correspondences found: 9
0.0 <-> ?answer <http://confOf#hasFirstName> "Dan". 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 
0.0 <-> ?answer <http://confOf#employedBy> <http://confOf-instances#orga1699753905>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Thing>. 
0.0 <-> <http://confOf-instances#paper8654177606562198> <http://confOf#writtenBy> ?answer. 
0.0 <-> ?answer <http://confOf#studyAt> <http://confOf-instances#orga1460571171>. 
0.875 <-> ?answer <http://confOf#reviewes> ?someObject. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
creating directory: reviewers_all

Binary query : ?answer0 a <http://cmt#PaperFullVersion> . ?answer0 <http://cmt#hasSubjectArea> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
0.5 <-> ?answer0 <http://confOf#dealsWith> ?answer1.  
creating directory: topic_of_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Review> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer1 <http://cmt#siteURL> ?answer0 .
Number of matched answers :5
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Author> .
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author>. 
creating directory: author

Binary query : ?answer0 a <http://cmt#PaperFullVersion> .   ?answer1 a <http://cmt#PaperAbstract> . ?answer0 <http://cmt#title> ?o2 . ?answer1 <http://cmt#title> ?o2 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperFullVersion> .
Number of matched answers :10
Number of correspondences found: 4
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Thing>. 
0.0 <-> ?someSubject <http://confOf#writes> ?answer. 
0.0 <-> ?answer <http://confOf#dealsWith> ?someObject. 
0.0 <-> ?answer <http://confOf#hasTitle> "On-line separation of ^{163}Ta and ^{162}Ta". 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommitteeChair> .
Number of matched answers :5
Number of correspondences found: 2
0.0 <-> ?answer <http://confOf#reviewes> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Chair_PC>. 

Binary query : ?answer0 <http://cmt#title> ?answer1 .   ?answer0 a <http://cmt#PaperFullVersion> .
Number of matched answers :10
Number of correspondences found: 1
1.125 <-> ?answer0 <http://confOf#hasTitle> ?answer1.  
creating directory: paper_title

Binary query : ?answer0 <http://cmt#title> ?answer1 .   ?answer0 a <http://cmt#PaperAbstract> .
Number of matched answers :0
Number of correspondences found: 0
Matching process ended
