===============================================================================
creating directory: ekaw-cmt
Running with 10 support instances - 0.4 similarity.
Number of CQAs: 65

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Proceedings_Publisher> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Rejected_Paper> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 1
0.7857142857142857 <-> ?someSubject <http://cmt#rejectPaper> ?answer. 
creating directory: rejected_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Track> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Participant> .   {?answer a <http://ekaw#Early-Registered_Participant> . } union{ ?answer a <http://ekaw#Late-Registered_Participant> . }
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 5
0.4545454545454546 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.4545454545454546 <-> ?someSubject <http://cmt#hasConferenceMember> ?answer. 
0.4545454545454546 <-> ?answer <http://cmt#memberOfConference> <http://cmt-instances#conference1428971814>. 
0.4545454545454546 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceMember>. 
creating directory: participant

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#OC_Member> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 4
0.5 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#ConferenceMember>. 
0.5 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.5 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#ConferenceMember>. 
0.5 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceMember>. 
creating directory: oc_member

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Proceedings> .   ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :0
Looking for similar answers
Number of similar answers :0
Number of correspondences found: 0

Binary query : ?answer1 <http://ekaw#hasEvent> ?answer0 .  ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .  ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://ekaw#reviewOfPaper> ?answer1 .
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 3
1.1153846153846152 <-> ?v1 <http://cmt#writeReview> ?answer0.  ?v1 a <http://cmt#Reviewer>.  ?answer1 <http://cmt#assignedTo> ?v1.  
1.1153846153846152 <-> ?v1 <http://cmt#writeReview> ?answer0.  ?v1 a <http://cmt#Reviewer>.  ?v1 <http://cmt#hasBeenAssigned> ?answer1.  
1.1153846153846152 <-> ?answer0 <http://cmt#writtenBy> ?v1.  ?v1 a <http://cmt#Reviewer>.  ?v1 <http://cmt#hasBeenAssigned> ?answer1.  
creating directory: review_of_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Tutorial> .  ?answer0 <http://ekaw#coversTopic> ?answer1 .
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 0

Binary query : ?answer0 a <http://ekaw#Conference_Participant>.      bind( if (exists {?answer0 a <http://ekaw#Early-Registered_Participant>}, "true"^^<http://www.w3.org/2001/XMLSchema#boolean>,        if (exists {?answer0 a <http://ekaw#Late-Registered_Participant>}, "false"^^<http://www.w3.org/2001/XMLSchema#boolean>,"")) as ?answer1) filter(?answer1 != "")
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 0

Binary query : ?answer1 <http://ekaw#hasEvent> ?answer0 .  ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .  ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Tutorial> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :0
Number of correspondences found: 0

Binary query : ?answer0 a <http://ekaw#Conference> . ?answer0 <http://ekaw#heldIn> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :0
Looking for similar answers
Number of similar answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Late-Registered_Participant> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 7
0.0 <-> ?answer <http://cmt#memberOfConference> <http://cmt-instances#conference1428971814>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceMember>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Author>. 
0.0 <-> ?answer <http://cmt#hasBeenAssigned> <http://cmt-instances#paper20629177143661114>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ExternalReviewer>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer>. 
0.0 <-> ?answer <http://cmt#name> "Rudolf Hell". 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#University> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :2
Number of correspondences found: 2
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#SubjectArea>. 
0.0 <-> <http://cmt-instances#paper-1571943214552198> <http://cmt#hasSubjectArea> ?answer. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Trip> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :0
Number of correspondences found: 0

Binary query : ?answer0 a <http://ekaw#Invited_Speaker>. ?answer1 <http://ekaw#writtenBy> ?answer0. ?answer1 a <http://ekaw#Invited_Talk_Abstract>.
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 0

Binary query : ?answer0 a <http://ekaw#Invited_Speaker>. ?o2 <http://ekaw#writtenBy> ?answer0. ?o2 <http://ekaw#paperPresentedAs> ?answer1. ?answer1 a <http://ekaw#Invited_Talk>.
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member> .   ?answer0 <http://ekaw#reviewerOfPaper> ?answer1 .
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 2
1.0333333333333332 <-> ?answer0 a <http://cmt#Reviewer>.  ?answer0 <http://cmt#hasBeenAssigned> ?answer1.  
1.0333333333333332 <-> ?answer0 a <http://cmt#Reviewer>.  ?answer1 <http://cmt#assignedTo> ?answer0.  
creating directory: is_assigned_reviewer_of

Unary query : ?o3 <http://ekaw#reviewWrittenBy> ?answer. MINUS{ ?answer <http://ekaw#reviewerOfPaper> ?o2. }
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 2
1.0 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Reviewer>. 
1.0 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Reviewer>. 
creating directory: external_reviewer

Binary query : ?answer1 <http://ekaw#reviewWrittenBy> ?answer0 .   ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member> .  ?answer0 <http://ekaw#reviewerOfPaper> ?o3.
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 2
2.3 <-> ?answer0 a <http://cmt#Reviewer>.  ?answer0 <http://cmt#writeReview> ?answer1.  ?answer1 a <http://cmt#Review>.  
2.9000000000000004 <-> ?answer0 a <http://cmt#Reviewer>.  ?answer1 <http://cmt#writtenBy> ?answer0.  ?answer1 a <http://cmt#Review>.  
creating directory: assigned_reviewer_author_of_review

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Track> .   ?answer1 <http://ekaw#hasEvent> ?answer0 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop_Chair> .   ?answer0 <http://ekaw#organises> ?answer1 . ?answer1 a <http://ekaw#Workshop> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .   ?answer0 a <http://ekaw#Person> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 1
1.9 <-> ?answer0 a <http://cmt#Person>.  ?answer0 <http://cmt#name> ?answer1.  
creating directory: person_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Person> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 9
1.0 <-> ?answer <http://cmt#addedBy> ?y. ?y a <http://cmt#Person>. 
1.0 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Person>. 
1.0 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#Person>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person>. 
1.0 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Person>. 
1.0 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#Person>. 
1.0 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#Person>. 
1.0 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#Person>. 
1.0 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#Person>. 
creating directory: person

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Banquet> .   ?answer0 <http://ekaw#heldIn> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :0
Looking for similar answers
Number of similar answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Proceedings> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :0
Number of correspondences found: 0

Binary query : ?answer1 <http://ekaw#publisherOf> ?answer0 .  ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Proceedings> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :0
Number of correspondences found: 0

Binary query : ?o3 <http://ekaw#reviewWrittenBy> ?answer0 .   ?o3 <http://ekaw#reviewWrittenBy> ?answer1 . ?answer1 <http://ekaw#reviewerOfPaper> ?o2 . ?o3 <http://ekaw#reviewOfPaper> ?o2 . FILTER(?o!=?answer0)
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 2
2.115384615384616 <-> ?answer1 <http://cmt#assignExternalReviewer> ?answer0.  ?answer1 a <http://cmt#Reviewer>.  
2.115384615384616 <-> ?answer0 <http://cmt#assignedByReviewer> ?answer1.  ?answer1 a <http://cmt#Reviewer>.  
creating directory: invite_reviewer

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Tutorial> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Early-Registered_Participant> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 9
0.0 <-> ?answer <http://cmt#name> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Author>. 
0.0 <-> ?answer <http://cmt#readPaper> <http://cmt-instances#paper20518448391311>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceMember>. 
0.0 <-> ?answer <http://cmt#co-writePaper> <http://cmt-instances#paper-12795566372181114>. 
0.0 <-> ?answer <http://cmt#writeReview> <http://cmt-instances#review-12358590847852198_-196548214>. 
0.0 <-> <http://cmt-instances#conference943277065> <http://cmt#hasConferenceMember> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ExternalReviewer>. 
0.0 <-> <http://cmt-instances#review10209702401732198_-196548213> <http://cmt#writtenBy> ?answer. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .  ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :0
Looking for similar answers
Number of similar answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member> .   ?answer <http://ekaw#reviewerOfPaper> ?o2.
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 2
0.5333333333333333 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer>. 
0.5333333333333333 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#Reviewer>. 
creating directory: pc_member

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://ekaw#coversTopic> ?answer1 .   ?answer0 a <http://ekaw#Conference> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member> .   ?answer <http://ekaw#reviewerOfPaper> ?o3.
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 2
0.5333333333333333 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer>. 
0.5333333333333333 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#Reviewer>. 
creating directory: assigned_reviewer

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Accepted_Paper> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 1
0.7857142857142857 <-> ?someSubject <http://cmt#acceptPaper> ?answer. 
creating directory: accepted_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Web_Site> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop_Chair> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 9
0.0 <-> ?answer <http://cmt#memberOfConference> <http://cmt-instances#conference1428971814>. 
0.0 <-> ?answer <http://cmt#assignExternalReviewer> <http://cmt-instances#person-1123416604>. 
0.0 <-> ?answer <http://cmt#hasBeenAssigned> <http://cmt-instances#paper20629177143661114>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ExternalReviewer>. 
0.0 <-> <http://cmt-instances#conference1428971814> <http://cmt#hasConferenceMember> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceMember>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer>. 
0.0 <-> ?answer <http://cmt#name> "Rudolf Hell". 
0.0 <-> ?answer <http://cmt#readPaper> <http://cmt-instances#paper15434030271382198>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Invited_Talk_Abstract> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Invited_Speaker> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 9
0.4666666666666667 <-> ?answer <http://cmt#writePaper> ?someObject. 
creating directory: invited_speaker

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper_Author> . ?answer0 <http://ekaw#authorOf> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 4
3.1666666666666665 <-> ?answer0 a <http://cmt#Author>.  ?answer0 <http://cmt#co-writePaper> ?answer1.  ?answer1 a <http://cmt#Paper>.  
3.6666666666666665 <-> ?answer0 a <http://cmt#Author>.  ?answer0 <http://cmt#writePaper> ?answer1.  ?answer1 a <http://cmt#Paper>.  
3.75 <-> ?answer0 a <http://cmt#Author>.  ?answer1 <http://cmt#hasCo-author> ?answer0.  ?answer1 a <http://cmt#Paper>.  
4.194444444444445 <-> ?answer0 a <http://cmt#Author>.  ?answer1 <http://cmt#hasAuthor> ?answer0.  ?answer1 a <http://cmt#Paper>.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Trip> .   ?answer0 <http://ekaw#heldIn> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :0
Looking for similar answers
Number of similar answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Research_Topic> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 3
0.0 <-> ?someSubject <http://cmt#hasSubjectArea> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#SubjectArea>. 
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> ?someObject. 

Binary query : ?answer0 a <http://ekaw#Invited_Talk_Abstract>. ?answer0 <http://ekaw#paperPresentedAs> ?answer1. ?answer1 a <http://ekaw#Invited_Talk>.
Number of matched answers :0
Looking for similar answers
Number of similar answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Student> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 7
0.5 <-> ?answer <http://cmt#co-writePaper> ?y. ?y a <http://cmt#Document>. 
0.5 <-> ?answer <http://cmt#writePaper> ?y. ?y a <http://cmt#Document>. 
0.5 <-> ?answer <http://cmt#submitPaper> ?y. ?y a <http://cmt#Document>. 
0.5 <-> ?x <http://cmt#hasCo-author> ?answer. ?x a <http://cmt#Document>. 
0.5 <-> ?answer <http://cmt#markConflictOfInterest> ?y. ?y a <http://cmt#Document>. 
creating directory: student

Unary query : ?o2 a <http://ekaw#Web_Site>. ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer. filter(regex(?s,"^http"))
Number of matched answers :0
Looking for similar answers
Number of similar answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#SC_Member> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 5
0.4375 <-> ?answer <http://cmt#addedBy> ?y. ?y a <http://cmt#ConferenceMember>. 
0.4375 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.4375 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.4375 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.4375 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceMember>. 
creating directory: sc_member

Binary query : ?answer1 <http://ekaw#reviewWrittenBy> ?answer0 .
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 6
0.5 <-> ?answer0 <http://cmt#writeReview> ?answer1.  
0.5 <-> ?answer0 <http://cmt#assignedByReviewer> ?v1.  ?v1 <http://cmt#writeReview> ?answer1.  
0.5 <-> ?v1 <http://cmt#assignExternalReviewer> ?answer0.  ?v1 <http://cmt#writeReview> ?answer1.  
1.1 <-> ?answer1 <http://cmt#writtenBy> ?answer0.  
1.1 <-> ?v1 <http://cmt#assignExternalReviewer> ?answer0.  ?answer1 <http://cmt#writtenBy> ?v1.  
1.1 <-> ?answer0 <http://cmt#assignedByReviewer> ?v1.  ?answer1 <http://cmt#writtenBy> ?v1.  
creating directory: all_reviewer_author_of_review

Unary query : {?answer <http://ekaw#reviewerOfPaper> ?o2 . } union{ ?o3 <http://ekaw#reviewOfPaper> ?o2 .  ?o3 <http://ekaw#reviewWrittenBy> ?answer .  }
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 7
0.9282051282051282 <-> ?answer <http://cmt#writePaper> ?someObject. 
1.0051282051282042 <-> ?answer <http://cmt#readPaper> ?someObject. 
1.2615384615384615 <-> ?answer <http://cmt#writeReview> ?y. ?y a <http://cmt#Review>. 
1.6153846153846154 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Reviewer>. 
1.6153846153846154 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Reviewer>. 
1.6153846153846154 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer>. 
1.8615384615384616 <-> ?x <http://cmt#writtenBy> ?answer. ?x a <http://cmt#Review>. 
creating directory: reviewers_all

Binary query : ?answer0 <http://ekaw#coversTopic> ?answer1 . ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://cmt#Paper>.  ?answer0 <http://cmt#hasSubjectArea> ?answer1.  
creating directory: topic_of_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Review> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 3
0.75 <-> ?answer <http://cmt#writtenBy> ?y. ?y a <http://cmt#Reviewer>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Review>. 
1.2954545454545454 <-> ?x <http://cmt#writeReview> ?answer. ?x a <http://cmt#Reviewer>. 
creating directory: review

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop> .  ?answer0 <http://ekaw#coversTopic> ?answer1 .
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Banquet> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :0
Number of correspondences found: 0

Binary query : ?answer1 a <http://ekaw#Conference> . ?o2 <http://ekaw#listsEvent> ?answer1 . ?o2 a <http://ekaw#Web_Site> . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer0 . filter(regex(?s,"^http"))
Number of matched answers :0
Looking for similar answers
Number of similar answers :0
Number of correspondences found: 0

Unary query : ?answer a <http://ekaw#Invited_Talk> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper_Author> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 2
0.9999999999999999 <-> ?x <http://cmt#hasAuthor> ?answer. ?x a <http://cmt#Paper>. 
0.9999999999999999 <-> ?x <http://cmt#hasCo-author> ?answer. ?x a <http://cmt#Paper>. 
creating directory: author

Binary query : ?answer0 <http://ekaw#presentationOfPaper> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#OC_Chair> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :5
Number of correspondences found: 4
0.0 <-> ?answer <http://cmt#hasBeenAssigned> <http://cmt-instances#paper-1980828754170257>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceMember>. 
0.0 <-> ?answer <http://cmt#co-writePaper> ?someObject. 
0.0 <-> <http://cmt-instances#person-1860172640> <http://cmt#assignExternalReviewer> ?answer. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 7
0.7142857142857139 <-> ?answer <http://cmt#paperID> ?someObject. 
0.8545454545454546 <-> ?x <http://cmt#rejectPaper> ?answer. ?x a <http://cmt#User>. 
0.8545454545454546 <-> ?x <http://cmt#acceptPaper> ?answer. ?x a <http://cmt#User>. 
0.8545454545454546 <-> ?x <http://cmt#submitPaper> ?answer. ?x a <http://cmt#User>. 
0.8943181818181818 <-> ?x <http://cmt#writePaper> ?answer. ?x a <http://cmt#User>. 
0.9555555555555556 <-> ?x <http://cmt#readPaper> ?answer. ?x a <http://cmt#User>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Paper>. 
creating directory: paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Chair> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :5
Number of correspondences found: 4
0.0 <-> ?answer <http://cmt#writeReview> ?someObject. 
0.0 <-> <http://cmt-instances#review-7190391553382198_-559246915> <http://cmt#writtenBy> ?answer. 
0.0 <-> <http://cmt-instances#person-901707288> <http://cmt#assignReviewer> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommitteeMember>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Regular_Paper> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 1
0.6153846153846156 <-> ?someSubject <http://cmt#readPaper> ?answer. 
creating directory: regular_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .   ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.
Number of matched answers :0
Looking for similar answers
Number of similar answers :10
Number of correspondences found: 1
1.8999999999999992 <-> ?answer0 a <http://cmt#Paper>.  ?answer0 <http://cmt#title> ?answer1.  
creating directory: paper_title

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Contributed_Talk> .
Number of matched answers :0
Looking for similar answers
Number of similar answers :0
Number of correspondences found: 0

Binary query : ?answer0 a <http://ekaw#Contributed_Talk>. ?answer1 a <http://ekaw#Conference> . ?answer0 <http://ekaw#partOfEvent> ?answer1.
Number of matched answers :0
Looking for similar answers
Number of similar answers :0
Number of correspondences found: 0
Matching process ended
