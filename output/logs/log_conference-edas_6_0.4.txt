===============================================================================
creating directory: conference-edas
Running with 6 support instances - 0.4 similarity.
Number of CQAs: 73

Unary query : ?answer a <http://conference#Contribution_1th-author>.
Number of matched answers :6
Number of correspondences found: 5
0.0 <-> ?someSubject <http://edas#isReviewedBy> ?answer. 
0.0 <-> <http://edas-instances#paper-4150600525412198> <http://edas#isWrittenBy> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Author>. 
0.0 <-> ?answer <http://edas#hasRelatedPaper> <http://edas-instances#paper829158470991114>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Publisher> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Rejected_contribution> .
Number of matched answers :6
Number of correspondences found: 2
0.4285714285714286 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#RejectedPaper>. 
0.4285714285714286 <-> ?answer <http://edas#hasRating> ?y. ?y a <http://edas#RejectRating>. 
creating directory: rejected_paper

Binary query : ?answer0 <http://conference#has_the_last_name> ?answer1 .
Number of matched answers :6
Number of correspondences found: 1
1.1470588235294115 <-> ?answer0 <http://edas#hasLastName> ?answer1.  
creating directory: person_last_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Track> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_participant> .
Number of matched answers :6
Number of correspondences found: 3
0.4545454545454546 <-> ?answer <http://edas#isMemberOf> ?someObject. 
0.4545454545454546 <-> <http://edas-instances#conference-1409861683> <http://edas#hasMember> ?answer. 
creating directory: participant

Unary query : ?answer <http://conference#was_a_member_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Organizing_committee> .
Number of matched answers :6
Number of correspondences found: 1
0.6 <-> ?answer <http://edas#isMemberOf> ?someObject. 
creating directory: oc_member

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_proceedings> .   ?answer0 <http://conference#has_a_name> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer1 <http://conference#has_workshops> ?answer0 .
Number of matched answers :6
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#was_a_member_of> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :6
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#reviews> ?answer1 .
Number of matched answers :6
Number of correspondences found: 6
1.357142857142857 <-> ?answer0 a <http://edas#Review>.  ?v1 <http://edas#hasRelatedDocument> ?answer0.  ?v1 <http://edas#hasRelatedDocument> ?answer1.  
1.357142857142857 <-> ?answer0 a <http://edas#Review>.  ?answer0 <http://edas#relatesTo> ?v1.  ?v1 <http://edas#hasRelatedDocument> ?answer1.  
1.3571428571428574 <-> ?answer0 a <http://edas#Review>.  ?v1 <http://edas#hasRelatedDocument> ?answer0.  ?answer1 <http://edas#relatesTo> ?v1.  
2.607142857142857 <-> ?answer0 a <http://edas#Review>.  ?v1 <http://edas#hasRelatedDocument> ?answer0.  ?v1 a <http://edas#Reviewer>.  ?answer1 <http://edas#isReviewedBy> ?v1.  
2.652597402597402 <-> ?answer0 a <http://edas#Review>.  ?answer0 <http://edas#relatesTo> ?v1.  ?v1 a <http://edas#Reviewer>.  ?v1 <http://edas#isReviewing> ?answer1.  
2.652597402597402 <-> ?answer0 a <http://edas#Review>.  ?v1 <http://edas#hasRelatedDocument> ?answer0.  ?v1 a <http://edas#Reviewer>.  ?v1 <http://edas#isReviewing> ?answer1.  
creating directory: review_of_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Tutorial> .  ?answer0 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1 .
Number of matched answers :6
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 . ?o2 a <http://conference#Important_dates>. ?o2 <http://conference#is_a_date_of_acceptance_announcement> ?answer1.
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 a <http://conference#Conference_participant>.      bind( if (exists {?answer0 a <http://conference#Early_paid_applicant>}, "true"^^<http://www.w3.org/2001/XMLSchema#boolean>,        if (exists {?answer0 a <http://conference#Late_paid_applicant>}, "false"^^<http://www.w3.org/2001/XMLSchema#boolean>,"")) as ?answer1)
Number of matched answers :6
Number of correspondences found: 0

Binary query : ?answer1 <http://conference#has_tutorials> ?answer0 .
Number of matched answers :6
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#has_a_location> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.0714285714285714 <-> ?answer0 <http://edas#location> ?answer1.  
creating directory: conference_location

Unary query : ?answer a <http://conference#Conference_participant> .   ?answer a <http://conference#Late_paid_applicant> .
Number of matched answers :6
Number of correspondences found: 4
0.5454545454545454 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceChair>. 
creating directory: late_registered_participant

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Contribution_co-author> .
Number of matched answers :6
Number of correspondences found: 4
0.0 <-> ?answer <http://edas#isReviewing> <http://edas-instances#paper-15212672341191263>. 
0.0 <-> ?someSubject <http://edas#isWrittenBy> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 
0.0 <-> ?answer <http://edas#isMemberOf> <http://edas-instances#conference-104081973>. 

Binary query : ?answer1 a <http://conference#Abstract>. ?answer0 <http://conference#contributes> ?answer1 . ?o2 <http://conference#has_an_abstract> ?answer1. ?o2 a <http://conference#Invited_talk>.
Number of matched answers :6
Number of correspondences found: 0

Binary query : ?answer0 a <http://conference#Invited_speaker>. ?answer0 <http://conference#contributes> ?answer1 . ?answer1 a <http://conference#Invited_talk>.
Number of matched answers :6
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#contributes> ?o2 .  ?o2 <http://conference#reviews> ?answer1 .  ?answer0 <http://conference#was_a_member_of> ?o3 . ?o3 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :6
Number of correspondences found: 6
1.75 <-> ?answer0 a <http://edas#Reviewer>.  ?answer1 <http://edas#isReviewedBy> ?answer0.  
1.7833333333333332 <-> ?answer0 a <http://edas#Reviewer>.  ?v1 <http://edas#hasMember> ?answer0.  ?answer1 <http://edas#relatesTo> ?v1.  
1.7833333333333332 <-> ?answer0 a <http://edas#Reviewer>.  ?v1 <http://edas#hasMember> ?answer0.  ?v1 <http://edas#hasRelatedDocument> ?answer1.  
1.7954545454545456 <-> ?answer0 a <http://edas#Reviewer>.  ?answer0 <http://edas#isReviewing> ?answer1.  
1.8500000000000003 <-> ?answer0 a <http://edas#Reviewer>.  ?answer0 <http://edas#isMemberOf> ?v1.  ?answer1 <http://edas#relatesTo> ?v1.  
1.8500000000000003 <-> ?answer0 a <http://edas#Reviewer>.  ?answer0 <http://edas#isMemberOf> ?v1.  ?v1 <http://edas#hasRelatedDocument> ?answer1.  
creating directory: is_assigned_reviewer_of

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#gives_presentations> ?answer1. ?answer1 a <http://conference#Presentation>.
Number of matched answers :6
Number of correspondences found: 3
2.473426212590299 <-> ?answer0 a <http://edas#Presenter>.  ?v1 <http://edas#isWrittenBy> ?answer0.  ?v1 <http://edas#relatedToEvent> ?answer1.  ?answer1 a <http://edas#PaperPresentation>.  
2.473426212590299 <-> ?answer0 a <http://edas#Presenter>.  ?answer0 <http://edas#hasRelatedPaper> ?v1.  ?v1 <http://edas#relatedToEvent> ?answer1.  ?answer1 a <http://edas#PaperPresentation>.  
2.473426212590299 <-> ?answer0 a <http://edas#Presenter>.  ?v1 <http://edas#relatesTo> ?answer0.  ?answer1 <http://edas#hasRelatedDocument> ?v1.  ?answer1 a <http://edas#PaperPresentation>.  
creating directory: presenter_of_paper_presentation

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer> .  ?answer <http://conference#invited_by> ?o2 .
Number of matched answers :6
Number of correspondences found: 4
0.75 <-> ?answer <http://edas#hasRelatedDocument> ?y. ?y a <http://edas#Review>. 
0.75 <-> ?x <http://edas#relatesTo> ?answer. ?x a <http://edas#Review>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reviewer>. 
1.0833333333333333 <-> ?someSubject <http://edas#isReviewedBy> ?answer. 
creating directory: external_reviewer

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 . ?o2 <http://conference#is_a_date_of_camera_ready_paper_submission> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
0.5 <-> ?answer0 <http://edas#paperDueOn> ?answer1.  
creating directory: conference_camera_date

Binary query : ?answer0 <http://conference#contributes> ?answer1 .   ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Review> .    ?answer0 <http://conference#was_a_member_of> ?o3 . ?o3 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :6
Number of correspondences found: 2
2.25 <-> ?answer0 a <http://edas#Reviewer>.  ?answer0 <http://edas#hasRelatedDocument> ?answer1.  ?answer1 a <http://edas#Review>.  
2.25 <-> ?answer0 a <http://edas#Reviewer>.  ?answer1 <http://edas#relatesTo> ?answer0.  ?answer1 a <http://edas#Review>.  
creating directory: assigned_reviewer_author_of_review

Binary query : ?answer1 <http://conference#has_tracks> ?answer0 .
Number of matched answers :6
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#was_a_track-workshop_chair_of> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop> .
Number of matched answers :6
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_volume> .
Number of matched answers :5
Number of correspondences found: 3
0.5882352941176471 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Conference>. 
0.5882352941176471 <-> ?x <http://edas#isMemberOf> ?answer. ?x a <http://edas#ConferenceChair>. 
0.65 <-> ?answer <http://edas#hasName> "International Symposium on Languages in Biology and Medicine (LBM)". 
creating directory: conference

Binary query : ?answer0 <http://conference#has_the_first_name> ?o1 .  ?answer0 <http://conference#has_the_last_name> ?o2 .   BIND( concat(STR(?o1)," ", STR(?o2)) as ?answer1)
Number of matched answers :6
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Person> .
Number of matched answers :6
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 
creating directory: person

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_proceedings> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#has_a_publisher> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 .   ?answer0 a <http://conference#Conference_volume> . ?o2 <http://conference#is_a_starting_date> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
2.4092879256965944 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#startDate> ?answer1.  
creating directory: conference_start_date

Binary query : ?answer0 <http://conference#invites_co-reviewers> ?answer1 .
Number of matched answers :6
Number of correspondences found: 6
0.5 <-> ?v1 <http://edas#relatesTo> ?answer0.  ?v1 <http://edas#relatesTo> ?answer1.  
0.5 <-> ?v1 <http://edas#relatesTo> ?answer0.  ?answer1 <http://edas#hasRelatedDocument> ?v1.  
0.5 <-> ?answer0 <http://edas#isMemberOf> ?v1.  ?v1 <http://edas#hasMember> ?answer1.  
0.5 <-> ?v1 <http://edas#hasMember> ?answer0.  ?answer1 <http://edas#isMemberOf> ?v1.  
0.5 <-> ?answer0 <http://edas#hasRelatedDocument> ?v1.  ?v1 <http://edas#relatesTo> ?answer1.  
1.3 <-> ?v1 <http://edas#isReviewedBy> ?answer0.  ?v1 <http://edas#isReviewedBy> ?answer1.  
creating directory: invite_reviewer

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Tutorial> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer a <http://conference#Conference_participant> .   ?answer a <http://conference#Early_paid_applicant> .
Number of matched answers :6
Number of correspondences found: 3
0.4545454545454546 <-> ?answer <http://edas#isMemberOf> ?someObject. 
0.4545454545454546 <-> <http://edas-instances#conference-1409861683> <http://edas#hasMember> ?answer. 
creating directory: early_registered_participant

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference> . ?answer0 <http://conference#has_a_name> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
2.7 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#hasName> ?answer1.  
creating directory: conference_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Abstract> .   ?answer <http://conference#is_the_1th_part_of> ?o2. ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://conference#was_a_member_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :6
Number of correspondences found: 1
0.6 <-> ?answer <http://edas#isMemberOf> ?someObject. 
creating directory: pc_member

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop> .
Number of matched answers :6
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Workshop>. 
creating directory: workshop

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 . ?o2 <http://conference#is_a_full_paper_submission_date> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
0.5 <-> ?answer0 <http://edas#manuscriptDueOn> ?answer1.  
creating directory: conference_paper_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer> . ?answer <http://conference#was_a_member_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :6
Number of correspondences found: 4
0.6 <-> ?answer <http://edas#isMemberOf> ?someObject. 
0.75 <-> ?answer <http://edas#hasRelatedDocument> ?y. ?y a <http://edas#Review>. 
0.75 <-> ?x <http://edas#relatesTo> ?answer. ?x a <http://edas#Review>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reviewer>. 
creating directory: assigned_reviewer

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Accepted_contribution> .
Number of matched answers :6
Number of correspondences found: 2
0.4285714285714286 <-> ?answer <http://edas#hasRating> ?y. ?y a <http://edas#AcceptRating>. 
0.4285714285714286 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#AcceptedPaper>. 
creating directory: accepted_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_www> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://conference#was_a_track-workshop_chair_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop> .
Number of matched answers :6
Number of correspondences found: 5
0.0 <-> ?answer <http://edas#isReviewing> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reviewer>. 
0.0 <-> <http://edas-instances#review1898681808222257_-196548213> <http://edas#relatesTo> ?answer. 
0.0 <-> ?answer <http://edas#hasLastName> "Collin". 
0.0 <-> ?answer <http://edas#hasRelatedPaper> <http://edas-instances#paper16834214795021114>. 

Unary query : ?answer <http://conference#is_the_1th_part_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Invited_talk> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Invited_speaker> .
Number of matched answers :6
Number of correspondences found: 4
0.4000000000000001 <-> ?answer <http://edas#isReviewing> ?y. ?y a <http://edas#AcceptedPaper>. 
creating directory: invited_speaker

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Regular_author> . ?answer0 <http://conference#contributes> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .   minus{  ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Abstract> .   }
Number of matched answers :6
Number of correspondences found: 2
0.5 <-> ?answer0 <http://edas#hasRelatedPaper> ?answer1.  
0.5 <-> ?answer1 <http://edas#isWrittenBy> ?answer0.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 . ?o2 <http://conference#is_an_ending_date> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
0.9117647058823529 <-> ?answer0 <http://edas#endDate> ?answer1.  
creating directory: conference_end_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Topic> .
Number of matched answers :6
Number of correspondences found: 2
0.625 <-> ?someSubject <http://edas#hasTopic> ?answer. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Topic>. 
creating directory: topic

Binary query : ?answer0 a <http://conference#Abstract>. ?answer1 <http://conference#has_an_abstract> ?answer0 . ?answer1 a <http://conference#Invited_talk>.
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?o2 a <http://conference#Conference_www>. ?o2 <http://conference#has_a_URL> ?answer .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://conference#was_a_member_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Steering_committee> .
Number of matched answers :6
Number of correspondences found: 1
0.6 <-> ?answer <http://edas#isMemberOf> ?someObject. 
creating directory: sc_member

Binary query : ?answer1 <http://conference#has_authors> ?answer0 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Review> .
Number of matched answers :6
Number of correspondences found: 2
2.25 <-> ?answer0 a <http://edas#Reviewer>.  ?answer0 <http://edas#hasRelatedDocument> ?answer1.  ?answer1 a <http://edas#Review>.  
2.25 <-> ?answer0 a <http://edas#Reviewer>.  ?answer1 <http://edas#relatesTo> ?answer0.  ?answer1 a <http://edas#Review>.  
creating directory: all_reviewer_author_of_review

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer> .
Number of matched answers :6
Number of correspondences found: 3
0.75 <-> ?answer <http://edas#hasRelatedDocument> ?y. ?y a <http://edas#Review>. 
0.75 <-> ?x <http://edas#relatesTo> ?answer. ?x a <http://edas#Review>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reviewer>. 
creating directory: reviewers_all

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Review> .
Number of matched answers :6
Number of correspondences found: 3
0.75 <-> ?x <http://edas#hasRelatedDocument> ?answer. ?x a <http://edas#Reviewer>. 
0.75 <-> ?answer <http://edas#relatesTo> ?y. ?y a <http://edas#Reviewer>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Review>. 
creating directory: review

Binary query : ?answer0 <http://conference#has_the_first_name> ?answer1 .
Number of matched answers :6
Number of correspondences found: 1
1.1666666666666667 <-> ?answer0 <http://edas#hasFirstName> ?answer1.  
creating directory: person_first_name

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop> .  ?answer0 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1 .
Number of matched answers :6
Number of correspondences found: 0

Unary query : ?answer a <http://conference#Invited_talk> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Regular_author> .
Number of matched answers :6
Number of correspondences found: 2
0.4285714285714286 <-> ?someSubject <http://edas#relatesTo> ?answer. 
0.4285714285714286 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Author>. 
creating directory: author

Binary query : ?answer0 a <http://conference#Written_contribution> . ?answer0 <http://conference#has_an_abstract> ?answer1 .
Number of matched answers :6
Number of correspondences found: 0

Binary query : ?answer0 a <http://conference#Presentation>. ?answer0 <http://conference#has_an_abstract> ?o2 . ?answer1 <http://conference#has_an_abstract> ?o2 . ?answer1 a <http://conference#Written_contribution> .
Number of matched answers :6
Number of correspondences found: 11
1.205882352941176 <-> ?answer0 a <http://edas#PaperPresentation>.  ?v1 <http://edas#relatedToEvent> ?answer0.  ?v2 <http://edas#hasRelatedDocument> ?v1.  ?answer1 <http://edas#relatesTo> ?v2.  
1.205882352941176 <-> ?answer0 a <http://edas#PaperPresentation>.  ?answer0 <http://edas#hasRelatedDocument> ?v1.  ?v1 <http://edas#relatesTo> ?v2.  ?v2 <http://edas#hasRelatedDocument> ?answer1.  
1.205882352941176 <-> ?answer0 a <http://edas#PaperPresentation>.  ?v1 <http://edas#relatedToEvent> ?answer0.  ?v1 <http://edas#relatesTo> ?v2.  ?v2 <http://edas#hasRelatedDocument> ?answer1.  
1.2058823529411764 <-> ?answer0 a <http://edas#PaperPresentation>.  ?answer0 <http://edas#relatedToPaper> ?answer1.  
1.2058823529411764 <-> ?answer0 a <http://edas#PaperPresentation>.  ?v1 <http://edas#relatedToEvent> ?answer0.  ?v2 <http://edas#isTopicOf> ?v1.  ?v2 <http://edas#isTopicOf> ?answer1.  
1.2058823529411764 <-> ?answer0 a <http://edas#PaperPresentation>.  ?answer1 <http://edas#relatedToEvent> ?answer0.  
1.2058823529411764 <-> ?answer0 a <http://edas#PaperPresentation>.  ?v1 <http://edas#relatedToEvent> ?answer0.  ?v2 <http://edas#hasRelatedDocument> ?v1.  ?v2 <http://edas#hasRelatedDocument> ?answer1.  
1.2058823529411764 <-> ?answer0 a <http://edas#PaperPresentation>.  ?v1 <http://edas#relatedToEvent> ?answer0.  ?v2 <http://edas#isReviewing> ?v1.  ?v2 <http://edas#isReviewing> ?answer1.  
1.2058823529411764 <-> ?answer0 a <http://edas#PaperPresentation>.  ?v1 <http://edas#relatedToEvent> ?answer0.  ?v2 <http://edas#hasRelatedPaper> ?v1.  ?v2 <http://edas#hasRelatedPaper> ?answer1.  
1.7892156862745097 <-> ?answer0 a <http://edas#PaperPresentation>.  ?v1 <http://edas#relatedToEvent> ?answer0.  ?v2 <http://edas#isReviewing> ?v1.  ?v2 a <http://edas#Presenter>.  ?v2 <http://edas#isReviewing> ?answer1.  
1.7892156862745097 <-> ?answer0 a <http://edas#PaperPresentation>.  ?v1 <http://edas#relatedToEvent> ?answer0.  ?v2 <http://edas#hasRelatedPaper> ?v1.  ?v2 a <http://edas#Presenter>.  ?v2 <http://edas#hasRelatedPaper> ?answer1.  
creating directory: presentation_of_paper

Unary query : ?answer <http://conference#was_a_committee_chair_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Organizing_committee> .
Number of matched answers :5
Number of correspondences found: 4
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 
0.0 <-> <http://edas-instances#review-554085786002198_-196548214> <http://edas#relatesTo> ?answer. 
0.0 <-> <http://edas-instances#paper-1921869894163257> <http://edas#isReviewedBy> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Author>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> . minus{ ?answer a  <http://conference#Abstract> . }
Number of matched answers :6
Number of correspondences found: 4
0.4 <-> <http://edas-instances#topic171133423> <http://edas#isTopicOf> ?answer. 
creating directory: paper

Unary query : ?answer <http://conference#gives_presentations> ?o3. ?o3 <http://conference#has_an_abstract> ?o2 . ?o4 <http://conference#has_an_abstract> ?o2 . ?o4 a <http://conference#Written_contribution> .
Number of matched answers :6
Number of correspondences found: 1
0.46666666666666673 <-> ?answer <http://edas#hasLastName> ?someObject. 
creating directory: presenter

Binary query : ?answer0 <http://conference#is_submitted_at> ?answer1 .
Number of matched answers :6
Number of correspondences found: 2
0.5 <-> ?answer0 <http://edas#relatesTo> ?answer1.  
0.5 <-> ?answer1 <http://edas#hasRelatedDocument> ?answer0.  
creating directory: paper_submitted_at_conference

Unary query : ?answer <http://conference#was_a_committee_chair_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :5
Number of correspondences found: 5
0.0 <-> <http://edas-instances#paper14883166801311263_v0> <http://edas#isReviewedBy> ?answer. 
0.0 <-> ?answer <http://edas#isReviewing> <http://edas-instances#paper-253329908202257_v0>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Presenter>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Attendee>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Paper> .
Number of matched answers :6
Number of correspondences found: 3
0.6666666666666667 <-> ?answer <http://edas#isWrittenBy> <http://edas-instances#person-1475411681>. 
0.6666666666666667 <-> <http://edas-instances#person-1475411681> <http://edas#hasRelatedPaper> ?answer. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Paper>. 
creating directory: regular_paper

Binary query : { ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Paper> .   } UNION { ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .   ?o2 <http://conference#is_the_1th_part_of> ?answer0. ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .   }  ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.
Number of matched answers :6
Number of correspondences found: 1
3.299999999999999 <-> ?answer0 a <http://edas#Paper>.  ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: paper_title

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Presentation> . minus{ ?answer a <http://conference#Invited_talk> . }
Number of matched answers :6
Number of correspondences found: 1
0.7058823529411764 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#PaperPresentation>. 
creating directory: paper_presentation

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Abstract> .   ?answer0 <http://conference#is_the_1th_part_of> ?o2. ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .    ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 a <http://conference#Presentation> . ?answer0 <http://conference#has_an_abstract> ?o2 . ?o4 <http://conference#has_an_abstract> ?o2 . ?o4 a <http://conference#Written_contribution> . ?o4 <http://conference#is_submitted_at> ?answer1.
Number of matched answers :6
Number of correspondences found: 3
1.2058823529411764 <-> ?answer0 a <http://edas#PaperPresentation>.  ?answer0 <http://edas#hasRelatedDocument> ?v1.  ?v1 <http://edas#relatesTo> ?answer1.  
1.2058823529411764 <-> ?answer0 a <http://edas#PaperPresentation>.  ?v1 <http://edas#relatedToEvent> ?answer0.  ?v1 <http://edas#relatesTo> ?answer1.  
1.2058823529411764 <-> ?answer0 a <http://edas#PaperPresentation>.  ?v1 <http://edas#relatedToEvent> ?answer0.  ?answer1 <http://edas#hasRelatedDocument> ?v1.  
creating directory: presentation_in_conference
Matching process ended
