===============================================================================
creating directory: confOf-cmt
Running with 6 support instances - 0.4 similarity.
Number of CQAs: 54

Binary query : ?answer0 <http://confOf#hasSurname> ?answer1 .
Number of matched answers :6
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant> .
Number of matched answers :6
Number of correspondences found: 3
0.4444444444444444 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Co-author>. 
0.4444444444444444 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Co-author>. 
0.4444444444444444 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#Co-author>. 
creating directory: participant

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Camera_Ready_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Registration_of_participants_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Tutorial> .  ?answer0 <http://confOf#hasTopic> ?answer1 .
Number of matched answers :6
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 a <http://confOf#Conference>. ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Reviewing_results_event> . ?o2 <http://confOf#starts_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#earlyRegistration> ?answer1 .
Number of matched answers :6
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#location> ?answer1 .   ?answer0 a <http://confOf#Conference> .
Number of matched answers :5
Number of correspondences found: 0

Unary query : ?answer <http://confOf#earlyRegistration> "false"^^<http://www.w3.org/2001/XMLSchema#boolean> .
Number of matched answers :6
Number of correspondences found: 5
0.47058823529411764 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#Administrator>. 
0.47058823529411764 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#Administrator>. 
creating directory: late_registered_participant

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Submission_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#University> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#reviewes> ?answer1 .   ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .
Number of matched answers :6
Number of correspondences found: 2
2.375 <-> ?answer0 a <http://cmt#Reviewer>.  ?answer0 <http://cmt#hasBeenAssigned> ?answer1.  
2.375 <-> ?answer0 a <http://cmt#Reviewer>.  ?answer1 <http://cmt#assignedTo> ?answer0.  
creating directory: is_assigned_reviewer_of

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Administrator> .
Number of matched answers :5
Number of correspondences found: 3
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Administrator>. 
1.0091787150620264 <-> ?x <http://cmt#assignedByAdministrator> ?answer. ?x a <http://cmt#Co-author>. 
1.141304347826087 <-> ?answer <http://cmt#addProgramCommitteeMember> ?y. ?y a <http://cmt#Administrator>. 
creating directory: administrator

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Camera_Ready_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> .
Number of matched answers :5
Number of correspondences found: 10
0.625 <-> ?answer <http://cmt#paperAssignmentFinalizedBy> ?y. ?y a <http://cmt#ConferenceMember>. 
0.625 <-> ?answer <http://cmt#paperAssignmentToolsRunBy> ?y. ?y a <http://cmt#ConferenceMember>. 
0.625 <-> ?answer <http://cmt#virtualMeetingEnabledBy> ?y. ?y a <http://cmt#ConferenceMember>. 
0.625 <-> ?answer <http://cmt#detailsEnteredBy> ?y. ?y a <http://cmt#ConferenceMember>. 
0.6912878787878788 <-> ?x <http://cmt#runPaperAssignmentTools> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.7007575757575758 <-> ?x <http://cmt#startReviewerBidding> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.7765151515151515 <-> ?x <http://cmt#enterConferenceDetails> ?answer. ?x a <http://cmt#ConferenceMember>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Conference>. 
1.1518782744812295 <-> ?answer <http://cmt#hasConferenceMember> ?y. ?y a <http://cmt#ConferenceMember>. 
1.1811516504926347 <-> ?x <http://cmt#memberOfConference> ?answer. ?x a <http://cmt#ConferenceMember>. 
creating directory: conference

Binary query : ?answer0 <http://confOf#hasFirstName> ?o1 .  ?answer0 <http://confOf#hasSurname> ?o2 .   BIND( concat(STR(?o1)," ", STR(?o2)) as ?answer1)
Number of matched answers :6
Number of correspondences found: 1
0.9 <-> ?answer0 <http://cmt#name> ?answer1.  
creating directory: person_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person> .
Number of matched answers :6
Number of correspondences found: 9
1.0 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#Person>. 
1.0 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#Person>. 
1.0 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Person>. 
1.0 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#Person>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person>. 
1.0 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#Person>. 
1.0 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#Person>. 
1.0 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Person>. 
1.0 <-> ?answer <http://cmt#addedBy> ?y. ?y a <http://cmt#Person>. 
creating directory: person

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#starts_on> ?answer1 .   ?answer0 a <http://confOf#Conference> .
Number of matched answers :5
Number of correspondences found: 1
1.9 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#date> ?answer1.  
creating directory: conference_start_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Tutorial> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://confOf#earlyRegistration> "true"^^<http://www.w3.org/2001/XMLSchema#boolean> .
Number of matched answers :6
Number of correspondences found: 6
0.47058823529411764 <-> ?answer <http://cmt#addedBy> ?y. ?y a <http://cmt#Administrator>. 
0.47058823529411764 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#Administrator>. 
creating directory: early_registered_participant

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#has_title> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.9 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#name> ?answer1.  
creating directory: conference_name

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .
Number of matched answers :6
Number of correspondences found: 6
0.6153846153846154 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Meta-Reviewer>. 
0.75 <-> ?x <http://cmt#writtenBy> ?answer. ?x a <http://cmt#Review>. 
0.75 <-> ?answer <http://cmt#writeReview> ?y. ?y a <http://cmt#Review>. 
0.9444444444444444 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#ExternalReviewer>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer>. 
1.053781512605042 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#ExternalReviewer>. 
creating directory: pc_member

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Submission_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#hasTopic> ?answer1 .  ?answer0 a  <http://confOf#Conference> .
Number of matched answers :6
Number of correspondences found: 7
2.525 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#detailsEnteredBy> ?v1.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#acceptPaper> ?v2.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
2.525 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#detailsEnteredBy> ?v1.  ?v1 a <http://cmt#ConferenceMember>.  ?v2 <http://cmt#rejectedBy> ?v1.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
2.525 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#detailsEnteredBy> ?v1.  ?v1 a <http://cmt#ConferenceMember>.  ?v2 <http://cmt#acceptedBy> ?v1.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
3.0805555555555557 <-> ?answer0 a <http://cmt#Conference>.  ?v1 <http://cmt#memberOfConference> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#hasBeenAssigned> ?v2.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
3.0805555555555557 <-> ?answer0 a <http://cmt#Conference>.  ?v1 <http://cmt#memberOfConference> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#co-writePaper> ?v2.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
3.0805555555555557 <-> ?answer0 a <http://cmt#Conference>.  ?v1 <http://cmt#memberOfConference> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v2 <http://cmt#hasCo-author> ?v1.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
3.0805555555555557 <-> ?answer0 a <http://cmt#Conference>.  ?v1 <http://cmt#memberOfConference> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v2 <http://cmt#assignedTo> ?v1.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
creating directory: conference_topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .  ?answer <http://confOf#reviewes> ?o2 .
Number of matched answers :6
Number of correspondences found: 11
0.8961397058823529 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#AuthorNotReviewer>. 
0.9227941176470587 <-> ?answer <http://cmt#addedBy> ?y. ?y a <http://cmt#AuthorNotReviewer>. 
0.9237132352941176 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#AuthorNotReviewer>. 
0.9375 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#ExternalReviewer>. 
1.071428571428571 <-> ?someSubject <http://cmt#readByReviewer> ?answer. 
1.1538461538461537 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Meta-Reviewer>. 
1.3819444444444444 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#ExternalReviewer>. 
1.5 <-> ?x <http://cmt#writtenBy> ?answer. ?x a <http://cmt#Review>. 
1.5 <-> ?answer <http://cmt#writeReview> ?y. ?y a <http://cmt#Review>. 
1.5850840336134453 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#ExternalReviewer>. 
1.875 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer>. 
creating directory: assigned_reviewer

Binary query : ?answer0 <http://confOf#writes> ?answer1 .
Number of matched answers :6
Number of correspondences found: 4
0.5 <-> ?answer1 <http://cmt#hasAuthor> ?answer0.  
0.5 <-> ?answer0 <http://cmt#co-writePaper> ?answer1.  
0.5 <-> ?answer1 <http://cmt#hasCo-author> ?answer0.  
1.0 <-> ?answer0 <http://cmt#writePaper> ?answer1.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Topic> .
Number of matched answers :6
Number of correspondences found: 2
0.0 <-> ?someSubject <http://cmt#hasSubjectArea> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#SubjectArea>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar> .
Number of matched answers :6
Number of correspondences found: 5
0.4444444444444444 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Co-author>. 
0.4444444444444444 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Co-author>. 
creating directory: student

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#dealsWith> ?answer1 .
Number of matched answers :6
Number of correspondences found: 1
0.5 <-> ?answer0 <http://cmt#hasSubjectArea> ?answer1.  
creating directory: topic_of_paper

Binary query : ?answer0 <http://confOf#hasFirstName> ?answer1 .
Number of matched answers :6
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .  ?answer0 <http://confOf#hasTopic> ?answer1 .
Number of matched answers :6
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author> .
Number of matched answers :6
Number of correspondences found: 9
0.6666666666666666 <-> ?someSubject <http://cmt#hasAuthor> ?answer. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Author>. 
1.0 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#Author>. 
1.0 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#Author>. 
1.1111111111111112 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Co-author>. 
1.1111111111111112 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#Co-author>. 
1.1111111111111112 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Co-author>. 
1.1111111111111112 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Co-author>. 
1.1111111111111112 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#Co-author>. 
creating directory: author

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Company> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Reviewing_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Contribution> .
Number of matched answers :6
Number of correspondences found: 16
0.8611111111111109 <-> ?x <http://cmt#markConflictOfInterest> ?answer. ?x a <http://cmt#Co-author>. 
0.861111111111111 <-> ?x <http://cmt#readPaper> ?answer. ?x a <http://cmt#Co-author>. 
0.861111111111111 <-> ?answer <http://cmt#assignedTo> ?y. ?y a <http://cmt#Co-author>. 
0.861111111111111 <-> ?answer <http://cmt#hasCo-author> ?y. ?y a <http://cmt#Co-author>. 
0.861111111111111 <-> ?x <http://cmt#co-writePaper> ?answer. ?x a <http://cmt#Co-author>. 
0.861111111111111 <-> ?x <http://cmt#submitPaper> ?answer. ?x a <http://cmt#Co-author>. 
0.861111111111111 <-> ?answer <http://cmt#readByMeta-Reviewer> ?y. ?y a <http://cmt#Co-author>. 
0.861111111111111 <-> ?x <http://cmt#hasBeenAssigned> ?answer. ?x a <http://cmt#Co-author>. 
0.861111111111111 <-> ?answer <http://cmt#rejectedBy> ?y. ?y a <http://cmt#Co-author>. 
0.861111111111111 <-> ?answer <http://cmt#readByReviewer> ?y. ?y a <http://cmt#Co-author>. 
0.861111111111111 <-> ?x <http://cmt#writePaper> ?answer. ?x a <http://cmt#Co-author>. 
0.861111111111111 <-> ?answer <http://cmt#hasAuthor> ?y. ?y a <http://cmt#Co-author>. 
0.861111111111111 <-> ?x <http://cmt#rejectPaper> ?answer. ?x a <http://cmt#Co-author>. 
0.861111111111111 <-> ?x <http://cmt#acceptPaper> ?answer. ?x a <http://cmt#Co-author>. 
0.861111111111111 <-> ?answer <http://cmt#acceptedBy> ?y. ?y a <http://cmt#Co-author>. 
1.076923076923077 <-> ?answer <http://cmt#hasSubjectArea> <http://cmt-instances#topic1932752118>. 
creating directory: paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Chair_PC> .
Number of matched answers :5
Number of correspondences found: 1
0.625 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Chairman>. 
creating directory: pc_chair

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Paper> .
Number of matched answers :6
Number of correspondences found: 7
0.7142857142857143 <-> ?answer <http://cmt#paperID> ?someObject. 
0.8545454545454546 <-> ?x <http://cmt#acceptPaper> ?answer. ?x a <http://cmt#User>. 
0.8767676767676769 <-> ?x <http://cmt#submitPaper> ?answer. ?x a <http://cmt#User>. 
0.8901010101010101 <-> ?x <http://cmt#rejectPaper> ?answer. ?x a <http://cmt#Co-author>. 
0.925925925925926 <-> ?x <http://cmt#writePaper> ?answer. ?x a <http://cmt#User>. 
0.9663569698657417 <-> ?x <http://cmt#readPaper> ?answer. ?x a <http://cmt#Co-author>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Paper>. 
creating directory: regular_paper

Binary query : ?answer0 <http://confOf#hasTitle> ?answer1 .
Number of matched answers :6
Number of correspondences found: 1
1.125 <-> ?answer0 <http://cmt#title> ?answer1.  
creating directory: paper_title

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .
Number of matched answers :0
Number of correspondences found: 0
Matching process ended
