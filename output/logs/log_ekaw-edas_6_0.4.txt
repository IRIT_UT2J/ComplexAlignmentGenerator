===============================================================================
creating directory: ekaw-edas
Running with 6 support instances - 0.4 similarity.
Number of CQAs: 65

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Proceedings_Publisher> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Rejected_Paper> .
Number of matched answers :6
Number of correspondences found: 1
0.9285714285714286 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#RejectedPaper>. 
creating directory: rejected_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Track> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Participant> .   {?answer a <http://ekaw#Early-Registered_Participant> . } union{ ?answer a <http://ekaw#Late-Registered_Participant> . }
Number of matched answers :6
Number of correspondences found: 3
0.4545454545454546 <-> <http://edas-instances#conference-104081973> <http://edas#hasMember> ?answer. 
0.4545454545454546 <-> ?answer <http://edas#isMemberOf> ?someObject. 
creating directory: participant

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#OC_Member> .
Number of matched answers :6
Number of correspondences found: 1
0.6666666666666669 <-> ?someSubject <http://edas#hasMember> ?answer. 
creating directory: oc_member

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Proceedings> .   ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer1 <http://ekaw#hasEvent> ?answer0 .  ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .  ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop> .
Number of matched answers :6
Number of correspondences found: 0

Binary query : ?answer0 <http://ekaw#reviewOfPaper> ?answer1 .
Number of matched answers :6
Number of correspondences found: 16
0.5 <-> ?v1 <http://edas#hasRelatedDocument> ?answer0.  ?answer1 <http://edas#relatesTo> ?v1.  
0.5 <-> ?v1 <http://edas#hasRelatedDocument> ?answer0.  ?v1 <http://edas#hasRelatedDocument> ?answer1.  
0.5 <-> ?answer0 <http://edas#relatesTo> ?v1.  ?v1 <http://edas#hasRelatedDocument> ?answer1.  
1.0384615384615383 <-> ?v1 <http://edas#hasRelatedDocument> ?answer0.  ?answer1 <http://edas#relatesTo> ?v1.  ?answer1 a <http://edas#RejectedPaper>.  
1.1153846153846152 <-> ?v1 <http://edas#hasRelatedDocument> ?answer0.  ?v1 a <http://edas#Reviewer>.  ?v1 <http://edas#isReviewing> ?answer1.  
1.1153846153846152 <-> ?answer0 <http://edas#relatesTo> ?v1.  ?v1 a <http://edas#Reviewer>.  ?v1 <http://edas#isReviewing> ?answer1.  
1.1153846153846154 <-> ?answer0 <http://edas#relatesTo> ?v1.  ?v1 a <http://edas#Reviewer>.  ?v1 <http://edas#isMemberOf> ?v2.  ?v2 <http://edas#hasRelatedDocument> ?answer1.  
1.1153846153846154 <-> ?v1 <http://edas#hasRelatedDocument> ?answer0.  ?v1 a <http://edas#Reviewer>.  ?answer1 <http://edas#isReviewedBy> ?v1.  
1.1153846153846154 <-> ?v1 <http://edas#hasRelatedDocument> ?answer0.  ?v1 a <http://edas#Reviewer>.  ?v1 <http://edas#isMemberOf> ?v2.  ?answer1 <http://edas#relatesTo> ?v2.  
1.1153846153846154 <-> ?answer0 <http://edas#relatesTo> ?v1.  ?v1 a <http://edas#Reviewer>.  ?v2 <http://edas#hasMember> ?v1.  ?v2 <http://edas#hasRelatedDocument> ?answer1.  
1.1153846153846154 <-> ?v1 <http://edas#hasRelatedDocument> ?answer0.  ?v1 a <http://edas#Reviewer>.  ?v2 <http://edas#hasMember> ?v1.  ?answer1 <http://edas#relatesTo> ?v2.  
1.1153846153846154 <-> ?v1 <http://edas#hasRelatedDocument> ?answer0.  ?v1 a <http://edas#Reviewer>.  ?v2 <http://edas#hasMember> ?v1.  ?v2 <http://edas#hasRelatedDocument> ?answer1.  
1.1153846153846154 <-> ?v1 <http://edas#hasRelatedDocument> ?answer0.  ?v1 a <http://edas#Reviewer>.  ?v1 <http://edas#isMemberOf> ?v2.  ?v2 <http://edas#hasRelatedDocument> ?answer1.  
1.6538461538461537 <-> ?v1 <http://edas#hasRelatedDocument> ?answer0.  ?v1 a <http://edas#Reviewer>.  ?v1 <http://edas#isReviewing> ?answer1.  ?answer1 a <http://edas#RejectedPaper>.  
1.6538461538461537 <-> ?answer0 <http://edas#relatesTo> ?v1.  ?v1 a <http://edas#Reviewer>.  ?v1 <http://edas#isReviewing> ?answer1.  ?answer1 a <http://edas#RejectedPaper>.  
1.6538461538461537 <-> ?v1 <http://edas#hasRelatedDocument> ?answer0.  ?v1 a <http://edas#Reviewer>.  ?answer1 <http://edas#isReviewedBy> ?v1.  ?answer1 a <http://edas#RejectedPaper>.  
creating directory: review_of_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Tutorial> .  ?answer0 <http://ekaw#coversTopic> ?answer1 .
Number of matched answers :6
Number of correspondences found: 0

Binary query : ?answer0 a <http://ekaw#Conference_Participant>.      bind( if (exists {?answer0 a <http://ekaw#Early-Registered_Participant>}, "true"^^<http://www.w3.org/2001/XMLSchema#boolean>,        if (exists {?answer0 a <http://ekaw#Late-Registered_Participant>}, "false"^^<http://www.w3.org/2001/XMLSchema#boolean>,"")) as ?answer1) filter(?answer1 != "")
Number of matched answers :6
Number of correspondences found: 0

Binary query : ?answer1 <http://ekaw#hasEvent> ?answer0 .  ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .  ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Tutorial> .
Number of matched answers :6
Number of correspondences found: 0

Binary query : ?answer0 a <http://ekaw#Conference> . ?answer0 <http://ekaw#heldIn> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#location> ?answer1.  
creating directory: conference_location

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Late-Registered_Participant> .
Number of matched answers :6
Number of correspondences found: 5
0.0 <-> <http://edas-instances#review-554085786002198_-196548214> <http://edas#relatesTo> ?answer. 
0.0 <-> ?someSubject <http://edas#isWrittenBy> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Attendee>. 
0.0 <-> ?answer <http://edas#isReviewing> <http://edas-instances#paper-9719043614511114>. 
0.0 <-> ?answer <http://edas#hasRelatedPaper> <http://edas-instances#paper10080176053721114>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#University> .
Number of matched answers :6
Number of correspondences found: 2
0.45 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> ?someObject. 
creating directory: university

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Trip> .
Number of matched answers :5
Number of correspondences found: 1
0.6666666666666667 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceEvent>. 
creating directory: trip

Binary query : ?answer0 a <http://ekaw#Invited_Speaker>. ?answer1 <http://ekaw#writtenBy> ?answer0. ?answer1 a <http://ekaw#Invited_Talk_Abstract>.
Number of matched answers :6
Number of correspondences found: 0

Binary query : ?answer0 a <http://ekaw#Invited_Speaker>. ?o2 <http://ekaw#writtenBy> ?answer0. ?o2 <http://ekaw#paperPresentedAs> ?answer1. ?answer1 a <http://ekaw#Invited_Talk>.
Number of matched answers :6
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member> .   ?answer0 <http://ekaw#reviewerOfPaper> ?answer1 .
Number of matched answers :6
Number of correspondences found: 4
1.0333333333333332 <-> ?answer0 a <http://edas#Reviewer>.  ?answer0 <http://edas#isReviewing> ?answer1.  
1.0333333333333332 <-> ?answer0 a <http://edas#Reviewer>.  ?answer1 <http://edas#isReviewedBy> ?answer0.  
1.5666666666666667 <-> ?answer0 a <http://edas#Reviewer>.  ?answer0 <http://edas#isReviewing> ?answer1.  ?answer1 a <http://edas#RejectedPaper>.  
1.5666666666666667 <-> ?answer0 a <http://edas#Reviewer>.  ?answer1 <http://edas#isReviewedBy> ?answer0.  ?answer1 a <http://edas#RejectedPaper>.  
creating directory: is_assigned_reviewer_of

Unary query : ?o3 <http://ekaw#reviewWrittenBy> ?answer. MINUS{ ?answer <http://ekaw#reviewerOfPaper> ?o2. }
Number of matched answers :6
Number of correspondences found: 7
0.8 <-> ?x <http://edas#relatesTo> ?answer. ?x a <http://edas#Review>. 
0.8 <-> ?answer <http://edas#hasRelatedDocument> ?y. ?y a <http://edas#Review>. 
0.8222222222222221 <-> ?answer <http://edas#isReviewing> ?y. ?y a <http://edas#RejectedPaper>. 
0.8614583333333334 <-> ?answer <http://edas#hasRelatedPaper> ?y. ?y a <http://edas#ActivePaper>. 
0.9 <-> ?x <http://edas#isReviewedBy> ?answer. ?x a <http://edas#AcceptedPaper>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reviewer>. 
1.1379337231968807 <-> ?x <http://edas#isWrittenBy> ?answer. ?x a <http://edas#PublishedPaper>. 
creating directory: external_reviewer

Binary query : ?answer1 <http://ekaw#reviewWrittenBy> ?answer0 .   ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member> .  ?answer0 <http://ekaw#reviewerOfPaper> ?o3.
Number of matched answers :6
Number of correspondences found: 2
2.3000000000000003 <-> ?answer0 a <http://edas#Reviewer>.  ?answer1 <http://edas#relatesTo> ?answer0.  ?answer1 a <http://edas#Review>.  
2.3000000000000003 <-> ?answer0 a <http://edas#Reviewer>.  ?answer0 <http://edas#hasRelatedDocument> ?answer1.  ?answer1 a <http://edas#Review>.  
creating directory: assigned_reviewer_author_of_review

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Track> .   ?answer1 <http://ekaw#hasEvent> ?answer0 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .
Number of matched answers :6
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop_Chair> .   ?answer0 <http://ekaw#organises> ?answer1 . ?answer1 a <http://ekaw#Workshop> .
Number of matched answers :6
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .
Number of matched answers :5
Number of correspondences found: 4
0.6444444444444445 <-> ?x <http://edas#isMemberOf> ?answer. ?x a <http://edas#ConferenceChair>. 
0.6666666666666667 <-> ?answer <http://edas#hasMember> ?y. ?y a <http://edas#ConferenceChair>. 
0.9699074074074074 <-> ?answer <http://edas#hasTopic> <http://edas-instances#topic1677946970>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Conference>. 
creating directory: conference

Binary query : ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .   ?answer0 a <http://ekaw#Person> .
Number of matched answers :6
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://edas#Person>.  ?answer0 <http://edas#hasLastName> ?answer1.  
creating directory: person_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Person> .
Number of matched answers :6
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 
creating directory: person

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Banquet> .   ?answer0 <http://ekaw#heldIn> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :5
Number of correspondences found: 2
2.716666666666667 <-> ?answer0 a <http://edas#ConferenceDinner>.  ?v1 <http://edas#isLocationOf> ?answer0.  ?v1 a <http://edas#ConferenceVenuePlace>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
2.716666666666667 <-> ?answer0 a <http://edas#ConferenceDinner>.  ?answer0 <http://edas#hasLocation> ?v1.  ?v1 a <http://edas#ConferenceVenuePlace>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: gala_dinner_location

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Proceedings> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer1 <http://ekaw#publisherOf> ?answer0 .  ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Proceedings> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?o3 <http://ekaw#reviewWrittenBy> ?answer0 .   ?o3 <http://ekaw#reviewWrittenBy> ?answer1 . ?answer1 <http://ekaw#reviewerOfPaper> ?o2 . ?o3 <http://ekaw#reviewOfPaper> ?o2 . FILTER(?o!=?answer0)
Number of matched answers :6
Number of correspondences found: 6
3.7307692307692304 <-> ?answer0 a <http://edas#Reviewer>.  ?v1 <http://edas#hasMember> ?answer0.  ?answer1 <http://edas#isMemberOf> ?v1.  ?answer1 a <http://edas#Reviewer>.  
3.730769230769231 <-> ?answer0 a <http://edas#Reviewer>.  ?answer0 <http://edas#isMemberOf> ?v1.  ?v1 <http://edas#hasMember> ?answer1.  ?answer1 a <http://edas#Reviewer>.  
4.664102564102564 <-> ?answer0 a <http://edas#Reviewer>.  ?v1 <http://edas#isReviewedBy> ?answer0.  ?v1 <http://edas#isReviewedBy> ?answer1.  ?answer1 a <http://edas#Reviewer>.  
4.992307692307692 <-> ?answer0 a <http://edas#Reviewer>.  ?v1 <http://edas#relatesTo> ?answer0.  ?v1 a <http://edas#Review>.  ?v1 <http://edas#relatesTo> ?answer1.  ?answer1 a <http://edas#Reviewer>.  
5.735897435897437 <-> ?answer0 a <http://edas#Reviewer>.  ?v1 <http://edas#isReviewedBy> ?answer0.  ?v1 a <http://edas#RejectedPaper>.  ?v1 <http://edas#isReviewedBy> ?answer1.  ?answer1 a <http://edas#Reviewer>.  
5.858974358974359 <-> ?answer0 a <http://edas#Reviewer>.  ?answer0 <http://edas#hasRelatedPaper> ?v1.  ?v1 a <http://edas#RatedPapers>.  ?v1 <http://edas#isReviewedBy> ?answer1.  ?answer1 a <http://edas#Reviewer>.  
creating directory: invite_reviewer

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Tutorial> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Early-Registered_Participant> .
Number of matched answers :6
Number of correspondences found: 6
0.0 <-> ?answer <http://edas#hasLastName> "Turnbull". 
0.0 <-> ?answer <http://edas#hasFirstName> "Ann". 
0.0 <-> ?answer <http://edas#hasRelatedDocument> <http://edas-instances#review20518448391311_-196548215>. 
0.0 <-> ?answer <http://edas#hasRelatedPaper> <http://edas-instances#paper-9336392552072198>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 
0.0 <-> ?answer <http://edas#isReviewing> <http://edas-instances#paper-12358590847852198>. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .  ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
2.0 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#hasName> ?answer1.  
creating directory: conference_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member> .   ?answer <http://ekaw#reviewerOfPaper> ?o2.
Number of matched answers :6
Number of correspondences found: 2
0.6666666666666669 <-> ?someSubject <http://edas#hasMember> ?answer. 
0.85167081956098 <-> ?answer <http://edas#hasRelatedPaper> ?y. ?y a <http://edas#ActivePaper>. 
creating directory: pc_member

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop> .
Number of matched answers :6
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Workshop>. 
creating directory: workshop

Binary query : ?answer0 <http://ekaw#coversTopic> ?answer1 .   ?answer0 a <http://ekaw#Conference> .
Number of matched answers :6
Number of correspondences found: 2
1.5 <-> ?answer0 a <http://edas#Conference>.  ?answer1 <http://edas#isTopicOf> ?answer0.  
2.0454545454545454 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#hasTopic> ?answer1.  
creating directory: conference_topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member> .   ?answer <http://ekaw#reviewerOfPaper> ?o3.
Number of matched answers :6
Number of correspondences found: 2
0.6666666666666669 <-> ?someSubject <http://edas#hasMember> ?answer. 
0.85167081956098 <-> ?answer <http://edas#hasRelatedPaper> ?y. ?y a <http://edas#ActivePaper>. 
creating directory: assigned_reviewer

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Accepted_Paper> .
Number of matched answers :6
Number of correspondences found: 2
0.6000000000000006 <-> ?someSubject <http://edas#hasRelatedPaper> ?answer. 
0.9285714285714286 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#AcceptedPaper>. 
creating directory: accepted_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Web_Site> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop_Chair> .
Number of matched answers :6
Number of correspondences found: 4
0.0 <-> ?answer <http://edas#isReviewing> ?someObject. 
0.0 <-> ?answer <http://edas#hasRelatedPaper> <http://edas-instances#paper10080176053721114>. 
0.0 <-> <http://edas-instances#paper-15765906475922198_v2> <http://edas#isWrittenBy> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Attendee>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Invited_Talk_Abstract> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Invited_Speaker> .
Number of matched answers :6
Number of correspondences found: 4
0.4 <-> ?x <http://edas#isReviewedBy> ?answer. ?x a <http://edas#AcceptedPaper>. 
0.4 <-> ?answer <http://edas#hasRelatedPaper> ?y. ?y a <http://edas#RatedPapers>. 
0.4 <-> ?x <http://edas#isWrittenBy> ?answer. ?x a <http://edas#RatedPapers>. 
0.4 <-> ?answer <http://edas#isReviewing> ?y. ?y a <http://edas#RatedPapers>. 
creating directory: invited_speaker

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper_Author> . ?answer0 <http://ekaw#authorOf> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .
Number of matched answers :6
Number of correspondences found: 2
3.166666666666666 <-> ?answer0 a <http://edas#Author>.  ?answer0 <http://edas#hasRelatedPaper> ?answer1.  ?answer1 a <http://edas#Paper>.  
3.166666666666666 <-> ?answer0 a <http://edas#Author>.  ?answer1 <http://edas#isWrittenBy> ?answer0.  ?answer1 a <http://edas#Paper>.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Trip> .   ?answer0 <http://ekaw#heldIn> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :5
Number of correspondences found: 2
2.716666666666667 <-> ?answer0 a <http://edas#ConferenceEvent>.  ?v1 <http://edas#isLocationOf> ?answer0.  ?v1 a <http://edas#ConferenceVenuePlace>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
2.716666666666667 <-> ?answer0 a <http://edas#ConferenceEvent>.  ?answer0 <http://edas#hasLocation> ?v1.  ?v1 a <http://edas#ConferenceVenuePlace>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: trip_location

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Research_Topic> .
Number of matched answers :6
Number of correspondences found: 1
0.42857142857142866 <-> ?someSubject <http://edas#hasTopic> ?answer. 
creating directory: topic

Binary query : ?answer0 a <http://ekaw#Invited_Talk_Abstract>. ?answer0 <http://ekaw#paperPresentedAs> ?answer1. ?answer1 a <http://ekaw#Invited_Talk>.
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Student> .
Number of matched answers :6
Number of correspondences found: 4
0.5 <-> ?answer <http://edas#isReviewing> ?y. ?y a <http://edas#Document>. 
0.5 <-> ?x <http://edas#isWrittenBy> ?answer. ?x a <http://edas#Document>. 
0.5 <-> ?answer <http://edas#hasRelatedPaper> ?y. ?y a <http://edas#Document>. 
creating directory: student

Unary query : ?o2 a <http://ekaw#Web_Site>. ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer. filter(regex(?s,"^http"))
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#SC_Member> .
Number of matched answers :6
Number of correspondences found: 1
0.6666666666666669 <-> ?someSubject <http://edas#hasMember> ?answer. 
creating directory: sc_member

Binary query : ?answer1 <http://ekaw#reviewWrittenBy> ?answer0 .
Number of matched answers :6
Number of correspondences found: 2
0.5 <-> ?answer0 <http://edas#hasRelatedDocument> ?answer1.  
0.5 <-> ?answer1 <http://edas#relatesTo> ?answer0.  
creating directory: all_reviewer_author_of_review

Unary query : {?answer <http://ekaw#reviewerOfPaper> ?o2 . } union{ ?o3 <http://ekaw#reviewOfPaper> ?o2 .  ?o3 <http://ekaw#reviewWrittenBy> ?answer .  }
Number of matched answers :6
Number of correspondences found: 7
1.1433934311406164 <-> ?answer <http://edas#isReviewing> ?y. ?y a <http://edas#RejectedPaper>. 
1.2615384615384615 <-> ?answer <http://edas#hasRelatedDocument> ?y. ?y a <http://edas#Review>. 
1.2615384615384615 <-> ?x <http://edas#relatesTo> ?answer. ?x a <http://edas#Review>. 
1.4190816701260827 <-> ?x <http://edas#isReviewedBy> ?answer. ?x a <http://edas#AcceptedPaper>. 
1.4431949003377575 <-> ?x <http://edas#isWrittenBy> ?answer. ?x a <http://edas#ActivePaper>. 
1.5661501850669826 <-> ?answer <http://edas#hasRelatedPaper> ?y. ?y a <http://edas#ActivePaper>. 
1.6153846153846154 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reviewer>. 
creating directory: reviewers_all

Binary query : ?answer0 <http://ekaw#coversTopic> ?answer1 . ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .
Number of matched answers :6
Number of correspondences found: 2
1.5 <-> ?answer0 a <http://edas#Paper>.  ?answer1 <http://edas#isTopicOf> ?answer0.  
2.0454545454545463 <-> ?answer0 a <http://edas#Paper>.  ?answer0 <http://edas#hasTopic> ?answer1.  
creating directory: topic_of_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Review> .
Number of matched answers :6
Number of correspondences found: 3
0.75 <-> ?x <http://edas#hasRelatedDocument> ?answer. ?x a <http://edas#Reviewer>. 
0.75 <-> ?answer <http://edas#relatesTo> ?y. ?y a <http://edas#Reviewer>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Review>. 
creating directory: review

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop> .  ?answer0 <http://ekaw#coversTopic> ?answer1 .
Number of matched answers :6
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Banquet> .
Number of matched answers :5
Number of correspondences found: 2
0.6666666666666667 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceEvent>. 
0.6666666666666667 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceDinner>. 
creating directory: gala_dinner

Binary query : ?answer1 a <http://ekaw#Conference> . ?o2 <http://ekaw#listsEvent> ?answer1 . ?o2 a <http://ekaw#Web_Site> . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer0 . filter(regex(?s,"^http"))
Number of matched answers :5
Number of correspondences found: 0

Unary query : ?answer a <http://ekaw#Invited_Talk> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper_Author> .
Number of matched answers :6
Number of correspondences found: 1
0.5 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Author>. 
creating directory: author

Binary query : ?answer0 <http://ekaw#presentationOfPaper> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .
Number of matched answers :6
Number of correspondences found: 9
1.5 <-> ?answer0 <http://edas#hasRelatedDocument> ?v1.  ?v1 <http://edas#relatesTo> ?v2.  ?v2 <http://edas#hasRelatedDocument> ?answer1.  ?answer1 a <http://edas#Paper>.  
1.5 <-> ?answer1 <http://edas#relatedToEvent> ?answer0.  ?answer1 a <http://edas#Paper>.  
1.9210526315789471 <-> ?answer0 <http://edas#hasRelatedDocument> ?v1.  ?v1 <http://edas#relatesTo> ?v2.  ?v2 <http://edas#hasRelatedPaper> ?answer1.  ?answer1 a <http://edas#Paper>.  
2.0263157894736845 <-> ?answer0 <http://edas#relatedToPaper> ?answer1.  ?answer1 a <http://edas#Paper>.  
2.5 <-> ?v1 <http://edas#relatedToEvent> ?answer0.  ?v1 a <http://edas#Paper>.  ?v1 <http://edas#relatesTo> ?v2.  ?v2 <http://edas#hasRelatedDocument> ?answer1.  ?answer1 a <http://edas#Paper>.  
2.5 <-> ?v1 <http://edas#relatedToEvent> ?answer0.  ?v1 a <http://edas#Paper>.  ?v2 <http://edas#isTopicOf> ?v1.  ?v2 <http://edas#isTopicOf> ?answer1.  ?answer1 a <http://edas#Paper>.  
2.5 <-> ?v1 <http://edas#relatedToEvent> ?answer0.  ?v1 a <http://edas#Paper>.  ?v2 <http://edas#hasRelatedDocument> ?v1.  ?answer1 <http://edas#relatesTo> ?v2.  ?answer1 a <http://edas#Paper>.  
2.5 <-> ?v1 <http://edas#relatedToEvent> ?answer0.  ?v1 a <http://edas#Paper>.  ?v2 <http://edas#isReviewing> ?v1.  ?v2 <http://edas#isReviewing> ?answer1.  ?answer1 a <http://edas#Paper>.  
3.3421052631578947 <-> ?v1 <http://edas#relatedToEvent> ?answer0.  ?v1 a <http://edas#Paper>.  ?v2 <http://edas#hasRelatedPaper> ?v1.  ?v2 <http://edas#hasRelatedPaper> ?answer1.  ?answer1 a <http://edas#Paper>.  
creating directory: presentation_of_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#OC_Chair> .
Number of matched answers :5
Number of correspondences found: 4
0.0 <-> <http://edas-instances#paper-1921869894163257> <http://edas#isReviewedBy> ?answer. 
0.0 <-> <http://edas-instances#review-554085786002198_-196548214> <http://edas#relatesTo> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Author>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .
Number of matched answers :6
Number of correspondences found: 5
0.6 <-> ?someSubject <http://edas#isReviewing> ?answer. 
0.6 <-> ?answer <http://edas#isWrittenBy> ?someObject. 
0.6 <-> ?someSubject <http://edas#hasRelatedPaper> ?answer. 
0.6 <-> ?answer <http://edas#isReviewedBy> ?someObject. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Paper>. 
creating directory: paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Chair> .
Number of matched answers :5
Number of correspondences found: 5
0.0 <-> ?answer <http://edas#isReviewing> <http://edas-instances#paper-253329908202257_v0>. 
0.0 <-> <http://edas-instances#paper14883166801311263_v0> <http://edas#isReviewedBy> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Presenter>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Attendee>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Regular_Paper> .
Number of matched answers :6
Number of correspondences found: 2
0.5384615384615384 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#RejectedPaper>. 
creating directory: regular_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .   ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.
Number of matched answers :6
Number of correspondences found: 1
3.299999999999999 <-> ?answer0 a <http://edas#Paper>.  ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: paper_title

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Contributed_Talk> .
Number of matched answers :6
Number of correspondences found: 4
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#PaperPresentation>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceEvent>. 
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> "Presentation of A microscopic calculation of potentials and mass parameters for heavy-ion reactions". 
0.0 <-> ?answer <http://edas#relatedToPaper> <http://edas-instances#paper14160164054951114>. 

Binary query : ?answer0 a <http://ekaw#Contributed_Talk>. ?answer1 a <http://ekaw#Conference> . ?answer0 <http://ekaw#partOfEvent> ?answer1.
Number of matched answers :6
Number of correspondences found: 3
2.566666666666667 <-> ?answer0 a <http://edas#ConferenceEvent>.  ?answer0 <http://edas#hasRelatedDocument> ?v1.  ?v1 <http://edas#relatesTo> ?answer1.  ?answer1 a <http://edas#Conference>.  
3.066666666666667 <-> ?answer0 a <http://edas#ConferenceEvent>.  ?v1 <http://edas#relatedToEvent> ?answer0.  ?answer1 <http://edas#hasRelatedDocument> ?v1.  ?answer1 a <http://edas#Conference>.  
3.066666666666667 <-> ?answer0 a <http://edas#ConferenceEvent>.  ?v1 <http://edas#relatedToEvent> ?answer0.  ?v1 <http://edas#relatesTo> ?answer1.  ?answer1 a <http://edas#Conference>.  
creating directory: presentation_in_conference
Matching process ended
