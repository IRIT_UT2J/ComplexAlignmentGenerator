===============================================================================
creating directory: confOf-ekaw
Running with 2 support instances - 0.4 similarity.
Number of CQAs: 54

Binary query : ?answer0 <http://confOf#hasSurname> ?answer1 .
Number of matched answers :2
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant> .
Number of matched answers :2
Number of correspondences found: 1
0.5 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Participant>. 
creating directory: participant

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Camera_Ready_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :2
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Registration_of_participants_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :2
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :2
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Tutorial> .  ?answer0 <http://confOf#hasTopic> ?answer1 .
Number of matched answers :2
Number of correspondences found: 2
1.5 <-> ?answer0 a <http://ekaw#Tutorial>.  ?answer1 <http://ekaw#topicCoveredBy> ?answer0.  
2.0454545454545454 <-> ?answer0 a <http://ekaw#Tutorial>.  ?answer0 <http://ekaw#coversTopic> ?answer1.  
creating directory: tutorial_topic

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :2
Number of correspondences found: 0

Binary query : ?answer0 a <http://confOf#Conference>. ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Reviewing_results_event> . ?o2 <http://confOf#starts_on> ?answer1 .
Number of matched answers :2
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#earlyRegistration> ?answer1 .
Number of matched answers :2
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#location> ?answer1 .   ?answer0 a <http://confOf#Conference> .
Number of matched answers :2
Number of correspondences found: 2
2.9 <-> ?answer0 a <http://ekaw#Conference>.  ?answer0 <http://ekaw#heldIn> ?v1.  ?v1 a <http://ekaw#Location>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
4.1 <-> ?answer0 a <http://ekaw#Conference>.  ?v1 <http://ekaw#locationOf> ?answer0.  ?v1 a <http://ekaw#Location>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: conference_location

Unary query : ?answer <http://confOf#earlyRegistration> "false"^^<http://www.w3.org/2001/XMLSchema#boolean> .
Number of matched answers :2
Number of correspondences found: 1
0.4444444444444444 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Late-Registered_Participant>. 
creating directory: late_registered_participant

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Submission_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :2
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#University> .
Number of matched answers :2
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#University>. 
creating directory: university

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .
Number of matched answers :2
Number of correspondences found: 2
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Trip>. 
0.0 <-> ?answer <http://ekaw#partOf> <http://ekaw-instances#conference943277065>. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :2
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#reviewes> ?answer1 .   ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .
Number of matched answers :2
Number of correspondences found: 6
2.3823529411764706 <-> ?answer0 a <http://ekaw#Possible_Reviewer>.  ?answer0 <http://ekaw#reviewerOfPaper> ?v1.  ?v1 <http://ekaw#hasUpdatedVersion> ?answer1.  
2.3823529411764706 <-> ?answer0 a <http://ekaw#Possible_Reviewer>.  ?answer0 <http://ekaw#reviewerOfPaper> ?answer1.  
2.3823529411764706 <-> ?answer0 a <http://ekaw#Possible_Reviewer>.  ?answer0 <http://ekaw#reviewerOfPaper> ?v1.  ?answer1 <http://ekaw#updatedVersionOf> ?v1.  
2.7459893048128343 <-> ?answer0 a <http://ekaw#Possible_Reviewer>.  ?v1 <http://ekaw#hasReviewer> ?answer0.  ?v1 <http://ekaw#hasUpdatedVersion> ?answer1.  
2.7459893048128343 <-> ?answer0 a <http://ekaw#Possible_Reviewer>.  ?v1 <http://ekaw#hasReviewer> ?answer0.  ?answer1 <http://ekaw#updatedVersionOf> ?v1.  
2.7459893048128343 <-> ?answer0 a <http://ekaw#Possible_Reviewer>.  ?answer1 <http://ekaw#hasReviewer> ?answer0.  
creating directory: is_assigned_reviewer_of

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Administrator> .
Number of matched answers :2
Number of correspondences found: 2
0.4 <-> ?answer <http://ekaw#organises> ?y. ?y a <http://ekaw#Conference>. 
creating directory: administrator

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Camera_Ready_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :2
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :2
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> .
Number of matched answers :2
Number of correspondences found: 5
0.6666666666666666 <-> ?answer <http://ekaw#hasEvent> ?y. ?y a <http://ekaw#Conference_Trip>. 
0.6666666666666667 <-> ?x <http://ekaw#partOf> ?answer. ?x a <http://ekaw#Conference_Trip>. 
0.6666666666666667 <-> ?x <http://ekaw#partOfEvent> ?answer. ?x a <http://ekaw#Conference_Trip>. 
0.6666666666666667 <-> ?answer <http://ekaw#hasPart> ?y. ?y a <http://ekaw#Conference_Trip>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference>. 
creating directory: conference

Binary query : ?answer0 <http://confOf#hasFirstName> ?o1 .  ?answer0 <http://confOf#hasSurname> ?o2 .   BIND( concat(STR(?o1)," ", STR(?o2)) as ?answer1)
Number of matched answers :2
Number of correspondences found: 1
0.5 <-> ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: person_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person> .
Number of matched answers :2
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Person>. 
creating directory: person

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :2
Number of correspondences found: 2
1.5 <-> ?answer0 <http://ekaw#heldIn> ?v1.  ?v1 a <http://ekaw#Location>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
2.7 <-> ?v1 <http://ekaw#locationOf> ?answer0.  ?v1 a <http://ekaw#Location>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: gala_dinner_location

Binary query : ?answer0 <http://confOf#starts_on> ?answer1 .   ?answer0 a <http://confOf#Conference> .
Number of matched answers :2
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Tutorial> .
Number of matched answers :2
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Tutorial>. 
creating directory: tutorial

Unary query : ?answer <http://confOf#earlyRegistration> "true"^^<http://www.w3.org/2001/XMLSchema#boolean> .
Number of matched answers :2
Number of correspondences found: 1
0.5714285714285714 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Early-Registered_Participant>. 
creating directory: early_registered_participant

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#has_title> ?answer1 .
Number of matched answers :2
Number of correspondences found: 1
1.9 <-> ?answer0 a <http://ekaw#Conference>.  ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: conference_name

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :2
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :2
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .
Number of matched answers :2
Number of correspondences found: 4
0.727272727272727 <-> ?someSubject <http://ekaw#hasReviewer> ?answer. 
0.75 <-> ?answer <http://ekaw#authorOf> ?y. ?y a <http://ekaw#Review>. 
0.9833333333333334 <-> ?x <http://ekaw#writtenBy> ?answer. ?x a <http://ekaw#Review>. 
1.2166666666666668 <-> ?x <http://ekaw#reviewWrittenBy> ?answer. ?x a <http://ekaw#Review>. 
creating directory: pc_member

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .
Number of matched answers :2
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop>. 
creating directory: workshop

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Submission_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :2
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#hasTopic> ?answer1 .  ?answer0 a  <http://confOf#Conference> .
Number of matched answers :2
Number of correspondences found: 2
1.9 <-> ?answer0 a <http://ekaw#Conference>.  ?answer1 <http://ekaw#topicCoveredBy> ?answer0.  
2.4454545454545453 <-> ?answer0 a <http://ekaw#Conference>.  ?answer0 <http://ekaw#coversTopic> ?answer1.  
creating directory: conference_topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .  ?answer <http://confOf#reviewes> ?o2 .
Number of matched answers :2
Number of correspondences found: 6
0.8823529411764706 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Possible_Reviewer>. 
1.0 <-> ?answer <http://ekaw#reviewerOfPaper> ?someObject. 
1.3636363636363635 <-> ?someSubject <http://ekaw#hasReviewer> ?answer. 
1.5 <-> ?answer <http://ekaw#authorOf> ?y. ?y a <http://ekaw#Review>. 
1.9666666666666668 <-> ?x <http://ekaw#writtenBy> ?answer. ?x a <http://ekaw#Review>. 
2.4333333333333336 <-> ?x <http://ekaw#reviewWrittenBy> ?answer. ?x a <http://ekaw#Review>. 
creating directory: assigned_reviewer

Binary query : ?answer0 <http://confOf#writes> ?answer1 .
Number of matched answers :2
Number of correspondences found: 2
0.5 <-> ?answer0 <http://ekaw#authorOf> ?answer1.  
1.0555555555555556 <-> ?answer1 <http://ekaw#writtenBy> ?answer0.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :2
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :2
Number of correspondences found: 2
1.5 <-> ?answer0 <http://ekaw#heldIn> ?v1.  ?v1 a <http://ekaw#Location>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
2.7 <-> ?v1 <http://ekaw#locationOf> ?answer0.  ?v1 a <http://ekaw#Location>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: trip_location

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Topic> .
Number of matched answers :2
Number of correspondences found: 1
0.4545454545454546 <-> ?someSubject <http://ekaw#coversTopic> ?answer. 
creating directory: topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar> .
Number of matched answers :2
Number of correspondences found: 2
0.0 <-> <http://ekaw-instances#review-17609859548142198_-196548214> <http://ekaw#writtenBy> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Participant>. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :2
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :2
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#dealsWith> ?answer1 .
Number of matched answers :2
Number of correspondences found: 2
0.5 <-> ?answer0 <http://ekaw#coversTopic> ?answer1.  
0.5 <-> ?answer1 <http://ekaw#topicCoveredBy> ?answer0.  
creating directory: topic_of_paper

Binary query : ?answer0 <http://confOf#hasFirstName> ?answer1 .
Number of matched answers :2
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .  ?answer0 <http://confOf#hasTopic> ?answer1 .
Number of matched answers :2
Number of correspondences found: 2
1.5 <-> ?answer0 a <http://ekaw#Workshop>.  ?answer1 <http://ekaw#topicCoveredBy> ?answer0.  
2.0454545454545454 <-> ?answer0 a <http://ekaw#Workshop>.  ?answer0 <http://ekaw#coversTopic> ?answer1.  
creating directory: workshop_topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .
Number of matched answers :2
Number of correspondences found: 2
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Banquet>. 
0.0 <-> ?answer <http://ekaw#heldIn> <http://ekaw-instances#location-1925982563>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author> .
Number of matched answers :2
Number of correspondences found: 1
0.75 <-> ?answer <http://ekaw#authorOf> ?someObject. 
creating directory: author

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Company> .
Number of matched answers :2
Number of correspondences found: 2
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> "Australian National University, Canberra". 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Organisation>. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :2
Number of correspondences found: 2
2.0555555555555554 <-> ?answer0 <http://ekaw#heldIn> ?v1.  ?v1 a <http://ekaw#Location>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
3.655555555555556 <-> ?v1 <http://ekaw#locationOf> ?answer0.  ?v1 a <http://ekaw#Location>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: reception_location

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Reviewing_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :2
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Contribution> .
Number of matched answers :2
Number of correspondences found: 2
0.5625 <-> ?answer <http://ekaw#paperPresentedAs> ?y. ?y a <http://ekaw#Contributed_Talk>. 
creating directory: paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Chair_PC> .
Number of matched answers :2
Number of correspondences found: 2
0.0 <-> <http://ekaw-instances#paper-8367213188212198> <http://ekaw#hasReviewer> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Paper> .
Number of matched answers :2
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper>. 
creating directory: regular_paper

Binary query : ?answer0 <http://confOf#hasTitle> ?answer1 .
Number of matched answers :2
Number of correspondences found: 1
0.5 <-> ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: paper_title

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .
Number of matched answers :2
Number of correspondences found: 1
0.9555555555555556 <-> ?x <http://ekaw#locationOf> ?answer. ?x a <http://ekaw#Location>. 
creating directory: reception
Matching process ended
