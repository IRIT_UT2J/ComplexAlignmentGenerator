===============================================================================
Running with 1 support instances - 0.4 similarity.
Number of CQAs: 6

Binary query : ?answer0 a ?answer1. ?answer1 <http://www.w3.org/2000/01/rdf-schema#subClassOf>+ <http://ontology.irstea.fr/agronomictaxon/core#Taxon>.
Number of matched answers :0
Looking for similar answers
[<http://taxref.mnhn.fr/lod/name/522798>, <http://taxref.mnhn.fr/lod/taxon/522798/10.0>, <http://taxref.mnhn.fr/lod/taxon/522798/11.0>] <--> [<http://taxref.mnhn.fr/lod/taxrank/Order>]
Number of similar answers :1
Number of correspondences found: 2
1.373015873015873 <-> ?answer0 <http://taxref.mnhn.fr/lod/property/hasRank> ?answer1.  
2.428571428571429 <-> ?answer0 a <http://rs.tdwg.org/ontology/voc/TaxonName#TaxonName>.  ?answer0 <http://taxref.mnhn.fr/lod/property/hasRank> ?answer1.  

Binary query : ?answer0 <http://ontology.irstea.fr/agronomictaxon/core#prefVernacularName> ?answer1. filter(lang(?answer1)= "en")
[<http://aims.fao.org/aos/agrovoc/c_3354>] <--> []
Number of matched answers :1
No path found, similar answers : [<http://aims.fao.org/aos/agrovoc/c_3354>, <http://taxref.mnhn.fr/lod/taxon/187444/11.0>, <http://taxref.mnhn.fr/lod/name/187444>, <http://taxref.mnhn.fr/lod/taxon/187444/10.0>] <--> []
Number of correspondences found: 0

Unary query : ?answer a <http://ontology.irstea.fr/agronomictaxon/core#GenusRank>.
[<http://aims.fao.org/aos/agrovoc/c_7950>]
Number of matched answers :1
Number of correspondences found: 1
0.0 <-> ?answer <http://www.w3.org/2004/02/skos/core#prefLabel> "تریتیکوم". 

Binary query : ?answer0 <http://ontology.irstea.fr/agronomictaxon/core#hasHigherRank> ?answer1.
[<http://aims.fao.org/aos/agrovoc/c_7950>] <--> [<http://aims.fao.org/aos/agrovoc/c_3354>]
Number of matched answers :1
Number of correspondences found: 1
0.5 <-> ?answer0 <http://www.w3.org/2004/02/skos/core#broader> ?answer1.  

Unary query : ?answer a <http://ontology.irstea.fr/agronomictaxon/core#Taxon>.
[<http://aims.fao.org/aos/agrovoc/c_3354>]
Number of matched answers :1
Number of correspondences found: 1
0.0 <-> <http://aims.fao.org/aos/agrovoc/c_2100> <http://www.w3.org/2004/02/skos/core#broader> ?answer. 

Binary query : ?answer0 <http://ontology.irstea.fr/agronomictaxon/core#hasHigherRank>+ ?answer1. ?answer1 a <http://ontology.irstea.fr/agronomictaxon/core#KingdomRank>.
Number of matched answers :0
Looking for similar answers
[<http://taxref.mnhn.fr/lod/name/544444>, <http://taxref.mnhn.fr/lod/taxon/544444/10.0>, <http://taxref.mnhn.fr/lod/taxon/544444/11.0>] <--> [<http://taxref.mnhn.fr/lod/name/187079>, <http://taxref.mnhn.fr/lod/taxon/187079/10.0>]
Number of similar answers :1
Number of correspondences found: 8
0.5 <-> ?answer0 <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?v1.  ?v1 <http://taxref.mnhn.fr/lod/property/hasReferenceName> ?v2.  ?v2 <http://www.w3.org/2004/02/skos/core#broader> ?answer1.  
0.5 <-> ?answer0 <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?v1.  ?v1 <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?answer1.  
0.5 <-> ?v1 <http://taxref.mnhn.fr/lod/property/isReferenceNameOf> ?answer0.  ?v1 <http://www.w3.org/2004/02/skos/core#broader> ?v2.  ?v2 <http://www.w3.org/2004/02/skos/core#broader> ?answer1.  
0.5 <-> ?answer0 <http://taxref.mnhn.fr/lod/property/isReferenceNameOf> ?v1.  ?v1 <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?v2.  ?v2 <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?answer1.  
0.5 <-> ?answer0 <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?v1.  ?v1 <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?v2.  ?answer1 <http://taxref.mnhn.fr/lod/property/isReferenceNameOf> ?v2.  
0.5 <-> ?v1 <http://taxref.mnhn.fr/lod/property/hasReferenceName> ?answer0.  ?v1 <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?v2.  ?v2 <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?answer1.  
0.5 <-> ?answer0 <http://www.w3.org/2004/02/skos/core#broader> ?v1.  ?v1 <http://www.w3.org/2004/02/skos/core#broader> ?answer1.  
0.5 <-> ?answer0 <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?v1.  ?v1 <http://www.w3.org/2000/01/rdf-schema#subClassOf> ?v2.  ?v2 <http://taxref.mnhn.fr/lod/property/hasReferenceName> ?answer1.  
Matching process ended
