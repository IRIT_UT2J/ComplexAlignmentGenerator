===============================================================================
creating directory: conference-cmt
Running with 10 support instances - 0.3 similarity.
Number of CQAs: 73

Unary query : ?answer a <http://conference#Contribution_1th-author>.
Number of matched answers :10
Number of correspondences found: 8
0.3913043478260869 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#Co-author>. 
0.3913043478260869 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Co-author>. 
0.3913043478260869 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#Co-author>. 
0.3913043478260869 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#Co-author>. 
0.3913043478260869 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Co-author>. 
0.3913043478260869 <-> ?x <http://cmt#assignedByAdministrator> ?answer. ?x a <http://cmt#Co-author>. 
0.3913043478260869 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Co-author>. 
creating directory: paper_1st_author

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Publisher> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Rejected_contribution> .
Number of matched answers :10
Number of correspondences found: 2
0.4285714285714286 <-> ?answer <http://cmt#rejectedBy> ?someObject. 
0.4285714285714286 <-> ?answer <http://cmt#hasDecision> ?y. ?y a <http://cmt#Rejection>. 
creating directory: rejected_paper

Binary query : ?answer0 <http://conference#has_the_last_name> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Track> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_participant> .
Number of matched answers :10
Number of correspondences found: 2
0.4545454545454546 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#ConferenceMember>. 
0.7727272727272727 <-> ?someSubject <http://cmt#hasConferenceMember> ?answer. 
creating directory: participant

Unary query : ?answer <http://conference#was_a_member_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Organizing_committee> .
Number of matched answers :10
Number of correspondences found: 8
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#User>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Author>. 
0.0 <-> ?answer <http://cmt#co-writePaper> <http://cmt-instances#paper1850386443322198>. 
0.0 <-> ?answer <http://cmt#memberOfConference> <http://cmt-instances#conference943277065>. 
0.0 <-> <http://cmt-instances#conference69622803> <http://cmt#hasConferenceMember> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceMember>. 
0.82 <-> ?x <http://cmt#hasProgramCommitteeMember> ?answer. ?x a <http://cmt#ProgramCommittee>. 
0.9166666666666666 <-> ?answer <http://cmt#memberOfProgramCommittee> ?y. ?y a <http://cmt#ProgramCommittee>. 
creating directory: oc_member

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_proceedings> .   ?answer0 <http://conference#has_a_name> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer1 <http://conference#has_workshops> ?answer0 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#was_a_member_of> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :10
Number of correspondences found: 2
2.7229946524064172 <-> ?answer0 a <http://cmt#ProgramCommitteeMember>.  ?answer1 <http://cmt#hasProgramCommitteeMember> ?answer0.  ?answer1 a <http://cmt#ProgramCommittee>.  
2.7479946524064176 <-> ?answer0 a <http://cmt#ProgramCommitteeMember>.  ?answer0 <http://cmt#memberOfProgramCommittee> ?answer1.  ?answer1 a <http://cmt#ProgramCommittee>.  
creating directory: member_of_pc

Binary query : ?answer0 <http://conference#reviews> ?answer1 .
Number of matched answers :10
Number of correspondences found: 3
2.107142857142857 <-> ?answer0 a <http://cmt#Review>.  ?answer0 <http://cmt#writtenBy> ?v1.  ?v1 a <http://cmt#Reviewer>.  ?v1 <http://cmt#hasBeenAssigned> ?answer1.  
2.5616883116883113 <-> ?answer0 a <http://cmt#Review>.  ?v1 <http://cmt#writeReview> ?answer0.  ?v1 a <http://cmt#Reviewer>.  ?answer1 <http://cmt#assignedTo> ?v1.  
2.5616883116883113 <-> ?answer0 a <http://cmt#Review>.  ?v1 <http://cmt#writeReview> ?answer0.  ?v1 a <http://cmt#Reviewer>.  ?v1 <http://cmt#hasBeenAssigned> ?answer1.  
creating directory: review_of_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Tutorial> .  ?answer0 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 . ?o2 a <http://conference#Important_dates>. ?o2 <http://conference#is_a_date_of_acceptance_announcement> ?answer1.
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 a <http://conference#Conference_participant>.      bind( if (exists {?answer0 a <http://conference#Early_paid_applicant>}, "true"^^<http://www.w3.org/2001/XMLSchema#boolean>,        if (exists {?answer0 a <http://conference#Late_paid_applicant>}, "false"^^<http://www.w3.org/2001/XMLSchema#boolean>,"")) as ?answer1)
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer1 <http://conference#has_tutorials> ?answer0 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#has_a_location> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Unary query : ?answer a <http://conference#Conference_participant> .   ?answer a <http://conference#Late_paid_applicant> .
Number of matched answers :10
Number of correspondences found: 2
0.4545454545454546 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.7727272727272727 <-> ?someSubject <http://cmt#hasConferenceMember> ?answer. 
creating directory: late_registered_participant

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Contribution_co-author> .
Number of matched answers :10
Number of correspondences found: 5
0.40909090909090906 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Co-author>. 
0.40909090909090906 <-> ?answer <http://cmt#addedBy> ?y. ?y a <http://cmt#Co-author>. 
0.40909090909090906 <-> ?someSubject <http://cmt#hasCo-author> ?answer. 
0.40909090909090906 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Co-author>. 
0.40909090909090906 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Co-author>. 
creating directory: paper_co_author

Binary query : ?answer1 a <http://conference#Abstract>. ?answer0 <http://conference#contributes> ?answer1 . ?o2 <http://conference#has_an_abstract> ?answer1. ?o2 a <http://conference#Invited_talk>.
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 a <http://conference#Invited_speaker>. ?answer0 <http://conference#contributes> ?answer1 . ?answer1 a <http://conference#Invited_talk>.
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#contributes> ?o2 .  ?o2 <http://conference#reviews> ?answer1 .  ?answer0 <http://conference#was_a_member_of> ?o3 . ?o3 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :10
Number of correspondences found: 2
1.1818181818181819 <-> ?answer0 a <http://cmt#ProgramCommitteeMember>.  ?answer0 <http://cmt#hasBeenAssigned> ?answer1.  
1.1818181818181819 <-> ?answer0 a <http://cmt#ProgramCommitteeMember>.  ?answer1 <http://cmt#assignedTo> ?answer0.  
creating directory: is_assigned_reviewer_of

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :5
Number of correspondences found: 3
0.9411764705882353 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommittee>. 
1.2821494016049553 <-> ?answer <http://cmt#hasProgramCommitteeMember> ?y. ?y a <http://cmt#ProgramCommitteeMember>. 
1.3072774592268332 <-> ?x <http://cmt#memberOfProgramCommittee> ?answer. ?x a <http://cmt#ProgramCommitteeMember>. 
creating directory: program_committee

Binary query : ?answer0 <http://conference#gives_presentations> ?answer1. ?answer1 a <http://conference#Presentation>.
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer> .  ?answer <http://conference#invited_by> ?o2 .
Number of matched answers :10
Number of correspondences found: 2
1.6636363636363636 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Reviewer>. 
1.7444444444444445 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Reviewer>. 
creating directory: external_reviewer

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 . ?o2 <http://conference#is_a_date_of_camera_ready_paper_submission> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#contributes> ?answer1 .   ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Review> .    ?answer0 <http://conference#was_a_member_of> ?o3 . ?o3 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :10
Number of correspondences found: 2
2.181818181818181 <-> ?answer0 a <http://cmt#ProgramCommitteeMember>.  ?answer1 <http://cmt#writtenBy> ?answer0.  ?answer1 a <http://cmt#Review>.  
2.727272727272727 <-> ?answer0 a <http://cmt#ProgramCommitteeMember>.  ?answer0 <http://cmt#writeReview> ?answer1.  ?answer1 a <http://cmt#Review>.  
creating directory: assigned_reviewer_author_of_review

Binary query : ?answer1 <http://conference#has_tracks> ?answer0 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#was_a_track-workshop_chair_of> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop> .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_volume> .
Number of matched answers :5
Number of correspondences found: 2
1.0427807486631018 <-> ?x <http://cmt#enterConferenceDetails> ?answer. ?x a <http://cmt#ConferenceMember>. 
1.061919504643963 <-> ?answer <http://cmt#hasConferenceMember> ?y. ?y a <http://cmt#ConferenceMember>. 
creating directory: conference

Binary query : ?answer0 <http://conference#has_the_first_name> ?o1 .  ?answer0 <http://conference#has_the_last_name> ?o2 .   BIND( concat(STR(?o1)," ", STR(?o2)) as ?answer1)
Number of matched answers :10
Number of correspondences found: 1
0.5 <-> ?answer0 <http://cmt#name> ?answer1.  
creating directory: person_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Person> .
Number of matched answers :10
Number of correspondences found: 3
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person>. 
1.0 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Person>. 
1.0 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Person>. 
creating directory: person

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_proceedings> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#has_a_publisher> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 .   ?answer0 a <http://conference#Conference_volume> . ?o2 <http://conference#is_a_starting_date> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.0882352941176472 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#date> ?answer1.  
creating directory: conference_start_date

Binary query : ?answer0 <http://conference#invites_co-reviewers> ?answer1 .
Number of matched answers :10
Number of correspondences found: 2
0.95 <-> ?answer1 <http://cmt#assignedByReviewer> ?answer0.  
0.9545454545454548 <-> ?answer0 <http://cmt#assignExternalReviewer> ?answer1.  
creating directory: invite_reviewer

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Tutorial> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer a <http://conference#Conference_participant> .   ?answer a <http://conference#Early_paid_applicant> .
Number of matched answers :10
Number of correspondences found: 2
0.4545454545454546 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#ConferenceMember>. 
0.7727272727272727 <-> ?someSubject <http://cmt#hasConferenceMember> ?answer. 
creating directory: early_registered_participant

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference> . ?answer0 <http://conference#has_a_name> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.9 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#name> ?answer1.  
creating directory: conference_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Abstract> .   ?answer <http://conference#is_the_1th_part_of> ?o2. ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .
Number of matched answers :10
Number of correspondences found: 1
0.6153846153846154 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperAbstract>. 
creating directory: paper_abstract

Unary query : ?answer <http://conference#was_a_member_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :10
Number of correspondences found: 4
0.5999999999999999 <-> ?someSubject <http://cmt#addProgramCommitteeMember> ?answer. 
0.6818181818181819 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommitteeMember>. 
1.5411764705882351 <-> ?x <http://cmt#hasProgramCommitteeMember> ?answer. ?x a <http://cmt#ProgramCommittee>. 
1.5661764705882353 <-> ?answer <http://cmt#memberOfProgramCommittee> ?y. ?y a <http://cmt#ProgramCommittee>. 
creating directory: pc_member

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 . ?o2 <http://conference#is_a_full_paper_submission_date> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer> . ?answer <http://conference#was_a_member_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :10
Number of correspondences found: 11
0.6153846153846154 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Meta-Reviewer>. 
0.6818181818181819 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommitteeMember>. 
0.75 <-> ?x <http://cmt#writtenBy> ?answer. ?x a <http://cmt#Review>. 
0.8636363636363636 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#ExternalReviewer>. 
0.9444444444444444 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#ExternalReviewer>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer>. 
1.042594537815126 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#ExternalReviewer>. 
1.0693627450980392 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#ConferenceMember>. 
1.1136363636363638 <-> ?answer <http://cmt#writeReview> ?y. ?y a <http://cmt#Review>. 
1.5411764705882351 <-> ?x <http://cmt#hasProgramCommitteeMember> ?answer. ?x a <http://cmt#ProgramCommittee>. 
1.5661764705882353 <-> ?answer <http://cmt#memberOfProgramCommittee> ?y. ?y a <http://cmt#ProgramCommittee>. 
creating directory: assigned_reviewer

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Accepted_contribution> .
Number of matched answers :10
Number of correspondences found: 2
0.47619047619047616 <-> ?answer <http://cmt#hasSubjectArea> <http://cmt-instances#topic372970145>. 
creating directory: accepted_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_www> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://conference#was_a_track-workshop_chair_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop> .
Number of matched answers :10
Number of correspondences found: 5
0.375 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#Person>. 
0.375 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#Person>. 
0.375 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Person>. 
0.375 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person>. 
0.375 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Person>. 
creating directory: workshop_chair

Unary query : ?answer <http://conference#is_the_1th_part_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Invited_talk> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Invited_speaker> .
Number of matched answers :10
Number of correspondences found: 5
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person>. 
0.33333333333333337 <-> ?answer <http://cmt#writeReview> ?someObject. 
0.4666666666666666 <-> ?answer <http://cmt#writePaper> ?someObject. 
0.6515151515151516 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Reviewer>. 
0.6666666666666667 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Reviewer>. 
creating directory: invited_speaker

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Regular_author> . ?answer0 <http://conference#contributes> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .   minus{  ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Abstract> .   }
Number of matched answers :10
Number of correspondences found: 4
0.8 <-> ?answer0 <http://cmt#writePaper> ?answer1.  
0.8846153846153847 <-> ?answer0 <http://cmt#co-writePaper> ?answer1.  
0.9285714285714287 <-> ?answer1 <http://cmt#hasCo-author> ?answer0.  
1.0 <-> ?answer1 <http://cmt#hasAuthor> ?answer0.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 . ?o2 <http://conference#is_an_ending_date> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Topic> .
Number of matched answers :10
Number of correspondences found: 2
0.0 <-> ?someSubject <http://cmt#hasSubjectArea> ?answer. 
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> ?someObject. 

Binary query : ?answer0 a <http://conference#Abstract>. ?answer1 <http://conference#has_an_abstract> ?answer0 . ?answer1 a <http://conference#Invited_talk>.
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?o2 a <http://conference#Conference_www>. ?o2 <http://conference#has_a_URL> ?answer .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://conference#was_a_member_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Steering_committee> .
Number of matched answers :10
Number of correspondences found: 8
0.0 <-> <http://cmt-instances#paper-148583572581263> <http://cmt#hasCo-author> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#User>. 
0.0 <-> ?answer <http://cmt#memberOfConference> <http://cmt-instances#conference1428971814>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceMember>. 
0.36363636363636365 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#ProgramCommitteeMember>. 
0.36363636363636365 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#ProgramCommitteeMember>. 
0.9555555555555556 <-> ?x <http://cmt#hasProgramCommitteeMember> ?answer. ?x a <http://cmt#ProgramCommittee>. 
1.0555555555555556 <-> ?answer <http://cmt#memberOfProgramCommittee> ?y. ?y a <http://cmt#ProgramCommittee>. 
creating directory: sc_member

Binary query : ?answer1 <http://conference#has_authors> ?answer0 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Review> .
Number of matched answers :10
Number of correspondences found: 2
2.25 <-> ?answer0 a <http://cmt#Reviewer>.  ?answer1 <http://cmt#writtenBy> ?answer0.  ?answer1 a <http://cmt#Review>.  
2.795454545454546 <-> ?answer0 a <http://cmt#Reviewer>.  ?answer0 <http://cmt#writeReview> ?answer1.  ?answer1 a <http://cmt#Review>.  
creating directory: all_reviewer_author_of_review

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer> .
Number of matched answers :10
Number of correspondences found: 9
0.6153846153846154 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Meta-Reviewer>. 
0.75 <-> ?x <http://cmt#writtenBy> ?answer. ?x a <http://cmt#Review>. 
0.8636363636363636 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#ExternalReviewer>. 
0.9444444444444444 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#ExternalReviewer>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer>. 
1.042016806722689 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#AuthorNotReviewer>. 
1.1136363636363638 <-> ?answer <http://cmt#writeReview> ?y. ?y a <http://cmt#Review>. 
1.3636363636363638 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Reviewer>. 
1.4444444444444444 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Reviewer>. 
creating directory: reviewers_all

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Review> .
Number of matched answers :10
Number of correspondences found: 3
0.75 <-> ?answer <http://cmt#writtenBy> ?y. ?y a <http://cmt#Reviewer>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Review>. 
1.2954545454545454 <-> ?x <http://cmt#writeReview> ?answer. ?x a <http://cmt#Reviewer>. 
creating directory: review

Binary query : ?answer0 <http://conference#has_the_first_name> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop> .  ?answer0 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer a <http://conference#Invited_talk> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Regular_author> .
Number of matched answers :10
Number of correspondences found: 5
0.5 <-> ?someSubject <http://cmt#hasAuthor> ?answer. 
creating directory: author

Binary query : ?answer0 a <http://conference#Written_contribution> . ?answer0 <http://conference#has_an_abstract> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 a <http://conference#Presentation>. ?answer0 <http://conference#has_an_abstract> ?o2 . ?answer1 <http://conference#has_an_abstract> ?o2 . ?answer1 a <http://conference#Written_contribution> .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://conference#was_a_committee_chair_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Organizing_committee> .
Number of matched answers :5
Number of correspondences found: 6
0.0 <-> ?answer <http://cmt#co-writePaper> <http://cmt-instances#paper111637910201263>. 
0.41666666666666663 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#ProgramCommitteeMember>. 
0.41666666666666663 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#ProgramCommitteeMember>. 
0.6399999999999999 <-> ?someSubject <http://cmt#addProgramCommitteeMember> ?answer. 
1.2916666666666665 <-> ?answer <http://cmt#memberOfProgramCommittee> ?y. ?y a <http://cmt#ProgramCommittee>. 
1.595 <-> ?x <http://cmt#hasProgramCommitteeMember> ?answer. ?x a <http://cmt#ProgramCommittee>. 
creating directory: oc_chair

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> . minus{ ?answer a  <http://conference#Abstract> . }
Number of matched answers :10
Number of correspondences found: 5
0.6076923076923078 <-> <http://cmt-instances#person-1122393118> ?somePredicate ?answer. 
0.6076923076923078 <-> ?answer <http://cmt#hasCo-author> <http://cmt-instances#person-1122393118>. 
0.6076923076923078 <-> <http://cmt-instances#person1926385373> <http://cmt#writePaper> ?answer. 
0.6166666666666668 <-> ?answer <http://cmt#assignedTo> ?someObject. 
0.630952380952381 <-> ?answer <http://cmt#hasDecision> ?y. ?y a <http://cmt#Decision>. 
creating directory: paper

Unary query : ?answer <http://conference#gives_presentations> ?o3. ?o3 <http://conference#has_an_abstract> ?o2 . ?o4 <http://conference#has_an_abstract> ?o2 . ?o4 a <http://conference#Written_contribution> .
Number of matched answers :10
Number of correspondences found: 6
0.33333333333333337 <-> ?someSubject <http://cmt#hasCo-author> ?answer. 
0.33333333333333337 <-> ?someSubject <http://cmt#hasAuthor> ?answer. 
0.6376811594202899 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#Administrator>. 
0.6555555555555556 <-> ?answer <http://cmt#writeReview> ?someObject. 
0.7055555555555556 <-> ?someSubject <http://cmt#writtenBy> ?answer. 
0.75 <-> ?answer <http://cmt#hasBeenAssigned> <http://cmt-instances#paper-5647298423341114>. 
creating directory: presenter

Binary query : ?answer0 <http://conference#is_submitted_at> ?answer1 .
Number of matched answers :10
Number of correspondences found: 9
0.5 <-> ?v1 <http://cmt#hasBeenAssigned> ?answer0.  ?v1 <http://cmt#memberOfConference> ?answer1.  
0.5 <-> ?v1 <http://cmt#rejectPaper> ?answer0.  ?answer1 <http://cmt#detailsEnteredBy> ?v1.  
0.5 <-> ?answer0 <http://cmt#acceptedBy> ?v1.  ?answer1 <http://cmt#detailsEnteredBy> ?v1.  
0.5 <-> ?v1 <http://cmt#co-writePaper> ?answer0.  ?v1 <http://cmt#memberOfConference> ?answer1.  
0.5 <-> ?v1 <http://cmt#acceptPaper> ?answer0.  ?answer1 <http://cmt#detailsEnteredBy> ?v1.  
0.5 <-> ?v1 <http://cmt#writePaper> ?answer0.  ?v1 <http://cmt#memberOfConference> ?answer1.  
0.5 <-> ?answer0 <http://cmt#acceptedBy> ?v1.  ?v1 <http://cmt#enterConferenceDetails> ?answer1.  
0.8333333333333333 <-> ?answer0 <http://cmt#assignedTo> ?v1.  ?v1 <http://cmt#memberOfConference> ?answer1.  
0.8333333333333333 <-> ?answer0 <http://cmt#assignedTo> ?v1.  ?answer1 <http://cmt#hasConferenceMember> ?v1.  
creating directory: paper_submitted_at_conference

Unary query : ?answer <http://conference#was_a_committee_chair_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :5
Number of correspondences found: 7
1.0984848484848486 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommitteeMember>. 
1.0984848484848486 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#ProgramCommitteeMember>. 
1.0984848484848486 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#ProgramCommitteeMember>. 
1.1396969696969697 <-> ?someSubject <http://cmt#addProgramCommitteeMember> ?answer. 
1.2976190476190474 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommitteeChair>. 
1.9411764705882353 <-> ?answer <http://cmt#memberOfProgramCommittee> ?y. ?y a <http://cmt#ProgramCommittee>. 
2.3161764705882355 <-> ?x <http://cmt#hasProgramCommitteeMember> ?answer. ?x a <http://cmt#ProgramCommittee>. 
creating directory: pc_chair

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Paper> .
Number of matched answers :10
Number of correspondences found: 8
0.7142857142857143 <-> ?answer <http://cmt#paperID> ?someObject. 
0.7846153846153846 <-> ?x <http://cmt#co-writePaper> ?answer. ?x a <http://cmt#User>. 
0.8545454545454546 <-> ?x <http://cmt#submitPaper> ?answer. ?x a <http://cmt#User>. 
0.8545454545454546 <-> ?x <http://cmt#rejectPaper> ?answer. ?x a <http://cmt#User>. 
0.8545454545454546 <-> ?x <http://cmt#acceptPaper> ?answer. ?x a <http://cmt#User>. 
0.868006993006993 <-> ?x <http://cmt#writePaper> ?answer. ?x a <http://cmt#User>. 
0.9555555555555556 <-> ?x <http://cmt#readPaper> ?answer. ?x a <http://cmt#User>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Paper>. 
creating directory: regular_paper

Binary query : { ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Paper> .   } UNION { ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .   ?o2 <http://conference#is_the_1th_part_of> ?answer0. ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .   }  ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.
Number of matched answers :10
Number of correspondences found: 1
1.9 <-> ?answer0 a <http://cmt#Paper>.  ?answer0 <http://cmt#title> ?answer1.  
creating directory: paper_title

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Presentation> . minus{ ?answer a <http://conference#Invited_talk> . }
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Abstract> .   ?answer0 <http://conference#is_the_1th_part_of> ?o2. ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .    ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.
Number of matched answers :10
Number of correspondences found: 1
1.1153846153846152 <-> ?answer0 a <http://cmt#PaperAbstract>.  ?answer0 <http://cmt#title> ?answer1.  
creating directory: paper_abstract_title

Binary query : ?answer0 a <http://conference#Presentation> . ?answer0 <http://conference#has_an_abstract> ?o2 . ?o4 <http://conference#has_an_abstract> ?o2 . ?o4 a <http://conference#Written_contribution> . ?o4 <http://conference#is_submitted_at> ?answer1.
Number of matched answers :10
Number of correspondences found: 0
Matching process ended
