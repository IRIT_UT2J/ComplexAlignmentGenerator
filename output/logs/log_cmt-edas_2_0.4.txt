===============================================================================
creating directory: cmt-edas
Running with 2 support instances - 0.4 similarity.
Number of CQAs: 34

Unary query : ?o2 <http://cmt#hasAuthor> ?answer .
Number of matched answers :2
Number of correspondences found: 1
0.6666666666666667 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Author>. 
creating directory: paper_1st_author

Unary query : { ?answer <http://cmt#hasDecision> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Rejection> . } UNION  { ?answer <http://cmt#rejectedBy> ?o2 . }
Number of matched answers :2
Number of correspondences found: 3
0.9 <-> ?answer <http://edas#isReviewedBy> ?y. ?y a <http://edas#Presenter>. 
1.076923076923077 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#RejectedPaper>. 
1.0833333333333333 <-> ?answer <http://edas#hasRating> ?y. ?y a <http://edas#RejectRating>. 
creating directory: rejected_paper

Binary query : ?answer0 <http://cmt#memberOfProgramCommittee> ?answer1 .
Number of matched answers :2
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceChair> .
Number of matched answers :2
Number of correspondences found: 3
0.6666666666666667 <-> ?x <http://edas#hasMember> ?answer. ?x a <http://edas#Conference>. 
0.6666666666666667 <-> ?answer <http://edas#isMemberOf> ?y. ?y a <http://edas#Conference>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceChair>. 
creating directory: conference_chair

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Co-author> .
Number of matched answers :2
Number of correspondences found: 1
0.6666666666666667 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Author>. 
creating directory: paper_co_author

Binary query : ?answer0 <http://cmt#hasBeenAssigned> ?answer1 .
Number of matched answers :2
Number of correspondences found: 2
0.5 <-> ?answer0 <http://edas#isReviewing> ?answer1.  
0.5 <-> ?answer1 <http://edas#isReviewedBy> ?answer0.  
creating directory: is_assigned_reviewer_of

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Administrator> .
Number of matched answers :2
Number of correspondences found: 2
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Author>. 
0.0 <-> ?answer <http://edas#hasRelatedPaper> <http://edas-instances#paper2788085435351114_v2>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommittee> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ExternalReviewer> .
Number of matched answers :2
Number of correspondences found: 1
0.5 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reviewer>. 
creating directory: external_reviewer

Binary query : ?answer0 <http://cmt#writeReview> ?answer1 .
Number of matched answers :2
Number of correspondences found: 2
1.0454545454545454 <-> ?answer0 <http://edas#hasRelatedDocument> ?answer1.  ?answer1 a <http://edas#Review>.  
1.0454545454545454 <-> ?answer1 <http://edas#relatesTo> ?answer0.  ?answer1 a <http://edas#Review>.  
creating directory: assigned_reviewer_author_of_review

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Conference> .
Number of matched answers :2
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Conference>. 
creating directory: conference

Binary query : ?answer0 <http://cmt#name> ?answer1 .   ?answer0 a <http://cmt#Person> .
Number of matched answers :2
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person> .
Number of matched answers :2
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 
creating directory: person

Binary query : ?answer0 <http://cmt#date> ?answer1 .
Number of matched answers :2
Number of correspondences found: 1
0.9444444444444444 <-> ?answer0 <http://edas#startDate> ?answer1.  
creating directory: conference_start_date

Binary query : ?answer0 <http://cmt#assignExternalReviewer> ?answer1 .
Number of matched answers :2
Number of correspondences found: 5
0.5 <-> ?v1 <http://edas#isReviewedBy> ?answer0.  ?v1 <http://edas#isReviewedBy> ?answer1.  
0.5 <-> ?answer0 <http://edas#isMemberOf> ?v1.  ?v1 <http://edas#hasMember> ?answer1.  
0.5 <-> ?v1 <http://edas#isReviewedBy> ?answer0.  ?answer1 <http://edas#hasRelatedPaper> ?v1.  
0.5 <-> ?v1 <http://edas#relatesTo> ?answer0.  ?v1 <http://edas#relatesTo> ?answer1.  
0.5 <-> ?v1 <http://edas#hasMember> ?answer0.  ?answer1 <http://edas#isMemberOf> ?v1.  
creating directory: invite_reviewer

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Conference> . ?answer0 <http://cmt#name> ?answer1 .
Number of matched answers :2
Number of correspondences found: 1
2.571428571428571 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#hasName> ?answer1.  
creating directory: conference_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperAbstract> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommitteeMember> .
Number of matched answers :2
Number of correspondences found: 2
0.0 <-> ?answer <http://edas#hasRelatedDocument> <http://edas-instances#review-13344984938372198_-196548216>. 
0.0 <-> <http://edas-instances#paper-20120836555111114_v0> <http://edas#isReviewedBy> ?answer. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer> .
Number of matched answers :2
Number of correspondences found: 3
0.75 <-> ?answer <http://edas#hasRelatedDocument> ?y. ?y a <http://edas#Review>. 
0.75 <-> ?x <http://edas#relatesTo> ?answer. ?x a <http://edas#Review>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reviewer>. 
creating directory: assigned_reviewer

Unary query : { ?answer <http://cmt#hasDecision> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Acceptance> . } UNION  { ?answer <http://cmt#acceptedBy> ?o2 . }
Number of matched answers :2
Number of correspondences found: 2
0.8166666666666667 <-> ?answer <http://edas#isReviewedBy> ?someObject. 
1.1538461538461537 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#AcceptedPaper>. 
creating directory: accepted_paper

Binary query : ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperFullVersion> .   { ?answer0 <http://cmt#writePaper> ?answer1 .   } union{ ?answer0 <http://cmt#co-writePaper> ?answer1 .   }
Number of matched answers :2
Number of correspondences found: 4
0.5 <-> ?answer1 <http://edas#isWrittenBy> ?answer0.  
1.5 <-> ?answer0 <http://edas#hasRelatedPaper> ?answer1.  
1.597902097902098 <-> ?answer1 <http://edas#isWrittenBy> ?answer0.  ?answer1 a <http://edas#RatedPapers>.  
2.597902097902098 <-> ?answer0 <http://edas#hasRelatedPaper> ?answer1.  ?answer1 a <http://edas#RatedPapers>.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://cmt#memberOfConference> ?answer1 .
Number of matched answers :2
Number of correspondences found: 2
1.0555555555555556 <-> ?answer1 <http://edas#hasMember> ?answer0.  ?answer1 a <http://edas#Conference>.  
1.0555555555555556 <-> ?answer0 <http://edas#isMemberOf> ?answer1.  ?answer1 a <http://edas#Conference>.  
creating directory: member_of_conference

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#SubjectArea> .
Number of matched answers :2
Number of correspondences found: 2
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> "rdf databases". 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Topic>. 

Unary query : ?o2 <http://cmt#siteURL> ?answer .
Number of matched answers :0
Number of correspondences found: 0

Unary query : { ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer> .   } union{ ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ExternalReviewer> .   }
Number of matched answers :2
Number of correspondences found: 3
0.75 <-> ?answer <http://edas#hasRelatedDocument> ?y. ?y a <http://edas#Review>. 
0.75 <-> ?x <http://edas#relatesTo> ?answer. ?x a <http://edas#Review>. 
1.5 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reviewer>. 
creating directory: reviewers_all

Binary query : ?answer0 a <http://cmt#PaperFullVersion> . ?answer0 <http://cmt#hasSubjectArea> ?answer1 .
Number of matched answers :2
Number of correspondences found: 2
0.5 <-> ?answer0 <http://edas#hasTopic> ?answer1.  
0.5 <-> ?answer1 <http://edas#isTopicOf> ?answer0.  
creating directory: topic_of_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Review> .
Number of matched answers :2
Number of correspondences found: 3
0.75 <-> ?x <http://edas#hasRelatedDocument> ?answer. ?x a <http://edas#Reviewer>. 
0.75 <-> ?answer <http://edas#relatesTo> ?y. ?y a <http://edas#Reviewer>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Review>. 
creating directory: review

Binary query : ?answer1 <http://cmt#siteURL> ?answer0 .
Number of matched answers :2
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Author> .
Number of matched answers :2
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Author>. 
creating directory: author

Binary query : ?answer0 a <http://cmt#PaperFullVersion> .   ?answer1 a <http://cmt#PaperAbstract> . ?answer0 <http://cmt#title> ?o2 . ?answer1 <http://cmt#title> ?o2 .
Number of matched answers :2
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperFullVersion> .
Number of matched answers :2
Number of correspondences found: 2
0.47058823529411764 <-> ?answer <http://edas#relatedToEvent> ?y. ?y a <http://edas#PaperPresentation>. 
creating directory: paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommitteeChair> .
Number of matched answers :2
Number of correspondences found: 2
0.0 <-> ?answer <http://edas#isReviewing> <http://edas-instances#paper-253329908202257_v0>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Attendee>. 

Binary query : ?answer0 <http://cmt#title> ?answer1 .   ?answer0 a <http://cmt#PaperFullVersion> .
Number of matched answers :2
Number of correspondences found: 1
0.5 <-> ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: paper_title

Binary query : ?answer0 <http://cmt#title> ?answer1 .   ?answer0 a <http://cmt#PaperAbstract> .
Number of matched answers :0
Number of correspondences found: 0
Matching process ended
