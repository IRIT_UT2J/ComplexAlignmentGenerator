===============================================================================
creating directory: cmt-confOf
Running with 10 support instances - 0.0 similarity.
Number of CQAs: 34

Unary query : ?o2 <http://cmt#hasAuthor> ?answer .
Number of matched answers :10
Number of correspondences found: 8
0.611111111111111 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar>. 
0.626984126984127 <-> ?answer <http://confOf#employedBy> <http://confOf-instances#orga1955322042>. 
0.711111111111111 <-> ?answer <http://confOf#hasSurname> ?someObject. 
0.7222222222222223 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
0.888888888888889 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author>. 
0.8904529306826805 <-> ?answer <http://confOf#reviewes> ?someObject. 
1.1159352047468396 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
1.2270463158579505 <-> ?answer <http://confOf#writes> ?someObject. 
creating directory: paper_1st_author

Unary query : { ?answer <http://cmt#hasDecision> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Rejection> . } UNION  { ?answer <http://cmt#rejectedBy> ?o2 . }
Number of matched answers :10
Number of correspondences found: 8
0.806060606060606 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Paper>. 
0.9964646464646465 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Contribution>. 
1.2967007645236985 <-> ?answer <http://confOf#hasTitle> ?someObject. 
1.3745164017037272 <-> ?answer <http://confOf#hasKeyword> ?someObject. 
1.7925792175859387 <-> ?answer <http://confOf#dealsWith> ?y. ?y a <http://confOf#Topic>. 
2.172195125639287 <-> ?x <http://confOf#reviewes> ?answer. ?x a <http://confOf#Member_PC>. 
2.223038926304138 <-> ?answer <http://confOf#writtenBy> ?y. ?y a <http://confOf#Member_PC>. 
2.3349154113859987 <-> ?x <http://confOf#writes> ?answer. ?x a <http://confOf#Person>. 
creating directory: rejected_paper

Binary query : ?answer0 <http://cmt#memberOfProgramCommittee> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceChair> .
Number of matched answers :5
Number of correspondences found: 5
0.6222222222222222 <-> ?answer <http://confOf#hasSurname> ?someObject. 
0.7166666666666667 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar>. 
0.7333333333333334 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
0.7866666666666666 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
1.128926453677108 <-> ?answer <http://confOf#reviewes> ?someObject. 
creating directory: conference_chair

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Co-author> .
Number of matched answers :10
Number of correspondences found: 8
0.6714285714285715 <-> ?answer <http://confOf#hasSurname> ?someObject. 
0.7051587301587301 <-> ?answer <http://confOf#employedBy> ?y. ?y a <http://confOf#Organization>. 
0.7382716049382716 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
0.9166666666666666 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar>. 
0.9883599918437894 <-> ?answer <http://confOf#reviewes> ?y. ?y a <http://confOf#Contribution>. 
1.1111111111111112 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author>. 
1.485376566381997 <-> ?x <http://confOf#writtenBy> ?answer. ?x a <http://confOf#Contribution>. 
1.5602882408342482 <-> ?answer <http://confOf#writes> ?someObject. 
creating directory: paper_co_author

Binary query : ?answer0 <http://cmt#hasBeenAssigned> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
0.7000000000000001 <-> ?answer0 <http://confOf#reviewes> ?answer1.  
creating directory: is_assigned_reviewer_of

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Administrator> .
Number of matched answers :5
Number of correspondences found: 7
0.6153846153846153 <-> ?answer <http://confOf#employedBy> ?y. ?y a <http://confOf#Organization>. 
0.6244343891402715 <-> ?answer <http://confOf#earlyRegistration> ?someObject. 
0.873076923076923 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
1.1052530130265354 <-> ?answer <http://confOf#reviewes> ?someObject. 
1.2307692307692308 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Administrator>. 
1.531122709825526 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
1.531122709825526 <-> ?answer <http://confOf#writes> ?someObject. 
creating directory: administrator

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommittee> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ExternalReviewer> .
Number of matched answers :10
Number of correspondences found: 7
0.6206395348837209 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar>. 
0.6524547803617571 <-> ?answer <http://confOf#hasSurname> ?someObject. 
0.6869645042839658 <-> ?answer <http://confOf#studyAt> <http://confOf-instances#orga959201041>. 
0.7051356589147287 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
0.7480832819792401 <-> ?answer <http://confOf#employedBy> ?y. ?y a <http://www.w3.org/2002/07/owl#NamedIndividual>. 
2.3021863639501645 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
2.349182487981172 <-> ?answer <http://confOf#writes> ?someObject. 
creating directory: external_reviewer

Binary query : ?answer0 <http://cmt#writeReview> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Conference> .
Number of matched answers :5
Number of correspondences found: 8
0.6 <-> ?answer <http://confOf#ends_on> ?someObject. 
0.6228335286848753 <-> ?answer <http://confOf#has_title> "Japan Conference on Discrete and Computational Geometry, Graphs, and Games (JCDCG3)". 
0.6833333333333333 <-> ?answer <http://confOf#has_short_title> ?someObject. 
0.7000000000000001 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Event>. 
0.8463089310959493 <-> ?answer <http://confOf#location> ?someObject. 
0.9324594410040588 <-> ?answer <http://confOf#hasAdministrativeEvent> ?y. ?y a <http://confOf#Reviewing_event>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference>. 
1.1002314814814815 <-> ?answer <http://confOf#hasTopic> ?y. ?y a <http://confOf#Topic>. 
creating directory: conference

Binary query : ?answer0 <http://cmt#name> ?answer1 .   ?answer0 a <http://cmt#Person> .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person> .
Number of matched answers :10
Number of correspondences found: 6
0.7458333333333333 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
0.825 <-> ?answer <http://confOf#hasSurname> "Bergeron". 
1.1140805183671674 <-> ?answer <http://confOf#writes> ?someObject. 
1.125 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 
1.1422587077839066 <-> ?answer <http://confOf#reviewes> ?someObject. 
1.2251916294782783 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
creating directory: person

Binary query : ?answer0 <http://cmt#date> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
0.8472222222222221 <-> ?answer0 <http://confOf#starts_on> ?answer1.  
creating directory: conference_start_date

Binary query : ?answer0 <http://cmt#assignExternalReviewer> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Conference> . ?answer0 <http://cmt#name> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
2.5472222222222225 <-> ?answer0 a <http://confOf#Conference>.  ?answer0 <http://confOf#has_title> ?answer1.  
creating directory: conference_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperAbstract> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommitteeMember> .
Number of matched answers :10
Number of correspondences found: 7
0.653030303030303 <-> ?answer <http://confOf#hasSurname> ?someObject. 
0.7090909090909091 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
0.810614458271659 <-> ?answer <http://confOf#employedBy> ?someObject. 
0.9136363636363635 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
1.94799876445839 <-> ?answer <http://confOf#reviewes> ?someObject. 
2.178429986582242 <-> ?answer <http://confOf#writes> ?someObject. 
2.2875208956731505 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
creating directory: pc_member

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer> .
Number of matched answers :10
Number of correspondences found: 4
1.111161748482572 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
1.1389395262603499 <-> ?answer <http://confOf#writes> ?someObject. 
1.2222222222222223 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
1.7566056886534862 <-> ?answer <http://confOf#reviewes> ?someObject. 
creating directory: assigned_reviewer

Unary query : { ?answer <http://cmt#hasDecision> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Acceptance> . } UNION  { ?answer <http://cmt#acceptedBy> ?o2 . }
Number of matched answers :10
Number of correspondences found: 9
0.8242424242424242 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Contribution>. 
0.9727272727272726 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Poster>. 
1.0727272727272725 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Paper>. 
1.465916913783976 <-> ?answer <http://confOf#hasTitle> ?someObject. 
1.5245773960316638 <-> ?answer <http://confOf#hasKeyword> ?someObject. 
1.6459696969696969 <-> ?answer <http://confOf#dealsWith> ?someObject. 
1.8314353013932845 <-> ?someSubject <http://confOf#reviewes> ?answer. 
2.368897217978099 <-> ?someSubject <http://confOf#writes> ?answer. 
2.4870790361599187 <-> ?answer <http://confOf#writtenBy> ?someObject. 
creating directory: accepted_paper

Binary query : ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperFullVersion> .   { ?answer0 <http://cmt#writePaper> ?answer1 .   } union{ ?answer0 <http://cmt#co-writePaper> ?answer1 .   }
Number of matched answers :10
Number of correspondences found: 10
3.886188811188811 <-> ?answer0 a <http://confOf#Participant>.  ?answer1 <http://confOf#writtenBy> ?answer0.  ?answer1 a <http://confOf#Poster>.  
4.125611888111888 <-> ?answer0 a <http://confOf#Participant>.  ?answer0 <http://confOf#writes> ?answer1.  ?answer1 a <http://confOf#Poster>.  
4.257371794871795 <-> ?answer0 a <http://confOf#Scholar>.  ?answer1 <http://confOf#writtenBy> ?answer0.  ?answer1 a <http://confOf#Poster>.  
4.288111888111888 <-> ?answer0 a <http://confOf#Participant>.  ?answer1 <http://confOf#writtenBy> ?answer0.  ?answer1 a <http://confOf#Paper>.  
4.496794871794872 <-> ?answer0 a <http://confOf#Scholar>.  ?answer0 <http://confOf#writes> ?answer1.  ?answer1 a <http://confOf#Poster>.  
4.527534965034965 <-> ?answer0 a <http://confOf#Participant>.  ?answer0 <http://confOf#writes> ?answer1.  ?answer1 a <http://confOf#Paper>.  
4.531730769230769 <-> ?answer0 a <http://confOf#Member_PC>.  ?answer1 <http://confOf#writtenBy> ?answer0.  ?answer1 a <http://confOf#Poster>.  
4.771153846153846 <-> ?answer0 a <http://confOf#Member_PC>.  ?answer0 <http://confOf#writes> ?answer1.  ?answer1 a <http://confOf#Poster>.  
4.933653846153846 <-> ?answer0 a <http://confOf#Member_PC>.  ?answer1 <http://confOf#writtenBy> ?answer0.  ?answer1 a <http://confOf#Paper>.  
5.173076923076923 <-> ?answer0 a <http://confOf#Member_PC>.  ?answer0 <http://confOf#writes> ?answer1.  ?answer1 a <http://confOf#Paper>.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://cmt#memberOfConference> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#SubjectArea> .
Number of matched answers :10
Number of correspondences found: 2
0.6615752668384248 <-> <http://confOf-instances#conference943277065> <http://confOf#hasTopic> ?answer. 
0.9482235606023315 <-> ?someSubject <http://confOf#dealsWith> ?answer. 
creating directory: topic

Unary query : ?o2 <http://cmt#siteURL> ?answer .
Number of matched answers :0
Number of correspondences found: 0

Unary query : { ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer> .   } union{ ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ExternalReviewer> .   }
Number of matched answers :10
Number of correspondences found: 10
0.7384005927952577 <-> ?answer <http://confOf#earlyRegistration> ?someObject. 
0.7430484006677397 <-> ?answer <http://confOf#studyAt> ?someObject. 
0.8249757751937984 <-> ?answer <http://confOf#hasSurname> ?someObject. 
0.9123062015503876 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar>. 
1.01374354005168 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
1.0879128486560579 <-> ?answer <http://confOf#employedBy> ?someObject. 
2.0570090439276485 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
3.5035119929008935 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
3.5782858947096776 <-> ?answer <http://confOf#writes> ?someObject. 
3.9295873425595578 <-> ?answer <http://confOf#reviewes> ?someObject. 
creating directory: reviewers_all

Binary query : ?answer0 a <http://cmt#PaperFullVersion> . ?answer0 <http://cmt#hasSubjectArea> ?answer1 .
Number of matched answers :10
Number of correspondences found: 2
1.625 <-> ?answer0 a <http://confOf#Poster>.  ?answer0 <http://confOf#dealsWith> ?answer1.  
1.75 <-> ?answer0 a <http://confOf#Paper>.  ?answer0 <http://confOf#dealsWith> ?answer1.  
creating directory: topic_of_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Review> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer1 <http://cmt#siteURL> ?answer0 .
Number of matched answers :5
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Author> .
Number of matched answers :10
Number of correspondences found: 8
0.6351851851851852 <-> ?answer <http://confOf#hasSurname> ?someObject. 
0.6388888888888888 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
0.6423076923076922 <-> ?answer <http://confOf#employedBy> ?y. ?y a <http://confOf#Organization>. 
0.7023809523809523 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar>. 
0.8205375524028496 <-> ?answer <http://confOf#reviewes> ?someObject. 
1.0243369751556326 <-> ?answer <http://confOf#writes> ?someObject. 
1.1774297332189245 <-> ?x <http://confOf#writtenBy> ?answer. ?x a <http://confOf#Contribution>. 
1.25 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author>. 
creating directory: author

Binary query : ?answer0 a <http://cmt#PaperFullVersion> .   ?answer1 a <http://cmt#PaperAbstract> . ?answer0 <http://cmt#title> ?o2 . ?answer1 <http://cmt#title> ?o2 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperFullVersion> .
Number of matched answers :10
Number of correspondences found: 5
0.6715413533834586 <-> ?answer <http://confOf#dealsWith> ?someObject. 
0.6875 <-> ?answer <http://confOf#hasKeyword> "parallelization". 
0.7757761196304352 <-> ?x <http://confOf#reviewes> ?answer. ?x a <http://confOf#Person>. 
0.7833710407239819 <-> ?answer <http://confOf#writtenBy> ?someObject. 
0.8458710407239819 <-> ?someSubject <http://confOf#writes> ?answer. 
creating directory: paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommitteeChair> .
Number of matched answers :5
Number of correspondences found: 5
0.5714285714285715 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
0.605952380952381 <-> ?answer <http://confOf#employedBy> <http://confOf-instances#orga-54062715>. 
1.1066886866964467 <-> ?answer <http://confOf#reviewes> ?someObject. 
1.3249910546984107 <-> ?answer <http://confOf#writes> ?someObject. 
1.3726101023174584 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
creating directory: pc_chair

Binary query : ?answer0 <http://cmt#title> ?answer1 .   ?answer0 a <http://cmt#PaperFullVersion> .
Number of matched answers :10
Number of correspondences found: 1
2.3125 <-> ?answer0 a <http://confOf#Contribution>.  ?answer0 <http://confOf#hasTitle> ?answer1.  
creating directory: paper_title

Binary query : ?answer0 <http://cmt#title> ?answer1 .   ?answer0 a <http://cmt#PaperAbstract> .
Number of matched answers :0
Number of correspondences found: 0
Matching process ended
