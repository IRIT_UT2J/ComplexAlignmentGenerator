===============================================================================
creating directory: cmt-confOf
Running with 10 support instances - 0.2 similarity.
Number of CQAs: 34

Unary query : ?o2 <http://cmt#hasAuthor> ?answer .
Number of matched answers :10
Number of correspondences found: 6
0.6666666666666666 <-> ?answer <http://confOf#hasSurname> ?someObject. 
0.7658638013005852 <-> ?x <http://confOf#writtenBy> ?answer. ?x a <http://confOf#Poster>. 
0.769382749861078 <-> ?answer <http://confOf#writes> ?someObject. 
0.7777777777777778 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
0.8528476989575388 <-> ?answer <http://confOf#reviewes> ?someObject. 
0.888888888888889 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author>. 
creating directory: paper_1st_author

Unary query : { ?answer <http://cmt#hasDecision> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Rejection> . } UNION  { ?answer <http://cmt#rejectedBy> ?o2 . }
Number of matched answers :10
Number of correspondences found: 6
0.8055555555555556 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Contribution>. 
1.0359909085205528 <-> ?answer <http://confOf#hasKeyword> ?someObject. 
1.1569960907910908 <-> ?answer <http://confOf#dealsWith> ?y. ?y a <http://www.w3.org/2002/07/owl#NamedIndividual>. 
1.178428515928516 <-> ?x <http://confOf#writes> ?answer. ?x a <http://confOf#Person>. 
1.4352916924345496 <-> ?x <http://confOf#reviewes> ?answer. ?x a <http://confOf#Person>. 
1.567917267917268 <-> ?answer <http://confOf#writtenBy> ?y. ?y a <http://confOf#Person>. 
creating directory: rejected_paper

Binary query : ?answer0 <http://cmt#memberOfProgramCommittee> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceChair> .
Number of matched answers :5
Number of correspondences found: 3
0.2666666666666667 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
0.5166666666666667 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar>. 
0.6226045883940621 <-> ?answer <http://confOf#reviewes> ?someObject. 
creating directory: conference_chair

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Co-author> .
Number of matched answers :10
Number of correspondences found: 8
0.6666666666666666 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
0.6666666666666666 <-> ?answer <http://confOf#hasSurname> ?someObject. 
0.6944444444444444 <-> ?answer <http://confOf#employedBy> ?y. ?y a <http://confOf#Organization>. 
0.861111111111111 <-> ?answer <http://confOf#reviewes> ?y. ?y a <http://confOf#Contribution>. 
0.9166666666666666 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar>. 
1.1111111111111112 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author>. 
1.3055555555555554 <-> ?x <http://confOf#writtenBy> ?answer. ?x a <http://confOf#Contribution>. 
1.3055555555555554 <-> ?answer <http://confOf#writes> ?y. ?y a <http://confOf#Contribution>. 
creating directory: paper_co_author

Binary query : ?answer0 <http://cmt#hasBeenAssigned> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
0.5 <-> ?answer0 <http://confOf#reviewes> ?answer1.  
creating directory: is_assigned_reviewer_of

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Administrator> .
Number of matched answers :5
Number of correspondences found: 4
0.7692307692307692 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
1.0802368666279956 <-> ?answer <http://confOf#writes> ?someObject. 
1.0802368666279958 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
1.2307692307692308 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Administrator>. 
creating directory: administrator

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommittee> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ExternalReviewer> .
Number of matched answers :10
Number of correspondences found: 6
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.20833333333333337 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar>. 
0.25 <-> ?answer <http://confOf#hasFirstName> "Avie". 
0.25 <-> ?answer <http://confOf#hasSurname> "Enlai". 
0.8180697563946193 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
0.9782879232025432 <-> ?answer <http://confOf#writes> ?someObject. 
creating directory: external_reviewer

Binary query : ?answer0 <http://cmt#writeReview> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Conference> .
Number of matched answers :5
Number of correspondences found: 4
0.7000000000000001 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Event>. 
0.7000000000000001 <-> ?answer <http://confOf#hasAdministrativeEvent> ?y. ?y a <http://confOf#Event>. 
0.8026461209528586 <-> ?answer <http://confOf#hasTopic> ?y. ?y a <http://confOf#Topic>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference>. 
creating directory: conference

Binary query : ?answer0 <http://cmt#name> ?answer1 .   ?answer0 a <http://cmt#Person> .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person> .
Number of matched answers :10
Number of correspondences found: 3
0.75 <-> ?answer <http://confOf#hasFirstName> "Deacon". 
0.8777062183602431 <-> ?answer <http://confOf#reviewes> ?someObject. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 
creating directory: person

Binary query : ?answer0 <http://cmt#date> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
0.7222222222222222 <-> ?answer0 <http://confOf#starts_on> ?answer1.  
creating directory: conference_start_date

Binary query : ?answer0 <http://cmt#assignExternalReviewer> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Conference> . ?answer0 <http://cmt#name> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
2.1222222222222222 <-> ?answer0 a <http://confOf#Conference>.  ?answer0 <http://confOf#has_title> ?answer1.  
creating directory: conference_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperAbstract> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommitteeMember> .
Number of matched answers :10
Number of correspondences found: 3
0.7333333333333334 <-> ?answer <http://confOf#writes> ?someObject. 
0.7342023693510856 <-> ?answer <http://confOf#reviewes> ?someObject. 
0.8249204876755897 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
creating directory: pc_member

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer> .
Number of matched answers :10
Number of correspondences found: 4
0.8568027210884354 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
0.8845804988662131 <-> ?answer <http://confOf#writes> ?someObject. 
1.2222222222222223 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
1.2337215779022488 <-> ?answer <http://confOf#reviewes> ?someObject. 
creating directory: assigned_reviewer

Unary query : { ?answer <http://cmt#hasDecision> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Acceptance> . } UNION  { ?answer <http://cmt#acceptedBy> ?o2 . }
Number of matched answers :10
Number of correspondences found: 5
0.6500000000000001 <-> ?someSubject <http://confOf#writes> ?answer. 
0.7028076858612249 <-> ?answer <http://confOf#hasKeyword> ?someObject. 
0.7113636363636364 <-> ?someSubject <http://confOf#reviewes> ?answer. 
0.7135295625427205 <-> ?answer <http://confOf#dealsWith> ?someObject. 
0.8729490178938712 <-> ?answer <http://confOf#writtenBy> ?y. ?y a <http://confOf#Participant>. 
creating directory: accepted_paper

Binary query : ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperFullVersion> .   { ?answer0 <http://cmt#writePaper> ?answer1 .   } union{ ?answer0 <http://cmt#co-writePaper> ?answer1 .   }
Number of matched answers :10
Number of correspondences found: 14
2.9660256410256407 <-> ?answer0 a <http://confOf#Scholar>.  ?answer1 <http://confOf#writtenBy> ?answer0.  ?answer1 a <http://confOf#Poster>.  
3.011188811188811 <-> ?answer0 a <http://confOf#Participant>.  ?answer1 <http://confOf#writtenBy> ?answer0.  ?answer1 a <http://confOf#Poster>.  
3.1153846153846154 <-> ?answer0 a <http://confOf#Member_PC>.  ?answer1 <http://confOf#writtenBy> ?answer0.  ?answer1 a <http://confOf#Poster>.  
3.142948717948718 <-> ?answer0 a <http://confOf#Scholar>.  ?answer0 <http://confOf#writes> ?answer1.  ?answer1 a <http://confOf#Poster>.  
3.188111888111888 <-> ?answer0 a <http://confOf#Participant>.  ?answer0 <http://confOf#writes> ?answer1.  ?answer1 a <http://confOf#Poster>.  
3.2923076923076926 <-> ?answer0 a <http://confOf#Member_PC>.  ?answer0 <http://confOf#writes> ?answer1.  ?answer1 a <http://confOf#Poster>.  
3.4721153846153845 <-> ?answer0 a <http://confOf#Person>.  ?answer1 <http://confOf#writtenBy> ?answer0.  ?answer1 a <http://confOf#Paper>.  
3.5554487179487184 <-> ?answer0 a <http://confOf#Scholar>.  ?answer1 <http://confOf#writtenBy> ?answer0.  ?answer1 a <http://confOf#Paper>.  
3.600611888111888 <-> ?answer0 a <http://confOf#Participant>.  ?answer1 <http://confOf#writtenBy> ?answer0.  ?answer1 a <http://confOf#Paper>.  
3.6490384615384617 <-> ?answer0 a <http://confOf#Person>.  ?answer0 <http://confOf#writes> ?answer1.  ?answer1 a <http://confOf#Paper>.  
3.7048076923076927 <-> ?answer0 a <http://confOf#Member_PC>.  ?answer1 <http://confOf#writtenBy> ?answer0.  ?answer1 a <http://confOf#Paper>.  
3.732371794871795 <-> ?answer0 a <http://confOf#Scholar>.  ?answer0 <http://confOf#writes> ?answer1.  ?answer1 a <http://confOf#Paper>.  
3.777534965034965 <-> ?answer0 a <http://confOf#Participant>.  ?answer0 <http://confOf#writes> ?answer1.  ?answer1 a <http://confOf#Paper>.  
3.8817307692307694 <-> ?answer0 a <http://confOf#Member_PC>.  ?answer0 <http://confOf#writes> ?answer1.  ?answer1 a <http://confOf#Paper>.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://cmt#memberOfConference> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#SubjectArea> .
Number of matched answers :10
Number of correspondences found: 3
0.5027340544001122 <-> ?someSubject <http://confOf#dealsWith> ?answer. 
creating directory: topic

Unary query : ?o2 <http://cmt#siteURL> ?answer .
Number of matched answers :0
Number of correspondences found: 0

Unary query : { ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer> .   } union{ ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ExternalReviewer> .   }
Number of matched answers :10
Number of correspondences found: 7
0.20833333333333337 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar>. 
0.46875 <-> ?answer <http://confOf#hasSurname> ?someObject. 
0.65 <-> ?answer <http://confOf#employedBy> <http://confOf-instances#orga-933477044>. 
1.1565083603019588 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
1.1842861380797367 <-> ?answer <http://confOf#writes> ?someObject. 
1.7222222222222223 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
1.9919125145504792 <-> ?answer <http://confOf#reviewes> ?someObject. 
creating directory: reviewers_all

Binary query : ?answer0 a <http://cmt#PaperFullVersion> . ?answer0 <http://cmt#hasSubjectArea> ?answer1 .
Number of matched answers :10
Number of correspondences found: 2
0.5 <-> ?answer0 <http://confOf#dealsWith> ?answer1.  
1.0267857142857144 <-> ?answer0 a <http://confOf#Paper>.  ?answer0 <http://confOf#dealsWith> ?answer1.  
creating directory: topic_of_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Review> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer1 <http://cmt#siteURL> ?answer0 .
Number of matched answers :5
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Author> .
Number of matched answers :10
Number of correspondences found: 5
0.625 <-> ?answer <http://confOf#employedBy> <http://confOf-instances#orga1503842536>. 
0.6968325791855203 <-> ?answer <http://confOf#reviewes> <http://confOf-instances#paper-1380439872802198>. 
0.75 <-> ?x <http://confOf#writtenBy> ?answer. ?x a <http://confOf#Contribution>. 
0.75 <-> ?answer <http://confOf#writes> ?y. ?y a <http://confOf#Contribution>. 
1.25 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author>. 
creating directory: author

Binary query : ?answer0 a <http://cmt#PaperFullVersion> .   ?answer1 a <http://cmt#PaperAbstract> . ?answer0 <http://cmt#title> ?o2 . ?answer1 <http://cmt#title> ?o2 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperFullVersion> .
Number of matched answers :10
Number of correspondences found: 4
0.46875 <-> ?x <http://confOf#reviewes> ?answer. ?x a <http://confOf#Person>. 
creating directory: paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommitteeChair> .
Number of matched answers :5
Number of correspondences found: 3
0.23809523809523814 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant>. 
0.43452380952380953 <-> ?answer <http://confOf#reviewes> ?someObject. 
0.6028417818740399 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
creating directory: pc_chair

Binary query : ?answer0 <http://cmt#title> ?answer1 .   ?answer0 a <http://cmt#PaperFullVersion> .
Number of matched answers :10
Number of correspondences found: 1
1.125 <-> ?answer0 <http://confOf#hasTitle> ?answer1.  
creating directory: paper_title

Binary query : ?answer0 <http://cmt#title> ?answer1 .   ?answer0 a <http://cmt#PaperAbstract> .
Number of matched answers :0
Number of correspondences found: 0
Matching process ended
