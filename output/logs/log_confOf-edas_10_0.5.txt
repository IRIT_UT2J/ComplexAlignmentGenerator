===============================================================================
creating directory: confOf-edas
Running with 10 support instances - 0.5 similarity.
Number of CQAs: 54

Binary query : ?answer0 <http://confOf#hasSurname> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
1.1363636363636365 <-> ?answer0 <http://edas#hasLastName> ?answer1.  
creating directory: person_last_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant> .
Number of matched answers :10
Number of correspondences found: 7
0.0 <-> ?someSubject <http://edas#isWrittenBy> ?answer. 
0.0 <-> ?answer <http://edas#hasFirstName> "Christine". 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Attendee>. 
0.0 <-> <http://edas-instances#paper16834214795021114> <http://edas#isReviewedBy> ?answer. 
0.0 <-> ?answer <http://edas#isReviewing> <http://edas-instances#paper6527112814221114_v0>. 
0.0 <-> <http://edas-instances#diapo-1414779963226257> <http://edas#relatesTo> ?answer. 

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Camera_Ready_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :10
Number of correspondences found: 2
1.5 <-> ?answer0 a <http://edas#Workshop>.  ?v1 <http://edas#forEvent> ?answer0.  ?v1 <http://edas#hasSubmissionDeadline> ?answer1.  
1.5 <-> ?answer0 a <http://edas#Workshop>.  ?answer0 <http://edas#hasCall> ?v1.  ?v1 <http://edas#hasSubmissionDeadline> ?answer1.  
creating directory: workshop_camera_date

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Registration_of_participants_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#registrationDueOn> ?answer1.  
creating directory: conference_regis_date

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://edas#Reception>.  ?answer0 <http://edas#hasEndDateTime> ?answer1.  
creating directory: reception_end_date

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Tutorial> .  ?answer0 <http://confOf#hasTopic> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
0.5 <-> ?answer0 <http://edas#hasEndDateTime> ?answer1.  
creating directory: trip_end_date

Binary query : ?answer0 a <http://confOf#Conference>. ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Reviewing_results_event> . ?o2 <http://confOf#starts_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#earlyRegistration> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#location> ?answer1 .   ?answer0 a <http://confOf#Conference> .
Number of matched answers :5
Number of correspondences found: 1
2.5 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#location> ?answer1.  
creating directory: conference_location

Unary query : ?answer <http://confOf#earlyRegistration> "false"^^<http://www.w3.org/2001/XMLSchema#boolean> .
Number of matched answers :10
Number of correspondences found: 8
0.0 <-> ?answer <http://edas#hasRelatedPaper> <http://edas-instances#paper20010707656001114>. 
0.0 <-> ?someSubject <http://edas#isWrittenBy> ?answer. 
0.0 <-> ?answer <http://edas#hasRelatedDocument> <http://edas-instances#review-14810052594411114_-196548215>. 
0.0 <-> ?answer <http://edas#isReviewing> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reviewer>. 
0.0 <-> <http://edas-instances#conference-1409861683> <http://edas#hasMember> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Attendee>. 

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Submission_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :10
Number of correspondences found: 2
2.0714285714285707 <-> ?answer0 a <http://edas#Workshop>.  ?answer0 <http://edas#hasCall> ?v1.  ?v1 <http://edas#hasSubmissionDeadline> ?answer1.  
2.0714285714285707 <-> ?answer0 a <http://edas#Workshop>.  ?v1 <http://edas#forEvent> ?answer0.  ?v1 <http://edas#hasSubmissionDeadline> ?answer1.  
creating directory: workshop_submission_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#University> .
Number of matched answers :10
Number of correspondences found: 3
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#AcademiaOrganization>. 
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Organization>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .
Number of matched answers :5
Number of correspondences found: 4
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#NonAcademicEvent>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceEvent>. 
0.0 <-> ?answer <http://edas#hasStartDateTime> "2008-04-07T18:00:00". 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Excursion>. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://edas#Reception>.  ?answer0 <http://edas#hasStartDateTime> ?answer1.  
creating directory: reception_start_date

Binary query : ?answer0 <http://confOf#reviewes> ?answer1 .   ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .
Number of matched answers :10
Number of correspondences found: 6
2.375 <-> ?answer0 a <http://edas#Reviewer>.  ?v1 <http://edas#hasMember> ?answer0.  ?v1 <http://edas#hasRelatedDocument> ?answer1.  
2.375 <-> ?answer0 a <http://edas#Reviewer>.  ?v1 <http://edas#hasMember> ?answer0.  ?answer1 <http://edas#relatesTo> ?v1.  
2.875 <-> ?answer0 a <http://edas#Reviewer>.  ?answer0 <http://edas#isMemberOf> ?v1.  ?v1 <http://edas#hasRelatedDocument> ?answer1.  
2.875 <-> ?answer0 a <http://edas#Reviewer>.  ?answer0 <http://edas#isMemberOf> ?v1.  ?answer1 <http://edas#relatesTo> ?v1.  
3.465909090909092 <-> ?answer0 a <http://edas#Reviewer>.  ?answer0 <http://edas#isReviewing> ?answer1.  
3.5416666666666665 <-> ?answer0 a <http://edas#Reviewer>.  ?answer1 <http://edas#isReviewedBy> ?answer0.  
creating directory: is_assigned_reviewer_of

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Administrator> .
Number of matched answers :5
Number of correspondences found: 5
0.0 <-> ?answer <http://edas#hasLastName> "Rich". 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Author>. 
0.0 <-> ?answer <http://edas#hasRelatedPaper> <http://edas-instances#paper2788085435351114_v2>. 
0.0 <-> ?answer <http://edas#hasRelatedDocument> <http://edas-instances#review-1446402940901263_-559246915>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Camera_Ready_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#paperDueOn> ?answer1.  
creating directory: conference_camera_date

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
0.5 <-> ?answer0 <http://edas#hasStartDateTime> ?answer1.  
creating directory: gala_dinner_start_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> .
Number of matched answers :5
Number of correspondences found: 3
0.6444444444444445 <-> ?x <http://edas#isMemberOf> ?answer. ?x a <http://edas#ConferenceChair>. 
0.6666666666666667 <-> ?answer <http://edas#hasMember> ?y. ?y a <http://edas#ConferenceChair>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Conference>. 
creating directory: conference

Binary query : ?answer0 <http://confOf#hasFirstName> ?o1 .  ?answer0 <http://confOf#hasSurname> ?o2 .   BIND( concat(STR(?o1)," ", STR(?o2)) as ?answer1)
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person> .
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 
creating directory: person

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :5
Number of correspondences found: 2
1.1666666666666667 <-> ?v1 <http://edas#isLocationOf> ?answer0.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
1.2272727272727273 <-> ?answer0 <http://edas#hasLocation> ?v1.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: gala_dinner_location

Binary query : ?answer0 <http://confOf#starts_on> ?answer1 .   ?answer0 a <http://confOf#Conference> .
Number of matched answers :5
Number of correspondences found: 1
2.0555555555555554 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#startDate> ?answer1.  
creating directory: conference_start_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Tutorial> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://confOf#earlyRegistration> "true"^^<http://www.w3.org/2001/XMLSchema#boolean> .
Number of matched answers :10
Number of correspondences found: 8
0.0 <-> ?someSubject <http://edas#isWrittenBy> ?answer. 
0.0 <-> ?answer <http://edas#hasLastName> "Burgess". 
0.0 <-> ?answer <http://edas#hasFirstName> "Christine". 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Attendee>. 
0.0 <-> <http://edas-instances#paper16834214795021114> <http://edas#isReviewedBy> ?answer. 
0.0 <-> ?answer <http://edas#isReviewing> <http://edas-instances#paper6527112814221114_v0>. 
0.0 <-> <http://edas-instances#diapo-1414779963226257> <http://edas#relatesTo> ?answer. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#has_title> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
2.0 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#hasName> ?answer1.  
creating directory: conference_name

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
0.5 <-> ?answer0 <http://edas#hasStartDateTime> ?answer1.  
creating directory: trip_start_date

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://edas#Workshop>.  ?answer0 <http://edas#hasEndDateTime> ?answer1.  
creating directory: workshop_end_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .
Number of matched answers :10
Number of correspondences found: 3
0.75 <-> ?answer <http://edas#hasRelatedDocument> ?y. ?y a <http://edas#Review>. 
0.75 <-> ?x <http://edas#relatesTo> ?answer. ?x a <http://edas#Review>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reviewer>. 
creating directory: pc_member

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Workshop>. 
creating directory: workshop

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Submission_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#manuscriptDueOn> ?answer1.  
creating directory: conference_paper_date

Binary query : ?answer0 <http://confOf#hasTopic> ?answer1 .  ?answer0 a  <http://confOf#Conference> .
Number of matched answers :10
Number of correspondences found: 2
2.680555555555556 <-> ?answer0 a <http://edas#Conference>.  ?answer1 <http://edas#isTopicOf> ?answer0.  ?answer1 a <http://edas#Topic>.  
3.125 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#hasTopic> ?answer1.  ?answer1 a <http://edas#Topic>.  
creating directory: conference_topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .  ?answer <http://confOf#reviewes> ?o2 .
Number of matched answers :10
Number of correspondences found: 6
1.0 <-> ?answer <http://edas#hasLastName> "Levine". 
1.0909090909090922 <-> ?answer <http://edas#isReviewing> ?someObject. 
1.1666666666666636 <-> ?someSubject <http://edas#isReviewedBy> ?answer. 
1.5 <-> ?x <http://edas#relatesTo> ?answer. ?x a <http://edas#Review>. 
1.5 <-> ?answer <http://edas#hasRelatedDocument> ?y. ?y a <http://edas#Review>. 
1.875 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reviewer>. 
creating directory: assigned_reviewer

Binary query : ?answer0 <http://confOf#writes> ?answer1 .
Number of matched answers :10
Number of correspondences found: 2
0.5 <-> ?answer0 <http://edas#hasRelatedPaper> ?answer1.  
0.5 <-> ?answer1 <http://edas#isWrittenBy> ?answer0.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#endDate> ?answer1.  
creating directory: conference_end_date

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :5
Number of correspondences found: 2
1.1666666666666667 <-> ?v1 <http://edas#isLocationOf> ?answer0.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
1.2272727272727273 <-> ?answer0 <http://edas#hasLocation> ?v1.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: trip_location

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Topic> .
Number of matched answers :10
Number of correspondences found: 2
0.625 <-> ?someSubject <http://edas#hasTopic> ?answer. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Topic>. 
creating directory: topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar> .
Number of matched answers :10
Number of correspondences found: 8
0.0 <-> <http://edas-instances#review-21262710595171114_-196548216> <http://edas#relatesTo> ?answer. 
0.0 <-> ?answer <http://edas#hasRelatedPaper> <http://edas-instances#paper20680300817251114>. 
0.0 <-> ?answer <http://edas#isReviewing> <http://edas-instances#paper-16395696186022198>. 
0.0 <-> ?answer <http://edas#hasLastName> ?someObject. 
0.0 <-> ?someSubject <http://edas#hasMember> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Author>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Attendee>. 
0.0 <-> ?answer <http://edas#isMemberOf> <http://edas-instances#conference-1409861683>. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://edas#Workshop>.  ?answer0 <http://edas#hasStartDateTime> ?answer1.  
creating directory: workshop_start_date

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
0.5 <-> ?answer0 <http://edas#hasEndDateTime> ?answer1.  
creating directory: gala_dinner_end_date

Binary query : ?answer0 <http://confOf#dealsWith> ?answer1 .
Number of matched answers :10
Number of correspondences found: 2
0.5 <-> ?answer1 <http://edas#isTopicOf> ?answer0.  
0.5 <-> ?answer0 <http://edas#hasTopic> ?answer1.  
creating directory: topic_of_paper

Binary query : ?answer0 <http://confOf#hasFirstName> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
1.5 <-> ?answer0 <http://edas#hasFirstName> ?answer1.  
creating directory: person_first_name

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .  ?answer0 <http://confOf#hasTopic> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .
Number of matched answers :5
Number of correspondences found: 5
0.0 <-> ?answer <http://edas#hasLocation> <http://edas-instances#location1961263292>. 
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> "Gala Dinner". 
0.0 <-> ?answer <http://edas#hasStartDateTime> "2005-05-13T19:00:00". 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#NonAcademicEvent>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceDinner>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author> .
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Author>. 
creating directory: author

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Company> .
Number of matched answers :10
Number of correspondences found: 3
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Organization>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#IndustryOrganization>. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :5
Number of correspondences found: 2
2.7222222222222223 <-> ?answer0 a <http://edas#Reception>.  ?v1 <http://edas#isLocationOf> ?answer0.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
2.782828282828283 <-> ?answer0 a <http://edas#Reception>.  ?answer0 <http://edas#hasLocation> ?v1.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: reception_location

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Reviewing_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :10
Number of correspondences found: 2
1.5 <-> ?answer0 a <http://edas#Workshop>.  ?v1 <http://edas#forEvent> ?answer0.  ?v1 <http://edas#hasSubmissionDeadline> ?answer1.  
1.5 <-> ?answer0 a <http://edas#Workshop>.  ?answer0 <http://edas#hasCall> ?v1.  ?v1 <http://edas#hasSubmissionDeadline> ?answer1.  
creating directory: workshop_notif_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Contribution> .
Number of matched answers :10
Number of correspondences found: 7
0.46153846153846156 <-> ?answer <http://edas#hasTopic> ?someObject. 
creating directory: paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Chair_PC> .
Number of matched answers :5
Number of correspondences found: 5
0.0 <-> <http://edas-instances#paper14883166801311263_v0> <http://edas#isReviewedBy> ?answer. 
0.0 <-> ?answer <http://edas#isReviewing> <http://edas-instances#paper-253329908202257_v0>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Presenter>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Attendee>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Paper> .
Number of matched answers :10
Number of correspondences found: 3
0.6 <-> <http://edas-instances#person-2116002286> <http://edas#hasRelatedPaper> ?answer. 
0.6 <-> ?answer <http://edas#isWrittenBy> <http://edas-instances#person-2116002286>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Paper>. 
creating directory: regular_paper

Binary query : ?answer0 <http://confOf#hasTitle> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
0.5 <-> ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: paper_title

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .
Number of matched answers :5
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reception>. 
creating directory: reception
Matching process ended
