===============================================================================
Running with 10 support instances - 0.4 similarity.
Number of CQAs: 49

Unary query : ?answer a <http://confOf#Contribution>.
Number of matched answers :10
Number of correspondences found: 6
Reassessing similarity
1.2911819301979481 <-> ?x <http://conference#contributes> ?answer. ?x a <http://conference#Contribution_co-author>. 
0.6 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution>. 
0.6 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Regular_contribution>. 
0.6 <-> ?x <http://conference#is_the_1th_part_of> ?answer. ?x a <http://conference#Regular_contribution>. 
1.1058823529411765 <-> ?x <http://conference#has_contributions> ?answer. ?x a <http://conference#Conference>. 
0.6 <-> ?answer <http://conference#has_an_abstract> ?y. ?y a <http://conference#Regular_contribution>. 
0.6 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution>. 
0.6 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Regular_contribution>. 
0.6 <-> ?x <http://conference#is_the_1th_part_of> ?answer. ?x a <http://conference#Regular_contribution>. 
0.6 <-> ?answer <http://conference#has_an_abstract> ?y. ?y a <http://conference#Regular_contribution>. 
1.1058823529411765 <-> ?x <http://conference#has_contributions> ?answer. ?x a <http://conference#Conference>. 
1.2911819301979481 <-> ?x <http://conference#contributes> ?answer. ?x a <http://conference#Contribution_co-author>. 

Unary query : ?answer a <http://confOf#Banquet>.
Number of matched answers :0
Number of correspondences found: 0
Reassessing similarity

Unary query : ?answer a <http://confOf#Social_event>.
Number of matched answers :0
Number of correspondences found: 0
Reassessing similarity

Binary query : ?answer0 <http://confOf#hasSurname> ?answer1.
Number of matched answers :10
Number of correspondences found: 1
Reassessing similarity
0.9705882352941175 <-> ?answer0 <http://conference#has_the_last_name> ?answer1.  
0.9705882352941175 <-> ?answer0 <http://conference#has_the_last_name> ?answer1.  

Unary query : ?answer a <http://confOf#Member_PC>.
Number of matched answers :10
Number of correspondences found: 5
Reassessing similarity
0.75 <-> ?answer <http://conference#contributes> ?y. ?y a <http://conference#Review>. 
0.75 <-> ?x <http://conference#has_authors> ?answer. ?x a <http://conference#Review>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer>. 
1.0 <-> ?x <http://conference#invited_by> ?answer. ?x a <http://conference#Reviewer>. 
1.4 <-> ?answer <http://conference#invites_co-reviewers> ?y. ?y a <http://conference#Reviewer>. 
0.3006731488406881 <-> ?answer <http://conference#contributes> ?y. ?y a <http://conference#Review>. 
0.3006731488406881 <-> ?x <http://conference#has_authors> ?answer. ?x a <http://conference#Review>. 
0.40089753178758414 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer>. 
1.0 <-> ?x <http://conference#invited_by> ?answer. ?x a <http://conference#Reviewer>. 
1.4 <-> ?answer <http://conference#invites_co-reviewers> ?y. ?y a <http://conference#Reviewer>. 

Binary query : ?answer0 <http://confOf#earlyRegistration> ?answer1.
Number of matched answers :10
Number of correspondences found: 0
Reassessing similarity

Unary query : ?answer a <http://confOf#Author>.
Number of matched answers :10
Number of correspondences found: 3
Reassessing similarity
0.5454545454545454 <-> ?someSubject <http://conference#has_authors> ?answer. 
0.6666666666666667 <-> <http://conference-instances#person-871656919> <http://conference#invites_co-reviewers> ?answer. 
0.6666666666666667 <-> ?answer <http://conference#invited_by> <http://conference-instances#person-871656919>. 
0.4675731472912365 <-> ?someSubject <http://conference#has_authors> ?answer. 
0.6666666666666667 <-> <http://conference-instances#person-871656919> <http://conference#invites_co-reviewers> ?answer. 
0.6666666666666667 <-> ?answer <http://conference#invited_by> <http://conference-instances#person-871656919>. 

Unary query : ?answer a <http://confOf#Event>.
Number of matched answers :10
Number of correspondences found: 4
Reassessing similarity
0.4 <-> ?x <http://conference#has_tutorials> ?answer. ?x a <http://conference#Conference>. 
0.4 <-> ?x <http://conference#has_parts> ?answer. ?x a <http://conference#Conference>. 
0.4 <-> ?x <http://conference#has_workshops> ?answer. ?x a <http://conference#Conference>. 
0.4 <-> ?answer <http://conference#is_part_of_conference_volumes> ?y. ?y a <http://conference#Conference>. 
0.4 <-> ?answer <http://conference#is_part_of_conference_volumes> ?y. ?y a <http://conference#Conference>. 
0.4 <-> ?x <http://conference#has_workshops> ?answer. ?x a <http://conference#Conference>. 
0.4 <-> ?x <http://conference#has_parts> ?answer. ?x a <http://conference#Conference>. 
0.4 <-> ?x <http://conference#has_tutorials> ?answer. ?x a <http://conference#Conference>. 

Binary query : ?answer0 <http://confOf#hasTopic> ?answer1.
Number of matched answers :10
Number of correspondences found: 2
Reassessing similarity
1.125 <-> ?answer0 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1.  ?answer1 a <http://conference#Topic>.  
1.125 <-> ?answer1 <http://conference#is_a_topis_of_conference_parts> ?answer0.  ?answer1 a <http://conference#Topic>.  
1.125 <-> ?answer0 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1.  ?answer1 a <http://conference#Topic>.  
1.125 <-> ?answer1 <http://conference#is_a_topis_of_conference_parts> ?answer0.  ?answer1 a <http://conference#Topic>.  

Unary query : ?answer a <http://confOf#Conference>.
Number of matched answers :5
Number of correspondences found: 6
Reassessing similarity
0.6666666666666667 <-> ?answer <http://conference#has_tutorials> ?y. ?y a <http://conference#Conference_part>. 
0.6666666666666667 <-> ?x <http://conference#is_part_of_conference_volumes> ?answer. ?x a <http://conference#Conference_part>. 
0.6666666666666667 <-> ?answer <http://conference#has_parts> ?y. ?y a <http://conference#Conference_part>. 
0.6666666666666667 <-> ?answer <http://conference#has_workshops> ?y. ?y a <http://conference#Conference_part>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference>. 
0.6666666666666667 <-> ?answer <http://conference#has_tracks> ?y. ?y a <http://conference#Conference_part>. 
0.6666666666666667 <-> ?answer <http://conference#has_tutorials> ?y. ?y a <http://conference#Conference_part>. 
0.6666666666666667 <-> ?x <http://conference#is_part_of_conference_volumes> ?answer. ?x a <http://conference#Conference_part>. 
0.6666666666666667 <-> ?answer <http://conference#has_parts> ?y. ?y a <http://conference#Conference_part>. 
0.6666666666666667 <-> ?answer <http://conference#has_workshops> ?y. ?y a <http://conference#Conference_part>. 
0.6666666666666667 <-> ?answer <http://conference#has_tracks> ?y. ?y a <http://conference#Conference_part>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference>. 

Unary query : ?answer a <http://confOf#Workshop>.
Number of matched answers :10
Number of correspondences found: 2
Reassessing similarity
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop>. 
0.6153846153846153 <-> ?someSubject <http://conference#has_workshops> ?answer. 
0.6153846153846153 <-> ?someSubject <http://conference#has_workshops> ?answer. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop>. 

Binary query : ?answer0 <http://confOf#hasAdministrativeEvent> ?answer1.
Number of matched answers :10
Number of correspondences found: 0
Reassessing similarity

Binary query : ?answer0 <http://confOf#starts_on> ?answer1.
Number of matched answers :10
Number of correspondences found: 2
Reassessing similarity
0.5 <-> ?answer0 <http://conference#has_important_dates> ?v1.  ?v1 <http://conference#is_a_starting_date> ?answer1.  
0.5 <-> ?v1 <http://conference#belong_to_a_conference_volume> ?answer0.  ?v1 <http://conference#is_a_starting_date> ?answer1.  
0.5 <-> ?v1 <http://conference#belong_to_a_conference_volume> ?answer0.  ?v1 <http://conference#is_a_starting_date> ?answer1.  
0.5 <-> ?answer0 <http://conference#has_important_dates> ?v1.  ?v1 <http://conference#is_a_starting_date> ?answer1.  

Unary query : ?answer a <http://confOf#Reviewing_event>.
Number of matched answers :0
Number of correspondences found: 0
Reassessing similarity

Binary query : ?answer0 <http://confOf#dealsWith> ?answer1.
Number of matched answers :10
Number of correspondences found: 4
Reassessing similarity
0.5 <-> ?v1 <http://conference#has_contributions> ?answer0.  ?v1 <http://conference#has_parts> ?v2.  ?v2 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1.  
0.5 <-> ?v1 <http://conference#has_contributions> ?answer0.  ?v2 <http://conference#is_part_of_conference_volumes> ?v1.  ?answer1 <http://conference#is_a_topis_of_conference_parts> ?v2.  
0.5 <-> ?v1 <http://conference#has_contributions> ?answer0.  ?v2 <http://conference#is_part_of_conference_volumes> ?v1.  ?v2 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1.  
0.5 <-> ?answer0 <http://conference#is_submitted_at> ?v1.  ?v1 <http://conference#has_parts> ?v2.  ?v2 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1.  
9.101997819216766E-4 <-> ?v1 <http://conference#has_contributions> ?answer0.  ?v1 <http://conference#has_parts> ?v2.  ?v2 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1.  
9.101997819216766E-4 <-> ?v1 <http://conference#has_contributions> ?answer0.  ?v2 <http://conference#is_part_of_conference_volumes> ?v1.  ?answer1 <http://conference#is_a_topis_of_conference_parts> ?v2.  
9.101997819216766E-4 <-> ?v1 <http://conference#has_contributions> ?answer0.  ?v2 <http://conference#is_part_of_conference_volumes> ?v1.  ?v2 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1.  
9.332581798693439E-4 <-> ?answer0 <http://conference#is_submitted_at> ?v1.  ?v1 <http://conference#has_parts> ?v2.  ?v2 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1.  

Unary query : ?answer a <http://confOf#Tutorial>.
Number of matched answers :10
Number of correspondences found: 2
Reassessing similarity
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Tutorial>. 
0.6153846153846153 <-> ?someSubject <http://conference#has_tutorials> ?answer. 
0.6153846153846153 <-> ?someSubject <http://conference#has_tutorials> ?answer. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Tutorial>. 

Unary query : ?answer a <http://confOf#Scholar>.
Number of matched answers :10
Number of correspondences found: 9
Reassessing similarity
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Passive_conference_participant>. 
0.0 <-> <http://conference-instances#abstr16309077755882198> <http://conference#has_authors> ?answer. 
0.0 <-> ?someSubject <http://conference#invites_co-reviewers> ?answer. 
0.0 <-> ?answer <http://conference#has_the_first_name> "Steve". 
0.0 <-> ?answer <http://conference#contributes> <http://conference-instances#paper-19584901103702198>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Paid_applicant>. 
0.0 <-> <http://conference-instances#presentation2623058461932198> <http://conference#is_given_by> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_contributor>. 
0.4285714285714286 <-> ?answer <http://conference#has_the_last_name> "Choling". 
0.4285714285714286 <-> ?answer <http://conference#has_the_last_name> "Choling". 

Unary query : ?answer a <http://confOf#Trip>.
Number of matched answers :0
Number of correspondences found: 0
Reassessing similarity

Unary query : ?answer a <http://confOf#Working_event>.
Number of matched answers :10
Number of correspondences found: 8
Reassessing similarity
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Tutorial>. 
0.0 <-> ?someSubject <http://conference#is_a_topis_of_conference_parts> ?answer. 
0.0 <-> ?answer <http://conference#has_a_track-workshop-tutorial_topic> <http://conference-instances#topic-1320318308>. 
0.0 <-> ?answer <http://conference#is_part_of_conference_volumes> <http://conference-instances#conference1428971814>. 
0.0 <-> ?answer <http://conference#has_a_track-workshop-tutorial_chair> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop>. 
0.0 <-> <http://conference-instances#conference943277065> <http://conference#has_workshops> ?answer. 
0.0 <-> <http://conference-instances#paper-1126645578261263> <http://conference#is_submitted_at> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Tutorial>. 
0.0 <-> ?someSubject <http://conference#is_a_topis_of_conference_parts> ?answer. 
0.0 <-> ?answer <http://conference#has_a_track-workshop-tutorial_topic> <http://conference-instances#topic-1320318308>. 
0.0 <-> ?answer <http://conference#is_part_of_conference_volumes> <http://conference-instances#conference1428971814>. 
0.0 <-> ?answer <http://conference#has_a_track-workshop-tutorial_chair> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop>. 
0.0 <-> <http://conference-instances#conference943277065> <http://conference#has_workshops> ?answer. 
0.0 <-> <http://conference-instances#paper-1126645578261263> <http://conference#is_submitted_at> ?answer. 

Binary query : ?answer0 <http://confOf#parallel_with> ?answer1.
Number of matched answers :0
Number of correspondences found: 0
Reassessing similarity

Binary query : ?answer0 <http://confOf#ends_on> ?answer1.
Number of matched answers :10
Number of correspondences found: 2
Reassessing similarity
0.5 <-> ?answer0 <http://conference#has_important_dates> ?v1.  ?v1 <http://conference#is_an_ending_date> ?answer1.  
0.5 <-> ?v1 <http://conference#belong_to_a_conference_volume> ?answer0.  ?v1 <http://conference#is_an_ending_date> ?answer1.  
0.5 <-> ?v1 <http://conference#belong_to_a_conference_volume> ?answer0.  ?v1 <http://conference#is_an_ending_date> ?answer1.  
0.5 <-> ?answer0 <http://conference#has_important_dates> ?v1.  ?v1 <http://conference#is_an_ending_date> ?answer1.  

Unary query : ?answer a <http://confOf#Person>.
Number of matched answers :10
Number of correspondences found: 5
Reassessing similarity
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Person>. 
1.0 <-> ?x <http://conference#invites_co-reviewers> ?answer. ?x a <http://conference#Person>. 
1.0 <-> ?answer <http://conference#invited_by> ?y. ?y a <http://conference#Person>. 
1.0 <-> ?x <http://conference#invited_by> ?answer. ?x a <http://conference#Person>. 
1.0 <-> ?answer <http://conference#invites_co-reviewers> ?y. ?y a <http://conference#Person>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Person>. 
1.0 <-> ?x <http://conference#invites_co-reviewers> ?answer. ?x a <http://conference#Person>. 
1.0 <-> ?answer <http://conference#invited_by> ?y. ?y a <http://conference#Person>. 
1.0 <-> ?x <http://conference#invited_by> ?answer. ?x a <http://conference#Person>. 
1.0 <-> ?answer <http://conference#invites_co-reviewers> ?y. ?y a <http://conference#Person>. 

Unary query : ?answer a <http://confOf#Submission_event>.
Number of matched answers :0
Number of correspondences found: 0
Reassessing similarity

Unary query : ?answer a <http://confOf#Chair_PC>.
Number of matched answers :5
Number of correspondences found: 3
Reassessing similarity
0.625 <-> ?x <http://conference#invited_by> ?answer. ?x a <http://conference#Chair>. 
0.625 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Chair>. 
0.625 <-> ?answer <http://conference#invites_co-reviewers> ?y. ?y a <http://conference#Chair>. 
0.20833333333333331 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Chair>. 

Binary query : ?answer0 <http://confOf#has_short_title> ?answer1.
Number of matched answers :10
Number of correspondences found: 1
Reassessing similarity
0.9 <-> ?answer0 <http://conference#has_a_name> ?answer1.  
0.04929328621908127 <-> ?answer0 <http://conference#has_a_name> ?answer1.  

Binary query : ?answer0 <http://confOf#writes> ?answer1.
Number of matched answers :10
Number of correspondences found: 2
Reassessing similarity
0.5 <-> ?answer1 <http://conference#has_authors> ?answer0.  
0.9545454545454548 <-> ?answer0 <http://conference#contributes> ?answer1.  
0.5 <-> ?answer1 <http://conference#has_authors> ?answer0.  
0.9545454545454548 <-> ?answer0 <http://conference#contributes> ?answer1.  

Unary query : ?answer a <http://confOf#Registration_of_participants_event>.
Number of matched answers :0
Number of correspondences found: 0
Reassessing similarity

Binary query : ?answer0 <http://confOf#writtenBy> ?answer1.
Number of matched answers :10
Number of correspondences found: 2
Reassessing similarity
0.5 <-> ?answer0 <http://conference#has_authors> ?answer1.  
0.5 <-> ?answer1 <http://conference#contributes> ?answer0.  
0.5 <-> ?answer1 <http://conference#contributes> ?answer0.  
0.5 <-> ?answer0 <http://conference#has_authors> ?answer1.  

Binary query : ?answer0 <http://confOf#employedBy> ?answer1.
Number of matched answers :10
Number of correspondences found: 0
Reassessing similarity

Unary query : ?answer a <http://confOf#Administrative_event>.
Number of matched answers :0
Number of correspondences found: 0
Reassessing similarity

Unary query : ?answer a <http://confOf#Paper>.
Number of matched answers :10
Number of correspondences found: 3
Reassessing similarity
0.6 <-> <http://conference-instances#person-2116002286> <http://conference#contributes> ?answer. 
0.6 <-> ?answer <http://conference#has_authors> <http://conference-instances#person-2116002286>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Paper>. 
0.19999999999999998 <-> <http://conference-instances#person-2116002286> <http://conference#contributes> ?answer. 
0.19999999999999998 <-> ?answer <http://conference#has_authors> <http://conference-instances#person-2116002286>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Paper>. 

Binary query : ?answer0 <http://confOf#hasTitle> ?answer1.
Number of matched answers :10
Number of correspondences found: 1
Reassessing similarity
0.5 <-> ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
0.3965326447805238 <-> ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  

Unary query : ?answer a <http://confOf#Organization>.
Number of matched answers :10
Number of correspondences found: 1
Reassessing similarity
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Organization>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Organization>. 

Binary query : ?answer0 <http://confOf#hasFirstName> ?answer1.
Number of matched answers :10
Number of correspondences found: 1
Reassessing similarity
1.1666666666666665 <-> ?answer0 <http://conference#has_the_first_name> ?answer1.  
1.1666666666666665 <-> ?answer0 <http://conference#has_the_first_name> ?answer1.  

Unary query : ?answer a <http://confOf#Company>.
Number of matched answers :10
Number of correspondences found: 3
Reassessing similarity
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Organization>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Organization>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 

Binary query : ?answer0 <http://confOf#follows> ?answer1.
Number of matched answers :0
Number of correspondences found: 0
Reassessing similarity

Unary query : ?answer a <http://confOf#Administrator>.
Number of matched answers :5
Number of correspondences found: 4
Reassessing similarity
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Person>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Regular_author>. 
0.0 <-> ?answer <http://conference#contributes> <http://conference-instances#paper-3462856462702198>. 
0.0 <-> ?answer <http://conference#was_a_member_of> <http://conference-instances#pc943277065>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Person>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Regular_author>. 
0.0 <-> ?answer <http://conference#contributes> <http://conference-instances#paper-3462856462702198>. 
0.0 <-> ?answer <http://conference#was_a_member_of> <http://conference-instances#pc943277065>. 

Binary query : ?answer0 <http://confOf#location> ?answer1.
Number of matched answers :5
Number of correspondences found: 1
Reassessing similarity
1.0714285714285714 <-> ?answer0 <http://conference#has_a_location> ?answer1.  
1.0714285714285714 <-> ?answer0 <http://conference#has_a_location> ?answer1.  

Binary query : ?answer0 <http://confOf#studyAt> ?answer1.
Number of matched answers :10
Number of correspondences found: 0
Reassessing similarity

Unary query : ?answer a <http://confOf#University>.
Number of matched answers :10
Number of correspondences found: 3
Reassessing similarity
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Organization>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Organization>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> ?someObject. 

Unary query : ?answer a <http://confOf#Reviewing_results_event>.
Number of matched answers :0
Number of correspondences found: 0
Reassessing similarity

Binary query : ?answer0 <http://confOf#has_title> ?answer1.
Number of matched answers :10
Number of correspondences found: 1
Reassessing similarity
1.0 <-> ?answer0 <http://conference#has_a_name> ?answer1.  
0.13604240282685512 <-> ?answer0 <http://conference#has_a_name> ?answer1.  

Unary query : ?answer a <http://confOf#Participant>.
Number of matched answers :10
Number of correspondences found: 3
Reassessing similarity
0.5 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_participant>. 
0.5 <-> ?answer <http://conference#invited_by> ?y. ?y a <http://conference#Conference_participant>. 
0.5 <-> ?answer <http://conference#invites_co-reviewers> ?y. ?y a <http://conference#Conference_participant>. 
0.5 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_participant>. 

Unary query : ?answer a <http://confOf#Poster>.
Number of matched answers :10
Number of correspondences found: 4
Reassessing similarity
0.4 <-> ?x <http://conference#has_contributions> ?answer. ?x a <http://conference#Conference>. 
0.5277777777777778 <-> ?answer <http://conference#has_authors> ?someObject. 
0.4722222222222222 <-> ?someSubject <http://conference#contributes> ?answer. 
0.4 <-> ?answer <http://conference#is_submitted_at> ?y. ?y a <http://conference#Conference>. 
0.4722222222222222 <-> ?someSubject <http://conference#contributes> ?answer. 

Binary query : ?answer0 <http://confOf#reviewes> ?answer1.
Number of matched answers :10
Number of correspondences found: 3
Reassessing similarity
3.0 <-> ?answer0 a <http://conference#Reviewer>.  ?answer0 <http://conference#contributes> ?v1.  ?v1 a <http://conference#Review>.  ?v1 <http://conference#reviews> ?answer1.  
3.0 <-> ?answer0 a <http://conference#Reviewer>.  ?v1 <http://conference#has_authors> ?answer0.  ?v1 a <http://conference#Review>.  ?v1 <http://conference#reviews> ?answer1.  
2.125 <-> ?answer0 a <http://conference#Reviewer>.  ?v1 <http://conference#has_authors> ?answer0.  ?v1 a <http://conference#Review>.  ?answer1 <http://conference#has_a_review> ?v1.  
1.8368461147790758 <-> ?answer0 a <http://conference#Reviewer>.  ?v1 <http://conference#has_authors> ?answer0.  ?v1 a <http://conference#Review>.  ?answer1 <http://conference#has_a_review> ?v1.  
2.5931945149822244 <-> ?answer0 a <http://conference#Reviewer>.  ?answer0 <http://conference#contributes> ?v1.  ?v1 a <http://conference#Review>.  ?v1 <http://conference#reviews> ?answer1.  
2.5931945149822244 <-> ?answer0 a <http://conference#Reviewer>.  ?v1 <http://conference#has_authors> ?answer0.  ?v1 a <http://conference#Review>.  ?v1 <http://conference#reviews> ?answer1.  

Binary query : ?answer0 <http://confOf#hasKeyword> ?answer1.
Number of matched answers :10
Number of correspondences found: 0
Reassessing similarity

Unary query : ?answer a <http://confOf#Reception>.
Number of matched answers :0
Number of correspondences found: 0
Reassessing similarity

Unary query : ?answer a <http://confOf#Camera_Ready_event>.
Number of matched answers :0
Number of correspondences found: 0
Reassessing similarity

Unary query : ?answer a <http://confOf#Topic>.
Number of matched answers :10
Number of correspondences found: 1
Reassessing similarity
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Topic>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Topic>. 
Matching process ended
