===============================================================================
creating directory: confOf-ekaw
Running with 10 support instances - 0.2 similarity.
Number of CQAs: 54

Binary query : ?answer0 <http://confOf#hasSurname> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant> .
Number of matched answers :10
Number of correspondences found: 1
0.7272727272727273 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Participant>. 
creating directory: participant

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Camera_Ready_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Registration_of_participants_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Tutorial> .  ?answer0 <http://confOf#hasTopic> ?answer1 .
Number of matched answers :10
Number of correspondences found: 2
1.9642857142857149 <-> ?answer0 a <http://ekaw#Tutorial>.  ?answer1 <http://ekaw#topicCoveredBy> ?answer0.  
2.6590909090909096 <-> ?answer0 a <http://ekaw#Tutorial>.  ?answer0 <http://ekaw#coversTopic> ?answer1.  
creating directory: tutorial_topic

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 a <http://confOf#Conference>. ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Reviewing_results_event> . ?o2 <http://confOf#starts_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#earlyRegistration> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#location> ?answer1 .   ?answer0 a <http://confOf#Conference> .
Number of matched answers :5
Number of correspondences found: 2
4.145370370370371 <-> ?answer0 a <http://ekaw#Conference>.  ?answer0 <http://ekaw#heldIn> ?v1.  ?v1 a <http://ekaw#Location>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
5.09537037037037 <-> ?answer0 a <http://ekaw#Conference>.  ?v1 <http://ekaw#locationOf> ?answer0.  ?v1 a <http://ekaw#Location>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: conference_location

Unary query : ?answer <http://confOf#earlyRegistration> "false"^^<http://www.w3.org/2001/XMLSchema#boolean> .
Number of matched answers :10
Number of correspondences found: 3
0.5798319327731092 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> "Phil Bronstein". 
creating directory: late_registered_participant

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Submission_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#University> .
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#University>. 
creating directory: university

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .
Number of matched answers :5
Number of correspondences found: 1
0.2666666666666667 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Trip>. 
creating directory: trip

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#reviewes> ?answer1 .   ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .
Number of matched answers :10
Number of correspondences found: 8
3.4347338935574228 <-> ?answer0 a <http://ekaw#Possible_Reviewer>.  ?answer0 <http://ekaw#reviewerOfPaper> ?v1.  ?v1 a <http://ekaw#Accepted_Paper>.  ?v1 <http://ekaw#hasUpdatedVersion> ?answer1.  
3.4347338935574228 <-> ?answer0 a <http://ekaw#Possible_Reviewer>.  ?answer0 <http://ekaw#reviewerOfPaper> ?v1.  ?v1 a <http://ekaw#Accepted_Paper>.  ?answer1 <http://ekaw#updatedVersionOf> ?v1.  
3.4347338935574228 <-> ?answer0 a <http://ekaw#Possible_Reviewer>.  ?answer0 <http://ekaw#reviewerOfPaper> ?answer1.  ?answer1 a <http://ekaw#Accepted_Paper>.  
3.5317035905271195 <-> ?answer0 a <http://ekaw#Possible_Reviewer>.  ?answer1 <http://ekaw#hasReviewer> ?answer0.  ?answer1 a <http://ekaw#Accepted_Paper>.  
3.5317035905271195 <-> ?answer0 a <http://ekaw#Possible_Reviewer>.  ?v1 <http://ekaw#hasReviewer> ?answer0.  ?v1 a <http://ekaw#Accepted_Paper>.  ?v1 <http://ekaw#hasUpdatedVersion> ?answer1.  
3.5317035905271195 <-> ?answer0 a <http://ekaw#Possible_Reviewer>.  ?v1 <http://ekaw#hasReviewer> ?answer0.  ?v1 a <http://ekaw#Accepted_Paper>.  ?answer1 <http://ekaw#updatedVersionOf> ?v1.  
3.711519607843137 <-> ?answer0 a <http://ekaw#Possible_Reviewer>.  ?answer0 <http://ekaw#reviewerOfPaper> ?answer1.  ?answer1 a <http://ekaw#Conference_Paper>.  
3.8084893048128343 <-> ?answer0 a <http://ekaw#Possible_Reviewer>.  ?answer1 <http://ekaw#hasReviewer> ?answer0.  ?answer1 a <http://ekaw#Conference_Paper>.  
creating directory: is_assigned_reviewer_of

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Administrator> .
Number of matched answers :5
Number of correspondences found: 4
0.23076923076923073 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> "Alexis Glick". 
0.5879120879120878 <-> ?answer <http://ekaw#authorOf> ?y. ?y a <http://ekaw#Assigned_Paper>. 
0.852991452991453 <-> ?x <http://ekaw#organisedBy> ?answer. ?x a <http://ekaw#Conference>. 
0.852991452991453 <-> ?answer <http://ekaw#organises> ?y. ?y a <http://ekaw#Conference>. 
creating directory: administrator

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Camera_Ready_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> .
Number of matched answers :5
Number of correspondences found: 7
0.6219540648612515 <-> ?answer <http://ekaw#hasEvent> ?y. ?y a <http://ekaw#Scientific_Event>. 
0.6666666666666667 <-> ?x <http://ekaw#partOf> ?answer. ?x a <http://ekaw#Conference_Trip>. 
0.6666666666666667 <-> ?answer <http://ekaw#hasPart> ?y. ?y a <http://ekaw#Conference_Trip>. 
0.7506313131313131 <-> ?answer <http://ekaw#coversTopic> ?someObject. 
0.7636183261183261 <-> ?someSubject <http://ekaw#topicCoveredBy> ?answer. 
0.8622334455667789 <-> ?someSubject <http://ekaw#partOfEvent> ?answer. 
1.3703703703703702 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference>. 
creating directory: conference

Binary query : ?answer0 <http://confOf#hasFirstName> ?o1 .  ?answer0 <http://confOf#hasSurname> ?o2 .   BIND( concat(STR(?o1)," ", STR(?o2)) as ?answer1)
Number of matched answers :10
Number of correspondences found: 1
0.5 <-> ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: person_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person> .
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Person>. 
creating directory: person

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :5
Number of correspondences found: 2
3.478721948549535 <-> ?answer0 a <http://ekaw#Conference_Banquet>.  ?answer0 <http://ekaw#heldIn> ?v1.  ?v1 a <http://ekaw#Location>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
4.428721948549535 <-> ?answer0 a <http://ekaw#Conference_Banquet>.  ?v1 <http://ekaw#locationOf> ?answer0.  ?v1 a <http://ekaw#Location>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: gala_dinner_location

Binary query : ?answer0 <http://confOf#starts_on> ?answer1 .   ?answer0 a <http://confOf#Conference> .
Number of matched answers :5
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Tutorial> .
Number of matched answers :10
Number of correspondences found: 3
0.6587301587301587 <-> <http://ekaw-instances#topic-1021130956> <http://ekaw#topicCoveredBy> ?answer. 
0.6811145510835913 <-> ?answer <http://ekaw#presentationOfPaper> ?y. ?y a <http://ekaw#Tutorial_Abstract>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Tutorial>. 
creating directory: tutorial

Unary query : ?answer <http://confOf#earlyRegistration> "true"^^<http://www.w3.org/2001/XMLSchema#boolean> .
Number of matched answers :10
Number of correspondences found: 1
0.5714285714285714 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Early-Registered_Participant>. 
creating directory: early_registered_participant

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#has_title> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
2.4925925925925925 <-> ?answer0 a <http://ekaw#Conference>.  ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: conference_name

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .
Number of matched answers :10
Number of correspondences found: 9
0.6666666666666667 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#OC_Member>. 
0.6666666666666667 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member>. 
0.8711159547246583 <-> ?answer <http://ekaw#authorOf> ?y. ?y a <http://ekaw#Review>. 
0.9222222222222223 <-> ?answer <http://ekaw#organises> ?y. ?y a <http://ekaw#Conference>. 
0.9727272727272728 <-> ?x <http://ekaw#organisedBy> ?answer. ?x a <http://ekaw#Conference>. 
1.0958036210292934 <-> ?x <http://ekaw#writtenBy> ?answer. ?x a <http://ekaw#Review>. 
1.4388888888888889 <-> ?x <http://ekaw#reviewWrittenBy> ?answer. ?x a <http://ekaw#Review>. 
1.5397727272727273 <-> ?x <http://ekaw#hasReviewer> ?answer. ?x a <http://ekaw#Conference_Paper>. 
1.6125 <-> ?answer <http://ekaw#reviewerOfPaper> ?y. ?y a <http://ekaw#Conference_Paper>. 
creating directory: pc_member

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .
Number of matched answers :10
Number of correspondences found: 2
0.6136363636363636 <-> ?answer <http://ekaw#coversTopic> ?someObject. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop>. 
creating directory: workshop

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Submission_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#hasTopic> ?answer1 .  ?answer0 a  <http://confOf#Conference> .
Number of matched answers :10
Number of correspondences found: 2
2.556084656084656 <-> ?answer0 a <http://ekaw#Conference>.  ?answer1 <http://ekaw#topicCoveredBy> ?answer0.  
3.4521885521885514 <-> ?answer0 a <http://ekaw#Conference>.  ?answer0 <http://ekaw#coversTopic> ?answer1.  
creating directory: conference_topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .  ?answer <http://confOf#reviewes> ?o2 .
Number of matched answers :10
Number of correspondences found: 12
0.7400727927043715 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> ?someObject. 
0.7777777777777778 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Presenter>. 
0.8823529411764706 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Possible_Reviewer>. 
0.888888888888889 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#OC_Member>. 
0.888888888888889 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member>. 
1.3046093782105441 <-> ?answer <http://ekaw#authorOf> ?y. ?y a <http://ekaw#Review>. 
1.5176767676767677 <-> ?x <http://ekaw#organisedBy> ?answer. ?x a <http://ekaw#Event>. 
1.5277777777777777 <-> ?answer <http://ekaw#organises> ?y. ?y a <http://ekaw#Event>. 
1.8004294748074607 <-> ?x <http://ekaw#writtenBy> ?answer. ?x a <http://ekaw#Review>. 
2.3291666666666666 <-> ?answer <http://ekaw#reviewerOfPaper> ?y. ?y a <http://ekaw#Conference_Paper>. 
2.4261363636363638 <-> ?x <http://ekaw#hasReviewer> ?answer. ?x a <http://ekaw#Conference_Paper>. 
2.655555555555556 <-> ?x <http://ekaw#reviewWrittenBy> ?answer. ?x a <http://ekaw#Review>. 
creating directory: assigned_reviewer

Binary query : ?answer0 <http://confOf#writes> ?answer1 .
Number of matched answers :10
Number of correspondences found: 6
0.5 <-> ?answer0 <http://ekaw#authorOf> ?answer1.  
0.5 <-> ?answer0 <http://ekaw#authorOf> ?v1.  ?v1 <http://ekaw#hasUpdatedVersion> ?answer1.  
0.5 <-> ?answer0 <http://ekaw#authorOf> ?v1.  ?answer1 <http://ekaw#updatedVersionOf> ?v1.  
1.0555555555555556 <-> ?v1 <http://ekaw#writtenBy> ?answer0.  ?answer1 <http://ekaw#updatedVersionOf> ?v1.  
1.0555555555555556 <-> ?answer1 <http://ekaw#writtenBy> ?answer0.  
1.0555555555555556 <-> ?v1 <http://ekaw#writtenBy> ?answer0.  ?v1 <http://ekaw#hasUpdatedVersion> ?answer1.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :5
Number of correspondences found: 2
2.975 <-> ?answer0 a <http://ekaw#Conference_Trip>.  ?answer0 <http://ekaw#heldIn> ?v1.  ?v1 a <http://ekaw#Location>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
3.925 <-> ?answer0 a <http://ekaw#Conference_Trip>.  ?v1 <http://ekaw#locationOf> ?answer0.  ?v1 a <http://ekaw#Location>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: trip_location

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Topic> .
Number of matched answers :10
Number of correspondences found: 2
0.6124423115931269 <-> ?answer <http://ekaw#topicCoveredBy> ?y. ?y a <http://ekaw#Contributed_Talk>. 
0.9949086150443056 <-> ?x <http://ekaw#coversTopic> ?answer. ?x a <http://ekaw#Conference_Paper>. 
creating directory: topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar> .
Number of matched answers :10
Number of correspondences found: 4
0.2857142857142857 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?y. ?y a <http://www.w3.org/2002/07/owl#Class>. 
0.5416666666666667 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> "Steve Horvat". 
0.6449633699633701 <-> ?x <http://ekaw#writtenBy> ?answer. ?x a <http://ekaw#Rejected_Paper>. 
0.6871892987964417 <-> ?answer <http://ekaw#authorOf> ?y. ?y a <http://ekaw#Rejected_Paper>. 
creating directory: student

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#dealsWith> ?answer1 .
Number of matched answers :10
Number of correspondences found: 2
0.5 <-> ?answer1 <http://ekaw#topicCoveredBy> ?answer0.  
0.8636363636363634 <-> ?answer0 <http://ekaw#coversTopic> ?answer1.  
creating directory: topic_of_paper

Binary query : ?answer0 <http://confOf#hasFirstName> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .  ?answer0 <http://confOf#hasTopic> ?answer1 .
Number of matched answers :10
Number of correspondences found: 2
2.392857142857143 <-> ?answer0 a <http://ekaw#Workshop>.  ?answer1 <http://ekaw#topicCoveredBy> ?answer0.  ?answer1 a <http://ekaw#Research_Topic>.  
3.6655844155844166 <-> ?answer0 a <http://ekaw#Workshop>.  ?answer0 <http://ekaw#coversTopic> ?answer1.  ?answer1 a <http://ekaw#Research_Topic>.  
creating directory: workshop_topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .
Number of matched answers :5
Number of correspondences found: 1
0.38888888888888884 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Banquet>. 
creating directory: gala_dinner

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author> .
Number of matched answers :10
Number of correspondences found: 1
0.8729029502460289 <-> ?answer <http://ekaw#authorOf> ?y. ?y a <http://ekaw#Evaluated_Paper>. 
creating directory: author

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Company> .
Number of matched answers :10
Number of correspondences found: 1
0.2857142857142857 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?y. ?y a <http://www.w3.org/2002/07/owl#Class>. 
creating directory: company

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :5
Number of correspondences found: 2
3.2638888888888893 <-> ?answer0 <http://ekaw#heldIn> ?v1.  ?v1 a <http://ekaw#Location>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
4.280555555555556 <-> ?v1 <http://ekaw#locationOf> ?answer0.  ?v1 a <http://ekaw#Location>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: reception_location

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Reviewing_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Contribution> .
Number of matched answers :10
Number of correspondences found: 9
0.65 <-> ?answer <http://ekaw#writtenBy> <http://ekaw-instances#person-475684424>. 
0.65 <-> <http://ekaw-instances#person-475684424> <http://ekaw#authorOf> ?answer. 
0.7352941176470589 <-> ?x <http://ekaw#hasUpdatedVersion> ?answer. ?x a <http://ekaw#Conference_Paper>. 
0.75 <-> ?answer <http://ekaw#updatedVersionOf> ?y. ?y a <http://ekaw#Conference_Paper>. 
0.7631578947368421 <-> ?x <http://ekaw#volumeContainsPaper> ?answer. ?x a <http://ekaw#Conference_Proceedings>. 
0.8464420856691819 <-> ?answer <http://ekaw#coversTopic> ?someObject. 
0.875 <-> ?answer <http://ekaw#paperPresentedAs> ?y. ?y a <http://ekaw#Contributed_Talk>. 
1.076923076923077 <-> <http://ekaw-instances#topic1932752118> <http://ekaw#topicCoveredBy> ?answer. 
1.2960526315789473 <-> ?x <http://ekaw#presentationOfPaper> ?answer. ?x a <http://ekaw#Contributed_Talk>. 
creating directory: paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Chair_PC> .
Number of matched answers :5
Number of correspondences found: 3
0.5 <-> ?answer <http://ekaw#reviewerOfPaper> ?y. ?y a <http://ekaw#Conference_Paper>. 
0.5 <-> ?x <http://ekaw#hasReviewer> ?answer. ?x a <http://ekaw#Conference_Paper>. 
creating directory: pc_chair

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Paper> .
Number of matched answers :10
Number of correspondences found: 12
0.6118881118881119 <-> ?answer <http://ekaw#paperInVolume> ?y. ?y a <http://ekaw#Conference_Proceedings>. 
0.625 <-> ?answer <http://ekaw#paperPresentedAs> ?y. ?y a <http://ekaw#Contributed_Talk>. 
0.6758241758241759 <-> <http://ekaw-instances#topic1932752118> <http://ekaw#topicCoveredBy> ?answer. 
0.6871073324532683 <-> ?answer <http://ekaw#hasReviewer> ?y. ?y a <http://ekaw#PC_Member>. 
0.6977777124835949 <-> ?answer <http://ekaw#coversTopic> ?someObject. 
0.747845092622587 <-> ?x <http://ekaw#reviewerOfPaper> ?answer. ?x a <http://ekaw#PC_Member>. 
0.7861842105263158 <-> ?x <http://ekaw#presentationOfPaper> ?answer. ?x a <http://ekaw#Contributed_Talk>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper>. 
1.2352941176470589 <-> ?answer <http://ekaw#hasUpdatedVersion> ?y. ?y a <http://ekaw#Paper>. 
1.2352941176470589 <-> ?x <http://ekaw#hasUpdatedVersion> ?answer. ?x a <http://ekaw#Paper>. 
1.25 <-> ?x <http://ekaw#updatedVersionOf> ?answer. ?x a <http://ekaw#Paper>. 
1.25 <-> ?answer <http://ekaw#updatedVersionOf> ?y. ?y a <http://ekaw#Paper>. 
creating directory: regular_paper

Binary query : ?answer0 <http://confOf#hasTitle> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
0.75 <-> ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: paper_title

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .
Number of matched answers :5
Number of correspondences found: 2
0.888888888888889 <-> ?answer <http://ekaw#heldIn> ?y. ?y a <http://ekaw#Location>. 
0.9555555555555556 <-> ?x <http://ekaw#locationOf> ?answer. ?x a <http://ekaw#Location>. 
creating directory: reception
Matching process ended
