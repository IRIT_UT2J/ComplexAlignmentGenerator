===============================================================================
creating directory: edas-cmt
Running with 9 support instances - 0.4 similarity.
Number of CQAs: 52

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#RejectedPaper> .
Number of matched answers :9
Number of correspondences found: 3
0.6153846153846155 <-> ?someSubject <http://cmt#readPaper> ?answer. 
0.6153846153846155 <-> ?answer <http://cmt#rejectedBy> ?someObject. 
0.8461538461538461 <-> ?someSubject <http://cmt#rejectPaper> ?answer. 
creating directory: rejected_paper

Binary query : ?answer0 <http://edas#hasLastName> ?answer1 .
Number of matched answers :9
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Attendee> .
Number of matched answers :9
Number of correspondences found: 7
0.0 <-> <http://cmt-instances#paper997091241182198> <http://cmt#hasAuthor> ?answer. 
0.0 <-> ?answer <http://cmt#markConflictOfInterest> ?someObject. 
0.0 <-> <http://cmt-instances#person1851376804> <http://cmt#assignExternalReviewer> ?answer. 
0.0 <-> ?answer <http://cmt#addedBy> <http://cmt-instances#person-434905565>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#User>. 
0.0 <-> <http://cmt-instances#paper-298723072123257> <http://cmt#hasCo-author> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person>. 

Binary query : ?answer0 a <http://edas#Workshop> . ?answer0 <http://edas#hasCall> ?o2 . ?o2 a <http://edas#CallForPapers>. ?o2 <http://edas#hasSubmissionDeadline> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://edas#registrationDueOn> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reception> .   ?answer0 <http://edas#hasEndDateTime> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Excursion> .   ?answer0 <http://edas#hasEndDateTime> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceChair> .
Number of matched answers :5
Number of correspondences found: 12
0.6666666666666667 <-> ?answer <http://cmt#memberOfConference> ?y. ?y a <http://cmt#Conference>. 
0.6875 <-> ?answer <http://cmt#addedBy> ?y. ?y a <http://cmt#ConferenceMember>. 
0.6875 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.6875 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#ConferenceMember>. 
0.6875 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceMember>. 
0.6875 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.6875 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#ConferenceMember>. 
0.6875 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#ConferenceMember>. 
0.6875 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.6875 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#ConferenceMember>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceChair>. 
1.2456140350877194 <-> ?x <http://cmt#hasConferenceMember> ?answer. ?x a <http://cmt#Conference>. 
creating directory: conference_chair

Binary query : ?answer0 a <http://edas#Workshop> . ?answer0 <http://edas#hasCall> ?o2 . ?o2 a <http://edas#CallForManuscripts>. ?o2 <http://edas#hasSubmissionDeadline> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#AcademiaOrganization> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Excursion> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reception> .   ?answer0 <http://edas#hasStartDateTime> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer1 a <http://edas#PaperPresentation>. ?o3 <http://edas#relatesTo> ?answer1 . ?o3 a <http://edas#SlideSet> . ?o3 <http://edas#relatesTo> ?answer0 . ?answer0 a <http://edas#Presenter> .
Number of matched answers :9
Number of correspondences found: 0

Binary query : ?answer0 <http://edas#paperDueOn> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceDinner> .   ?answer0 <http://edas#hasStartDateTime> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Conference> .
Number of matched answers :5
Number of correspondences found: 10
0.625 <-> ?answer <http://cmt#paperAssignmentFinalizedBy> ?y. ?y a <http://cmt#ConferenceMember>. 
0.625 <-> ?answer <http://cmt#paperAssignmentToolsRunBy> ?y. ?y a <http://cmt#ConferenceMember>. 
0.625 <-> ?answer <http://cmt#virtualMeetingEnabledBy> ?y. ?y a <http://cmt#ConferenceMember>. 
0.625 <-> ?answer <http://cmt#detailsEnteredBy> ?y. ?y a <http://cmt#ConferenceMember>. 
0.6912878787878788 <-> ?x <http://cmt#runPaperAssignmentTools> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.7007575757575758 <-> ?x <http://cmt#startReviewerBidding> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.7765151515151515 <-> ?x <http://cmt#enterConferenceDetails> ?answer. ?x a <http://cmt#ConferenceMember>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Conference>. 
1.1518782744812295 <-> ?answer <http://cmt#hasConferenceMember> ?y. ?y a <http://cmt#ConferenceMember>. 
1.1811516788984664 <-> ?x <http://cmt#memberOfConference> ?answer. ?x a <http://cmt#ConferenceMember>. 
creating directory: conference

Binary query : ?answer0 <http://edas#hasFirstName> ?o1 .  ?answer0 <http://edas#hasLastName> ?o2 .   BIND( concat(STR(?o1)," ", STR(?o2)) as ?answer1)
Number of matched answers :9
Number of correspondences found: 1
0.5 <-> ?answer0 <http://cmt#name> ?answer1.  
creating directory: person_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person> .
Number of matched answers :9
Number of correspondences found: 9
1.0 <-> ?answer <http://cmt#addedBy> ?y. ?y a <http://cmt#Person>. 
1.0 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#Person>. 
1.0 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#Person>. 
1.0 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Person>. 
1.0 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Person>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person>. 
1.0 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#Person>. 
1.0 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#Person>. 
1.0 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#Person>. 
creating directory: person

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceDinner> .   ?answer0 <http://edas#hasLocation> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://edas#startDate> ?answer1 .    ?answer0 a <http://edas#Conference> .
Number of matched answers :5
Number of correspondences found: 1
1.9444444444444442 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#date> ?answer1.  
creating directory: conference_start_date

Binary query : ?answer0 <http://edas#hasName> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.0714285714285714 <-> ?answer0 <http://cmt#name> ?answer1.  
creating directory: conference_name

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Excursion> .   ?answer0 <http://edas#hasStartDateTime> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Workshop> .   ?answer0 <http://edas#hasEndDateTime> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Workshop> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://edas#manuscriptDueOn> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://edas#hasTopic> ?answer1 .  ?answer0 a <http://edas#Conference> .
Number of matched answers :9
Number of correspondences found: 7
2.125 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#detailsEnteredBy> ?v1.  ?v1 a <http://cmt#ConferenceMember>.  ?v2 <http://cmt#acceptedBy> ?v1.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
2.125 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#detailsEnteredBy> ?v1.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#acceptPaper> ?v2.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
2.125 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#detailsEnteredBy> ?v1.  ?v1 a <http://cmt#ConferenceMember>.  ?v2 <http://cmt#rejectedBy> ?v1.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
2.6805555555555554 <-> ?answer0 a <http://cmt#Conference>.  ?v1 <http://cmt#memberOfConference> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#writePaper> ?v2.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
2.6805555555555554 <-> ?answer0 a <http://cmt#Conference>.  ?v1 <http://cmt#memberOfConference> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#co-writePaper> ?v2.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
2.6805555555555554 <-> ?answer0 a <http://cmt#Conference>.  ?v1 <http://cmt#memberOfConference> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v2 <http://cmt#hasAuthor> ?v1.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
2.6805555555555554 <-> ?answer0 a <http://cmt#Conference>.  ?v1 <http://cmt#memberOfConference> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v2 <http://cmt#hasCo-author> ?v1.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
creating directory: conference_topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#AcceptedPaper> .
Number of matched answers :9
Number of correspondences found: 2
0.6153846153846155 <-> ?answer <http://cmt#acceptedBy> ?someObject. 
0.8461538461538461 <-> ?someSubject <http://cmt#acceptPaper> ?answer. 
creating directory: accepted_paper

Binary query : ?answer0 <http://edas#hasRelatedPaper> ?answer1 .
Number of matched answers :9
Number of correspondences found: 4
0.5 <-> ?answer1 <http://cmt#hasCo-author> ?answer0.  
0.5 <-> ?answer1 <http://cmt#hasAuthor> ?answer0.  
0.9666666666666667 <-> ?answer0 <http://cmt#co-writePaper> ?answer1.  
1.0333333333333332 <-> ?answer0 <http://cmt#writePaper> ?answer1.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://edas#isMemberOf> ?answer1 .
Number of matched answers :9
Number of correspondences found: 2
0.5 <-> ?answer1 <http://cmt#hasConferenceMember> ?answer0.  
0.5 <-> ?answer0 <http://cmt#memberOfConference> ?answer1.  
creating directory: member_of_conference

Binary query : ?answer0 <http://edas#endDate> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Excursion> .   ?answer0 <http://edas#hasLocation> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Topic> .
Number of matched answers :9
Number of correspondences found: 2
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> "region connection calculus (rcc)". 
0.0 <-> ?someSubject <http://cmt#hasSubjectArea> ?answer. 

Binary query : ?answer0 <http://edas#hasRelatedDocument> ?answer1 .  ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reviewer> .   ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Review> .
Number of matched answers :9
Number of correspondences found: 6
4.0 <-> ?answer0 a <http://cmt#Reviewer>.  ?answer1 <http://cmt#writtenBy> ?answer0.  ?answer1 a <http://cmt#Review>.  
4.0 <-> ?v1 <http://cmt#assignExternalReviewer> ?answer0.  ?v1 a <http://cmt#Reviewer>.  ?answer1 <http://cmt#writtenBy> ?v1.  ?answer1 a <http://cmt#Review>.  
4.444444444444445 <-> ?answer0 <http://cmt#assignedByReviewer> ?v1.  ?v1 a <http://cmt#Reviewer>.  ?answer1 <http://cmt#writtenBy> ?v1.  ?answer1 a <http://cmt#Review>.  
4.545454545454545 <-> ?v1 <http://cmt#assignExternalReviewer> ?answer0.  ?v1 a <http://cmt#Reviewer>.  ?v1 <http://cmt#writeReview> ?answer1.  ?answer1 a <http://cmt#Review>.  
4.545454545454546 <-> ?answer0 a <http://cmt#Reviewer>.  ?answer0 <http://cmt#writeReview> ?answer1.  ?answer1 a <http://cmt#Review>.  
4.98989898989899 <-> ?answer0 <http://cmt#assignedByReviewer> ?v1.  ?v1 a <http://cmt#Reviewer>.  ?v1 <http://cmt#writeReview> ?answer1.  ?answer1 a <http://cmt#Review>.  
creating directory: all_reviewer_author_of_review

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Workshop> .   ?answer0 <http://edas#hasStartDateTime> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceDinner> .   ?answer0 <http://edas#hasEndDateTime> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reviewer> .
Number of matched answers :9
Number of correspondences found: 10
0.6153846153846154 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Meta-Reviewer>. 
0.75 <-> ?answer <http://cmt#writeReview> ?y. ?y a <http://cmt#Review>. 
0.75 <-> ?x <http://cmt#writtenBy> ?answer. ?x a <http://cmt#Review>. 
0.9444444444444444 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#ExternalReviewer>. 
1.0 <-> ?answer <http://cmt#addedBy> ?y. ?y a <http://cmt#Reviewer>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer>. 
1.0 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Reviewer>. 
1.042016806722689 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#AuthorNotReviewer>. 
1.3809523809523807 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#Reviewer>. 
1.4444444444444444 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Reviewer>. 
creating directory: reviewers_all

Binary query : ?answer0 <http://edas#hasTopic> ?answer1 . ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Paper> .
Number of matched answers :9
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://cmt#Paper>.  ?answer0 <http://cmt#hasSubjectArea> ?answer1.  
creating directory: topic_of_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Review> .
Number of matched answers :9
Number of correspondences found: 3
0.75 <-> ?answer <http://cmt#writtenBy> ?y. ?y a <http://cmt#Reviewer>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Review>. 
1.2954545454545454 <-> ?x <http://cmt#writeReview> ?answer. ?x a <http://cmt#Reviewer>. 
creating directory: review

Binary query : ?answer0 <http://edas#hasFirstName> ?answer1 .
Number of matched answers :9
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceDinner> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Author> .
Number of matched answers :9
Number of correspondences found: 11
0.6666666666666667 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Co-author>. 
0.6666666666666669 <-> ?someSubject <http://cmt#hasAuthor> ?answer. 
1.0 <-> ?answer <http://cmt#addedBy> ?y. ?y a <http://cmt#Author>. 
1.0 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#Author>. 
1.0 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Author>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Author>. 
1.0 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#Author>. 
1.0 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Author>. 
1.0 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#Author>. 
1.0 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#Author>. 
1.0 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#Author>. 
creating directory: author

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#IndustryOrganization> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reception> .   ?answer0 <http://edas#hasLocation> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://edas#relatedToPaper> ?answer1 .
Number of matched answers :9
Number of correspondences found: 0

Binary query : ?answer0 a <http://edas#Workshop> . ?answer0 <http://edas#hasCall> ?o2 . ?o2 a <http://edas#CallForReviews>. ?o2 <http://edas#hasSubmissionDeadline> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Paper> .
Number of matched answers :9
Number of correspondences found: 7
0.7142857142857143 <-> ?answer <http://cmt#paperID> ?someObject. 
0.8545454545454546 <-> ?x <http://cmt#submitPaper> ?answer. ?x a <http://cmt#User>. 
0.8545454545454546 <-> ?x <http://cmt#rejectPaper> ?answer. ?x a <http://cmt#User>. 
0.8545454545454546 <-> ?x <http://cmt#acceptPaper> ?answer. ?x a <http://cmt#User>. 
0.8848484848484849 <-> ?x <http://cmt#writePaper> ?answer. ?x a <http://cmt#User>. 
0.9555555555555556 <-> ?x <http://cmt#readPaper> ?answer. ?x a <http://cmt#User>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Paper>. 
creating directory: paper

Unary query : ?answer a <http://edas#Presenter> .
Number of matched answers :9
Number of correspondences found: 5
0.4444444444444444 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Reviewer>. 
0.4444444444444444 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer>. 
0.4444444444444444 <-> ?answer <http://cmt#addedBy> ?y. ?y a <http://cmt#Reviewer>. 
0.4444444444444444 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Reviewer>. 
creating directory: presenter

Binary query : ?answer0 <http://edas#relatesTo> ?answer1 .  ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Paper> .   ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Conference> .
Number of matched answers :9
Number of correspondences found: 8
3.125 <-> ?answer0 a <http://cmt#Paper>.  ?answer0 <http://cmt#acceptedBy> ?v1.  ?v1 a <http://cmt#ConferenceMember>.  ?answer1 <http://cmt#detailsEnteredBy> ?v1.  ?answer1 a <http://cmt#Conference>.  
3.5795454545454546 <-> ?answer0 a <http://cmt#Paper>.  ?answer0 <http://cmt#acceptedBy> ?v1.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#enterConferenceDetails> ?answer1.  ?answer1 a <http://cmt#Conference>.  
3.5795454545454546 <-> ?answer0 a <http://cmt#Paper>.  ?v1 <http://cmt#rejectPaper> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?answer1 <http://cmt#detailsEnteredBy> ?v1.  ?answer1 a <http://cmt#Conference>.  
3.5795454545454546 <-> ?answer0 a <http://cmt#Paper>.  ?v1 <http://cmt#acceptPaper> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?answer1 <http://cmt#detailsEnteredBy> ?v1.  ?answer1 a <http://cmt#Conference>.  
3.6513157894736845 <-> ?answer0 a <http://cmt#Paper>.  ?answer0 <http://cmt#assignedTo> ?v1.  ?v1 a <http://cmt#ConferenceMember>.  ?answer1 <http://cmt#hasConferenceMember> ?v1.  ?answer1 a <http://cmt#Conference>.  
3.6805555555555554 <-> ?answer0 a <http://cmt#Paper>.  ?v1 <http://cmt#co-writePaper> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#memberOfConference> ?answer1.  ?answer1 a <http://cmt#Conference>.  
3.680555555555556 <-> ?answer0 a <http://cmt#Paper>.  ?answer0 <http://cmt#assignedTo> ?v1.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#memberOfConference> ?answer1.  ?answer1 a <http://cmt#Conference>.  
3.680555555555556 <-> ?answer0 a <http://cmt#Paper>.  ?v1 <http://cmt#hasBeenAssigned> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#memberOfConference> ?answer1.  ?answer1 a <http://cmt#Conference>.  
creating directory: paper_submitted_at_conference

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Paper> .   ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.
Number of matched answers :9
Number of correspondences found: 1
1.9000000000000001 <-> ?answer0 a <http://cmt#Paper>.  ?answer0 <http://cmt#title> ?answer1.  
creating directory: paper_title

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#PaperPresentation> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reception> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 a <http://edas#PaperPresentation> . ?answer0 <http://edas#relatedToPaper> ?o2 . ?answer1 <http://edas#hasRelatedDocument> ?o2 . ?answer1 a <http://edas#Conference> .
Number of matched answers :9
Number of correspondences found: 0
Matching process ended
