===============================================================================
creating directory: ekaw-conference
Running with 10 support instances - 0.9 similarity.
Number of CQAs: 65

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Proceedings_Publisher> .
Number of matched answers :5
Number of correspondences found: 4
0.0 <-> <http://conference-instances#proceedings-1409861683> <http://conference#has_a_publisher> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://conference#issues> <http://conference-instances#proceedings69622803>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Publisher>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Rejected_Paper> .
Number of matched answers :10
Number of correspondences found: 4
0.0 <-> ?someSubject <http://conference#contributes> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Rejected_contribution>. 
0.0 <-> ?answer <http://conference#has_a_review> ?someObject. 
0.0 <-> ?answer <http://conference#has_authors> <http://conference-instances#person1087422646>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Track> .
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Track>. 
creating directory: track

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Participant> .   {?answer a <http://ekaw#Early-Registered_Participant> . } union{ ?answer a <http://ekaw#Late-Registered_Participant> . }
Number of matched answers :10
Number of correspondences found: 5
1.0 <-> ?x <http://conference#invites_co-reviewers> ?answer. ?x a <http://conference#Conference_participant>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_participant>. 
1.0 <-> ?answer <http://conference#invites_co-reviewers> ?y. ?y a <http://conference#Conference_participant>. 
1.0 <-> ?x <http://conference#invited_by> ?answer. ?x a <http://conference#Conference_participant>. 
1.0 <-> ?answer <http://conference#invited_by> ?y. ?y a <http://conference#Conference_participant>. 
creating directory: participant

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#OC_Member> .
Number of matched answers :10
Number of correspondences found: 7
0.0 <-> ?answer <http://conference#contributes> ?someObject. 
0.0 <-> <http://conference-instances#workshop-743781487> <http://conference#has_a_track-workshop-tutorial_chair> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Organizer>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Committee_member>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Paid_applicant>. 
0.0 <-> ?answer <http://conference#was_a_committee_chair_of> <http://conference-instances#oc-1409861683>. 
0.0 <-> <http://conference-instances#paper9877661704621114> <http://conference#has_authors> ?answer. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Proceedings> .   ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://conference#Conference_proceedings>.  ?answer0 <http://conference#has_a_name> ?answer1.  
creating directory: conference_proceedings_name

Binary query : ?answer1 <http://ekaw#hasEvent> ?answer0 .  ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .  ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop> .
Number of matched answers :10
Number of correspondences found: 2
1.5 <-> ?answer1 <http://conference#has_parts> ?answer0.  ?answer1 a <http://conference#Conference>.  
1.5 <-> ?answer0 <http://conference#is_part_of_conference_volumes> ?answer1.  ?answer1 a <http://conference#Conference>.  
creating directory: workshop_of_conference

Binary query : ?answer0 <http://ekaw#reviewOfPaper> ?answer1 .
Number of matched answers :10
Number of correspondences found: 2
0.5 <-> ?answer1 <http://conference#has_a_review> ?answer0.  
0.5 <-> ?answer0 <http://conference#reviews> ?answer1.  
creating directory: review_of_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Tutorial> .  ?answer0 <http://ekaw#coversTopic> ?answer1 .
Number of matched answers :10
Number of correspondences found: 2
1.5 <-> ?answer0 a <http://conference#Tutorial>.  ?answer0 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1.  
1.5 <-> ?answer0 a <http://conference#Tutorial>.  ?answer1 <http://conference#is_a_topis_of_conference_parts> ?answer0.  
creating directory: tutorial_topic

Binary query : ?answer0 a <http://ekaw#Conference_Participant>.      bind( if (exists {?answer0 a <http://ekaw#Early-Registered_Participant>}, "true"^^<http://www.w3.org/2001/XMLSchema#boolean>,        if (exists {?answer0 a <http://ekaw#Late-Registered_Participant>}, "false"^^<http://www.w3.org/2001/XMLSchema#boolean>,"")) as ?answer1) filter(?answer1 != "")
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer1 <http://ekaw#hasEvent> ?answer0 .  ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .  ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Tutorial> .
Number of matched answers :10
Number of correspondences found: 2
1.5 <-> ?answer0 <http://conference#is_part_of_conference_volumes> ?answer1.  ?answer1 a <http://conference#Conference>.  
1.5 <-> ?answer1 <http://conference#has_parts> ?answer0.  ?answer1 a <http://conference#Conference>.  
creating directory: tutorial_of_conference

Binary query : ?answer0 a <http://ekaw#Conference> . ?answer0 <http://ekaw#heldIn> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://conference#Conference>.  ?answer0 <http://conference#has_a_location> ?answer1.  
creating directory: conference_location

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Late-Registered_Participant> .
Number of matched answers :10
Number of correspondences found: 7
0.0 <-> ?someSubject <http://conference#has_a_track-workshop-tutorial_chair> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Passive_conference_participant>. 
0.0 <-> ?someSubject <http://conference#has_authors> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Person>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Contribution_co-author>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_applicant>. 
0.0 <-> ?answer <http://conference#was_a_committee_chair_of> <http://conference-instances#oc-1409861683>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#University> .
Number of matched answers :10
Number of correspondences found: 3
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Organization>. 
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> ?someObject. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Trip> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 a <http://ekaw#Invited_Speaker>. ?answer1 <http://ekaw#writtenBy> ?answer0. ?answer1 a <http://ekaw#Invited_Talk_Abstract>.
Number of matched answers :10
Number of correspondences found: 2
1.5 <-> ?answer0 a <http://conference#Invited_speaker>.  ?answer0 <http://conference#contributes> ?answer1.  
1.5 <-> ?answer0 a <http://conference#Invited_speaker>.  ?answer1 <http://conference#has_authors> ?answer0.  
creating directory: author_of_inv_talk_abstract

Binary query : ?answer0 a <http://ekaw#Invited_Speaker>. ?o2 <http://ekaw#writtenBy> ?answer0. ?o2 <http://ekaw#paperPresentedAs> ?answer1. ?answer1 a <http://ekaw#Invited_Talk>.
Number of matched answers :10
Number of correspondences found: 2
2.5 <-> ?answer0 a <http://conference#Invited_speaker>.  ?answer0 <http://conference#contributes> ?answer1.  ?answer1 a <http://conference#Invited_talk>.  
2.5 <-> ?answer0 a <http://conference#Invited_speaker>.  ?answer1 <http://conference#has_authors> ?answer0.  ?answer1 a <http://conference#Invited_talk>.  
creating directory: invited_speaker_of_talk

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member> .   ?answer0 <http://ekaw#reviewerOfPaper> ?answer1 .
Number of matched answers :10
Number of correspondences found: 3
0.5 <-> ?v1 <http://conference#has_authors> ?answer0.  ?answer1 <http://conference#has_a_review> ?v1.  
0.5 <-> ?answer0 <http://conference#contributes> ?v1.  ?v1 <http://conference#reviews> ?answer1.  
0.5 <-> ?v1 <http://conference#has_authors> ?answer0.  ?v1 <http://conference#reviews> ?answer1.  
creating directory: is_assigned_reviewer_of

Unary query : ?o3 <http://ekaw#reviewWrittenBy> ?answer. MINUS{ ?answer <http://ekaw#reviewerOfPaper> ?o2. }
Number of matched answers :10
Number of correspondences found: 6
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer>. 
0.0 <-> ?answer <http://conference#has_the_last_name> ?someObject. 
0.0 <-> ?answer <http://conference#contributes> <http://conference-instances#abstr-305185392452198>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Contribution_co-author>. 
0.0 <-> ?someSubject <http://conference#has_authors> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Regular_author>. 

Binary query : ?answer1 <http://ekaw#reviewWrittenBy> ?answer0 .   ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member> .  ?answer0 <http://ekaw#reviewerOfPaper> ?o3.
Number of matched answers :10
Number of correspondences found: 2
0.5 <-> ?answer1 <http://conference#has_authors> ?answer0.  
0.5 <-> ?answer0 <http://conference#contributes> ?answer1.  
creating directory: assigned_reviewer_author_of_review

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Track> .   ?answer1 <http://ekaw#hasEvent> ?answer0 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .
Number of matched answers :10
Number of correspondences found: 2
2.5 <-> ?answer0 a <http://conference#Track>.  ?answer0 <http://conference#is_part_of_conference_volumes> ?answer1.  ?answer1 a <http://conference#Conference>.  
2.5 <-> ?answer0 a <http://conference#Track>.  ?answer1 <http://conference#has_parts> ?answer0.  ?answer1 a <http://conference#Conference>.  
creating directory: track_of_conference

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop_Chair> .   ?answer0 <http://ekaw#organises> ?answer1 . ?answer1 a <http://ekaw#Workshop> .
Number of matched answers :10
Number of correspondences found: 2
1.5 <-> ?answer0 <http://conference#was_a_track-workshop_chair_of> ?answer1.  ?answer1 a <http://conference#Workshop>.  
1.5 <-> ?answer1 <http://conference#has_a_track-workshop-tutorial_chair> ?answer0.  ?answer1 a <http://conference#Workshop>.  
creating directory: chair_of_workshop

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .
Number of matched answers :5
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference>. 
creating directory: conference

Binary query : ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .   ?answer0 a <http://ekaw#Person> .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Person> .
Number of matched answers :10
Number of correspondences found: 5
1.0 <-> ?x <http://conference#invited_by> ?answer. ?x a <http://conference#Person>. 
1.0 <-> ?x <http://conference#invites_co-reviewers> ?answer. ?x a <http://conference#Person>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Person>. 
1.0 <-> ?answer <http://conference#invites_co-reviewers> ?y. ?y a <http://conference#Person>. 
1.0 <-> ?answer <http://conference#invited_by> ?y. ?y a <http://conference#Person>. 
creating directory: person

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Banquet> .   ?answer0 <http://ekaw#heldIn> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Proceedings> .
Number of matched answers :5
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_proceedings>. 
creating directory: conference_proceedings

Binary query : ?answer1 <http://ekaw#publisherOf> ?answer0 .  ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Proceedings> .
Number of matched answers :5
Number of correspondences found: 2
1.5 <-> ?answer0 a <http://conference#Conference_proceedings>.  ?answer0 <http://conference#has_a_publisher> ?answer1.  
1.5 <-> ?answer0 a <http://conference#Conference_proceedings>.  ?answer1 <http://conference#issues> ?answer0.  
creating directory: proceedings_publisher

Binary query : ?o3 <http://ekaw#reviewWrittenBy> ?answer0 .   ?o3 <http://ekaw#reviewWrittenBy> ?answer1 . ?answer1 <http://ekaw#reviewerOfPaper> ?o2 . ?o3 <http://ekaw#reviewOfPaper> ?o2 . FILTER(?o!=?answer0)
Number of matched answers :10
Number of correspondences found: 2
0.5 <-> ?answer0 <http://conference#invited_by> ?answer1.  
0.5 <-> ?answer1 <http://conference#invites_co-reviewers> ?answer0.  
creating directory: invite_reviewer

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Tutorial> .
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Tutorial>. 
creating directory: tutorial

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Early-Registered_Participant> .
Number of matched answers :10
Number of correspondences found: 6
0.0 <-> ?answer <http://conference#has_the_first_name> "Akeem". 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Committee_member>. 
0.0 <-> ?answer <http://conference#contributes> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Contribution_co-author>. 
0.0 <-> <http://conference-instances#pc-1409861683> <http://conference#has_members> ?answer. 
0.0 <-> ?answer <http://conference#has_the_last_name> ?someObject. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .  ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://conference#Conference>.  ?answer0 <http://conference#has_a_name> ?answer1.  
creating directory: conference_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member> .   ?answer <http://ekaw#reviewerOfPaper> ?o2.
Number of matched answers :10
Number of correspondences found: 7
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Committee_member>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_contributor>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Contribution_1th-author>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Person>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Registeered_applicant>. 
0.0 <-> <http://conference-instances#pc-1409861683> <http://conference#has_members> ?answer. 
0.0 <-> ?answer <http://conference#contributes> ?someObject. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop> .
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop>. 
creating directory: workshop

Binary query : ?answer0 <http://ekaw#coversTopic> ?answer1 .   ?answer0 a <http://ekaw#Conference> .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member> .   ?answer <http://ekaw#reviewerOfPaper> ?o3.
Number of matched answers :10
Number of correspondences found: 7
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Committee_member>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_contributor>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Contribution_1th-author>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Person>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Registeered_applicant>. 
0.0 <-> <http://conference-instances#pc-1409861683> <http://conference#has_members> ?answer. 
0.0 <-> ?answer <http://conference#contributes> ?someObject. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Accepted_Paper> .
Number of matched answers :10
Number of correspondences found: 8
0.0 <-> ?someSubject <http://conference#reviews> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution>. 
0.0 <-> <http://conference-instances#abstr2891462627831114> <http://conference#is_the_1th_part_of> ?answer. 
0.0 <-> ?answer <http://conference#has_a_review> <http://conference-instances#review6374187426251114_-196548213>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Accepted_contribution>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Submitted_contribution>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Camera_ready_contribution>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Paper>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Web_Site> .
Number of matched answers :5
Number of correspondences found: 4
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> "Website of Workshop on Self-Organizing Maps (WSOM)". 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_www>. 
0.0 <-> ?answer <http://conference#has_a_URL> <http://dbpedia.org/resource/Duck>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop_Chair> .
Number of matched answers :10
Number of correspondences found: 9
0.0 <-> ?someSubject <http://conference#has_a_track-workshop-tutorial_chair> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Regular_author>. 
0.0 <-> <http://conference-instances#review-295779770244257_-196548213> <http://conference#has_authors> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_applicant>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Contribution_co-author>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_contributor>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Thing>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Person>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Invited_Talk_Abstract> .
Number of matched answers :10
Number of correspondences found: 6
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Regular_contribution>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_contribution>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_document>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Extended_abstract>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Invited_Speaker> .
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Invited_speaker>. 
creating directory: invited_speaker

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper_Author> . ?answer0 <http://ekaw#authorOf> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .
Number of matched answers :10
Number of correspondences found: 4
0.5 <-> ?answer1 <http://conference#has_authors> ?answer0.  
0.5 <-> ?answer0 <http://conference#contributes> ?answer1.  
1.5 <-> ?answer1 <http://conference#has_authors> ?answer0.  ?answer1 a <http://conference#Paper>.  
1.5 <-> ?answer0 <http://conference#contributes> ?answer1.  ?answer1 a <http://conference#Paper>.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Trip> .   ?answer0 <http://ekaw#heldIn> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Research_Topic> .
Number of matched answers :10
Number of correspondences found: 4
0.0 <-> ?answer <http://conference#has_a_name> "probabilistic retrieval". 
0.0 <-> ?answer <http://conference#is_a_topis_of_conference_parts> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Topic>. 

Binary query : ?answer0 a <http://ekaw#Invited_Talk_Abstract>. ?answer0 <http://ekaw#paperPresentedAs> ?answer1. ?answer1 a <http://ekaw#Invited_Talk>.
Number of matched answers :10
Number of correspondences found: 2
1.5 <-> ?answer1 <http://conference#has_an_abstract> ?answer0.  ?answer1 a <http://conference#Invited_talk>.  
1.5 <-> ?answer0 <http://conference#is_the_1th_part_of> ?answer1.  ?answer1 a <http://conference#Invited_talk>.  
creating directory: abstract_of_invited_talk

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Student> .
Number of matched answers :10
Number of correspondences found: 7
0.0 <-> ?answer <http://conference#has_the_first_name> "Akeem". 
0.0 <-> ?answer <http://conference#contributes> <http://conference-instances#paper-12851958061401263>. 
0.0 <-> ?someSubject <http://conference#has_authors> ?answer. 
0.0 <-> <http://conference-instances#person259360223> <http://conference#invites_co-reviewers> ?answer. 
0.0 <-> ?answer <http://conference#has_the_last_name> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Person>. 

Unary query : ?o2 a <http://ekaw#Web_Site>. ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer. filter(regex(?s,"^http"))
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#SC_Member> .
Number of matched answers :10
Number of correspondences found: 7
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer>. 
0.0 <-> ?someSubject <http://conference#has_authors> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Organizer>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?someSubject <http://conference#has_members> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Contribution_co-author>. 
0.0 <-> ?answer <http://conference#contributes> <http://conference-instances#paper-261363742211311>. 

Binary query : ?answer1 <http://ekaw#reviewWrittenBy> ?answer0 .
Number of matched answers :10
Number of correspondences found: 2
0.5 <-> ?answer0 <http://conference#contributes> ?answer1.  
0.5 <-> ?answer1 <http://conference#has_authors> ?answer0.  
creating directory: all_reviewer_author_of_review

Unary query : {?answer <http://ekaw#reviewerOfPaper> ?o2 . } union{ ?o3 <http://ekaw#reviewOfPaper> ?o2 .  ?o3 <http://ekaw#reviewWrittenBy> ?answer .  }
Number of matched answers :10
Number of correspondences found: 7
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Committee_member>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Contribution_1th-author>. 
0.0 <-> <http://conference-instances#pc-1409861683> <http://conference#has_members> ?answer. 
0.0 <-> ?answer <http://conference#contributes> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Regular_author>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Person>. 

Binary query : ?answer0 <http://ekaw#coversTopic> ?answer1 . ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Review> .
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Review>. 
creating directory: review

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop> .  ?answer0 <http://ekaw#coversTopic> ?answer1 .
Number of matched answers :10
Number of correspondences found: 2
1.5 <-> ?answer0 a <http://conference#Workshop>.  ?answer0 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1.  
1.5 <-> ?answer0 a <http://conference#Workshop>.  ?answer1 <http://conference#is_a_topis_of_conference_parts> ?answer0.  
creating directory: workshop_topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Banquet> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer1 a <http://ekaw#Conference> . ?o2 <http://ekaw#listsEvent> ?answer1 . ?o2 a <http://ekaw#Web_Site> . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer0 . filter(regex(?s,"^http"))
Number of matched answers :5
Number of correspondences found: 0

Unary query : ?answer a <http://ekaw#Invited_Talk> .
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Invited_talk>. 
creating directory: invited_talk

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper_Author> .
Number of matched answers :10
Number of correspondences found: 6
0.0 <-> ?answer <http://conference#has_the_last_name> ?someObject. 
0.0 <-> ?answer <http://conference#contributes> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_contributor>. 
0.0 <-> ?someSubject <http://conference#has_authors> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Contribution_co-author>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Person>. 

Binary query : ?answer0 <http://ekaw#presentationOfPaper> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .
Number of matched answers :10
Number of correspondences found: 8
0.5 <-> ?v1 <http://conference#gives_presentations> ?answer0.  ?answer1 <http://conference#has_authors> ?v1.  
0.5 <-> ?v1 <http://conference#gives_presentations> ?answer0.  ?v1 <http://conference#contributes> ?answer1.  
0.5 <-> ?answer0 <http://conference#is_given_by> ?v1.  ?v1 <http://conference#contributes> ?answer1.  
0.5 <-> ?v1 <http://conference#is_the_1th_part_of> ?answer0.  ?v1 <http://conference#is_the_1th_part_of> ?answer1.  
1.5 <-> ?v1 <http://conference#gives_presentations> ?answer0.  ?answer1 <http://conference#has_authors> ?v1.  ?answer1 a <http://conference#Paper>.  
1.5 <-> ?v1 <http://conference#gives_presentations> ?answer0.  ?v1 <http://conference#contributes> ?answer1.  ?answer1 a <http://conference#Paper>.  
1.5 <-> ?answer0 <http://conference#is_given_by> ?v1.  ?v1 <http://conference#contributes> ?answer1.  ?answer1 a <http://conference#Paper>.  
1.5 <-> ?v1 <http://conference#is_the_1th_part_of> ?answer0.  ?v1 <http://conference#is_the_1th_part_of> ?answer1.  ?answer1 a <http://conference#Paper>.  
creating directory: presentation_of_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#OC_Chair> .
Number of matched answers :5
Number of correspondences found: 4
0.0 <-> ?answer <http://conference#contributes> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_applicant>. 
0.0 <-> ?answer <http://conference#was_a_committee_chair_of> <http://conference-instances#oc-1409861683>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Thing>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .
Number of matched answers :10
Number of correspondences found: 5
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Rejected_contribution>. 
0.0 <-> ?answer <http://conference#has_a_review> <http://conference-instances#review-5993335381051263_-196548212>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Accepted_contribution>. 
0.0 <-> <http://conference-instances#person-230339663> <http://conference#contributes> ?answer. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Paper>. 
creating directory: paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Chair> .
Number of matched answers :5
Number of correspondences found: 5
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Person>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Chair>. 
0.0 <-> ?answer <http://conference#contributes> <http://conference-instances#review-1657941092681263_-196548216>. 
0.0 <-> <http://conference-instances#review100134572362198_-196548216> <http://conference#has_authors> ?answer. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Regular_Paper> .
Number of matched answers :10
Number of correspondences found: 5
0.0 <-> ?someSubject <http://conference#contributes> ?answer. 
0.0 <-> ?answer <http://conference#has_a_review> ?someObject. 
0.0 <-> ?someSubject <http://conference#reviews> ?answer. 
0.0 <-> ?answer <http://conference#has_authors> <http://conference-instances#person1087422646>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Paper>. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .   ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.
Number of matched answers :10
Number of correspondences found: 2
1.5 <-> ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
2.5 <-> ?answer0 a <http://conference#Paper>.  ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: paper_title

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Contributed_Talk> .
Number of matched answers :10
Number of correspondences found: 6
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_contribution>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> "Presentation of Measurement of hadron and lepton-pair production at 130 GeV sqrt {s} 140 GeV at LEP". 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_document>. 
0.0 <-> ?someSubject <http://conference#gives_presentations> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Presentation>. 

Binary query : ?answer0 a <http://ekaw#Contributed_Talk>. ?answer1 a <http://ekaw#Conference> . ?answer0 <http://ekaw#partOfEvent> ?answer1.
Number of matched answers :10
Number of correspondences found: 7
1.5 <-> ?answer0 <http://conference#is_given_by> ?v1.  ?v1 <http://conference#contributes> ?v2.  ?v2 <http://conference#is_submitted_at> ?answer1.  ?answer1 a <http://conference#Conference>.  
1.5 <-> ?v1 <http://conference#gives_presentations> ?answer0.  ?v2 <http://conference#has_authors> ?v1.  ?v2 <http://conference#is_submitted_at> ?answer1.  ?answer1 a <http://conference#Conference>.  
1.5 <-> ?v1 <http://conference#gives_presentations> ?answer0.  ?v2 <http://conference#has_members> ?v1.  ?answer1 <http://conference#has_a_commtitee> ?v2.  ?answer1 a <http://conference#Conference>.  
1.5 <-> ?v1 <http://conference#is_the_1th_part_of> ?answer0.  ?v1 <http://conference#is_the_1th_part_of> ?v2.  ?v2 <http://conference#is_submitted_at> ?answer1.  ?answer1 a <http://conference#Conference>.  
1.5 <-> ?v1 <http://conference#gives_presentations> ?answer0.  ?v2 <http://conference#has_authors> ?v1.  ?answer1 <http://conference#has_contributions> ?v2.  ?answer1 a <http://conference#Conference>.  
1.5 <-> ?v1 <http://conference#is_the_1th_part_of> ?answer0.  ?v2 <http://conference#has_an_abstract> ?v1.  ?v2 <http://conference#is_submitted_at> ?answer1.  ?answer1 a <http://conference#Conference>.  
1.5 <-> ?v1 <http://conference#gives_presentations> ?answer0.  ?v1 <http://conference#contributes> ?v2.  ?v2 <http://conference#is_submitted_at> ?answer1.  ?answer1 a <http://conference#Conference>.  
creating directory: presentation_in_conference
Matching process ended
