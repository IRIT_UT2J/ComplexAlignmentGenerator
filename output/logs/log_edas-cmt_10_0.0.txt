===============================================================================
creating directory: edas-cmt
Running with 10 support instances - 0.0 similarity.
Number of CQAs: 52

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#RejectedPaper> .
Number of matched answers :10
Number of correspondences found: 11
0.6009615384615384 <-> ?answer <http://cmt#hasCo-author> <http://cmt-instances#person1979886470>. 
0.6399267399267399 <-> ?answer <http://cmt#hasSubjectArea> ?someObject. 
0.6492286048207218 <-> ?x <http://cmt#markConflictOfInterest> ?answer. ?x a <http://cmt#Reviewer>. 
0.7055424902754623 <-> ?answer <http://cmt#readByMeta-Reviewer> ?y. ?y a <http://cmt#Reviewer>. 
0.7417582417582417 <-> ?answer <http://cmt#readByReviewer> ?y. ?y a <http://cmt#Reviewer>. 
0.8151091848188294 <-> ?x <http://cmt#co-writePaper> ?answer. ?x a <http://cmt#ExternalReviewer>. 
0.8310557874511362 <-> ?x <http://cmt#submitPaper> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.8713849135360763 <-> ?x <http://cmt#writePaper> ?answer. ?x a <http://cmt#Reviewer>. 
0.9405353475120916 <-> ?answer <http://cmt#rejectedBy> ?y. ?y a <http://cmt#ExternalReviewer>. 
1.0 <-> ?x <http://cmt#readPaper> ?answer. ?x a <http://cmt#Reviewer>. 
1.1648021019677999 <-> ?x <http://cmt#rejectPaper> ?answer. ?x a <http://cmt#ExternalReviewer>. 
creating directory: rejected_paper

Binary query : ?answer0 <http://edas#hasLastName> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Attendee> .
Number of matched answers :10
Number of correspondences found: 10
0.3214285714285714 <-> ?answer <http://cmt#name> ?someObject. 
0.5350121627598409 <-> ?someSubject <http://cmt#hasConferenceMember> ?answer. 
0.6025 <-> <http://cmt-instances#person-434905565> <http://cmt#addProgramCommitteeMember> ?answer. 
0.6060606060606061 <-> ?x <http://cmt#writtenBy> ?answer. ?x a <http://cmt#Meta-Review>. 
0.6158422459893047 <-> ?someSubject <http://cmt#assignExternalReviewer> ?answer. 
0.6208927510398098 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#ProgramCommitteeMember>. 
0.6434348739495799 <-> ?someSubject <http://cmt#assignReviewer> ?answer. 
0.665921255746837 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#ExternalReviewer>. 
0.6673680324843115 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#ExternalReviewer>. 
0.7327205882352942 <-> ?answer <http://cmt#addedBy> ?someObject. 
creating directory: participant

Binary query : ?answer0 a <http://edas#Workshop> . ?answer0 <http://edas#hasCall> ?o2 . ?o2 a <http://edas#CallForPapers>. ?o2 <http://edas#hasSubmissionDeadline> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://edas#registrationDueOn> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reception> .   ?answer0 <http://edas#hasEndDateTime> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Excursion> .   ?answer0 <http://edas#hasEndDateTime> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceChair> .
Number of matched answers :5
Number of correspondences found: 12
0.6875 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceMember>. 
0.7899154589371981 <-> ?answer <http://cmt#addedBy> ?y. ?y a <http://cmt#ConferenceMember>. 
0.8475 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.8541666666666666 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.8541666666666666 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#ConferenceMember>. 
0.8614130434782609 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#ConferenceMember>. 
0.8741666666666666 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.9147727272727273 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#ConferenceMember>. 
0.9147727272727273 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.9479467126264843 <-> ?answer <http://cmt#memberOfConference> ?y. ?y a <http://cmt#Conference>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceChair>. 
1.2494898622831407 <-> ?x <http://cmt#hasConferenceMember> ?answer. ?x a <http://cmt#Conference>. 
creating directory: conference_chair

Binary query : ?answer0 a <http://edas#Workshop> . ?answer0 <http://edas#hasCall> ?o2 . ?o2 a <http://edas#CallForManuscripts>. ?o2 <http://edas#hasSubmissionDeadline> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#AcademiaOrganization> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Excursion> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reception> .   ?answer0 <http://edas#hasStartDateTime> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer1 a <http://edas#PaperPresentation>. ?o3 <http://edas#relatesTo> ?answer1 . ?o3 a <http://edas#SlideSet> . ?o3 <http://edas#relatesTo> ?answer0 . ?answer0 a <http://edas#Presenter> .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://edas#paperDueOn> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceDinner> .   ?answer0 <http://edas#hasStartDateTime> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Conference> .
Number of matched answers :5
Number of correspondences found: 11
0.8167799451454343 <-> ?answer <http://cmt#paperAssignmentFinalizedBy> ?y. ?y a <http://cmt#ConferenceMember>. 
0.8315789473684211 <-> ?x <http://cmt#enableVirtualMeeting> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.8339789196310935 <-> ?answer <http://cmt#paperAssignmentToolsRunBy> ?y. ?y a <http://cmt#ConferenceMember>. 
0.8528326745718049 <-> ?answer <http://cmt#virtualMeetingEnabledBy> ?y. ?y a <http://cmt#ConferenceMember>. 
0.8538108114005201 <-> ?x <http://cmt#runPaperAssignmentTools> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.8538376146854803 <-> ?x <http://cmt#startReviewerBidding> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.8712121212121213 <-> ?x <http://cmt#enterConferenceDetails> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.8760979358805446 <-> ?answer <http://cmt#detailsEnteredBy> ?y. ?y a <http://cmt#ConferenceMember>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Conference>. 
1.1518782744812295 <-> ?answer <http://cmt#hasConferenceMember> ?y. ?y a <http://cmt#ConferenceMember>. 
1.1811516788984664 <-> ?x <http://cmt#memberOfConference> ?answer. ?x a <http://cmt#ConferenceMember>. 
creating directory: conference

Binary query : ?answer0 <http://edas#hasFirstName> ?o1 .  ?answer0 <http://edas#hasLastName> ?o2 .   BIND( concat(STR(?o1)," ", STR(?o2)) as ?answer1)
Number of matched answers :10
Number of correspondences found: 2
1.196969696969697 <-> ?answer0 <http://cmt#name> ?answer1.  
1.7333333333333334 <-> ?answer0 a <http://cmt#ProgramCommitteeMember>.  ?answer0 <http://cmt#name> ?answer1.  
creating directory: person_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person> .
Number of matched answers :10
Number of correspondences found: 9
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person>. 
1.065217391304348 <-> ?answer <http://cmt#addedBy> ?y. ?y a <http://cmt#Person>. 
1.0876190476190477 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#Person>. 
1.0909090909090908 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#Person>. 
1.0909090909090908 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Person>. 
1.1111111111111112 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Person>. 
1.1111111111111112 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#Person>. 
1.12 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#Person>. 
1.1304347826086958 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#Person>. 
creating directory: person

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceDinner> .   ?answer0 <http://edas#hasLocation> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://edas#startDate> ?answer1 .    ?answer0 a <http://edas#Conference> .
Number of matched answers :5
Number of correspondences found: 1
2.611111111111111 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#date> ?answer1.  
creating directory: conference_start_date

Binary query : ?answer0 <http://edas#hasName> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.8714285714285714 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#name> ?answer1.  
creating directory: conference_name

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Excursion> .   ?answer0 <http://edas#hasStartDateTime> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Workshop> .   ?answer0 <http://edas#hasEndDateTime> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Workshop> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://edas#manuscriptDueOn> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://edas#hasTopic> ?answer1 .  ?answer0 a <http://edas#Conference> .
Number of matched answers :10
Number of correspondences found: 10
3.3535714285714286 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#detailsEnteredBy> ?v1.  ?v1 a <http://cmt#ConferenceMember>.  ?v2 <http://cmt#rejectedBy> ?v1.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
3.3535714285714286 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#detailsEnteredBy> ?v1.  ?v1 a <http://cmt#ConferenceMember>.  ?v2 <http://cmt#acceptedBy> ?v1.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
3.4262987012987014 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#detailsEnteredBy> ?v1.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#rejectPaper> ?v2.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
3.4262987012987014 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#detailsEnteredBy> ?v1.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#acceptPaper> ?v2.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
3.5827380952380956 <-> ?answer0 a <http://cmt#Conference>.  ?v1 <http://cmt#memberOfConference> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v2 <http://cmt#assignedTo> ?v1.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
3.6827380952380953 <-> ?answer0 a <http://cmt#Conference>.  ?v1 <http://cmt#memberOfConference> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#writePaper> ?v2.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
3.7160714285714285 <-> ?answer0 a <http://cmt#Conference>.  ?v1 <http://cmt#memberOfConference> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v2 <http://cmt#hasAuthor> ?v1.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
3.799404761904762 <-> ?answer0 a <http://cmt#Conference>.  ?v1 <http://cmt#memberOfConference> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v2 <http://cmt#hasCo-author> ?v1.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
3.8442765567765567 <-> ?answer0 a <http://cmt#Conference>.  ?v1 <http://cmt#memberOfConference> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#co-writePaper> ?v2.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
3.8494047619047618 <-> ?answer0 a <http://cmt#Conference>.  ?v1 <http://cmt#memberOfConference> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#hasBeenAssigned> ?v2.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
creating directory: conference_topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#AcceptedPaper> .
Number of matched answers :10
Number of correspondences found: 11
0.610576923076923 <-> ?answer <http://cmt#assignedTo> ?someObject. 
0.6153846153846153 <-> ?answer <http://cmt#hasDecision> ?y. ?y a <http://cmt#Acceptance>. 
0.6267942583732058 <-> ?answer <http://cmt#readByMeta-Reviewer> ?y. ?y a <http://cmt#ProgramCommitteeMember>. 
0.6421469086958939 <-> ?x <http://cmt#markConflictOfInterest> ?answer. ?x a <http://cmt#ProgramCommitteeMember>. 
0.6617408198097052 <-> ?answer <http://cmt#hasSubjectArea> ?someObject. 
0.7715284961646951 <-> ?x <http://cmt#submitPaper> ?answer. ?x a <http://cmt#ProgramCommitteeMember>. 
0.7824130985138641 <-> ?x <http://cmt#co-writePaper> ?answer. ?x a <http://cmt#ExternalReviewer>. 
0.8769875302536041 <-> ?x <http://cmt#writePaper> ?answer. ?x a <http://cmt#AuthorNotReviewer>. 
0.9040253225035835 <-> ?x <http://cmt#readPaper> ?answer. ?x a <http://cmt#ProgramCommitteeMember>. 
0.9221334600951564 <-> ?answer <http://cmt#acceptedBy> ?y. ?y a <http://cmt#AuthorNotReviewer>. 
1.1408673975363441 <-> ?x <http://cmt#acceptPaper> ?answer. ?x a <http://cmt#AuthorNotReviewer>. 
creating directory: accepted_paper

Binary query : ?answer0 <http://edas#hasRelatedPaper> ?answer1 .
Number of matched answers :10
Number of correspondences found: 4
0.8333333333333333 <-> ?answer1 <http://cmt#hasCo-author> ?answer0.  
0.8333333333333334 <-> ?answer1 <http://cmt#hasAuthor> ?answer0.  
0.9666666666666667 <-> ?answer0 <http://cmt#co-writePaper> ?answer1.  
1.0333333333333332 <-> ?answer0 <http://cmt#writePaper> ?answer1.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://edas#isMemberOf> ?answer1 .
Number of matched answers :10
Number of correspondences found: 2
0.7631578947368423 <-> ?answer1 <http://cmt#hasConferenceMember> ?answer0.  
0.8333333333333333 <-> ?answer0 <http://cmt#memberOfConference> ?answer1.  
creating directory: member_of_conference

Binary query : ?answer0 <http://edas#endDate> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Excursion> .   ?answer0 <http://edas#hasLocation> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Topic> .
Number of matched answers :10
Number of correspondences found: 2
0.33333333333333337 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> "e-tourism". 
creating directory: topic

Binary query : ?answer0 <http://edas#hasRelatedDocument> ?answer1 .  ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reviewer> .   ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Review> .
Number of matched answers :10
Number of correspondences found: 6
5.0 <-> ?answer0 a <http://cmt#Reviewer>.  ?answer1 <http://cmt#writtenBy> ?answer0.  ?answer1 a <http://cmt#Review>.  
5.52020202020202 <-> ?answer0 a <http://cmt#Reviewer>.  ?answer0 <http://cmt#writeReview> ?answer1.  ?answer1 a <http://cmt#Review>.  
6.9980913789053325 <-> ?answer0 a <http://cmt#ExternalReviewer>.  ?v1 <http://cmt#assignExternalReviewer> ?answer0.  ?v1 a <http://cmt#Reviewer>.  ?answer1 <http://cmt#writtenBy> ?v1.  ?answer1 a <http://cmt#Review>.  
7.124354005167959 <-> ?answer0 a <http://cmt#ExternalReviewer>.  ?answer0 <http://cmt#assignedByReviewer> ?v1.  ?v1 a <http://cmt#Reviewer>.  ?answer1 <http://cmt#writtenBy> ?v1.  ?answer1 a <http://cmt#Review>.  
7.518293399107353 <-> ?answer0 a <http://cmt#ExternalReviewer>.  ?v1 <http://cmt#assignExternalReviewer> ?answer0.  ?v1 a <http://cmt#Reviewer>.  ?v1 <http://cmt#writeReview> ?answer1.  ?answer1 a <http://cmt#Review>.  
7.64455602536998 <-> ?answer0 a <http://cmt#ExternalReviewer>.  ?answer0 <http://cmt#assignedByReviewer> ?v1.  ?v1 a <http://cmt#Reviewer>.  ?v1 <http://cmt#writeReview> ?answer1.  ?answer1 a <http://cmt#Review>.  
creating directory: all_reviewer_author_of_review

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Workshop> .   ?answer0 <http://edas#hasStartDateTime> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceDinner> .   ?answer0 <http://edas#hasEndDateTime> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reviewer> .
Number of matched answers :10
Number of correspondences found: 15
0.6010230179028133 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#AuthorNotReviewer>. 
0.6328138092843976 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Meta-Reviewer>. 
0.6705882352941176 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#AuthorNotReviewer>. 
0.6710526315789473 <-> ?x <http://cmt#readByMeta-Reviewer> ?answer. ?x a <http://cmt#Paper>. 
0.6944444444444444 <-> ?answer <http://cmt#readPaper> ?y. ?y a <http://cmt#Paper>. 
0.8214285714285714 <-> ?x <http://cmt#readByReviewer> ?answer. ?x a <http://cmt#Paper>. 
0.9256518675123326 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#ExternalReviewer>. 
0.9722222222222222 <-> ?x <http://cmt#writtenBy> ?answer. ?x a <http://cmt#Review>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer>. 
1.0064599483204133 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#ExternalReviewer>. 
1.1136363636363638 <-> ?answer <http://cmt#writeReview> ?y. ?y a <http://cmt#Review>. 
1.1277173913043481 <-> ?answer <http://cmt#addedBy> ?y. ?y a <http://cmt#Reviewer>. 
1.2798319327731091 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#Reviewer>. 
1.3636363636363638 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Reviewer>. 
1.4444444444444444 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Reviewer>. 
creating directory: reviewers_all

Binary query : ?answer0 <http://edas#hasTopic> ?answer1 . ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Paper> .
Number of matched answers :10
Number of correspondences found: 1
2.25 <-> ?answer0 a <http://cmt#Paper>.  ?answer0 <http://cmt#hasSubjectArea> ?answer1.  
creating directory: topic_of_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Review> .
Number of matched answers :10
Number of correspondences found: 3
0.9722222222222222 <-> ?answer <http://cmt#writtenBy> ?y. ?y a <http://cmt#Reviewer>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Review>. 
1.2954545454545454 <-> ?x <http://cmt#writeReview> ?answer. ?x a <http://cmt#Reviewer>. 
creating directory: review

Binary query : ?answer0 <http://edas#hasFirstName> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceDinner> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Author> .
Number of matched answers :10
Number of correspondences found: 12
0.6666666666666667 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Co-author>. 
0.6875 <-> ?x <http://cmt#hasCo-author> ?answer. ?x a <http://cmt#PaperFullVersion>. 
0.8541666666666667 <-> ?x <http://cmt#hasAuthor> ?answer. ?x a <http://cmt#PaperFullVersion>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Author>. 
1.1111111111111112 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Author>. 
1.1111111111111112 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#Author>. 
1.12 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#Author>. 
1.1260952380952383 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#Author>. 
1.1363636363636362 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#Author>. 
1.1363636363636362 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Author>. 
1.1583850931677018 <-> ?answer <http://cmt#addedBy> ?y. ?y a <http://cmt#Author>. 
1.1739130434782608 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#Author>. 
creating directory: author

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#IndustryOrganization> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reception> .   ?answer0 <http://edas#hasLocation> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://edas#relatedToPaper> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 a <http://edas#Workshop> . ?answer0 <http://edas#hasCall> ?o2 . ?o2 a <http://edas#CallForReviews>. ?o2 <http://edas#hasSubmissionDeadline> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Paper> .
Number of matched answers :10
Number of correspondences found: 12
0.6 <-> ?answer <http://cmt#acceptedBy> ?y. ?y a <http://cmt#User>. 
0.6142857142857143 <-> ?answer <http://cmt#readByReviewer> ?y. ?y a <http://cmt#User>. 
0.6212301587301587 <-> ?answer <http://cmt#hasAuthor> ?y. ?y a <http://cmt#User>. 
0.6698412698412699 <-> ?answer <http://cmt#hasSubjectArea> ?someObject. 
0.7142857142857143 <-> ?answer <http://cmt#paperID> ?someObject. 
0.7846153846153846 <-> ?x <http://cmt#co-writePaper> ?answer. ?x a <http://cmt#User>. 
0.8545454545454546 <-> ?x <http://cmt#submitPaper> ?answer. ?x a <http://cmt#User>. 
0.8545454545454546 <-> ?x <http://cmt#acceptPaper> ?answer. ?x a <http://cmt#User>. 
0.8545454545454546 <-> ?x <http://cmt#rejectPaper> ?answer. ?x a <http://cmt#User>. 
0.8958677685950414 <-> ?x <http://cmt#writePaper> ?answer. ?x a <http://cmt#User>. 
0.9555555555555556 <-> ?x <http://cmt#readPaper> ?answer. ?x a <http://cmt#User>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Paper>. 
creating directory: paper

Unary query : ?answer a <http://edas#Presenter> .
Number of matched answers :10
Number of correspondences found: 14
0.6135852451641925 <-> ?answer <http://cmt#memberOfConference> <http://cmt-instances#conference943277065>. 
0.6183574879227053 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#Reviewer>. 
0.6190476190476191 <-> ?x <http://cmt#readByReviewer> ?answer. ?x a <http://cmt#Paper>. 
0.6222659285159284 <-> ?someSubject <http://cmt#assignedByReviewer> ?answer. 
0.6273164335664335 <-> ?answer <http://cmt#assignExternalReviewer> ?someObject. 
0.6367585789114008 <-> ?someSubject <http://cmt#hasConferenceMember> ?answer. 
0.6410256410256411 <-> ?answer <http://cmt#co-writePaper> ?y. ?y a <http://cmt#Paper>. 
0.6630291005291005 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#User>. 
0.6666666666666667 <-> ?answer <http://cmt#readPaper> ?y. ?y a <http://cmt#Paper>. 
0.6773488562091503 <-> ?answer <http://cmt#assignedByReviewer> ?someObject. 
0.6823993612596554 <-> ?someSubject <http://cmt#assignExternalReviewer> ?answer. 
0.6844444444444444 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#Reviewer>. 
0.696969696969697 <-> ?answer <http://cmt#writeReview> ?y. ?y a <http://cmt#Review>. 
0.7333333333333334 <-> ?answer <http://cmt#writePaper> ?y. ?y a <http://cmt#Paper>. 
creating directory: presenter

Binary query : ?answer0 <http://edas#relatesTo> ?answer1 .  ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Paper> .   ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Conference> .
Number of matched answers :10
Number of correspondences found: 12
5.120833333333334 <-> ?answer0 a <http://cmt#Paper>.  ?answer0 <http://cmt#acceptedBy> ?v1.  ?v1 a <http://cmt#User>.  ?answer1 <http://cmt#detailsEnteredBy> ?v1.  ?answer1 a <http://cmt#Conference>.  
5.16060606060606 <-> ?answer0 a <http://cmt#Paper>.  ?answer0 <http://cmt#acceptedBy> ?v1.  ?v1 a <http://cmt#User>.  ?v1 <http://cmt#enterConferenceDetails> ?answer1.  ?answer1 a <http://cmt#Conference>.  
5.257196969696969 <-> ?answer0 a <http://cmt#Paper>.  ?v1 <http://cmt#acceptPaper> ?answer0.  ?v1 a <http://cmt#User>.  ?answer1 <http://cmt#detailsEnteredBy> ?v1.  ?answer1 a <http://cmt#Conference>.  
5.347222222222221 <-> ?answer0 a <http://cmt#Paper>.  ?answer0 <http://cmt#acceptedBy> ?v1.  ?v1 a <http://cmt#ConferenceMember>.  ?answer1 <http://cmt#detailsEnteredBy> ?v1.  ?answer1 a <http://cmt#Conference>.  
5.386994949494949 <-> ?answer0 a <http://cmt#Paper>.  ?answer0 <http://cmt#acceptedBy> ?v1.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#enterConferenceDetails> ?answer1.  ?answer1 a <http://cmt#Conference>.  
5.404166666666668 <-> ?answer0 a <http://cmt#Paper>.  ?v1 <http://cmt#hasBeenAssigned> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#memberOfConference> ?answer1.  ?answer1 a <http://cmt#Conference>.  
5.4375 <-> ?answer0 a <http://cmt#Paper>.  ?answer0 <http://cmt#assignedTo> ?v1.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#memberOfConference> ?answer1.  ?answer1 a <http://cmt#Conference>.  
5.483585858585858 <-> ?answer0 a <http://cmt#Paper>.  ?v1 <http://cmt#acceptPaper> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?answer1 <http://cmt#detailsEnteredBy> ?v1.  ?answer1 a <http://cmt#Conference>.  
5.501827485380118 <-> ?answer0 a <http://cmt#Paper>.  ?answer0 <http://cmt#assignedTo> ?v1.  ?v1 a <http://cmt#ConferenceMember>.  ?answer1 <http://cmt#hasConferenceMember> ?v1.  ?answer1 a <http://cmt#Conference>.  
5.665404040404041 <-> ?answer0 a <http://cmt#Paper>.  ?v1 <http://cmt#rejectPaper> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?answer1 <http://cmt#detailsEnteredBy> ?v1.  ?answer1 a <http://cmt#Conference>.  
5.7375 <-> ?answer0 a <http://cmt#Paper>.  ?v1 <http://cmt#writePaper> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#memberOfConference> ?answer1.  ?answer1 a <http://cmt#Conference>.  
5.783653846153846 <-> ?answer0 a <http://cmt#Paper>.  ?v1 <http://cmt#co-writePaper> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#memberOfConference> ?answer1.  ?answer1 a <http://cmt#Conference>.  
creating directory: paper_submitted_at_conference

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Paper> .   ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.
Number of matched answers :10
Number of correspondences found: 1
1.9 <-> ?answer0 a <http://cmt#Paper>.  ?answer0 <http://cmt#title> ?answer1.  
creating directory: paper_title

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#PaperPresentation> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reception> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 a <http://edas#PaperPresentation> . ?answer0 <http://edas#relatedToPaper> ?o2 . ?answer1 <http://edas#hasRelatedDocument> ?o2 . ?answer1 a <http://edas#Conference> .
Number of matched answers :10
Number of correspondences found: 0
Matching process ended
