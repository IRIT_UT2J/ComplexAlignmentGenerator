===============================================================================
creating directory: conference-confOf
Running with 8 support instances - 0.4 similarity.
Number of CQAs: 73

Unary query : ?answer a <http://conference#Contribution_1th-author>.
Number of matched answers :8
Number of correspondences found: 3
0.5217391304347826 <-> ?answer <http://confOf#reviewes> ?y. ?y a <http://confOf#Contribution>. 
0.5217391304347826 <-> ?x <http://confOf#writtenBy> ?answer. ?x a <http://confOf#Contribution>. 
0.5217391304347826 <-> ?answer <http://confOf#writes> ?y. ?y a <http://confOf#Contribution>. 
creating directory: paper_1st_author

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Publisher> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Rejected_contribution> .
Number of matched answers :8
Number of correspondences found: 1
0.5714285714285714 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Contribution>. 
creating directory: rejected_paper

Binary query : ?answer0 <http://conference#has_the_last_name> ?answer1 .
Number of matched answers :8
Number of correspondences found: 1
0.9705882352941176 <-> ?answer0 <http://confOf#hasSurname> ?answer1.  
creating directory: person_last_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Track> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_participant> .
Number of matched answers :8
Number of correspondences found: 1
0.5 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant>. 
creating directory: participant

Unary query : ?answer <http://conference#was_a_member_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Organizing_committee> .
Number of matched answers :8
Number of correspondences found: 2
0.4666666666666667 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
creating directory: oc_member

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_proceedings> .   ?answer0 <http://conference#has_a_name> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer1 <http://conference#has_workshops> ?answer0 .
Number of matched answers :8
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#was_a_member_of> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :8
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#reviews> ?answer1 .
Number of matched answers :8
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Tutorial> .  ?answer0 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1 .
Number of matched answers :8
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://confOf#Tutorial>.  ?answer0 <http://confOf#hasTopic> ?answer1.  
creating directory: tutorial_topic

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 . ?o2 a <http://conference#Important_dates>. ?o2 <http://conference#is_a_date_of_acceptance_announcement> ?answer1.
Number of matched answers :5
Number of correspondences found: 1
0.5 <-> ?answer0 <http://confOf#hasAdministrativeEvent> ?v1.  ?v1 <http://confOf#starts_on> ?answer1.  
creating directory: conference_notif_date

Binary query : ?answer0 a <http://conference#Conference_participant>.      bind( if (exists {?answer0 a <http://conference#Early_paid_applicant>}, "true"^^<http://www.w3.org/2001/XMLSchema#boolean>,        if (exists {?answer0 a <http://conference#Late_paid_applicant>}, "false"^^<http://www.w3.org/2001/XMLSchema#boolean>,"")) as ?answer1)
Number of matched answers :8
Number of correspondences found: 1
0.9000000000000001 <-> ?answer0 <http://confOf#earlyRegistration> ?answer1.  
creating directory: early_registration

Binary query : ?answer1 <http://conference#has_tutorials> ?answer0 .
Number of matched answers :8
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#has_a_location> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.0714285714285714 <-> ?answer0 <http://confOf#location> ?answer1.  
creating directory: conference_location

Unary query : ?answer a <http://conference#Conference_participant> .   ?answer a <http://conference#Late_paid_applicant> .
Number of matched answers :8
Number of correspondences found: 1
0.5 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant>. 
creating directory: late_registered_participant

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Contribution_co-author> .
Number of matched answers :8
Number of correspondences found: 2
0.5454545454545454 <-> ?answer <http://confOf#writes> ?y. ?y a <http://confOf#Contribution>. 
0.5454545454545454 <-> ?x <http://confOf#writtenBy> ?answer. ?x a <http://confOf#Contribution>. 
creating directory: paper_co_author

Binary query : ?answer1 a <http://conference#Abstract>. ?answer0 <http://conference#contributes> ?answer1 . ?o2 <http://conference#has_an_abstract> ?answer1. ?o2 a <http://conference#Invited_talk>.
Number of matched answers :8
Number of correspondences found: 0

Binary query : ?answer0 a <http://conference#Invited_speaker>. ?answer0 <http://conference#contributes> ?answer1 . ?answer1 a <http://conference#Invited_talk>.
Number of matched answers :8
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#contributes> ?o2 .  ?o2 <http://conference#reviews> ?answer1 .  ?answer0 <http://conference#was_a_member_of> ?o3 . ?o3 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :8
Number of correspondences found: 1
3.3416666666666677 <-> ?answer0 a <http://confOf#Member_PC>.  ?answer0 <http://confOf#reviewes> ?answer1.  ?answer1 a <http://confOf#Contribution>.  
creating directory: is_assigned_reviewer_of

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#gives_presentations> ?answer1. ?answer1 a <http://conference#Presentation>.
Number of matched answers :8
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer> .  ?answer <http://conference#invited_by> ?o2 .
Number of matched answers :8
Number of correspondences found: 3
0.39999999999999997 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
creating directory: external_reviewer

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 . ?o2 <http://conference#is_a_date_of_camera_ready_paper_submission> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
0.5 <-> ?answer0 <http://confOf#hasAdministrativeEvent> ?v1.  ?v1 <http://confOf#ends_on> ?answer1.  
creating directory: conference_camera_date

Binary query : ?answer0 <http://conference#contributes> ?answer1 .   ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Review> .    ?answer0 <http://conference#was_a_member_of> ?o3 . ?o3 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :8
Number of correspondences found: 0

Binary query : ?answer1 <http://conference#has_tracks> ?answer0 .
Number of matched answers :8
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#was_a_track-workshop_chair_of> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop> .
Number of matched answers :8
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_volume> .
Number of matched answers :5
Number of correspondences found: 1
0.5882352941176471 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference>. 
creating directory: conference

Binary query : ?answer0 <http://conference#has_the_first_name> ?o1 .  ?answer0 <http://conference#has_the_last_name> ?o2 .   BIND( concat(STR(?o1)," ", STR(?o2)) as ?answer1)
Number of matched answers :8
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Person> .
Number of matched answers :8
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 
creating directory: person

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_proceedings> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#has_a_publisher> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 .   ?answer0 a <http://conference#Conference_volume> . ?o2 <http://conference#is_a_starting_date> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.0882352941176472 <-> ?answer0 a <http://confOf#Conference>.  ?answer0 <http://confOf#starts_on> ?answer1.  
creating directory: conference_start_date

Binary query : ?answer0 <http://conference#invites_co-reviewers> ?answer1 .
Number of matched answers :8
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Tutorial> .
Number of matched answers :8
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Tutorial>. 
creating directory: tutorial

Unary query : ?answer a <http://conference#Conference_participant> .   ?answer a <http://conference#Early_paid_applicant> .
Number of matched answers :8
Number of correspondences found: 1
0.5 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant>. 
creating directory: early_registered_participant

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference> . ?answer0 <http://conference#has_a_name> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
2.4 <-> ?answer0 a <http://confOf#Conference>.  ?answer0 <http://confOf#has_title> ?answer1.  
creating directory: conference_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Abstract> .   ?answer <http://conference#is_the_1th_part_of> ?o2. ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://conference#was_a_member_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :8
Number of correspondences found: 2
0.47058823529411764 <-> ?answer <http://confOf#reviewes> <http://confOf-instances#paper96649742662257>. 
creating directory: pc_member

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop> .
Number of matched answers :8
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop>. 
creating directory: workshop

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 . ?o2 <http://conference#is_a_full_paper_submission_date> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
0.5 <-> ?answer0 <http://confOf#hasAdministrativeEvent> ?v1.  ?v1 <http://confOf#starts_on> ?answer1.  
creating directory: conference_paper_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer> . ?answer <http://conference#was_a_member_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :8
Number of correspondences found: 2
0.9018288841641072 <-> ?answer <http://confOf#reviewes> ?someObject. 
1.4666666666666668 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
creating directory: assigned_reviewer

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Accepted_contribution> .
Number of matched answers :8
Number of correspondences found: 1
0.5714285714285714 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Contribution>. 
creating directory: accepted_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_www> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://conference#was_a_track-workshop_chair_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop> .
Number of matched answers :8
Number of correspondences found: 7
0.4444444444444444 <-> ?answer <http://confOf#writes> <http://confOf-instances#paper16834214795021114>. 
creating directory: workshop_chair

Unary query : ?answer <http://conference#is_the_1th_part_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Invited_talk> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Invited_speaker> .
Number of matched answers :8
Number of correspondences found: 8
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 
0.0 <-> ?answer <http://confOf#hasFirstName> "Wilhelm". 
0.0 <-> ?answer <http://confOf#hasSurname> "Jordan". 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Chair_PC>. 
0.0 <-> ?answer <http://confOf#reviewes> <http://confOf-instances#paper-337047316182257>. 
0.0 <-> <http://confOf-instances#paper11642622845741114> <http://confOf#writtenBy> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author>. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Regular_author> . ?answer0 <http://conference#contributes> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .   minus{  ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Abstract> .   }
Number of matched answers :8
Number of correspondences found: 2
2.25 <-> ?answer1 <http://confOf#writtenBy> ?answer0.  ?answer1 a <http://confOf#Contribution>.  
2.3045454545454547 <-> ?answer0 <http://confOf#writes> ?answer1.  ?answer1 a <http://confOf#Contribution>.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 . ?o2 <http://conference#is_an_ending_date> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
0.5 <-> ?answer0 <http://confOf#ends_on> ?answer1.  
creating directory: conference_end_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Topic> .
Number of matched answers :8
Number of correspondences found: 2
0.625 <-> ?someSubject <http://confOf#hasTopic> ?answer. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Topic>. 
creating directory: topic

Binary query : ?answer0 a <http://conference#Abstract>. ?answer1 <http://conference#has_an_abstract> ?answer0 . ?answer1 a <http://conference#Invited_talk>.
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?o2 a <http://conference#Conference_www>. ?o2 <http://conference#has_a_URL> ?answer .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://conference#was_a_member_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Steering_committee> .
Number of matched answers :8
Number of correspondences found: 6
0.4666666666666667 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
creating directory: sc_member

Binary query : ?answer1 <http://conference#has_authors> ?answer0 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Review> .
Number of matched answers :8
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer> .
Number of matched answers :8
Number of correspondences found: 5
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://confOf#writes> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 
0.0 <-> ?answer <http://confOf#hasFirstName> "Anthony". 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Review> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#has_the_first_name> ?answer1 .
Number of matched answers :8
Number of correspondences found: 1
1.1666666666666665 <-> ?answer0 <http://confOf#hasFirstName> ?answer1.  
creating directory: person_first_name

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop> .  ?answer0 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1 .
Number of matched answers :8
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://confOf#Workshop>.  ?answer0 <http://confOf#hasTopic> ?answer1.  
creating directory: workshop_topic

Unary query : ?answer a <http://conference#Invited_talk> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Regular_author> .
Number of matched answers :8
Number of correspondences found: 1
0.4285714285714286 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author>. 
creating directory: author

Binary query : ?answer0 a <http://conference#Written_contribution> . ?answer0 <http://conference#has_an_abstract> ?answer1 .
Number of matched answers :8
Number of correspondences found: 0

Binary query : ?answer0 a <http://conference#Presentation>. ?answer0 <http://conference#has_an_abstract> ?o2 . ?answer1 <http://conference#has_an_abstract> ?o2 . ?answer1 a <http://conference#Written_contribution> .
Number of matched answers :8
Number of correspondences found: 0

Unary query : ?answer <http://conference#was_a_committee_chair_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Organizing_committee> .
Number of matched answers :5
Number of correspondences found: 1
0.4 <-> ?answer <http://confOf#employedBy> ?y. ?y a <http://confOf#Organization>. 
creating directory: oc_chair

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> . minus{ ?answer a  <http://conference#Abstract> . }
Number of matched answers :8
Number of correspondences found: 1
0.6 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Contribution>. 
creating directory: paper

Unary query : ?answer <http://conference#gives_presentations> ?o3. ?o3 <http://conference#has_an_abstract> ?o2 . ?o4 <http://conference#has_an_abstract> ?o2 . ?o4 a <http://conference#Written_contribution> .
Number of matched answers :8
Number of correspondences found: 3
0.6 <-> ?answer <http://confOf#reviewes> ?y. ?y a <http://confOf#Contribution>. 
0.6110526315789474 <-> ?answer <http://confOf#writes> ?y. ?y a <http://confOf#Contribution>. 
1.0110526315789472 <-> ?x <http://confOf#writtenBy> ?answer. ?x a <http://confOf#Contribution>. 
creating directory: presenter

Binary query : ?answer0 <http://conference#is_submitted_at> ?answer1 .
Number of matched answers :8
Number of correspondences found: 0

Unary query : ?answer <http://conference#was_a_committee_chair_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :5
Number of correspondences found: 2
0.0 <-> ?answer <http://confOf#reviewes> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Chair_PC>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Paper> .
Number of matched answers :8
Number of correspondences found: 3
0.6666666666666667 <-> ?answer <http://confOf#writtenBy> <http://confOf-instances#person-1475411681>. 
0.6666666666666667 <-> <http://confOf-instances#person-1475411681> <http://confOf#writes> ?answer. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Paper>. 
creating directory: regular_paper

Binary query : { ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Paper> .   } UNION { ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .   ?o2 <http://conference#is_the_1th_part_of> ?answer0. ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .   }  ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.
Number of matched answers :8
Number of correspondences found: 2
0.5 <-> ?answer0 <http://confOf#hasTitle> ?answer1.  
1.9000000000000001 <-> ?answer0 a <http://confOf#Paper>.  ?answer0 <http://confOf#hasTitle> ?answer1.  
creating directory: paper_title

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Presentation> . minus{ ?answer a <http://conference#Invited_talk> . }
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Abstract> .   ?answer0 <http://conference#is_the_1th_part_of> ?o2. ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .    ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 a <http://conference#Presentation> . ?answer0 <http://conference#has_an_abstract> ?o2 . ?o4 <http://conference#has_an_abstract> ?o2 . ?o4 a <http://conference#Written_contribution> . ?o4 <http://conference#is_submitted_at> ?answer1.
Number of matched answers :8
Number of correspondences found: 0
Matching process ended
