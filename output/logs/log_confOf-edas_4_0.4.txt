===============================================================================
creating directory: confOf-edas
Running with 4 support instances - 0.4 similarity.
Number of CQAs: 54

Binary query : ?answer0 <http://confOf#hasSurname> ?answer1 .
Number of matched answers :4
Number of correspondences found: 1
1.1363636363636362 <-> ?answer0 <http://edas#hasLastName> ?answer1.  
creating directory: person_last_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant> .
Number of matched answers :4
Number of correspondences found: 2
0.4 <-> ?answer <http://edas#isMemberOf> ?y. ?y a <http://edas#Conference>. 
0.4 <-> ?x <http://edas#hasMember> ?answer. ?x a <http://edas#Conference>. 
creating directory: participant

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Camera_Ready_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :4
Number of correspondences found: 2
1.5 <-> ?answer0 a <http://edas#Workshop>.  ?v1 <http://edas#forEvent> ?answer0.  ?v1 <http://edas#hasSubmissionDeadline> ?answer1.  
1.5 <-> ?answer0 a <http://edas#Workshop>.  ?answer0 <http://edas#hasCall> ?v1.  ?v1 <http://edas#hasSubmissionDeadline> ?answer1.  
creating directory: workshop_camera_date

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Registration_of_participants_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :4
Number of correspondences found: 1
2.7529411764705882 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#registrationDueOn> ?answer1.  
creating directory: conference_regis_date

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :4
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://edas#Reception>.  ?answer0 <http://edas#hasEndDateTime> ?answer1.  
creating directory: reception_end_date

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Tutorial> .  ?answer0 <http://confOf#hasTopic> ?answer1 .
Number of matched answers :4
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :4
Number of correspondences found: 1
0.5 <-> ?answer0 <http://edas#hasEndDateTime> ?answer1.  
creating directory: trip_end_date

Binary query : ?answer0 a <http://confOf#Conference>. ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Reviewing_results_event> . ?o2 <http://confOf#starts_on> ?answer1 .
Number of matched answers :4
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#earlyRegistration> ?answer1 .
Number of matched answers :4
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#location> ?answer1 .   ?answer0 a <http://confOf#Conference> .
Number of matched answers :4
Number of correspondences found: 1
2.9 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#location> ?answer1.  
creating directory: conference_location

Unary query : ?answer <http://confOf#earlyRegistration> "false"^^<http://www.w3.org/2001/XMLSchema#boolean> .
Number of matched answers :4
Number of correspondences found: 4
0.4444444444444444 <-> ?answer <http://edas#hasLastName> "Bronstein". 
creating directory: late_registered_participant

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Submission_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :4
Number of correspondences found: 2
2.071428571428571 <-> ?answer0 a <http://edas#Workshop>.  ?v1 <http://edas#forEvent> ?answer0.  ?v1 <http://edas#hasSubmissionDeadline> ?answer1.  
2.071428571428571 <-> ?answer0 a <http://edas#Workshop>.  ?answer0 <http://edas#hasCall> ?v1.  ?v1 <http://edas#hasSubmissionDeadline> ?answer1.  
creating directory: workshop_submission_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#University> .
Number of matched answers :4
Number of correspondences found: 2
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#AcademiaOrganization>. 
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> "Clark & Parsia, LLC". 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .
Number of matched answers :4
Number of correspondences found: 3
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#NonAcademicEvent>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceEvent>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Excursion>. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :4
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://edas#Reception>.  ?answer0 <http://edas#hasStartDateTime> ?answer1.  
creating directory: reception_start_date

Binary query : ?answer0 <http://confOf#reviewes> ?answer1 .   ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .
Number of matched answers :4
Number of correspondences found: 6
2.375 <-> ?answer0 a <http://edas#Reviewer>.  ?v1 <http://edas#hasMember> ?answer0.  ?answer1 <http://edas#relatesTo> ?v1.  
2.375 <-> ?answer0 a <http://edas#Reviewer>.  ?v1 <http://edas#hasMember> ?answer0.  ?v1 <http://edas#hasRelatedDocument> ?answer1.  
2.875 <-> ?answer0 a <http://edas#Reviewer>.  ?answer0 <http://edas#isMemberOf> ?v1.  ?answer1 <http://edas#relatesTo> ?v1.  
2.875 <-> ?answer0 a <http://edas#Reviewer>.  ?answer0 <http://edas#isMemberOf> ?v1.  ?v1 <http://edas#hasRelatedDocument> ?answer1.  
3.4659090909090904 <-> ?answer0 a <http://edas#Reviewer>.  ?answer0 <http://edas#isReviewing> ?answer1.  
3.541666666666667 <-> ?answer0 a <http://edas#Reviewer>.  ?answer1 <http://edas#isReviewedBy> ?answer0.  
creating directory: is_assigned_reviewer_of

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Administrator> .
Number of matched answers :4
Number of correspondences found: 3
0.4 <-> ?answer <http://edas#isMemberOf> ?y. ?y a <http://edas#Conference>. 
0.4 <-> ?x <http://edas#hasMember> ?answer. ?x a <http://edas#Conference>. 
creating directory: administrator

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Camera_Ready_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :4
Number of correspondences found: 1
2.3 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#paperDueOn> ?answer1.  
creating directory: conference_camera_date

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :4
Number of correspondences found: 1
0.5 <-> ?answer0 <http://edas#hasStartDateTime> ?answer1.  
creating directory: gala_dinner_start_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> .
Number of matched answers :4
Number of correspondences found: 3
0.6444444444444445 <-> ?x <http://edas#isMemberOf> ?answer. ?x a <http://edas#ConferenceChair>. 
0.6666666666666667 <-> ?answer <http://edas#hasMember> ?y. ?y a <http://edas#ConferenceChair>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Conference>. 
creating directory: conference

Binary query : ?answer0 <http://confOf#hasFirstName> ?o1 .  ?answer0 <http://confOf#hasSurname> ?o2 .   BIND( concat(STR(?o1)," ", STR(?o2)) as ?answer1)
Number of matched answers :4
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person> .
Number of matched answers :4
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 
creating directory: person

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :4
Number of correspondences found: 2
1.1666666666666667 <-> ?v1 <http://edas#isLocationOf> ?answer0.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
1.2272727272727273 <-> ?answer0 <http://edas#hasLocation> ?v1.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: gala_dinner_location

Binary query : ?answer0 <http://confOf#starts_on> ?answer1 .   ?answer0 a <http://confOf#Conference> .
Number of matched answers :4
Number of correspondences found: 1
2.4555555555555557 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#startDate> ?answer1.  
creating directory: conference_start_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Tutorial> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://confOf#earlyRegistration> "true"^^<http://www.w3.org/2001/XMLSchema#boolean> .
Number of matched answers :4
Number of correspondences found: 4
0.0 <-> ?answer <http://edas#hasFirstName> "Christine". 
0.0 <-> <http://edas-instances#paper11757482104701114_v2> <http://edas#isWrittenBy> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 
0.0 <-> ?answer <http://edas#isReviewing> <http://edas-instances#paper6527112814221114_v0>. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#has_title> ?answer1 .
Number of matched answers :4
Number of correspondences found: 1
2.844444444444444 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#hasName> ?answer1.  
creating directory: conference_name

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :4
Number of correspondences found: 1
0.5 <-> ?answer0 <http://edas#hasStartDateTime> ?answer1.  
creating directory: trip_start_date

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :4
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://edas#Workshop>.  ?answer0 <http://edas#hasEndDateTime> ?answer1.  
creating directory: workshop_end_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .
Number of matched answers :4
Number of correspondences found: 4
0.75 <-> ?x <http://edas#relatesTo> ?answer. ?x a <http://edas#Review>. 
0.75 <-> ?answer <http://edas#hasRelatedDocument> ?y. ?y a <http://edas#Review>. 
0.9 <-> ?answer <http://edas#isMemberOf> ?y. ?y a <http://edas#Conference>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reviewer>. 
creating directory: pc_member

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .
Number of matched answers :4
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Workshop>. 
creating directory: workshop

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Submission_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :4
Number of correspondences found: 1
1.9 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#manuscriptDueOn> ?answer1.  
creating directory: conference_paper_date

Binary query : ?answer0 <http://confOf#hasTopic> ?answer1 .  ?answer0 a  <http://confOf#Conference> .
Number of matched answers :4
Number of correspondences found: 2
3.0805555555555557 <-> ?answer0 a <http://edas#Conference>.  ?answer1 <http://edas#isTopicOf> ?answer0.  ?answer1 a <http://edas#Topic>.  
3.525 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#hasTopic> ?answer1.  ?answer1 a <http://edas#Topic>.  
creating directory: conference_topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .  ?answer <http://confOf#reviewes> ?o2 .
Number of matched answers :4
Number of correspondences found: 7
0.9 <-> ?answer <http://edas#isMemberOf> ?y. ?y a <http://edas#Conference>. 
1.0 <-> ?answer <http://edas#hasLastName> "Levine". 
1.090909090909092 <-> ?answer <http://edas#isReviewing> ?someObject. 
1.1666666666666683 <-> ?someSubject <http://edas#isReviewedBy> ?answer. 
1.5 <-> ?answer <http://edas#hasRelatedDocument> ?y. ?y a <http://edas#Review>. 
1.5 <-> ?x <http://edas#relatesTo> ?answer. ?x a <http://edas#Review>. 
1.875 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reviewer>. 
creating directory: assigned_reviewer

Binary query : ?answer0 <http://confOf#writes> ?answer1 .
Number of matched answers :4
Number of correspondences found: 2
0.5 <-> ?answer0 <http://edas#hasRelatedPaper> ?answer1.  
0.9545454545454548 <-> ?answer1 <http://edas#isWrittenBy> ?answer0.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :4
Number of correspondences found: 1
2.3285714285714283 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#endDate> ?answer1.  
creating directory: conference_end_date

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :4
Number of correspondences found: 2
1.1666666666666667 <-> ?v1 <http://edas#isLocationOf> ?answer0.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
1.2272727272727273 <-> ?answer0 <http://edas#hasLocation> ?v1.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: trip_location

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Topic> .
Number of matched answers :4
Number of correspondences found: 3
0.6367647058823531 <-> ?someSubject <http://edas#hasTopic> ?answer. 
0.9555555555555556 <-> ?answer <http://edas#isTopicOf> ?y. ?y a <http://edas#Conference>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Topic>. 
creating directory: topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar> .
Number of matched answers :4
Number of correspondences found: 3
0.4 <-> ?x <http://edas#hasMember> ?answer. ?x a <http://edas#Conference>. 
creating directory: student

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :4
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://edas#Workshop>.  ?answer0 <http://edas#hasStartDateTime> ?answer1.  
creating directory: workshop_start_date

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :4
Number of correspondences found: 1
0.5 <-> ?answer0 <http://edas#hasEndDateTime> ?answer1.  
creating directory: gala_dinner_end_date

Binary query : ?answer0 <http://confOf#dealsWith> ?answer1 .
Number of matched answers :4
Number of correspondences found: 2
0.5 <-> ?answer0 <http://edas#hasTopic> ?answer1.  
0.5 <-> ?answer1 <http://edas#isTopicOf> ?answer0.  
creating directory: topic_of_paper

Binary query : ?answer0 <http://confOf#hasFirstName> ?answer1 .
Number of matched answers :4
Number of correspondences found: 1
1.5 <-> ?answer0 <http://edas#hasFirstName> ?answer1.  
creating directory: person_first_name

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .  ?answer0 <http://confOf#hasTopic> ?answer1 .
Number of matched answers :4
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .
Number of matched answers :4
Number of correspondences found: 4
0.0 <-> ?answer <http://edas#hasLocation> <http://edas-instances#location1961263292>. 
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> "Gala Dinner". 
0.0 <-> ?answer <http://edas#hasStartDateTime> "2005-05-13T19:00:00". 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#NonAcademicEvent>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author> .
Number of matched answers :4
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Author>. 
creating directory: author

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Company> .
Number of matched answers :4
Number of correspondences found: 3
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Organization>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#IndustryOrganization>. 
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> "Computing Lab. Univ. of Oxford". 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :4
Number of correspondences found: 2
2.7222222222222223 <-> ?answer0 a <http://edas#Reception>.  ?v1 <http://edas#isLocationOf> ?answer0.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
2.782828282828283 <-> ?answer0 a <http://edas#Reception>.  ?answer0 <http://edas#hasLocation> ?v1.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: reception_location

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Reviewing_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :4
Number of correspondences found: 2
1.5 <-> ?answer0 a <http://edas#Workshop>.  ?answer0 <http://edas#hasCall> ?v1.  ?v1 <http://edas#hasSubmissionDeadline> ?answer1.  
1.5 <-> ?answer0 a <http://edas#Workshop>.  ?v1 <http://edas#forEvent> ?answer0.  ?v1 <http://edas#hasSubmissionDeadline> ?answer1.  
creating directory: workshop_notif_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Contribution> .
Number of matched answers :4
Number of correspondences found: 4
0.4 <-> ?x <http://edas#hasRelatedDocument> ?answer. ?x a <http://edas#Conference>. 
0.4 <-> ?answer <http://edas#relatesTo> ?y. ?y a <http://edas#Conference>. 
1.076923076923077 <-> <http://edas-instances#topic1932752118> <http://edas#isTopicOf> ?answer. 
1.076923076923077 <-> ?answer <http://edas#hasTopic> <http://edas-instances#topic1932752118>. 
creating directory: paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Chair_PC> .
Number of matched answers :4
Number of correspondences found: 2
0.4 <-> ?x <http://edas#hasMember> ?answer. ?x a <http://edas#Conference>. 
0.4 <-> ?answer <http://edas#isMemberOf> ?y. ?y a <http://edas#Conference>. 
creating directory: pc_chair

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Paper> .
Number of matched answers :4
Number of correspondences found: 5
0.6 <-> ?answer <http://edas#isWrittenBy> <http://edas-instances#person-2116002286>. 
0.6 <-> <http://edas-instances#person-2116002286> <http://edas#hasRelatedPaper> ?answer. 
0.8 <-> <http://edas-instances#person-1446206600> <http://edas#isReviewing> ?answer. 
0.8 <-> ?answer <http://edas#isReviewedBy> <http://edas-instances#person-1446206600>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Paper>. 
creating directory: regular_paper

Binary query : ?answer0 <http://confOf#hasTitle> ?answer1 .
Number of matched answers :4
Number of correspondences found: 1
0.5 <-> ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: paper_title

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .
Number of matched answers :4
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reception>. 
creating directory: reception
Matching process ended
