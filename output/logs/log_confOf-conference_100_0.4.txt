===============================================================================
creating directory: confOf-conference
Running with 100 support instances - 0.4 similarity.
Number of CQAs: 54

Binary query : ?answer0 <http://confOf#hasSurname> ?answer1 .
Number of matched answers :100
Number of correspondences found: 1
0.9705882352941163 <-> ?answer0 <http://conference#has_the_last_name> ?answer1.  
creating directory: person_last_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant> .
Number of matched answers :100
Number of correspondences found: 5
0.5 <-> ?x <http://conference#invites_co-reviewers> ?answer. ?x a <http://conference#Conference_participant>. 
0.5 <-> ?x <http://conference#invited_by> ?answer. ?x a <http://conference#Conference_participant>. 
0.5 <-> ?answer <http://conference#invites_co-reviewers> ?y. ?y a <http://conference#Conference_participant>. 
0.5 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_participant>. 
0.5 <-> ?answer <http://conference#invited_by> ?y. ?y a <http://conference#Conference_participant>. 
creating directory: participant

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Camera_Ready_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :45
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Registration_of_participants_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Tutorial> .  ?answer0 <http://confOf#hasTopic> ?answer1 .
Number of matched answers :100
Number of correspondences found: 2
2.125 <-> ?answer0 a <http://conference#Tutorial>.  ?answer0 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1.  ?answer1 a <http://conference#Topic>.  
2.125 <-> ?answer0 a <http://conference#Tutorial>.  ?answer1 <http://conference#is_a_topis_of_conference_parts> ?answer0.  ?answer1 a <http://conference#Topic>.  
creating directory: tutorial_topic

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 a <http://confOf#Conference>. ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Reviewing_results_event> . ?o2 <http://confOf#starts_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 2
1.9 <-> ?answer0 a <http://conference#Conference>.  ?answer0 <http://conference#has_important_dates> ?v1.  ?v1 <http://conference#is_a_date_of_acceptance_announcement> ?answer1.  
1.9 <-> ?answer0 a <http://conference#Conference>.  ?v1 <http://conference#belong_to_a_conference_volume> ?answer0.  ?v1 <http://conference#is_a_date_of_acceptance_announcement> ?answer1.  
creating directory: conference_notif_date

Binary query : ?answer0 <http://confOf#earlyRegistration> ?answer1 .
Number of matched answers :100
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#location> ?answer1 .   ?answer0 a <http://confOf#Conference> .
Number of matched answers :5
Number of correspondences found: 1
2.4714285714285715 <-> ?answer0 a <http://conference#Conference>.  ?answer0 <http://conference#has_a_location> ?answer1.  
creating directory: conference_location

Unary query : ?answer <http://confOf#earlyRegistration> "false"^^<http://www.w3.org/2001/XMLSchema#boolean> .
Number of matched answers :47
Number of correspondences found: 11
0.46825396825396826 <-> ?answer <http://conference#has_the_last_name> ?someObject. 
creating directory: late_registered_participant

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Submission_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :45
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#University> .
Number of matched answers :100
Number of correspondences found: 3
0.1010294350077632 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> ?someObject. 
creating directory: university

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#reviewes> ?answer1 .   ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .
Number of matched answers :100
Number of correspondences found: 3
3.875 <-> ?answer0 a <http://conference#Reviewer>.  ?v1 <http://conference#has_authors> ?answer0.  ?v1 a <http://conference#Review>.  ?answer1 <http://conference#has_a_review> ?v1.  
5.5 <-> ?answer0 a <http://conference#Reviewer>.  ?answer0 <http://conference#contributes> ?v1.  ?v1 a <http://conference#Review>.  ?v1 <http://conference#reviews> ?answer1.  
5.5 <-> ?answer0 a <http://conference#Reviewer>.  ?v1 <http://conference#has_authors> ?answer0.  ?v1 a <http://conference#Review>.  ?v1 <http://conference#reviews> ?answer1.  
creating directory: is_assigned_reviewer_of

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Administrator> .
Number of matched answers :5
Number of correspondences found: 4
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Person>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Regular_author>. 
0.0 <-> ?answer <http://conference#contributes> <http://conference-instances#paper-3462856462702198>. 
0.0 <-> ?answer <http://conference#was_a_member_of> <http://conference-instances#pc943277065>. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Camera_Ready_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 2
1.9 <-> ?answer0 a <http://conference#Conference>.  ?v1 <http://conference#belong_to_a_conference_volume> ?answer0.  ?v1 <http://conference#is_a_date_of_camera_ready_paper_submission> ?answer1.  
1.9 <-> ?answer0 a <http://conference#Conference>.  ?answer0 <http://conference#has_important_dates> ?v1.  ?v1 <http://conference#is_a_date_of_camera_ready_paper_submission> ?answer1.  
creating directory: conference_camera_date

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> .
Number of matched answers :5
Number of correspondences found: 6
0.6666666666666667 <-> ?answer <http://conference#has_tutorials> ?y. ?y a <http://conference#Conference_part>. 
0.6666666666666667 <-> ?x <http://conference#is_part_of_conference_volumes> ?answer. ?x a <http://conference#Conference_part>. 
0.6666666666666667 <-> ?answer <http://conference#has_parts> ?y. ?y a <http://conference#Conference_part>. 
0.6666666666666667 <-> ?answer <http://conference#has_workshops> ?y. ?y a <http://conference#Conference_part>. 
0.6666666666666667 <-> ?answer <http://conference#has_tracks> ?y. ?y a <http://conference#Conference_part>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference>. 
creating directory: conference

Binary query : ?answer0 <http://confOf#hasFirstName> ?o1 .  ?answer0 <http://confOf#hasSurname> ?o2 .   BIND( concat(STR(?o1)," ", STR(?o2)) as ?answer1)
Number of matched answers :100
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person> .
Number of matched answers :100
Number of correspondences found: 5
1.0 <-> ?answer <http://conference#invited_by> ?y. ?y a <http://conference#Person>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Person>. 
1.0 <-> ?x <http://conference#invites_co-reviewers> ?answer. ?x a <http://conference#Person>. 
1.0 <-> ?x <http://conference#invited_by> ?answer. ?x a <http://conference#Person>. 
1.0 <-> ?answer <http://conference#invites_co-reviewers> ?y. ?y a <http://conference#Person>. 
creating directory: person

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#starts_on> ?answer1 .   ?answer0 a <http://confOf#Conference> .
Number of matched answers :5
Number of correspondences found: 2
1.9 <-> ?answer0 a <http://conference#Conference>.  ?v1 <http://conference#belong_to_a_conference_volume> ?answer0.  ?v1 <http://conference#is_a_starting_date> ?answer1.  
1.9 <-> ?answer0 a <http://conference#Conference>.  ?answer0 <http://conference#has_important_dates> ?v1.  ?v1 <http://conference#is_a_starting_date> ?answer1.  
creating directory: conference_start_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Tutorial> .
Number of matched answers :27
Number of correspondences found: 2
0.6153846153846152 <-> ?someSubject <http://conference#has_tutorials> ?answer. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Tutorial>. 
creating directory: tutorial

Unary query : ?answer <http://confOf#earlyRegistration> "true"^^<http://www.w3.org/2001/XMLSchema#boolean> .
Number of matched answers :100
Number of correspondences found: 9
0.5714285714285714 <-> ?answer <http://conference#has_the_last_name> "Noonan". 
creating directory: early_registered_participant

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#has_title> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
2.4 <-> ?answer0 a <http://conference#Conference>.  ?answer0 <http://conference#has_a_name> ?answer1.  
creating directory: conference_name

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :45
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .
Number of matched answers :100
Number of correspondences found: 5
0.75 <-> ?answer <http://conference#contributes> ?y. ?y a <http://conference#Review>. 
0.75 <-> ?x <http://conference#has_authors> ?answer. ?x a <http://conference#Review>. 
1.0 <-> ?x <http://conference#invited_by> ?answer. ?x a <http://conference#Reviewer>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer>. 
1.4 <-> ?answer <http://conference#invites_co-reviewers> ?y. ?y a <http://conference#Reviewer>. 
creating directory: pc_member

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .
Number of matched answers :45
Number of correspondences found: 2
0.6153846153846158 <-> ?someSubject <http://conference#has_workshops> ?answer. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop>. 
creating directory: workshop

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Submission_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 2
1.9 <-> ?answer0 a <http://conference#Conference>.  ?v1 <http://conference#belong_to_a_conference_volume> ?answer0.  ?v1 <http://conference#is_a_full_paper_submission_date> ?answer1.  
1.9 <-> ?answer0 a <http://conference#Conference>.  ?answer0 <http://conference#has_important_dates> ?v1.  ?v1 <http://conference#is_a_full_paper_submission_date> ?answer1.  
creating directory: conference_paper_date

Binary query : ?answer0 <http://confOf#hasTopic> ?answer1 .  ?answer0 a  <http://confOf#Conference> .
Number of matched answers :100
Number of correspondences found: 3
3.1916666666666664 <-> ?answer0 a <http://conference#Conference>.  ?v1 <http://conference#is_part_of_conference_volumes> ?answer0.  ?v1 a <http://conference#Conference_part>.  ?answer1 <http://conference#is_a_topis_of_conference_parts> ?v1.  ?answer1 a <http://conference#Topic>.  
3.1916666666666664 <-> ?answer0 a <http://conference#Conference>.  ?answer0 <http://conference#has_parts> ?v1.  ?v1 a <http://conference#Conference_part>.  ?v1 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1.  ?answer1 a <http://conference#Topic>.  
3.1916666666666664 <-> ?answer0 a <http://conference#Conference>.  ?v1 <http://conference#is_part_of_conference_volumes> ?answer0.  ?v1 a <http://conference#Conference_part>.  ?v1 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1.  ?answer1 a <http://conference#Topic>.  
creating directory: conference_topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .  ?answer <http://confOf#reviewes> ?o2 .
Number of matched answers :100
Number of correspondences found: 6
1.0 <-> ?answer <http://conference#has_the_last_name> "Levine". 
1.5 <-> ?answer <http://conference#contributes> ?y. ?y a <http://conference#Review>. 
1.5 <-> ?x <http://conference#has_authors> ?answer. ?x a <http://conference#Review>. 
1.875 <-> ?x <http://conference#invited_by> ?answer. ?x a <http://conference#Reviewer>. 
1.875 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer>. 
2.675 <-> ?answer <http://conference#invites_co-reviewers> ?y. ?y a <http://conference#Reviewer>. 
creating directory: assigned_reviewer

Binary query : ?answer0 <http://confOf#writes> ?answer1 .
Number of matched answers :100
Number of correspondences found: 2
0.5 <-> ?answer1 <http://conference#has_authors> ?answer0.  
0.9545454545454536 <-> ?answer0 <http://conference#contributes> ?answer1.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 2
1.9 <-> ?answer0 a <http://conference#Conference>.  ?answer0 <http://conference#has_important_dates> ?v1.  ?v1 <http://conference#is_an_ending_date> ?answer1.  
1.9 <-> ?answer0 a <http://conference#Conference>.  ?v1 <http://conference#belong_to_a_conference_volume> ?answer0.  ?v1 <http://conference#is_an_ending_date> ?answer1.  
creating directory: conference_end_date

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Topic> .
Number of matched answers :100
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Topic>. 
creating directory: topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar> .
Number of matched answers :100
Number of correspondences found: 22
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Contribution_co-author>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Passive_conference_participant>. 
0.0 <-> ?someSubject <http://conference#has_authors> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer>. 
0.0 <-> ?answer <http://conference#contributes> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Regular_author>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Late_paid_applicant>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Track-workshop_chair>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Person>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_applicant>. 
0.0 <-> ?answer <http://conference#was_a_track-workshop_chair_of> <http://conference-instances#workshop70044726>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Contribution_1th-author>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Paid_applicant>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_participant>. 
0.0 <-> ?someSubject <http://conference#is_given_by> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Registeered_applicant>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_contributor>. 
0.04761904761904762 <-> ?answer <http://conference#has_the_first_name> ?someObject. 
0.1285714285714286 <-> ?answer <http://conference#has_the_last_name> ?someObject. 
0.1875 <-> ?someSubject <http://conference#invites_co-reviewers> ?answer. 
0.625 <-> ?answer <http://conference#invited_by> <http://conference-instances#person-288128780>. 
creating directory: student

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :45
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#dealsWith> ?answer1 .
Number of matched answers :100
Number of correspondences found: 4
0.5 <-> ?v1 <http://conference#has_contributions> ?answer0.  ?v2 <http://conference#is_part_of_conference_volumes> ?v1.  ?v2 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1.  
0.5 <-> ?v1 <http://conference#has_contributions> ?answer0.  ?v2 <http://conference#is_part_of_conference_volumes> ?v1.  ?answer1 <http://conference#is_a_topis_of_conference_parts> ?v2.  
0.5 <-> ?v1 <http://conference#has_contributions> ?answer0.  ?v1 <http://conference#has_parts> ?v2.  ?v2 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1.  
0.5 <-> ?answer0 <http://conference#is_submitted_at> ?v1.  ?v1 <http://conference#has_parts> ?v2.  ?v2 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1.  
creating directory: topic_of_paper

Binary query : ?answer0 <http://confOf#hasFirstName> ?answer1 .
Number of matched answers :100
Number of correspondences found: 1
1.1666666666666683 <-> ?answer0 <http://conference#has_the_first_name> ?answer1.  
creating directory: person_first_name

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .  ?answer0 <http://confOf#hasTopic> ?answer1 .
Number of matched answers :100
Number of correspondences found: 2
2.125 <-> ?answer0 a <http://conference#Workshop>.  ?answer1 <http://conference#is_a_topis_of_conference_parts> ?answer0.  ?answer1 a <http://conference#Topic>.  
2.125 <-> ?answer0 a <http://conference#Workshop>.  ?answer0 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1.  ?answer1 a <http://conference#Topic>.  
creating directory: workshop_topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author> .
Number of matched answers :100
Number of correspondences found: 4
0.545454545454546 <-> ?someSubject <http://conference#has_authors> ?answer. 
0.6666666666666667 <-> <http://conference-instances#person-871656919> <http://conference#invites_co-reviewers> ?answer. 
0.6666666666666667 <-> ?answer <http://conference#has_the_first_name> "Thor". 
0.6666666666666667 <-> ?answer <http://conference#invited_by> <http://conference-instances#person-871656919>. 
creating directory: author

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Company> .
Number of matched answers :20
Number of correspondences found: 3
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Organization>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Reviewing_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :45
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Contribution> .
Number of matched answers :100
Number of correspondences found: 6
0.6 <-> ?answer <http://conference#has_an_abstract> ?y. ?y a <http://conference#Regular_contribution>. 
0.6 <-> ?x <http://conference#is_the_1th_part_of> ?answer. ?x a <http://conference#Regular_contribution>. 
0.6 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution>. 
0.6 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Regular_contribution>. 
1.1058823529411765 <-> ?x <http://conference#has_contributions> ?answer. ?x a <http://conference#Conference>. 
1.2910993364948244 <-> ?x <http://conference#contributes> ?answer. ?x a <http://conference#Contribution_co-author>. 
creating directory: paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Chair_PC> .
Number of matched answers :5
Number of correspondences found: 3
0.625 <-> ?x <http://conference#invited_by> ?answer. ?x a <http://conference#Chair>. 
0.625 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Chair>. 
0.625 <-> ?answer <http://conference#invites_co-reviewers> ?y. ?y a <http://conference#Chair>. 
creating directory: pc_chair

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Paper> .
Number of matched answers :100
Number of correspondences found: 3
0.6 <-> ?someSubject <http://conference#contributes> ?answer. 
0.6 <-> ?answer <http://conference#has_authors> ?someObject. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Paper>. 
creating directory: regular_paper

Binary query : ?answer0 <http://confOf#hasTitle> ?answer1 .
Number of matched answers :100
Number of correspondences found: 1
0.5 <-> ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: paper_title

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .
Number of matched answers :0
Number of correspondences found: 0
Matching process ended
