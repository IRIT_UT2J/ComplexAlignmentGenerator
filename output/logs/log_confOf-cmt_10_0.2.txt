===============================================================================
creating directory: confOf-cmt
Running with 10 support instances - 0.2 similarity.
Number of CQAs: 54

Binary query : ?answer0 <http://confOf#hasSurname> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant> .
Number of matched answers :10
Number of correspondences found: 7
0.2727272727272727 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person>. 
0.4444444444444444 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Co-author>. 
0.6060606060606061 <-> ?x <http://cmt#hasCo-author> ?answer. ?x a <http://cmt#Paper>. 
0.6105263157894737 <-> ?x <http://cmt#hasConferenceMember> ?answer. ?x a <http://cmt#Conference>. 
0.6222222222222222 <-> ?answer <http://cmt#memberOfConference> ?y. ?y a <http://cmt#Conference>. 
0.751048951048951 <-> ?answer <http://cmt#co-writePaper> ?y. ?y a <http://cmt#Paper>. 
0.8181818181818181 <-> ?answer <http://cmt#markConflictOfInterest> ?y. ?y a <http://cmt#Paper>. 
creating directory: participant

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Camera_Ready_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Registration_of_participants_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Tutorial> .  ?answer0 <http://confOf#hasTopic> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 a <http://confOf#Conference>. ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Reviewing_results_event> . ?o2 <http://confOf#starts_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#earlyRegistration> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#location> ?answer1 .   ?answer0 a <http://confOf#Conference> .
Number of matched answers :5
Number of correspondences found: 0

Unary query : ?answer <http://confOf#earlyRegistration> "false"^^<http://www.w3.org/2001/XMLSchema#boolean> .
Number of matched answers :10
Number of correspondences found: 4
0.23529411764705888 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Person>. 
0.42627960275019094 <-> ?answer <http://cmt#name> ?someObject. 
0.5777777777777778 <-> ?answer <http://cmt#memberOfConference> ?y. ?y a <http://cmt#Conference>. 
0.7749360613810742 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#Administrator>. 
creating directory: late_registered_participant

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Submission_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#University> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#reviewes> ?answer1 .   ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .
Number of matched answers :10
Number of correspondences found: 2
2.597222222222222 <-> ?answer0 a <http://cmt#Reviewer>.  ?answer0 <http://cmt#hasBeenAssigned> ?answer1.  
2.597222222222222 <-> ?answer0 a <http://cmt#Reviewer>.  ?answer1 <http://cmt#assignedTo> ?answer0.  
creating directory: is_assigned_reviewer_of

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Administrator> .
Number of matched answers :5
Number of correspondences found: 18
0.629191798638161 <-> ?answer <http://cmt#runPaperAssignmentTools> ?y. ?y a <http://cmt#Conference>. 
0.6350375350602211 <-> ?answer <http://cmt#enterReviewCriteria> ?y. ?y a <http://cmt#Conference>. 
0.6379615116684082 <-> ?answer <http://cmt#enterConferenceDetails> ?y. ?y a <http://cmt#Conference>. 
0.6464122055798258 <-> ?x <http://cmt#hasConferenceMember> ?answer. ?x a <http://cmt#Conference>. 
0.653053273004646 <-> ?x <http://cmt#reviewerBiddingStartedBy> ?answer. ?x a <http://cmt#Conference>. 
0.6635046113306984 <-> ?x <http://cmt#detailsEnteredBy> ?answer. ?x a <http://cmt#Conference>. 
0.6752136752136751 <-> ?x <http://cmt#addedBy> ?answer. ?x a <http://cmt#Co-author>. 
0.6752136752136751 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Co-author>. 
0.6819499341238472 <-> ?x <http://cmt#reviewCriteriaEnteredBy> ?answer. ?x a <http://cmt#Conference>. 
0.703030303030303 <-> ?x <http://cmt#hardcopyMailingManifestsPrintedBy> ?answer. ?x a <http://cmt#Conference>. 
0.7214854111405835 <-> ?answer <http://cmt#acceptPaper> <http://cmt-instances#paper-19067245591111263>. 
0.8049397078244898 <-> ?answer <http://cmt#addProgramCommitteeMember> ?y. ?y a <http://cmt#Co-author>. 
0.8506792101651115 <-> ?answer <http://cmt#assignReviewer> ?y. ?y a <http://cmt#Co-author>. 
0.9529914529914529 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#Co-author>. 
0.9933954933954934 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#Co-author>. 
1.0641741388729618 <-> ?x <http://cmt#assignedByAdministrator> ?answer. ?x a <http://cmt#Co-author>. 
1.2307692307692308 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Administrator>. 
1.5133779264214047 <-> ?answer <http://cmt#addedBy> ?y. ?y a <http://cmt#Administrator>. 
creating directory: administrator

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Camera_Ready_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> .
Number of matched answers :5
Number of correspondences found: 10
0.7067152503293808 <-> ?answer <http://cmt#paperAssignmentFinalizedBy> ?y. ?y a <http://cmt#ConferenceMember>. 
0.7505489679402723 <-> ?answer <http://cmt#paperAssignmentToolsRunBy> ?y. ?y a <http://cmt#ConferenceMember>. 
0.7808491089383538 <-> ?x <http://cmt#startReviewerBidding> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.7930708688717842 <-> ?x <http://cmt#runPaperAssignmentTools> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.8006587615283267 <-> ?answer <http://cmt#virtualMeetingEnabledBy> ?y. ?y a <http://cmt#ConferenceMember>. 
0.8642344497607656 <-> ?x <http://cmt#enterConferenceDetails> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.8760979358805446 <-> ?answer <http://cmt#detailsEnteredBy> ?y. ?y a <http://cmt#ConferenceMember>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Conference>. 
1.1518782744812295 <-> ?answer <http://cmt#hasConferenceMember> ?y. ?y a <http://cmt#ConferenceMember>. 
1.1811516504926347 <-> ?x <http://cmt#memberOfConference> ?answer. ?x a <http://cmt#ConferenceMember>. 
creating directory: conference

Binary query : ?answer0 <http://confOf#hasFirstName> ?o1 .  ?answer0 <http://confOf#hasSurname> ?o2 .   BIND( concat(STR(?o1)," ", STR(?o2)) as ?answer1)
Number of matched answers :10
Number of correspondences found: 2
1.2333333333333334 <-> ?answer0 <http://cmt#name> ?answer1.  
1.9833333333333336 <-> ?answer0 a <http://cmt#ConferenceMember>.  ?answer0 <http://cmt#name> ?answer1.  
creating directory: person_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person> .
Number of matched answers :10
Number of correspondences found: 14
0.6105263157894737 <-> ?x <http://cmt#hasConferenceMember> ?answer. ?x a <http://cmt#Conference>. 
0.6117632367632366 <-> ?answer <http://cmt#co-writePaper> ?y. ?y a <http://cmt#PaperFullVersion>. 
0.6477272727272727 <-> ?answer <http://cmt#markConflictOfInterest> ?y. ?y a <http://cmt#PaperFullVersion>. 
0.7083333333333334 <-> ?x <http://cmt#hasCo-author> ?answer. ?x a <http://cmt#PaperFullVersion>. 
0.8444444444444444 <-> ?answer <http://cmt#memberOfConference> ?y. ?y a <http://cmt#Conference>. 
1.0 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#Person>. 
1.0 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Person>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person>. 
1.0 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#Person>. 
1.0 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#Person>. 
1.0 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#Person>. 
1.0 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#Person>. 
1.0 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Person>. 
1.0 <-> ?answer <http://cmt#addedBy> ?y. ?y a <http://cmt#Person>. 
creating directory: person

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#starts_on> ?answer1 .   ?answer0 a <http://confOf#Conference> .
Number of matched answers :5
Number of correspondences found: 1
2.1222222222222222 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#date> ?answer1.  
creating directory: conference_start_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Tutorial> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://confOf#earlyRegistration> "true"^^<http://www.w3.org/2001/XMLSchema#boolean> .
Number of matched answers :10
Number of correspondences found: 5
0.23529411764705888 <-> ?answer <http://cmt#name> "Vanda Symon". 
0.23529411764705888 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Person>. 
0.23529411764705888 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person>. 
0.5777777777777778 <-> ?answer <http://cmt#memberOfConference> ?y. ?y a <http://cmt#Conference>. 
0.7749360613810742 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#Administrator>. 
creating directory: early_registered_participant

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#has_title> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
2.1222222222222222 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#name> ?answer1.  
creating directory: conference_name

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .
Number of matched answers :10
Number of correspondences found: 20
0.6944444444444444 <-> ?x <http://cmt#hasAuthor> ?answer. ?x a <http://cmt#Paper>. 
0.7449494949494949 <-> ?answer <http://cmt#markConflictOfInterest> ?y. ?y a <http://cmt#Paper>. 
0.7463131313131313 <-> ?answer <http://cmt#submitPaper> ?y. ?y a <http://cmt#Paper>. 
0.7707070707070707 <-> ?answer <http://cmt#writePaper> ?y. ?y a <http://cmt#Paper>. 
0.8055555555555556 <-> ?x <http://cmt#hasCo-author> ?answer. ?x a <http://cmt#Paper>. 
0.8461538461538461 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Meta-Reviewer>. 
0.9092092803030304 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#ConferenceMember>. 
0.9166666666666666 <-> ?answer <http://cmt#readPaper> ?y. ?y a <http://cmt#Paper>. 
0.9844115497076021 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#ConferenceMember>. 
1.0106837606837606 <-> ?answer <http://cmt#co-writePaper> ?y. ?y a <http://cmt#Paper>. 
1.1038011695906431 <-> ?x <http://cmt#readByMeta-Reviewer> ?answer. ?x a <http://cmt#Paper>. 
1.1736842105263159 <-> ?x <http://cmt#hasConferenceMember> ?answer. ?x a <http://cmt#Conference>. 
1.1944444444444444 <-> ?x <http://cmt#writtenBy> ?answer. ?x a <http://cmt#Review>. 
1.2072310405643738 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#Reviewer>. 
1.2222222222222223 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer>. 
1.2222222222222223 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#Reviewer>. 
1.2222222222222223 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#Reviewer>. 
1.257936507936508 <-> ?x <http://cmt#readByReviewer> ?answer. ?x a <http://cmt#Paper>. 
1.3358585858585859 <-> ?answer <http://cmt#writeReview> ?y. ?y a <http://cmt#Review>. 
1.5333333333333332 <-> ?answer <http://cmt#memberOfConference> ?y. ?y a <http://cmt#Conference>. 
creating directory: pc_member

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Submission_event> . ?o2 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#hasTopic> ?answer1 .  ?answer0 a  <http://confOf#Conference> .
Number of matched answers :10
Number of correspondences found: 6
3.3107142857142855 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#detailsEnteredBy> ?v1.  ?v1 a <http://cmt#ConferenceMember>.  ?v2 <http://cmt#acceptedBy> ?v1.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
3.3107142857142855 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#detailsEnteredBy> ?v1.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#acceptPaper> ?v2.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
3.8384920634920636 <-> ?answer0 a <http://cmt#Conference>.  ?v1 <http://cmt#memberOfConference> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v2 <http://cmt#assignedTo> ?v1.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
4.10515873015873 <-> ?answer0 a <http://cmt#Conference>.  ?v1 <http://cmt#memberOfConference> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#hasBeenAssigned> ?v2.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
4.376953601953602 <-> ?answer0 a <http://cmt#Conference>.  ?v1 <http://cmt#memberOfConference> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#co-writePaper> ?v2.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
4.50515873015873 <-> ?answer0 a <http://cmt#Conference>.  ?v1 <http://cmt#memberOfConference> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v2 <http://cmt#hasCo-author> ?v1.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
creating directory: conference_topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC> .  ?answer <http://confOf#reviewes> ?o2 .
Number of matched answers :10
Number of correspondences found: 23
0.6944444444444444 <-> ?x <http://cmt#hasAuthor> ?answer. ?x a <http://cmt#Paper>. 
0.7400727927043715 <-> ?answer <http://cmt#name> ?someObject. 
0.7607070707070707 <-> ?answer <http://cmt#submitPaper> ?y. ?y a <http://cmt#Paper>. 
0.7833333333333333 <-> ?answer <http://cmt#writePaper> ?y. ?y a <http://cmt#Paper>. 
0.8055555555555556 <-> ?x <http://cmt#hasCo-author> ?answer. ?x a <http://cmt#Paper>. 
0.8125 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceMember>. 
0.9722222222222222 <-> ?answer <http://cmt#markConflictOfInterest> ?y. ?y a <http://cmt#Paper>. 
1.1540266106442576 <-> ?answer <http://cmt#addedBy> ?y. ?y a <http://cmt#ExternalReviewer>. 
1.1648760893246188 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#ExternalReviewer>. 
1.2414529914529915 <-> ?answer <http://cmt#co-writePaper> ?y. ?y a <http://cmt#Paper>. 
1.25 <-> ?answer <http://cmt#readPaper> ?y. ?y a <http://cmt#Paper>. 
1.2997875816993463 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#AuthorNotReviewer>. 
1.3842105263157896 <-> ?x <http://cmt#hasConferenceMember> ?answer. ?x a <http://cmt#Conference>. 
1.3846153846153846 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Meta-Reviewer>. 
1.4722222222222223 <-> ?x <http://cmt#readByMeta-Reviewer> ?answer. ?x a <http://cmt#Paper>. 
1.5333333333333332 <-> ?answer <http://cmt#memberOfConference> ?y. ?y a <http://cmt#Conference>. 
1.6193181818181819 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#ExternalReviewer>. 
1.757936507936508 <-> ?x <http://cmt#readByReviewer> ?answer. ?x a <http://cmt#Paper>. 
1.7708333333333333 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#ExternalReviewer>. 
2.0972222222222223 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer>. 
2.113276792198361 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#Reviewer>. 
2.166666666666667 <-> ?x <http://cmt#writtenBy> ?answer. ?x a <http://cmt#Review>. 
2.44949494949495 <-> ?answer <http://cmt#writeReview> ?y. ?y a <http://cmt#Review>. 
creating directory: assigned_reviewer

Binary query : ?answer0 <http://confOf#writes> ?answer1 .
Number of matched answers :10
Number of correspondences found: 4
0.7222222222222222 <-> ?answer1 <http://cmt#hasAuthor> ?answer0.  
0.8333333333333333 <-> ?answer1 <http://cmt#hasCo-author> ?answer0.  
1.0 <-> ?answer0 <http://cmt#writePaper> ?answer1.  
1.1153846153846152 <-> ?answer0 <http://cmt#co-writePaper> ?answer1.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Conference> . ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Trip> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Topic> .
Number of matched answers :10
Number of correspondences found: 3
0.1111111111111111 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> ?someObject. 
creating directory: topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar> .
Number of matched answers :10
Number of correspondences found: 9
0.2857142857142857 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?y. ?y a <http://www.w3.org/2002/07/owl#Class>. 
0.5416666666666667 <-> ?answer <http://cmt#name> "Steve Horvat". 
0.6105263157894737 <-> ?x <http://cmt#hasConferenceMember> ?answer. ?x a <http://cmt#Conference>. 
0.6222222222222222 <-> ?answer <http://cmt#memberOfConference> ?y. ?y a <http://cmt#Conference>. 
0.6681763285024154 <-> ?x <http://cmt#hasAuthor> ?answer. ?x a <http://cmt#PaperFullVersion>. 
0.9166666666666666 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Co-author>. 
0.9166666666666666 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Co-author>. 
0.9166666666666666 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Co-author>. 
1.125 <-> ?x <http://cmt#hasCo-author> ?answer. ?x a <http://cmt#PaperFullVersion>. 
creating directory: student

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .   ?answer0 <http://confOf#starts_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .   ?answer0 <http://confOf#ends_on> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#dealsWith> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
0.5 <-> ?answer0 <http://cmt#hasSubjectArea> ?answer1.  
creating directory: topic_of_paper

Binary query : ?answer0 <http://confOf#hasFirstName> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop> .  ?answer0 <http://confOf#hasTopic> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Banquet> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author> .
Number of matched answers :10
Number of correspondences found: 14
0.6105263157894737 <-> ?x <http://cmt#hasConferenceMember> ?answer. ?x a <http://cmt#Conference>. 
0.6222222222222222 <-> ?answer <http://cmt#memberOfConference> ?y. ?y a <http://cmt#Conference>. 
0.8333333333333334 <-> ?someSubject <http://cmt#hasCo-author> ?answer. 
0.888888888888889 <-> ?someSubject <http://cmt#hasAuthor> ?answer. 
1.1111111111111112 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Co-author>. 
1.25 <-> ?answer <http://cmt#addedBy> ?y. ?y a <http://cmt#Author>. 
1.25 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Author>. 
1.25 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Author>. 
1.25 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#Author>. 
1.25 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Author>. 
1.25 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#Author>. 
1.25 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#Author>. 
1.25 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#Author>. 
1.25 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#Author>. 
creating directory: author

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Company> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .   ?answer0 <http://confOf#location> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 a <http://confOf#Workshop> . ?answer0 <http://confOf#hasAdministrativeEvent> ?o2 . ?o2 a <http://confOf#Reviewing_event>. ?o2 <http://confOf#ends_on> ?answer1.
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Contribution> .
Number of matched answers :10
Number of correspondences found: 16
0.8055555555555556 <-> ?answer <http://cmt#hasDecision> ?y. ?y a <http://cmt#Rejection>. 
0.8611111111111109 <-> ?x <http://cmt#readPaper> ?answer. ?x a <http://cmt#Co-author>. 
0.861111111111111 <-> ?answer <http://cmt#assignedTo> ?y. ?y a <http://cmt#Co-author>. 
0.861111111111111 <-> ?answer <http://cmt#rejectedBy> ?y. ?y a <http://cmt#Co-author>. 
0.861111111111111 <-> ?x <http://cmt#hasBeenAssigned> ?answer. ?x a <http://cmt#Co-author>. 
0.861111111111111 <-> ?answer <http://cmt#readByMeta-Reviewer> ?y. ?y a <http://cmt#Co-author>. 
0.861111111111111 <-> ?answer <http://cmt#readByReviewer> ?y. ?y a <http://cmt#Co-author>. 
0.861111111111111 <-> ?x <http://cmt#writePaper> ?answer. ?x a <http://cmt#Co-author>. 
0.861111111111111 <-> ?x <http://cmt#acceptPaper> ?answer. ?x a <http://cmt#Co-author>. 
0.861111111111111 <-> ?x <http://cmt#rejectPaper> ?answer. ?x a <http://cmt#Co-author>. 
0.861111111111111 <-> ?x <http://cmt#submitPaper> ?answer. ?x a <http://cmt#Co-author>. 
1.076923076923077 <-> ?answer <http://cmt#hasSubjectArea> <http://cmt-instances#topic1932752118>. 
1.22137451171875 <-> ?x <http://cmt#markConflictOfInterest> ?answer. ?x a <http://cmt#Co-author>. 
1.270138888888889 <-> ?answer <http://cmt#hasAuthor> ?y. ?y a <http://cmt#ConferenceMember>. 
1.3995726495726495 <-> ?x <http://cmt#co-writePaper> ?answer. ?x a <http://cmt#Co-author>. 
1.4444444444444444 <-> ?answer <http://cmt#hasCo-author> ?y. ?y a <http://cmt#Co-author>. 
creating directory: paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Chair_PC> .
Number of matched answers :5
Number of correspondences found: 10
0.625 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Chairman>. 
0.6666666666666666 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#Co-author>. 
0.6666666666666666 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#Co-author>. 
0.6666666666666666 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#Co-author>. 
0.6666666666666666 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#Co-author>. 
0.6666666666666666 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Co-author>. 
0.6666666666666666 <-> ?answer <http://cmt#addedBy> ?y. ?y a <http://cmt#Co-author>. 
0.6666666666666666 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#Co-author>. 
0.9105263157894737 <-> ?x <http://cmt#hasConferenceMember> ?answer. ?x a <http://cmt#Conference>. 
0.9222222222222223 <-> ?answer <http://cmt#memberOfConference> ?y. ?y a <http://cmt#Conference>. 
creating directory: pc_chair

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Paper> .
Number of matched answers :10
Number of correspondences found: 17
0.6666666666666666 <-> ?x <http://cmt#hasBeenAssigned> ?answer. ?x a <http://cmt#Co-author>. 
0.6666666666666666 <-> ?answer <http://cmt#readByMeta-Reviewer> ?y. ?y a <http://cmt#Co-author>. 
0.6666666666666667 <-> ?answer <http://cmt#rejectedBy> ?y. ?y a <http://cmt#Co-author>. 
0.6758241758241759 <-> ?answer <http://cmt#hasSubjectArea> <http://cmt-instances#topic1932752118>. 
0.6764069264069265 <-> ?answer <http://cmt#assignedTo> ?y. ?y a <http://cmt#Co-author>. 
0.7142857142857143 <-> ?answer <http://cmt#paperID> ?someObject. 
0.8322860962566844 <-> ?x <http://cmt#markConflictOfInterest> ?answer. ?x a <http://cmt#Co-author>. 
0.8606498942301408 <-> ?answer <http://cmt#readByReviewer> ?y. ?y a <http://cmt#User>. 
1.0 <-> ?answer <http://cmt#hasCo-author> ?y. ?y a <http://cmt#Co-author>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Paper>. 
1.0142857142857145 <-> ?x <http://cmt#writePaper> ?answer. ?x a <http://cmt#Co-author>. 
1.0145454545454546 <-> ?x <http://cmt#submitPaper> ?answer. ?x a <http://cmt#User>. 
1.0323232323232323 <-> ?x <http://cmt#acceptPaper> ?answer. ?x a <http://cmt#Co-author>. 
1.0323232323232323 <-> ?x <http://cmt#rejectPaper> ?answer. ?x a <http://cmt#Co-author>. 
1.054298617203955 <-> ?x <http://cmt#readPaper> ?answer. ?x a <http://cmt#Co-author>. 
1.0603174603174605 <-> ?answer <http://cmt#hasAuthor> ?y. ?y a <http://cmt#User>. 
1.267153136718354 <-> ?x <http://cmt#co-writePaper> ?answer. ?x a <http://cmt#Co-author>. 
creating directory: regular_paper

Binary query : ?answer0 <http://confOf#hasTitle> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
1.125 <-> ?answer0 <http://cmt#title> ?answer1.  
creating directory: paper_title

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Reception> .
Number of matched answers :0
Number of correspondences found: 0
Matching process ended
