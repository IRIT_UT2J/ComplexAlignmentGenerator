===============================================================================
creating directory: cmt-conference
Running with 10 support instances - 0.0 similarity.
Number of CQAs: 34

Unary query : ?o2 <http://cmt#hasAuthor> ?answer .
Number of matched answers :10
Number of correspondences found: 3
0.6 <-> ?x <http://conference#invites_co-reviewers> ?answer. ?x a <http://conference#Regular_author>. 
0.6023148148148149 <-> ?answer <http://conference#invites_co-reviewers> ?y. ?y a <http://conference#Regular_author>. 
0.9484925328417307 <-> ?someSubject <http://conference#has_authors> ?answer. 
creating directory: paper_1st_author

Unary query : { ?answer <http://cmt#hasDecision> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Rejection> . } UNION  { ?answer <http://cmt#rejectedBy> ?o2 . }
Number of matched answers :10
Number of correspondences found: 16
0.6842105263157895 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_document>. 
0.7017512695742034 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> ?someObject. 
0.7272727272727272 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Submitted_contribution>. 
0.85 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Regular_contribution>. 
0.8695652173913044 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_contribution>. 
0.9 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution>. 
0.9523809523809523 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewed_contribution>. 
1.0081402801991037 <-> ?someSubject <http://conference#contributes> ?answer. 
1.1428571428571428 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Rejected_contribution>. 
1.288888888888889 <-> ?x <http://conference#is_the_1th_part_of> ?answer. ?x a <http://conference#Written_contribution>. 
1.3 <-> ?answer <http://conference#has_an_abstract> ?y. ?y a <http://conference#Written_contribution>. 
1.3151515151515154 <-> ?answer <http://conference#has_a_review> ?y. ?y a <http://conference#Review>. 
1.3704058243315864 <-> ?answer <http://conference#is_submitted_at> ?someObject. 
1.5191919191919192 <-> ?x <http://conference#reviews> ?answer. ?x a <http://conference#Review>. 
1.5535948256536496 <-> ?answer <http://conference#has_authors> ?someObject. 
1.8792041436593174 <-> ?someSubject <http://conference#has_contributions> ?answer. 
creating directory: rejected_paper

Binary query : ?answer0 <http://cmt#memberOfProgramCommittee> ?answer1 .
Number of matched answers :10
Number of correspondences found: 2
1.2083333333333337 <-> ?answer1 <http://conference#has_members> ?answer0.  ?answer1 a <http://conference#Program_committee>.  
1.2083333333333337 <-> ?answer0 <http://conference#was_a_member_of> ?answer1.  ?answer1 a <http://conference#Program_committee>.  
creating directory: member_of_pc

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceChair> .
Number of matched answers :5
Number of correspondences found: 7
0.55 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_applicant>. 
0.6456140350877193 <-> ?x <http://conference#has_authors> ?answer. ?x a <http://conference#Conference_document>. 
0.7106060606060606 <-> ?x <http://conference#invited_by> ?answer. ?x a <http://conference#Conference_applicant>. 
0.7242424242424241 <-> ?answer <http://conference#invited_by> ?y. ?y a <http://conference#Conference_contributor>. 
0.7704545454545454 <-> ?answer <http://conference#invites_co-reviewers> ?y. ?y a <http://conference#Conference_contributor>. 
0.7909090909090908 <-> ?x <http://conference#invites_co-reviewers> ?answer. ?x a <http://conference#Conference_contributor>. 
0.8456140350877194 <-> ?answer <http://conference#contributes> ?y. ?y a <http://conference#Conference_document>. 
creating directory: conference_chair

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Co-author> .
Number of matched answers :10
Number of correspondences found: 4
0.6060606060606061 <-> ?answer <http://conference#contributes> ?y. ?y a <http://conference#Poster>. 
0.6341736694677871 <-> ?someSubject <http://conference#invites_co-reviewers> ?answer. 
0.6755291005291005 <-> ?answer <http://conference#invites_co-reviewers> ?y. ?y a <http://conference#Regular_author>. 
0.7907284336645019 <-> ?x <http://conference#has_authors> ?answer. ?x a <http://conference#Conference_document>. 
creating directory: paper_co_author

Binary query : ?answer0 <http://cmt#hasBeenAssigned> ?answer1 .
Number of matched answers :10
Number of correspondences found: 3
0.7666666666666666 <-> ?answer0 <http://conference#contributes> ?v1.  ?v1 <http://conference#reviews> ?answer1.  
0.9000000000000001 <-> ?v1 <http://conference#has_authors> ?answer0.  ?v1 <http://conference#reviews> ?answer1.  
1.0999999999999999 <-> ?v1 <http://conference#has_authors> ?answer0.  ?answer1 <http://conference#has_a_review> ?v1.  
creating directory: is_assigned_reviewer_of

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Administrator> .
Number of matched answers :5
Number of correspondences found: 4
0.45299145299145294 <-> ?answer <http://conference#has_the_first_name> ?someObject. 
0.5384615384615384 <-> ?answer <http://conference#contributes> ?y. ?y a <http://conference#Abstract>. 
0.5384615384615384 <-> ?x <http://conference#has_authors> ?answer. ?x a <http://conference#Abstract>. 
0.6009615384615383 <-> ?answer <http://conference#invited_by> <http://conference-instances#person-776571692>. 
creating directory: administrator

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommittee> .
Number of matched answers :5
Number of correspondences found: 10
0.638643790849673 <-> ?answer <http://conference#has_members> ?someObject. 
0.6386437908496732 <-> ?someSubject <http://conference#was_a_member_of> ?answer. 
0.7110294117647059 <-> ?someSubject <http://conference#was_a_committee_chair_of> ?answer. 
0.7646008403361344 <-> ?answer <http://conference#has_a_committee_chair> ?someObject. 
0.8322917618830372 <-> ?someSubject <http://conference#has_a_commtitee> ?answer. 
0.8372980559710763 <-> ?answer <http://conference#was_a_committee_of> ?someObject. 
0.9411764705882353 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee>. 
0.9526826713556916 <-> ?answer <http://conference#was_a_program_committee_of> ?someObject. 
0.955149319279754 <-> <http://conference-instances#conference69622803> ?somePredicate ?answer. 
1.0279439357960807 <-> ?someSubject <http://conference#has_a_program_committee> ?answer. 
creating directory: program_committee

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ExternalReviewer> .
Number of matched answers :10
Number of correspondences found: 6
0.6010964912280702 <-> <http://conference-instances#presentation-11924102145372198> <http://conference#is_given_by> ?answer. 
0.6416484559234829 <-> ?someSubject <http://conference#has_authors> ?answer. 
0.6981232150142799 <-> ?answer <http://conference#gives_presentations> <http://conference-instances#presentation-11924102145372198>. 
0.7440975834785268 <-> ?answer <http://conference#contributes> ?y. ?y a <http://conference#Review>. 
0.7535368217054265 <-> ?answer <http://conference#invited_by> ?someObject. 
1.20281007751938 <-> ?x <http://conference#invites_co-reviewers> ?answer. ?x a <http://conference#Reviewer>. 
creating directory: external_reviewer

Binary query : ?answer0 <http://cmt#writeReview> ?answer1 .
Number of matched answers :10
Number of correspondences found: 2
1.0454545454545452 <-> ?answer1 <http://conference#has_authors> ?answer0.  ?answer1 a <http://conference#Review>.  
1.2272727272727268 <-> ?answer0 <http://conference#contributes> ?answer1.  ?answer1 a <http://conference#Review>.  
creating directory: assigned_reviewer_author_of_review

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Conference> .
Number of matched answers :5
Number of correspondences found: 7
0.6820512820512821 <-> ?answer <http://conference#has_parts> ?y. ?y a <http://conference#Conference_part>. 
0.7027863777089784 <-> ?answer <http://conference#has_contributions> ?y. ?y a <http://conference#Conference_document>. 
0.7435897435897436 <-> ?answer <http://conference#has_workshops> ?y. ?y a <http://conference#Conference_part>. 
0.7435897435897436 <-> ?answer <http://conference#has_tutorials> ?y. ?y a <http://conference#Conference_part>. 
0.7666666666666667 <-> ?answer <http://conference#has_tracks> ?y. ?y a <http://conference#Conference_part>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference>. 
1.0114942528735633 <-> ?x <http://conference#is_part_of_conference_volumes> ?answer. ?x a <http://conference#Conference_part>. 
creating directory: conference

Binary query : ?answer0 <http://cmt#name> ?answer1 .   ?answer0 a <http://cmt#Person> .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person> .
Number of matched answers :10
Number of correspondences found: 7
0.6764705882352942 <-> ?answer <http://conference#has_the_last_name> "Bergeron". 
0.7222222222222222 <-> ?answer <http://conference#has_the_first_name> "Deacon". 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Person>. 
1.1 <-> ?x <http://conference#invited_by> ?answer. ?x a <http://conference#Person>. 
1.1 <-> ?x <http://conference#invites_co-reviewers> ?answer. ?x a <http://conference#Person>. 
1.1 <-> ?answer <http://conference#invites_co-reviewers> ?y. ?y a <http://conference#Person>. 
1.1 <-> ?answer <http://conference#invited_by> ?y. ?y a <http://conference#Person>. 
creating directory: person

Binary query : ?answer0 <http://cmt#date> ?answer1 .
Number of matched answers :5
Number of correspondences found: 2
0.7911877394636015 <-> ?v1 <http://conference#belong_to_a_conference_volume> ?answer0.  ?v1 <http://conference#is_a_starting_date> ?answer1.  
0.932748538011696 <-> ?answer0 <http://conference#has_important_dates> ?v1.  ?v1 <http://conference#is_a_starting_date> ?answer1.  
creating directory: conference_start_date

Binary query : ?answer0 <http://cmt#assignExternalReviewer> ?answer1 .
Number of matched answers :10
Number of correspondences found: 2
0.6818181818181818 <-> ?answer1 <http://conference#invited_by> ?answer0.  
0.9545454545454548 <-> ?answer0 <http://conference#invites_co-reviewers> ?answer1.  
creating directory: invite_reviewer

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Conference> . ?answer0 <http://cmt#name> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
2.2 <-> ?answer0 a <http://conference#Conference>.  ?answer0 <http://conference#has_a_name> ?answer1.  
creating directory: conference_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperAbstract> .
Number of matched answers :10
Number of correspondences found: 4
0.6153846153846154 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Abstract>. 
0.6890397184514829 <-> ?answer <http://conference#has_authors> ?someObject. 
0.689039718451483 <-> ?someSubject <http://conference#contributes> ?answer. 
0.9336037741100276 <-> ?x <http://conference#has_an_abstract> ?answer. ?x a <http://conference#Conference_contribution>. 
creating directory: paper_abstract

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommitteeMember> .
Number of matched answers :10
Number of correspondences found: 9
0.6530759455428825 <-> ?someSubject <http://conference#has_authors> ?answer. 
0.6871797819088359 <-> ?answer <http://conference#contributes> ?someObject. 
0.7305064335268322 <-> ?answer <http://conference#gives_presentations> ?someObject. 
0.770995670995671 <-> ?someSubject <http://conference#invited_by> ?answer. 
0.7727272727272727 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Committee_member>. 
0.7850518880722865 <-> ?someSubject <http://conference#is_given_by> ?answer. 
0.88008658008658 <-> ?answer <http://conference#invites_co-reviewers> ?someObject. 
1.0717171717171718 <-> ?answer <http://conference#was_a_member_of> ?y. ?y a <http://conference#Program_committee>. 
1.1535353535353536 <-> ?x <http://conference#has_members> ?answer. ?x a <http://conference#Program_committee>. 
creating directory: pc_member

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer> .
Number of matched answers :10
Number of correspondences found: 5
0.8409090909090909 <-> ?x <http://conference#has_authors> ?answer. ?x a <http://conference#Review>. 
0.9318181818181818 <-> ?answer <http://conference#contributes> ?y. ?y a <http://conference#Review>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer>. 
1.3 <-> ?x <http://conference#invited_by> ?answer. ?x a <http://conference#Reviewer>. 
1.4 <-> ?answer <http://conference#invites_co-reviewers> ?y. ?y a <http://conference#Reviewer>. 
creating directory: assigned_reviewer

Unary query : { ?answer <http://cmt#hasDecision> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Acceptance> . } UNION  { ?answer <http://cmt#acceptedBy> ?o2 . }
Number of matched answers :10
Number of correspondences found: 11
0.6 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Regular_contribution>. 
0.7209729820568097 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> ?someObject. 
0.9888888888888889 <-> ?x <http://conference#is_the_1th_part_of> ?answer. ?x a <http://conference#Regular_contribution>. 
1.008133971291866 <-> ?x <http://conference#reviews> ?answer. ?x a <http://conference#Conference_document>. 
1.0476190476190477 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Accepted_contribution>. 
1.1455014960529666 <-> ?someSubject <http://conference#contributes> ?answer. 
1.192982456140351 <-> ?answer <http://conference#has_a_review> ?y. ?y a <http://conference#Conference_document>. 
1.2 <-> ?answer <http://conference#has_an_abstract> ?y. ?y a <http://conference#Regular_contribution>. 
1.3610418020788917 <-> ?answer <http://conference#is_submitted_at> ?someObject. 
1.6000469505984205 <-> ?answer <http://conference#has_authors> ?someObject. 
1.8356295888338772 <-> ?someSubject <http://conference#has_contributions> ?answer. 
creating directory: accepted_paper

Binary query : ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperFullVersion> .   { ?answer0 <http://cmt#writePaper> ?answer1 .   } union{ ?answer0 <http://cmt#co-writePaper> ?answer1 .   }
Number of matched answers :10
Number of correspondences found: 14
2.3405594405594408 <-> ?answer0 a <http://conference#Conference_participant>.  ?answer1 <http://conference#has_authors> ?answer0.  ?answer1 a <http://conference#Written_contribution>.  
2.676660839160839 <-> ?answer0 a <http://conference#Conference_participant>.  ?answer0 <http://conference#contributes> ?answer1.  ?answer1 a <http://conference#Written_contribution>.  
2.7671214654910306 <-> ?answer0 a <http://conference#Contribution_1th-author>.  ?answer1 <http://conference#has_authors> ?answer0.  ?answer1 a <http://conference#Paper>.  
2.796765734265734 <-> ?answer0 a <http://conference#Contribution_co-author>.  ?answer1 <http://conference#has_authors> ?answer0.  ?answer1 a <http://conference#Paper>.  
2.8876748251748254 <-> ?answer0 a <http://conference#Conference_participant>.  ?answer1 <http://conference#has_authors> ?answer0.  ?answer1 a <http://conference#Paper>.  
2.9101398601398603 <-> ?answer0 a <http://conference#Reviewer>.  ?answer1 <http://conference#has_authors> ?answer0.  ?answer1 a <http://conference#Paper>.  
2.9101398601398603 <-> ?answer0 a <http://conference#Organizer>.  ?answer1 <http://conference#has_authors> ?answer0.  ?answer1 a <http://conference#Paper>.  
2.9774475524475528 <-> ?answer0 a <http://conference#Invited_speaker>.  ?answer1 <http://conference#has_authors> ?answer0.  ?answer1 a <http://conference#Paper>.  
3.1032228640924293 <-> ?answer0 a <http://conference#Contribution_1th-author>.  ?answer0 <http://conference#contributes> ?answer1.  ?answer1 a <http://conference#Paper>.  
3.1328671328671325 <-> ?answer0 a <http://conference#Contribution_co-author>.  ?answer0 <http://conference#contributes> ?answer1.  ?answer1 a <http://conference#Paper>.  
3.2237762237762237 <-> ?answer0 a <http://conference#Conference_participant>.  ?answer0 <http://conference#contributes> ?answer1.  ?answer1 a <http://conference#Paper>.  
3.2462412587412586 <-> ?answer0 a <http://conference#Reviewer>.  ?answer0 <http://conference#contributes> ?answer1.  ?answer1 a <http://conference#Paper>.  
3.2462412587412586 <-> ?answer0 a <http://conference#Organizer>.  ?answer0 <http://conference#contributes> ?answer1.  ?answer1 a <http://conference#Paper>.  
3.313548951048951 <-> ?answer0 a <http://conference#Invited_speaker>.  ?answer0 <http://conference#contributes> ?answer1.  ?answer1 a <http://conference#Paper>.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://cmt#memberOfConference> ?answer1 .
Number of matched answers :10
Number of correspondences found: 5
1.2222222222222223 <-> ?v1 <http://conference#has_authors> ?answer0.  ?answer1 <http://conference#has_contributions> ?v1.  ?answer1 a <http://conference#Conference>.  
1.2222222222222223 <-> ?v1 <http://conference#has_authors> ?answer0.  ?v1 <http://conference#is_submitted_at> ?answer1.  ?answer1 a <http://conference#Conference>.  
1.277777777777778 <-> ?answer0 <http://conference#contributes> ?v1.  ?v1 <http://conference#is_submitted_at> ?answer1.  ?answer1 a <http://conference#Conference>.  
1.277777777777778 <-> ?v1 <http://conference#has_members> ?answer0.  ?v1 <http://conference#was_a_committee_of> ?answer1.  ?answer1 a <http://conference#Conference>.  
1.3333333333333335 <-> ?v1 <http://conference#has_members> ?answer0.  ?answer1 <http://conference#has_a_commtitee> ?v1.  ?answer1 a <http://conference#Conference>.  
creating directory: member_of_conference

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#SubjectArea> .
Number of matched answers :10
Number of correspondences found: 2
0.409090909090909 <-> ?answer <http://conference#is_a_topis_of_conference_parts> ?y. ?y a <http://conference#Tutorial>. 
creating directory: topic

Unary query : ?o2 <http://cmt#siteURL> ?answer .
Number of matched answers :0
Number of correspondences found: 0

Unary query : { ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer> .   } union{ ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ExternalReviewer> .   }
Number of matched answers :10
Number of correspondences found: 13
0.6852928509905256 <-> ?answer <http://conference#has_the_first_name> ?someObject. 
0.7874202006383949 <-> ?answer <http://conference#has_the_last_name> ?someObject. 
0.8820834029469101 <-> ?x <http://conference#has_authors> ?answer. ?x a <http://conference#Review>. 
0.8839362618432385 <-> ?answer <http://conference#was_a_member_of> ?y. ?y a <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.9489115965860151 <-> ?x <http://conference#has_members> ?answer. ?x a <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.9768132509880872 <-> ?someSubject <http://conference#is_given_by> ?answer. 
1.0499165297982205 <-> ?answer <http://conference#gives_presentations> ?someObject. 
1.2029495309912837 <-> ?answer <http://conference#contributes> ?y. ?y a <http://conference#Review>. 
1.562015503875969 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer>. 
2.041279069767442 <-> ?answer <http://conference#invited_by> ?y. ?y a <http://conference#Reviewer>. 
2.041279069767442 <-> ?x <http://conference#invited_by> ?answer. ?x a <http://conference#Reviewer>. 
2.586046511627907 <-> ?answer <http://conference#invites_co-reviewers> ?y. ?y a <http://conference#Reviewer>. 
2.586046511627907 <-> ?x <http://conference#invites_co-reviewers> ?answer. ?x a <http://conference#Reviewer>. 
creating directory: reviewers_all

Binary query : ?answer0 a <http://cmt#PaperFullVersion> . ?answer0 <http://cmt#hasSubjectArea> ?answer1 .
Number of matched answers :10
Number of correspondences found: 4
2.208868202453395 <-> ?answer0 a <http://conference#Paper>.  ?v1 <http://conference#has_contributions> ?answer0.  ?v2 <http://conference#is_part_of_conference_volumes> ?v1.  ?answer1 <http://conference#is_a_topis_of_conference_parts> ?v2.  
2.2136301072153 <-> ?answer0 a <http://conference#Paper>.  ?v1 <http://conference#has_contributions> ?answer0.  ?v2 <http://conference#is_part_of_conference_volumes> ?v1.  ?v2 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1.  
2.276190476190476 <-> ?answer0 a <http://conference#Paper>.  ?answer0 <http://conference#is_submitted_at> ?v1.  ?v1 <http://conference#has_parts> ?v2.  ?v2 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1.  
2.3509453781512604 <-> ?answer0 a <http://conference#Paper>.  ?v1 <http://conference#has_contributions> ?answer0.  ?v1 <http://conference#has_parts> ?v2.  ?v2 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1.  
creating directory: topic_of_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Review> .
Number of matched answers :10
Number of correspondences found: 5
0.75 <-> ?answer <http://conference#has_authors> ?y. ?y a <http://conference#Reviewer>. 
0.7857142857142857 <-> ?x <http://conference#has_a_review> ?answer. ?x a <http://conference#Reviewed_contribution>. 
0.9318181818181818 <-> ?x <http://conference#contributes> ?answer. ?x a <http://conference#Reviewer>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Review>. 
1.1428571428571428 <-> ?answer <http://conference#reviews> ?y. ?y a <http://conference#Reviewed_contribution>. 
creating directory: review

Binary query : ?answer1 <http://cmt#siteURL> ?answer0 .
Number of matched answers :5
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Author> .
Number of matched answers :10
Number of correspondences found: 1
0.7236775009270568 <-> ?x <http://conference#has_authors> ?answer. ?x a <http://www.w3.org/2002/07/owl#NamedIndividual>. 
creating directory: author

Binary query : ?answer0 a <http://cmt#PaperFullVersion> .   ?answer1 a <http://cmt#PaperAbstract> . ?answer0 <http://cmt#title> ?o2 . ?answer1 <http://cmt#title> ?o2 .
Number of matched answers :10
Number of correspondences found: 6
2.4185247120029727 <-> ?answer0 a <http://conference#Conference_contribution>.  ?answer1 <http://conference#is_the_1th_part_of> ?answer0.  ?answer1 a <http://conference#Abstract>.  
2.48982905982906 <-> ?answer0 a <http://conference#Camera_ready_contribution>.  ?answer1 <http://conference#is_the_1th_part_of> ?answer0.  ?answer1 a <http://conference#Abstract>.  
2.5069444444444446 <-> ?answer0 a <http://conference#Paper>.  ?answer1 <http://conference#is_the_1th_part_of> ?answer0.  ?answer1 a <http://conference#Abstract>.  
2.765746934225195 <-> ?answer0 a <http://conference#Conference_contribution>.  ?answer0 <http://conference#has_an_abstract> ?answer1.  ?answer1 a <http://conference#Abstract>.  
2.837051282051282 <-> ?answer0 a <http://conference#Camera_ready_contribution>.  ?answer0 <http://conference#has_an_abstract> ?answer1.  ?answer1 a <http://conference#Abstract>.  
2.8541666666666665 <-> ?answer0 a <http://conference#Paper>.  ?answer0 <http://conference#has_an_abstract> ?answer1.  ?answer1 a <http://conference#Abstract>.  
creating directory: abstract_of_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperFullVersion> .
Number of matched answers :10
Number of correspondences found: 2
0.6353609625668449 <-> ?someSubject <http://conference#contributes> ?answer. 
0.6766323676321485 <-> ?answer <http://conference#has_authors> ?y. ?y a <http://conference#Person>. 
creating directory: paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommitteeChair> .
Number of matched answers :5
Number of correspondences found: 4
0.8571428571428572 <-> ?answer <http://conference#was_a_member_of> ?y. ?y a <http://conference#Program_committee>. 
1.0317460317460319 <-> ?x <http://conference#has_members> ?answer. ?x a <http://conference#Program_committee>. 
1.2095238095238094 <-> ?answer <http://conference#was_a_committee_chair_of> ?y. ?y a <http://conference#Program_committee>. 
1.380952380952381 <-> ?x <http://conference#has_a_committee_chair> ?answer. ?x a <http://conference#Program_committee>. 
creating directory: pc_chair

Binary query : ?answer0 <http://cmt#title> ?answer1 .   ?answer0 a <http://cmt#PaperFullVersion> .
Number of matched answers :10
Number of correspondences found: 1
0.6875 <-> ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: paper_title

Binary query : ?answer0 <http://cmt#title> ?answer1 .   ?answer0 a <http://cmt#PaperAbstract> .
Number of matched answers :10
Number of correspondences found: 1
1.3942307692307694 <-> ?answer0 a <http://conference#Abstract>.  ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: paper_abstract_title
Matching process ended
