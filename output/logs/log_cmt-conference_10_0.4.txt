===============================================================================
creating directory: cmt-conference
Running with 10 support instances - 0.4 similarity.
Number of CQAs: 34

Unary query : ?o2 <http://cmt#hasAuthor> ?answer .
Number of matched answers :10
Number of correspondences found: 1
0.8181818181818163 <-> ?someSubject <http://conference#has_authors> ?answer. 
creating directory: paper_1st_author

Unary query : { ?answer <http://cmt#hasDecision> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Rejection> . } UNION  { ?answer <http://cmt#rejectedBy> ?o2 . }
Number of matched answers :10
Number of correspondences found: 1
0.8571428571428572 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Rejected_contribution>. 
creating directory: rejected_paper

Binary query : ?answer0 <http://cmt#memberOfProgramCommittee> ?answer1 .
Number of matched answers :10
Number of correspondences found: 2
1.125 <-> ?answer0 <http://conference#was_a_member_of> ?answer1.  ?answer1 a <http://conference#Program_committee>.  
1.125 <-> ?answer1 <http://conference#has_members> ?answer0.  ?answer1 a <http://conference#Program_committee>.  
creating directory: member_of_pc

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceChair> .
Number of matched answers :5
Number of correspondences found: 4
0.5909090909090908 <-> ?answer <http://conference#invited_by> ?y. ?y a <http://conference#Conference_contributor>. 
0.5909090909090908 <-> ?answer <http://conference#invites_co-reviewers> ?y. ?y a <http://conference#Conference_contributor>. 
creating directory: conference_chair

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Co-author> .
Number of matched answers :10
Number of correspondences found: 1
0.5454545454545453 <-> ?someSubject <http://conference#has_authors> ?answer. 
creating directory: paper_co_author

Binary query : ?answer0 <http://cmt#hasBeenAssigned> ?answer1 .
Number of matched answers :10
Number of correspondences found: 3
0.5 <-> ?answer0 <http://conference#contributes> ?v1.  ?v1 <http://conference#reviews> ?answer1.  
0.5 <-> ?v1 <http://conference#has_authors> ?answer0.  ?v1 <http://conference#reviews> ?answer1.  
0.5 <-> ?v1 <http://conference#has_authors> ?answer0.  ?answer1 <http://conference#has_a_review> ?v1.  
creating directory: is_assigned_reviewer_of

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Administrator> .
Number of matched answers :5
Number of correspondences found: 4
0.0 <-> ?answer <http://conference#contributes> <http://conference-instances#paper-3462856462702198>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Regular_author>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Person>. 
0.0 <-> ?answer <http://conference#was_a_member_of> <http://conference-instances#pc943277065>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommittee> .
Number of matched answers :5
Number of correspondences found: 3
0.6153846153846154 <-> ?answer <http://conference#was_a_program_committee_of> ?someObject. 
0.6956521739130435 <-> ?someSubject <http://conference#has_a_program_committee> ?answer. 
0.9411764705882353 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee>. 
creating directory: program_committee

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ExternalReviewer> .
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?x <http://conference#invites_co-reviewers> ?answer. ?x a <http://conference#Reviewer>. 
creating directory: external_reviewer

Binary query : ?answer0 <http://cmt#writeReview> ?answer1 .
Number of matched answers :10
Number of correspondences found: 2
1.0454545454545452 <-> ?answer0 <http://conference#contributes> ?answer1.  ?answer1 a <http://conference#Review>.  
1.0454545454545452 <-> ?answer1 <http://conference#has_authors> ?answer0.  ?answer1 a <http://conference#Review>.  
creating directory: assigned_reviewer_author_of_review

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Conference> .
Number of matched answers :5
Number of correspondences found: 6
0.6666666666666667 <-> ?x <http://conference#is_part_of_conference_volumes> ?answer. ?x a <http://conference#Conference_part>. 
0.6666666666666667 <-> ?answer <http://conference#has_tutorials> ?y. ?y a <http://conference#Conference_part>. 
0.6666666666666667 <-> ?answer <http://conference#has_parts> ?y. ?y a <http://conference#Conference_part>. 
0.6666666666666667 <-> ?answer <http://conference#has_workshops> ?y. ?y a <http://conference#Conference_part>. 
0.6666666666666667 <-> ?answer <http://conference#has_tracks> ?y. ?y a <http://conference#Conference_part>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference>. 
creating directory: conference

Binary query : ?answer0 <http://cmt#name> ?answer1 .   ?answer0 a <http://cmt#Person> .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person> .
Number of matched answers :10
Number of correspondences found: 5
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Person>. 
1.0 <-> ?x <http://conference#invited_by> ?answer. ?x a <http://conference#Person>. 
1.0 <-> ?x <http://conference#invites_co-reviewers> ?answer. ?x a <http://conference#Person>. 
1.0 <-> ?answer <http://conference#invites_co-reviewers> ?y. ?y a <http://conference#Person>. 
1.0 <-> ?answer <http://conference#invited_by> ?y. ?y a <http://conference#Person>. 
creating directory: person

Binary query : ?answer0 <http://cmt#date> ?answer1 .
Number of matched answers :5
Number of correspondences found: 2
0.5 <-> ?v1 <http://conference#belong_to_a_conference_volume> ?answer0.  ?v1 <http://conference#is_a_starting_date> ?answer1.  
0.5 <-> ?answer0 <http://conference#has_important_dates> ?v1.  ?v1 <http://conference#is_a_starting_date> ?answer1.  
creating directory: conference_start_date

Binary query : ?answer0 <http://cmt#assignExternalReviewer> ?answer1 .
Number of matched answers :10
Number of correspondences found: 2
0.5 <-> ?answer1 <http://conference#invited_by> ?answer0.  
0.9545454545454548 <-> ?answer0 <http://conference#invites_co-reviewers> ?answer1.  
creating directory: invite_reviewer

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Conference> . ?answer0 <http://cmt#name> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.9 <-> ?answer0 a <http://conference#Conference>.  ?answer0 <http://conference#has_a_name> ?answer1.  
creating directory: conference_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperAbstract> .
Number of matched answers :10
Number of correspondences found: 2
0.5999999999999998 <-> ?someSubject <http://conference#has_an_abstract> ?answer. 
0.6153846153846154 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Abstract>. 
creating directory: paper_abstract

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommitteeMember> .
Number of matched answers :10
Number of correspondences found: 5
0.6363636363636364 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Committee_member>. 
0.6363636363636364 <-> ?answer <http://conference#invites_co-reviewers> ?y. ?y a <http://conference#Committee_member>. 
0.6363636363636364 <-> ?x <http://conference#invited_by> ?answer. ?x a <http://conference#Committee_member>. 
0.6818181818181819 <-> ?x <http://conference#has_members> ?answer. ?x a <http://conference#Program_committee>. 
0.6818181818181819 <-> ?answer <http://conference#was_a_member_of> ?y. ?y a <http://conference#Program_committee>. 
creating directory: pc_member

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer> .
Number of matched answers :10
Number of correspondences found: 5
0.75 <-> ?answer <http://conference#contributes> ?y. ?y a <http://conference#Review>. 
0.75 <-> ?x <http://conference#has_authors> ?answer. ?x a <http://conference#Review>. 
1.0 <-> ?x <http://conference#invited_by> ?answer. ?x a <http://conference#Reviewer>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer>. 
1.4 <-> ?answer <http://conference#invites_co-reviewers> ?y. ?y a <http://conference#Reviewer>. 
creating directory: assigned_reviewer

Unary query : { ?answer <http://cmt#hasDecision> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Acceptance> . } UNION  { ?answer <http://cmt#acceptedBy> ?o2 . }
Number of matched answers :10
Number of correspondences found: 1
0.4285714285714286 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Accepted_contribution>. 
creating directory: accepted_paper

Binary query : ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperFullVersion> .   { ?answer0 <http://cmt#writePaper> ?answer1 .   } union{ ?answer0 <http://cmt#co-writePaper> ?answer1 .   }
Number of matched answers :10
Number of correspondences found: 2
0.5 <-> ?answer0 <http://conference#contributes> ?answer1.  
0.5 <-> ?answer1 <http://conference#has_authors> ?answer0.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://cmt#memberOfConference> ?answer1 .
Number of matched answers :10
Number of correspondences found: 4
1.0555555555555556 <-> ?v1 <http://conference#has_authors> ?answer0.  ?answer1 <http://conference#has_contributions> ?v1.  ?answer1 a <http://conference#Conference>.  
1.0555555555555556 <-> ?answer0 <http://conference#contributes> ?v1.  ?v1 <http://conference#is_submitted_at> ?answer1.  ?answer1 a <http://conference#Conference>.  
1.0555555555555556 <-> ?v1 <http://conference#has_authors> ?answer0.  ?v1 <http://conference#is_submitted_at> ?answer1.  ?answer1 a <http://conference#Conference>.  
1.0555555555555556 <-> ?v1 <http://conference#has_members> ?answer0.  ?answer1 <http://conference#has_a_commtitee> ?v1.  ?answer1 a <http://conference#Conference>.  
creating directory: member_of_conference

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#SubjectArea> .
Number of matched answers :10
Number of correspondences found: 4
0.0 <-> ?answer <http://conference#has_a_name> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://conference#is_a_topis_of_conference_parts> ?someObject. 
0.0 <-> <http://conference-instances#tutorial1403843381> <http://conference#has_a_track-workshop-tutorial_topic> ?answer. 

Unary query : ?o2 <http://cmt#siteURL> ?answer .
Number of matched answers :0
Number of correspondences found: 0

Unary query : { ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer> .   } union{ ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ExternalReviewer> .   }
Number of matched answers :10
Number of correspondences found: 7
0.75 <-> ?answer <http://conference#contributes> ?y. ?y a <http://conference#Review>. 
0.75 <-> ?x <http://conference#has_authors> ?answer. ?x a <http://conference#Review>. 
1.5 <-> ?x <http://conference#invited_by> ?answer. ?x a <http://conference#Reviewer>. 
1.5 <-> ?answer <http://conference#invited_by> ?y. ?y a <http://conference#Reviewer>. 
1.5 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer>. 
2.4 <-> ?answer <http://conference#invites_co-reviewers> ?y. ?y a <http://conference#Reviewer>. 
2.4 <-> ?x <http://conference#invites_co-reviewers> ?answer. ?x a <http://conference#Reviewer>. 
creating directory: reviewers_all

Binary query : ?answer0 a <http://cmt#PaperFullVersion> . ?answer0 <http://cmt#hasSubjectArea> ?answer1 .
Number of matched answers :10
Number of correspondences found: 4
0.5 <-> ?answer0 <http://conference#is_submitted_at> ?v1.  ?v1 <http://conference#has_parts> ?v2.  ?v2 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1.  
0.5 <-> ?v1 <http://conference#has_contributions> ?answer0.  ?v1 <http://conference#has_parts> ?v2.  ?v2 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1.  
0.5 <-> ?v1 <http://conference#has_contributions> ?answer0.  ?v2 <http://conference#is_part_of_conference_volumes> ?v1.  ?v2 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1.  
0.5 <-> ?v1 <http://conference#has_contributions> ?answer0.  ?v2 <http://conference#is_part_of_conference_volumes> ?v1.  ?answer1 <http://conference#is_a_topis_of_conference_parts> ?v2.  
creating directory: topic_of_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Review> .
Number of matched answers :10
Number of correspondences found: 4
0.75 <-> ?answer <http://conference#has_authors> ?y. ?y a <http://conference#Reviewer>. 
0.75 <-> ?x <http://conference#contributes> ?answer. ?x a <http://conference#Reviewer>. 
0.8571428571428574 <-> ?answer <http://conference#reviews> ?someObject. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Review>. 
creating directory: review

Binary query : ?answer1 <http://cmt#siteURL> ?answer0 .
Number of matched answers :5
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Author> .
Number of matched answers :10
Number of correspondences found: 1
0.5454545454545453 <-> ?someSubject <http://conference#has_authors> ?answer. 
creating directory: author

Binary query : ?answer0 a <http://cmt#PaperFullVersion> .   ?answer1 a <http://cmt#PaperAbstract> . ?answer0 <http://cmt#title> ?o2 . ?answer1 <http://cmt#title> ?o2 .
Number of matched answers :10
Number of correspondences found: 2
1.1153846153846152 <-> ?answer1 <http://conference#is_the_1th_part_of> ?answer0.  ?answer1 a <http://conference#Abstract>.  
1.715384615384615 <-> ?answer0 <http://conference#has_an_abstract> ?answer1.  ?answer1 a <http://conference#Abstract>.  
creating directory: abstract_of_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperFullVersion> .
Number of matched answers :10
Number of correspondences found: 6
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_contribution>. 
0.0 <-> ?someSubject <http://conference#reviews> ?answer. 
0.0 <-> ?answer <http://conference#has_authors> <http://conference-instances#person-1712757423>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://conference#has_a_review> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Submitted_contribution>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommitteeChair> .
Number of matched answers :5
Number of correspondences found: 4
0.7142857142857143 <-> ?answer <http://conference#was_a_member_of> ?y. ?y a <http://conference#Program_committee>. 
0.9365079365079364 <-> ?x <http://conference#has_members> ?answer. ?x a <http://conference#Program_committee>. 
1.1809523809523808 <-> ?answer <http://conference#was_a_committee_chair_of> ?y. ?y a <http://conference#Program_committee>. 
1.380952380952381 <-> ?x <http://conference#has_a_committee_chair> ?answer. ?x a <http://conference#Program_committee>. 
creating directory: pc_chair

Binary query : ?answer0 <http://cmt#title> ?answer1 .   ?answer0 a <http://cmt#PaperFullVersion> .
Number of matched answers :10
Number of correspondences found: 1
0.5 <-> ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: paper_title

Binary query : ?answer0 <http://cmt#title> ?answer1 .   ?answer0 a <http://cmt#PaperAbstract> .
Number of matched answers :10
Number of correspondences found: 1
1.1153846153846152 <-> ?answer0 a <http://conference#Abstract>.  ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: paper_abstract_title
Matching process ended
