===============================================================================
Running with 10 support instances - 0.4 similarity.
Number of CQAs: 49

Unary query : ?answer a <http://confOf#Contribution>.
Number of matched answers :10
Number of correspondences found: 7
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Regular_Paper>. 
0.0 <-> ?answer <http://ekaw#hasReview> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper>. 
0.2 <-> ?answer <http://ekaw#writtenBy> ?someObject. 
0.5625 <-> ?answer <http://ekaw#paperPresentedAs> ?y. ?y a <http://ekaw#Contributed_Talk>. 
0.7179487179487181 <-> ?someSubject <http://ekaw#topicCoveredBy> ?answer. 
1.076923076923077 <-> ?answer <http://ekaw#coversTopic> <http://ekaw-instances#topic1932752118>. 

Unary query : ?answer a <http://confOf#Banquet>.
Number of matched answers :5
Number of correspondences found: 4
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Banquet>. 
0.0 <-> ?answer <http://ekaw#heldIn> ?someObject. 
0.0 <-> ?answer <http://ekaw#partOfEvent> <http://ekaw-instances#conference943277065>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Social_Event>. 

Unary query : ?answer a <http://confOf#Social_event>.
Number of matched answers :10
Number of correspondences found: 3
0.9166666666666666 <-> ?answer <http://ekaw#partOfEvent> ?y. ?y a <http://ekaw#Scientific_Event>. 
1.0 <-> ?x <http://ekaw#hasEvent> ?answer. ?x a <http://ekaw#Scientific_Event>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Social_Event>. 

Binary query : ?answer0 <http://confOf#hasSurname> ?answer1.
Number of matched answers :10
Number of correspondences found: 1
0.5 <-> ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  

Unary query : ?answer a <http://confOf#Member_PC>.
Number of matched answers :10
Number of correspondences found: 4
0.7272727272727276 <-> ?someSubject <http://ekaw#hasReviewer> ?answer. 
0.75 <-> ?answer <http://ekaw#authorOf> ?y. ?y a <http://ekaw#Review>. 
0.8083333333333333 <-> ?x <http://ekaw#writtenBy> ?answer. ?x a <http://ekaw#Review>. 
1.2166666666666668 <-> ?x <http://ekaw#reviewWrittenBy> ?answer. ?x a <http://ekaw#Review>. 

Binary query : ?answer0 <http://confOf#earlyRegistration> ?answer1.
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer a <http://confOf#Author>.
Number of matched answers :10
Number of correspondences found: 1
0.75 <-> ?answer <http://ekaw#authorOf> ?someObject. 

Unary query : ?answer a <http://confOf#Event>.
Number of matched answers :10
Number of correspondences found: 5
1.0 <-> ?x <http://ekaw#hasPart> ?answer. ?x a <http://ekaw#Event>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Event>. 
1.0 <-> ?answer <http://ekaw#partOf> ?y. ?y a <http://ekaw#Event>. 
1.1136363636363635 <-> ?answer <http://ekaw#partOfEvent> ?y. ?y a <http://ekaw#Event>. 
1.2708333333333335 <-> ?x <http://ekaw#hasEvent> ?answer. ?x a <http://ekaw#Event>. 

Binary query : ?answer0 <http://confOf#hasTopic> ?answer1.
Number of matched answers :10
Number of correspondences found: 2
0.5 <-> ?answer1 <http://ekaw#topicCoveredBy> ?answer0.  
1.0454545454545452 <-> ?answer0 <http://ekaw#coversTopic> ?answer1.  

Unary query : ?answer a <http://confOf#Conference>.
Number of matched answers :5
Number of correspondences found: 5
0.6666666666666666 <-> ?answer <http://ekaw#hasPart> ?y. ?y a <http://ekaw#Conference_Trip>. 
0.6666666666666667 <-> ?x <http://ekaw#partOf> ?answer. ?x a <http://ekaw#Conference_Trip>. 
0.6666666666666667 <-> ?x <http://ekaw#partOfEvent> ?answer. ?x a <http://ekaw#Conference_Trip>. 
0.6666666666666667 <-> ?answer <http://ekaw#hasEvent> ?y. ?y a <http://ekaw#Conference_Trip>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference>. 

Unary query : ?answer a <http://confOf#Workshop>.
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop>. 

Binary query : ?answer0 <http://confOf#hasAdministrativeEvent> ?answer1.
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#starts_on> ?answer1.
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer a <http://confOf#Reviewing_event>.
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#dealsWith> ?answer1.
Number of matched answers :10
Number of correspondences found: 2
0.5 <-> ?answer1 <http://ekaw#topicCoveredBy> ?answer0.  
0.5 <-> ?answer0 <http://ekaw#coversTopic> ?answer1.  

Unary query : ?answer a <http://confOf#Tutorial>.
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Tutorial>. 

Unary query : ?answer a <http://confOf#Scholar>.
Number of matched answers :10
Number of correspondences found: 7
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Possible_Reviewer>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper_Author>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Student>. 
0.0 <-> ?someSubject <http://ekaw#writtenBy> ?answer. 
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Participant>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Person>. 

Unary query : ?answer a <http://confOf#Trip>.
Number of matched answers :5
Number of correspondences found: 4
0.0 <-> ?answer <http://ekaw#partOfEvent> <http://ekaw-instances#conference69622803>. 
0.0 <-> ?answer <http://ekaw#partOf> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Social_Event>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Trip>. 

Unary query : ?answer a <http://confOf#Working_event>.
Number of matched answers :10
Number of correspondences found: 5
0.6153846153846154 <-> ?x <http://ekaw#partOf> ?answer. ?x a <http://ekaw#Social_Event>. 
0.6153846153846154 <-> ?answer <http://ekaw#hasEvent> ?y. ?y a <http://ekaw#Social_Event>. 
0.6153846153846154 <-> ?answer <http://ekaw#hasPart> ?y. ?y a <http://ekaw#Social_Event>. 
0.8990384615384616 <-> ?answer <http://ekaw#partOfEvent> ?y. ?y a <http://ekaw#Scientific_Event>. 
0.905771819643771 <-> ?x <http://ekaw#partOfEvent> ?answer. ?x a <http://ekaw#Scientific_Event>. 

Binary query : ?answer0 <http://confOf#parallel_with> ?answer1.
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#ends_on> ?answer1.
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer a <http://confOf#Person>.
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Person>. 

Unary query : ?answer a <http://confOf#Submission_event>.
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer a <http://confOf#Chair_PC>.
Number of matched answers :5
Number of correspondences found: 5
0.0 <-> <http://ekaw-instances#paper-8367213188212198> <http://ekaw#hasReviewer> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member>. 
0.0 <-> <http://ekaw-instances#review-1897148521981263_-559246915> <http://ekaw#writtenBy> ?answer. 
0.0 <-> ?answer <http://ekaw#authorOf> <http://ekaw-instances#review19126782906431114_-559246915>. 
0.0 <-> ?answer <http://ekaw#reviewerOfPaper> <http://ekaw-instances#paper459168755124257>. 

Binary query : ?answer0 <http://confOf#has_short_title> ?answer1.
Number of matched answers :10
Number of correspondences found: 1
0.5 <-> ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  

Binary query : ?answer0 <http://confOf#writes> ?answer1.
Number of matched answers :10
Number of correspondences found: 6
0.5 <-> ?answer0 <http://ekaw#authorOf> ?answer1.  
0.5 <-> ?answer0 <http://ekaw#authorOf> ?v1.  ?v1 <http://ekaw#hasUpdatedVersion> ?answer1.  
0.5 <-> ?answer0 <http://ekaw#authorOf> ?v1.  ?answer1 <http://ekaw#updatedVersionOf> ?v1.  
1.0555555555555556 <-> ?answer1 <http://ekaw#writtenBy> ?answer0.  
1.0555555555555556 <-> ?v1 <http://ekaw#writtenBy> ?answer0.  ?v1 <http://ekaw#hasUpdatedVersion> ?answer1.  
1.0555555555555556 <-> ?v1 <http://ekaw#writtenBy> ?answer0.  ?answer1 <http://ekaw#updatedVersionOf> ?v1.  

Unary query : ?answer a <http://confOf#Registration_of_participants_event>.
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#writtenBy> ?answer1.
Number of matched answers :10
Number of correspondences found: 6
0.5 <-> ?answer1 <http://ekaw#authorOf> ?answer0.  
0.5 <-> ?answer0 <http://ekaw#updatedVersionOf> ?v1.  ?answer1 <http://ekaw#authorOf> ?v1.  
0.5 <-> ?v1 <http://ekaw#hasUpdatedVersion> ?answer0.  ?answer1 <http://ekaw#authorOf> ?v1.  
1.5 <-> ?answer0 <http://ekaw#writtenBy> ?answer1.  
1.5 <-> ?answer0 <http://ekaw#updatedVersionOf> ?v1.  ?v1 <http://ekaw#writtenBy> ?answer1.  
1.5 <-> ?v1 <http://ekaw#hasUpdatedVersion> ?answer0.  ?v1 <http://ekaw#writtenBy> ?answer1.  

Binary query : ?answer0 <http://confOf#employedBy> ?answer1.
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer a <http://confOf#Administrative_event>.
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer a <http://confOf#Paper>.
Number of matched answers :10
Number of correspondences found: 5
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper>. 
1.0 <-> ?answer <http://ekaw#hasUpdatedVersion> ?y. ?y a <http://ekaw#Paper>. 
1.0 <-> ?answer <http://ekaw#updatedVersionOf> ?y. ?y a <http://ekaw#Paper>. 
1.0 <-> ?x <http://ekaw#updatedVersionOf> ?answer. ?x a <http://ekaw#Paper>. 
1.0 <-> ?x <http://ekaw#hasUpdatedVersion> ?answer. ?x a <http://ekaw#Paper>. 

Binary query : ?answer0 <http://confOf#hasTitle> ?answer1.
Number of matched answers :10
Number of correspondences found: 1
0.5 <-> ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  

Unary query : ?answer a <http://confOf#Organization>.
Number of matched answers :10
Number of correspondences found: 1
0.9166666666666666 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Organisation>. 

Binary query : ?answer0 <http://confOf#hasFirstName> ?answer1.
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer a <http://confOf#Company>.
Number of matched answers :10
Number of correspondences found: 2
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Organisation>. 
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> ?someObject. 

Binary query : ?answer0 <http://confOf#follows> ?answer1.
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer a <http://confOf#Administrator>.
Number of matched answers :5
Number of correspondences found: 5
0.4 <-> ?answer <http://ekaw#organises> ?y. ?y a <http://ekaw#Conference>. 
0.4 <-> ?x <http://ekaw#organisedBy> ?answer. ?x a <http://ekaw#Conference>. 

Binary query : ?answer0 <http://confOf#location> ?answer1.
Number of matched answers :10
Number of correspondences found: 2
1.5 <-> ?answer0 <http://ekaw#heldIn> ?v1.  ?v1 a <http://ekaw#Location>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
2.6999999999999997 <-> ?v1 <http://ekaw#locationOf> ?answer0.  ?v1 a <http://ekaw#Location>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  

Binary query : ?answer0 <http://confOf#studyAt> ?answer1.
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer a <http://confOf#University>.
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#University>. 

Unary query : ?answer a <http://confOf#Reviewing_results_event>.
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://confOf#has_title> ?answer1.
Number of matched answers :10
Number of correspondences found: 1
0.5 <-> ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  

Unary query : ?answer a <http://confOf#Participant>.
Number of matched answers :10
Number of correspondences found: 1
0.5 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Participant>. 

Unary query : ?answer a <http://confOf#Poster>.
Number of matched answers :10
Number of correspondences found: 4
0.5555555555555556 <-> ?answer <http://ekaw#writtenBy> ?y. ?y a <http://ekaw#Presenter>. 
0.5555555555555556 <-> ?x <http://ekaw#authorOf> ?answer. ?x a <http://ekaw#Presenter>. 
0.5555555555555556 <-> ?answer <http://ekaw#hasReviewer> ?y. ?y a <http://ekaw#Presenter>. 
0.5555555555555556 <-> ?x <http://ekaw#reviewerOfPaper> ?answer. ?x a <http://ekaw#Presenter>. 

Binary query : ?answer0 <http://confOf#reviewes> ?answer1.
Number of matched answers :10
Number of correspondences found: 6
0.9666666666666666 <-> ?answer0 <http://ekaw#reviewerOfPaper> ?v1.  ?answer1 <http://ekaw#updatedVersionOf> ?v1.  
0.9666666666666666 <-> ?answer0 <http://ekaw#reviewerOfPaper> ?answer1.  
0.9666666666666666 <-> ?answer0 <http://ekaw#reviewerOfPaper> ?v1.  ?v1 <http://ekaw#hasUpdatedVersion> ?answer1.  
1.1363636363636362 <-> ?v1 <http://ekaw#hasReviewer> ?answer0.  ?answer1 <http://ekaw#updatedVersionOf> ?v1.  
1.1363636363636362 <-> ?v1 <http://ekaw#hasReviewer> ?answer0.  ?v1 <http://ekaw#hasUpdatedVersion> ?answer1.  
1.1363636363636365 <-> ?answer1 <http://ekaw#hasReviewer> ?answer0.  

Binary query : ?answer0 <http://confOf#hasKeyword> ?answer1.
Number of matched answers :10
Number of correspondences found: 2
0.5 <-> ?answer0 <http://ekaw#coversTopic> ?v1.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
0.5 <-> ?v1 <http://ekaw#topicCoveredBy> ?answer0.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  

Unary query : ?answer a <http://confOf#Reception>.
Number of matched answers :5
Number of correspondences found: 1
0.9555555555555556 <-> ?x <http://ekaw#locationOf> ?answer. ?x a <http://ekaw#Location>. 

Unary query : ?answer a <http://confOf#Camera_Ready_event>.
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer a <http://confOf#Topic>.
Number of matched answers :10
Number of correspondences found: 1
0.4945454545454546 <-> ?someSubject <http://ekaw#coversTopic> ?answer. 
Matching process ended
