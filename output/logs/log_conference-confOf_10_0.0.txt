===============================================================================
creating directory: conference-confOf
Running with 10 support instances - 0.0 similarity.
Number of CQAs: 73

Unary query : ?answer a <http://conference#Contribution_1th-author>.
Number of matched answers :10
Number of correspondences found: 6
0.6260869565217392 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
0.6521739130434783 <-> ?answer <http://confOf#studyAt> <http://confOf-instances#orga-1529662043>. 
0.6596644053859595 <-> ?answer <http://confOf#employedBy> ?someObject. 
1.1602474079139096 <-> ?answer <http://confOf#reviewes> ?someObject. 
1.3867167024175284 <-> ?answer <http://confOf#writes> ?someObject. 
1.4301949632870938 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
creating directory: paper_1st_author

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Publisher> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Rejected_contribution> .
Number of matched answers :10
Number of correspondences found: 7
0.6015406162464987 <-> ?answer <http://confOf#hasTitle> ?someObject. 
0.6820367233410711 <-> ?answer <http://confOf#hasKeyword> ?someObject. 
0.7672508818342154 <-> ?x <http://confOf#reviewes> ?answer. ?x a <http://confOf#Member_PC>. 
0.7934209239283392 <-> ?answer <http://confOf#dealsWith> ?y. ?y a <http://confOf#Topic>. 
0.8095238095238095 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Contribution>. 
0.9223873389606823 <-> ?x <http://confOf#writes> ?answer. ?x a <http://confOf#Member_PC>. 
0.9754464285714285 <-> ?answer <http://confOf#writtenBy> ?someObject. 
creating directory: rejected_paper

Binary query : ?answer0 <http://conference#has_the_last_name> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
1.088235294117647 <-> ?answer0 <http://confOf#hasSurname> ?answer1.  
creating directory: person_last_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Track> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_participant> .
Number of matched answers :10
Number of correspondences found: 7
0.6195014662756599 <-> ?answer <http://confOf#employedBy> ?someObject. 
0.6363636363636364 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
0.6666666666666666 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
0.7272727272727273 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant>. 
1.2452152128626823 <-> ?answer <http://confOf#reviewes> ?someObject. 
1.466121134062165 <-> ?answer <http://confOf#writes> ?someObject. 
1.4661211340621652 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
creating directory: participant

Unary query : ?answer <http://conference#was_a_member_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Organizing_committee> .
Number of matched answers :10
Number of correspondences found: 8
0.65 <-> ?answer <http://confOf#studyAt> ?y. ?y a <http://confOf#Organization>. 
0.9099999999999999 <-> ?answer <http://confOf#hasSurname> ?someObject. 
0.9606744615711617 <-> ?answer <http://confOf#employedBy> ?someObject. 
0.9633333333333333 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
1.0833333333333335 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
1.8817696826947115 <-> ?answer <http://confOf#reviewes> ?someObject. 
2.031397115202358 <-> ?answer <http://confOf#writes> ?someObject. 
2.0480637818690246 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
creating directory: oc_member

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_proceedings> .   ?answer0 <http://conference#has_a_name> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer1 <http://conference#has_workshops> ?answer0 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#was_a_member_of> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#reviews> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Tutorial> .  ?answer0 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
3.2607142857142852 <-> ?answer0 a <http://confOf#Tutorial>.  ?answer0 <http://confOf#hasTopic> ?answer1.  ?answer1 a <http://confOf#Topic>.  
creating directory: tutorial_topic

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 . ?o2 a <http://conference#Important_dates>. ?o2 <http://conference#is_a_date_of_acceptance_announcement> ?answer1.
Number of matched answers :5
Number of correspondences found: 1
4.538888888888888 <-> ?answer0 a <http://confOf#Event>.  ?answer0 <http://confOf#hasAdministrativeEvent> ?v1.  ?v1 a <http://confOf#Camera_Ready_event>.  ?v1 <http://confOf#starts_on> ?answer1.  
creating directory: conference_notif_date

Binary query : ?answer0 a <http://conference#Conference_participant>.      bind( if (exists {?answer0 a <http://conference#Early_paid_applicant>}, "true"^^<http://www.w3.org/2001/XMLSchema#boolean>,        if (exists {?answer0 a <http://conference#Late_paid_applicant>}, "false"^^<http://www.w3.org/2001/XMLSchema#boolean>,"")) as ?answer1)
Number of matched answers :10
Number of correspondences found: 1
3.368428088938926 <-> ?answer0 a <http://confOf#Participant>.  ?answer0 <http://confOf#earlyRegistration> ?answer1.  
creating directory: early_registration

Binary query : ?answer1 <http://conference#has_tutorials> ?answer0 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#has_a_location> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.4236453201970445 <-> ?answer0 <http://confOf#location> ?answer1.  
creating directory: conference_location

Unary query : ?answer a <http://conference#Conference_participant> .   ?answer a <http://conference#Late_paid_applicant> .
Number of matched answers :10
Number of correspondences found: 11
0.6267942583732058 <-> ?answer <http://confOf#earlyRegistration> ?someObject. 
0.771531100478469 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar>. 
0.803296119085593 <-> ?answer <http://confOf#hasSurname> ?someObject. 
0.8434416170079956 <-> ?answer <http://confOf#studyAt> ?y. ?y a <http://confOf#Organization>. 
0.9937634898585663 <-> ?answer <http://confOf#employedBy> ?y. ?y a <http://confOf#Organization>. 
1.0047846889952154 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
1.089712918660287 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
1.0956937799043063 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant>. 
2.3032489239287464 <-> ?answer <http://confOf#reviewes> ?someObject. 
2.8834257457570245 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
2.883425745757025 <-> ?answer <http://confOf#writes> ?someObject. 
creating directory: late_registered_participant

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Contribution_co-author> .
Number of matched answers :10
Number of correspondences found: 7
0.6174242424242424 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar>. 
0.6666666666666665 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
0.6731141199226305 <-> ?answer <http://confOf#employedBy> ?someObject. 
0.6818181818181818 <-> ?answer <http://confOf#hasSurname> ?someObject. 
1.1385789178679555 <-> ?answer <http://confOf#reviewes> ?someObject. 
1.5532195651294716 <-> ?answer <http://confOf#writes> ?someObject. 
1.6379850686825204 <-> ?x <http://confOf#writtenBy> ?answer. ?x a <http://confOf#Contribution>. 
creating directory: paper_co_author

Binary query : ?answer1 a <http://conference#Abstract>. ?answer0 <http://conference#contributes> ?answer1 . ?o2 <http://conference#has_an_abstract> ?answer1. ?o2 a <http://conference#Invited_talk>.
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 a <http://conference#Invited_speaker>. ?answer0 <http://conference#contributes> ?answer1 . ?answer1 a <http://conference#Invited_talk>.
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#contributes> ?o2 .  ?o2 <http://conference#reviews> ?answer1 .  ?answer0 <http://conference#was_a_member_of> ?o3 . ?o3 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :10
Number of correspondences found: 1
6.188398692810458 <-> ?answer0 a <http://confOf#Member_PC>.  ?answer0 <http://confOf#reviewes> ?answer1.  ?answer1 a <http://confOf#Contribution>.  
creating directory: is_assigned_reviewer_of

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#gives_presentations> ?answer1. ?answer1 a <http://conference#Presentation>.
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer> .  ?answer <http://conference#invited_by> ?o2 .
Number of matched answers :10
Number of correspondences found: 5
0.6999999999999998 <-> ?answer <http://confOf#studyAt> ?someObject. 
0.8816666666666666 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
1.0023809523809524 <-> ?answer <http://confOf#employedBy> ?y. ?y a <http://www.w3.org/2002/07/owl#NamedIndividual>. 
2.208578724005959 <-> ?answer <http://confOf#writes> ?someObject. 
2.2808009462281813 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
creating directory: external_reviewer

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 . ?o2 <http://conference#is_a_date_of_camera_ready_paper_submission> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
2.4622920938710413 <-> ?answer0 <http://confOf#hasAdministrativeEvent> ?v1.  ?v1 a <http://confOf#Camera_Ready_event>.  ?v1 <http://confOf#ends_on> ?answer1.  
creating directory: conference_camera_date

Binary query : ?answer0 <http://conference#contributes> ?answer1 .   ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Review> .    ?answer0 <http://conference#was_a_member_of> ?o3 . ?o3 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer1 <http://conference#has_tracks> ?answer0 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#was_a_track-workshop_chair_of> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop> .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_volume> .
Number of matched answers :5
Number of correspondences found: 4
0.6389022719838631 <-> ?answer <http://confOf#has_title> "Japan Conference on Discrete and Computational Geometry, Graphs, and Games (JCDCG3)". 
0.7799991161597477 <-> ?answer <http://confOf#hasAdministrativeEvent> ?y. ?y a <http://confOf#Reviewing_event>. 
0.867342799188641 <-> ?answer <http://confOf#location> ?someObject. 
0.8823529411764706 <-> ?answer <http://confOf#hasTopic> ?y. ?y a <http://confOf#Topic>. 
creating directory: conference

Binary query : ?answer0 <http://conference#has_the_first_name> ?o1 .  ?answer0 <http://conference#has_the_last_name> ?o2 .   BIND( concat(STR(?o1)," ", STR(?o2)) as ?answer1)
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Person> .
Number of matched answers :10
Number of correspondences found: 5
0.7273809523809524 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
0.7416666666666667 <-> ?answer <http://confOf#hasSurname> ?someObject. 
1.125 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 
1.2487501422791534 <-> ?answer <http://confOf#writes> ?someObject. 
1.3598612533902643 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
creating directory: person

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_proceedings> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#has_a_publisher> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 .   ?answer0 a <http://conference#Conference_volume> . ?o2 <http://conference#is_a_starting_date> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
2.6468868249054007 <-> ?answer0 a <http://confOf#Conference>.  ?answer0 <http://confOf#starts_on> ?answer1.  
creating directory: conference_start_date

Binary query : ?answer0 <http://conference#invites_co-reviewers> ?answer1 .
Number of matched answers :10
Number of correspondences found: 3
1.6 <-> ?answer0 a <http://confOf#Member_PC>.  ?v1 <http://confOf#writtenBy> ?answer0.  ?v1 <http://confOf#writtenBy> ?answer1.  
1.65 <-> ?answer0 a <http://confOf#Member_PC>.  ?v1 <http://confOf#writtenBy> ?answer0.  ?answer1 <http://confOf#writes> ?v1.  
1.65 <-> ?answer0 a <http://confOf#Member_PC>.  ?answer0 <http://confOf#writes> ?v1.  ?v1 <http://confOf#writtenBy> ?answer1.  
creating directory: invite_reviewer

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Tutorial> .
Number of matched answers :10
Number of correspondences found: 2
0.875 <-> ?answer <http://confOf#hasTopic> ?y. ?y a <http://confOf#Topic>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Tutorial>. 
creating directory: tutorial

Unary query : ?answer a <http://conference#Conference_participant> .   ?answer a <http://conference#Early_paid_applicant> .
Number of matched answers :10
Number of correspondences found: 11
0.721969696969697 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar>. 
0.750909090909091 <-> ?answer <http://confOf#hasSurname> ?someObject. 
0.8136363636363637 <-> ?answer <http://confOf#earlyRegistration> ?someObject. 
0.8775139454243932 <-> ?answer <http://confOf#studyAt> ?someObject. 
0.9363636363636364 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
1.040909090909091 <-> ?answer <http://confOf#employedBy> ?y. ?y a <http://confOf#Organization>. 
1.0568181818181819 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
1.1272727272727274 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant>. 
2.2579829774107147 <-> ?answer <http://confOf#reviewes> ?someObject. 
2.569783327840756 <-> ?answer <http://confOf#writes> ?someObject. 
2.569783327840756 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
creating directory: early_registered_participant

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference> . ?answer0 <http://conference#has_a_name> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
2.6 <-> ?answer0 a <http://confOf#Conference>.  ?answer0 <http://confOf#has_title> ?answer1.  
creating directory: conference_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Abstract> .   ?answer <http://conference#is_the_1th_part_of> ?o2. ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://conference#was_a_member_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :10
Number of correspondences found: 8
0.6196078431372549 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 
0.8752941176470589 <-> ?answer <http://confOf#hasSurname> ?someObject. 
0.966984943977591 <-> ?answer <http://confOf#employedBy> ?someObject. 
1.006078431372549 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
1.203921568627451 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
1.9541352375513996 <-> ?answer <http://confOf#reviewes> ?someObject. 
2.2931351209292616 <-> ?answer <http://confOf#writes> ?someObject. 
2.3009782581841627 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
creating directory: pc_member

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop> .
Number of matched answers :10
Number of correspondences found: 4
0.6907407407407407 <-> ?answer <http://confOf#has_short_title> ?someObject. 
0.6944444444444444 <-> ?answer <http://confOf#has_title> "ZiF Workshop (ZiF)". 
0.75 <-> ?answer <http://confOf#hasTopic> ?y. ?y a <http://confOf#Topic>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop>. 
creating directory: workshop

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 . ?o2 <http://conference#is_a_full_paper_submission_date> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
2.4033062627868107 <-> ?answer0 <http://confOf#hasAdministrativeEvent> ?v1.  ?v1 a <http://confOf#Reviewing_event>.  ?v1 <http://confOf#starts_on> ?answer1.  
creating directory: conference_paper_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer> . ?answer <http://conference#was_a_member_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :10
Number of correspondences found: 11
0.6191176470588236 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author>. 
0.7269162210338681 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant>. 
0.7446078431372549 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 
0.7799019607843138 <-> ?answer <http://confOf#earlyRegistration> ?someObject. 
1.0100163398692814 <-> ?answer <http://confOf#hasSurname> ?someObject. 
1.279722748656572 <-> ?answer <http://confOf#employedBy> ?someObject. 
1.3269117647058826 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
2.4261437908496735 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
3.368293067371717 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
3.388227707894594 <-> ?answer <http://confOf#writes> ?someObject. 
3.688131219329773 <-> ?answer <http://confOf#reviewes> ?someObject. 
creating directory: assigned_reviewer

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Accepted_contribution> .
Number of matched answers :10
Number of correspondences found: 7
0.6082251082251083 <-> ?answer <http://confOf#hasTitle> ?someObject. 
0.6640452557288177 <-> ?x <http://confOf#reviewes> ?answer. ?x a <http://confOf#Member_PC>. 
0.6850517598343687 <-> ?answer <http://confOf#hasKeyword> ?someObject. 
0.7920209898466393 <-> ?answer <http://confOf#dealsWith> ?y. ?y a <http://confOf#Topic>. 
0.8095238095238095 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Contribution>. 
0.918550641137137 <-> ?x <http://confOf#writes> ?answer. ?x a <http://confOf#Scholar>. 
0.9842283201521873 <-> ?answer <http://confOf#writtenBy> ?y. ?y a <http://confOf#Member_PC>. 
creating directory: accepted_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_www> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://conference#was_a_track-workshop_chair_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop> .
Number of matched answers :10
Number of correspondences found: 11
0.7059386973180077 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
0.7382662835249043 <-> ?answer <http://confOf#hasSurname> ?someObject. 
0.7421630094043887 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant>. 
0.7987807049889919 <-> ?answer <http://confOf#employedBy> ?someObject. 
0.810344827586207 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author>. 
0.8663793103448276 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 
0.8790229885057472 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
0.9755747126436782 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar>. 
1.5845986032575081 <-> ?answer <http://confOf#reviewes> ?someObject. 
2.219732977399502 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
2.3931046249090806 <-> ?answer <http://confOf#writes> ?someObject. 
creating directory: workshop_chair

Unary query : ?answer <http://conference#is_the_1th_part_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Invited_talk> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Invited_speaker> .
Number of matched answers :10
Number of correspondences found: 5
0.6 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
0.7433333333333332 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
1.095207515696529 <-> ?answer <http://confOf#reviewes> ?someObject. 
1.0963721856868218 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
1.1630388523534887 <-> ?answer <http://confOf#writes> ?someObject. 
creating directory: invited_speaker

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Regular_author> . ?answer0 <http://conference#contributes> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .   minus{  ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Abstract> .   }
Number of matched answers :10
Number of correspondences found: 6
6.066305916305916 <-> ?answer0 a <http://confOf#Author>.  ?answer1 <http://confOf#writtenBy> ?answer0.  ?answer1 a <http://confOf#Contribution>.  
6.118326118326118 <-> ?answer0 a <http://confOf#Member_PC>.  ?answer1 <http://confOf#writtenBy> ?answer0.  ?answer1 a <http://confOf#Contribution>.  
6.27435064935065 <-> ?answer0 a <http://confOf#Author>.  ?answer0 <http://confOf#writes> ?answer1.  ?answer1 a <http://confOf#Contribution>.  
6.326370851370852 <-> ?answer0 a <http://confOf#Member_PC>.  ?answer0 <http://confOf#writes> ?answer1.  ?answer1 a <http://confOf#Contribution>.  
6.343686868686868 <-> ?answer0 a <http://confOf#Scholar>.  ?answer1 <http://confOf#writtenBy> ?answer0.  ?answer1 a <http://confOf#Contribution>.  
6.551731601731602 <-> ?answer0 a <http://confOf#Scholar>.  ?answer0 <http://confOf#writes> ?answer1.  ?answer1 a <http://confOf#Contribution>.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 . ?o2 <http://conference#is_an_ending_date> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.0572755417956659 <-> ?answer0 <http://confOf#ends_on> ?answer1.  
creating directory: conference_end_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Topic> .
Number of matched answers :10
Number of correspondences found: 3
1.0439229249001392 <-> ?someSubject <http://confOf#dealsWith> ?answer. 
1.0864613860165369 <-> ?x <http://confOf#hasTopic> ?answer. ?x a <http://confOf#Tutorial>. 
1.125 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Topic>. 
creating directory: topic

Binary query : ?answer0 a <http://conference#Abstract>. ?answer1 <http://conference#has_an_abstract> ?answer0 . ?answer1 a <http://conference#Invited_talk>.
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?o2 a <http://conference#Conference_www>. ?o2 <http://conference#has_a_URL> ?answer .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://conference#was_a_member_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Steering_committee> .
Number of matched answers :10
Number of correspondences found: 7
0.845019545954595 <-> ?answer <http://confOf#employedBy> ?someObject. 
0.9233333333333332 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
0.9444444444444443 <-> ?answer <http://confOf#hasSurname> ?someObject. 
1.1777777777777776 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
2.023438808730821 <-> ?answer <http://confOf#reviewes> ?someObject. 
2.3171687898749886 <-> ?answer <http://confOf#writes> ?someObject. 
2.3282799009861 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
creating directory: sc_member

Binary query : ?answer1 <http://conference#has_authors> ?answer0 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Review> .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer> .
Number of matched answers :10
Number of correspondences found: 6
0.29999999999999993 <-> ?answer <http://confOf#employedBy> ?y. ?y a <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.3583333333333334 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
1.1377402151306835 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
1.1655179929084607 <-> ?answer <http://confOf#writes> ?someObject. 
1.2222222222222223 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
1.7122448219028734 <-> ?answer <http://confOf#reviewes> ?someObject. 
creating directory: reviewers_all

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Review> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#has_the_first_name> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
1.5777777777777777 <-> ?answer0 <http://confOf#hasFirstName> ?answer1.  
creating directory: person_first_name

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop> .  ?answer0 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
3.135714285714286 <-> ?answer0 a <http://confOf#Workshop>.  ?answer0 <http://confOf#hasTopic> ?answer1.  ?answer1 a <http://confOf#Topic>.  
creating directory: workshop_topic

Unary query : ?answer a <http://conference#Invited_talk> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Regular_author> .
Number of matched answers :10
Number of correspondences found: 4
0.6071428571428572 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar>. 
1.0331000449103593 <-> ?answer <http://confOf#reviewes> ?someObject. 
1.135080508893367 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
1.2065090803219387 <-> ?answer <http://confOf#writes> ?someObject. 
creating directory: author

Binary query : ?answer0 a <http://conference#Written_contribution> . ?answer0 <http://conference#has_an_abstract> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 a <http://conference#Presentation>. ?answer0 <http://conference#has_an_abstract> ?o2 . ?answer1 <http://conference#has_an_abstract> ?o2 . ?answer1 a <http://conference#Written_contribution> .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://conference#was_a_committee_chair_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Organizing_committee> .
Number of matched answers :5
Number of correspondences found: 8
0.6833333333333332 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant>. 
0.8083333333333333 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
0.9283333333333333 <-> ?answer <http://confOf#hasSurname> ?someObject. 
1.0499999999999998 <-> ?answer <http://confOf#employedBy> ?y. ?y a <http://confOf#Organization>. 
1.185 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
1.9861370993118088 <-> ?answer <http://confOf#reviewes> ?someObject. 
2.053687549730582 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
2.062020883063915 <-> ?answer <http://confOf#writes> ?someObject. 
creating directory: oc_chair

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> . minus{ ?answer a  <http://conference#Abstract> . }
Number of matched answers :10
Number of correspondences found: 8
0.65 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Poster>. 
0.8058149603253799 <-> ?answer <http://confOf#hasKeyword> ?someObject. 
0.9243207413683117 <-> ?answer <http://confOf#hasTitle> ?someObject. 
1.0711870520437772 <-> ?answer <http://confOf#dealsWith> ?y. ?y a <http://confOf#Topic>. 
1.0787955135941076 <-> ?someSubject <http://confOf#reviewes> ?answer. 
1.1 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Contribution>. 
1.5677058575955636 <-> ?someSubject <http://confOf#writes> ?answer. 
1.7038169687066747 <-> ?answer <http://confOf#writtenBy> ?someObject. 
creating directory: paper

Unary query : ?answer <http://conference#gives_presentations> ?o3. ?o3 <http://conference#has_an_abstract> ?o2 . ?o4 <http://conference#has_an_abstract> ?o2 . ?o4 a <http://conference#Written_contribution> .
Number of matched answers :10
Number of correspondences found: 13
0.9298245614035088 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author>. 
1.049122807017544 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant>. 
1.0710526315789473 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 
1.2543859649122808 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
1.3236842105263156 <-> ?answer <http://confOf#studyAt> ?y. ?y a <http://confOf#Organization>. 
1.4011351909184726 <-> ?answer <http://confOf#earlyRegistration> ?someObject. 
1.4048245614035086 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar>. 
1.4828070175438597 <-> ?answer <http://confOf#hasSurname> ?someObject. 
1.7062280701754389 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
1.7815789473684212 <-> ?answer <http://confOf#employedBy> ?y. ?y a <http://confOf#Organization>. 
3.569200467663907 <-> ?answer <http://confOf#reviewes> ?someObject. 
4.1272908383148215 <-> ?answer <http://confOf#writes> ?someObject. 
4.277290838314821 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
creating directory: presenter

Binary query : ?answer0 <http://conference#is_submitted_at> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://conference#was_a_committee_chair_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :5
Number of correspondences found: 11
0.6274509803921569 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author>. 
0.6446078431372549 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 
0.7107843137254902 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Chair_PC>. 
0.7450980392156863 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant>. 
0.9019607843137255 <-> ?answer <http://confOf#hasSurname> ?someObject. 
0.928921568627451 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
1.08297339914987 <-> ?answer <http://confOf#employedBy> ?someObject. 
1.1416666666666666 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
2.127278573053706 <-> ?answer <http://confOf#reviewes> ?someObject. 
2.5285951638581703 <-> ?someSubject <http://confOf#writtenBy> ?answer. 
2.545752026603268 <-> ?answer <http://confOf#writes> ?someObject. 
creating directory: pc_chair

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Paper> .
Number of matched answers :10
Number of correspondences found: 4
0.7642040149393089 <-> ?someSubject <http://confOf#reviewes> ?answer. 
0.7656288609719982 <-> ?someSubject <http://confOf#writes> ?answer. 
0.785287252155764 <-> ?answer <http://confOf#writtenBy> ?someObject. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Paper>. 
creating directory: regular_paper

Binary query : { ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Paper> .   } UNION { ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .   ?o2 <http://conference#is_the_1th_part_of> ?answer0. ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .   }  ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.
Number of matched answers :10
Number of correspondences found: 2
3.147222222222222 <-> ?answer0 a <http://confOf#Poster>.  ?answer0 <http://confOf#hasTitle> ?answer1.  
3.7749999999999995 <-> ?answer0 a <http://confOf#Paper>.  ?answer0 <http://confOf#hasTitle> ?answer1.  
creating directory: paper_title

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Presentation> . minus{ ?answer a <http://conference#Invited_talk> . }
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Abstract> .   ?answer0 <http://conference#is_the_1th_part_of> ?o2. ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .    ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 a <http://conference#Presentation> . ?answer0 <http://conference#has_an_abstract> ?o2 . ?o4 <http://conference#has_an_abstract> ?o2 . ?o4 a <http://conference#Written_contribution> . ?o4 <http://conference#is_submitted_at> ?answer1.
Number of matched answers :10
Number of correspondences found: 0
Matching process ended
