===============================================================================
creating directory: conference-cmt
Running with 10 support instances - 0.7 similarity.
Number of CQAs: 73

Unary query : ?answer a <http://conference#Contribution_1th-author>.
Number of matched answers :10
Number of correspondences found: 8
0.0 <-> ?answer <http://cmt#co-writePaper> ?someObject. 
0.0 <-> ?answer <http://cmt#memberOfConference> <http://cmt-instances#conference1428971814>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceMember>. 
0.0 <-> ?answer <http://cmt#name> "Anthony Quinlan". 
0.0 <-> <http://cmt-instances#conference-1409861683> <http://cmt#hasConferenceMember> ?answer. 
0.0 <-> <http://cmt-instances#person687459034> <http://cmt#assignedByAdministrator> ?answer. 
0.0 <-> <http://cmt-instances#paper2861402034461114> <http://cmt#assignedTo> ?answer. 
0.0 <-> <http://cmt-instances#paper-19756913143112198> <http://cmt#hasCo-author> ?answer. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Publisher> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Rejected_contribution> .
Number of matched answers :10
Number of correspondences found: 6
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperFullVersion>. 
0.0 <-> ?someSubject <http://cmt#hasBeenAssigned> ?answer. 
0.0 <-> ?answer <http://cmt#hasSubjectArea> <http://cmt-instances#topic1815364945>. 
0.0 <-> ?answer <http://cmt#paperID> "1191263". 
0.0 <-> <http://cmt-instances#person-901707288> <http://cmt#rejectPaper> ?answer. 
0.0 <-> ?answer <http://cmt#hasCo-author> <http://cmt-instances#person69880442>. 

Binary query : ?answer0 <http://conference#has_the_last_name> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Track> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_participant> .
Number of matched answers :10
Number of correspondences found: 8
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ExternalReviewer>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#User>. 
0.0 <-> ?someSubject <http://cmt#writtenBy> ?answer. 
0.0 <-> ?answer <http://cmt#co-writePaper> <http://cmt-instances#paper-2070512379231263>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Co-author>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Author>. 
0.0 <-> ?answer <http://cmt#assignedByReviewer> <http://cmt-instances#person63389009>. 

Unary query : ?answer <http://conference#was_a_member_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Organizing_committee> .
Number of matched answers :10
Number of correspondences found: 8
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#User>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Author>. 
0.0 <-> <http://cmt-instances#paper-2012208257164257> <http://cmt#assignedTo> ?answer. 
0.0 <-> ?answer <http://cmt#co-writePaper> <http://cmt-instances#paper1850386443322198>. 
0.0 <-> <http://cmt-instances#paper-15610573324781114> <http://cmt#readByReviewer> ?answer. 
0.0 <-> ?answer <http://cmt#memberOfConference> <http://cmt-instances#conference943277065>. 
0.0 <-> <http://cmt-instances#conference69622803> <http://cmt#hasConferenceMember> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceMember>. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_proceedings> .   ?answer0 <http://conference#has_a_name> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer1 <http://conference#has_workshops> ?answer0 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#was_a_member_of> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :10
Number of correspondences found: 2
1.4411764705882353 <-> ?answer0 <http://cmt#memberOfProgramCommittee> ?answer1.  ?answer1 a <http://cmt#ProgramCommittee>.  
1.4411764705882353 <-> ?answer1 <http://cmt#hasProgramCommitteeMember> ?answer0.  ?answer1 a <http://cmt#ProgramCommittee>.  
creating directory: member_of_pc

Binary query : ?answer0 <http://conference#reviews> ?answer1 .
Number of matched answers :10
Number of correspondences found: 3
2.107142857142857 <-> ?answer0 a <http://cmt#Review>.  ?v1 <http://cmt#writeReview> ?answer0.  ?v1 a <http://cmt#Reviewer>.  ?v1 <http://cmt#hasBeenAssigned> ?answer1.  
2.107142857142857 <-> ?answer0 a <http://cmt#Review>.  ?v1 <http://cmt#writeReview> ?answer0.  ?v1 a <http://cmt#Reviewer>.  ?answer1 <http://cmt#assignedTo> ?v1.  
2.107142857142857 <-> ?answer0 a <http://cmt#Review>.  ?answer0 <http://cmt#writtenBy> ?v1.  ?v1 a <http://cmt#Reviewer>.  ?v1 <http://cmt#hasBeenAssigned> ?answer1.  
creating directory: review_of_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Tutorial> .  ?answer0 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 . ?o2 a <http://conference#Important_dates>. ?o2 <http://conference#is_a_date_of_acceptance_announcement> ?answer1.
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 a <http://conference#Conference_participant>.      bind( if (exists {?answer0 a <http://conference#Early_paid_applicant>}, "true"^^<http://www.w3.org/2001/XMLSchema#boolean>,        if (exists {?answer0 a <http://conference#Late_paid_applicant>}, "false"^^<http://www.w3.org/2001/XMLSchema#boolean>,"")) as ?answer1)
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer1 <http://conference#has_tutorials> ?answer0 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#has_a_location> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Unary query : ?answer a <http://conference#Conference_participant> .   ?answer a <http://conference#Late_paid_applicant> .
Number of matched answers :10
Number of correspondences found: 8
0.0 <-> ?someSubject <http://cmt#assignExternalReviewer> ?answer. 
0.0 <-> <http://cmt-instances#conference-1409861683> <http://cmt#hasConferenceMember> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Meta-Reviewer>. 
0.0 <-> ?answer <http://cmt#hasBeenAssigned> <http://cmt-instances#paper20629177143661114>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceMember>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer>. 
0.0 <-> ?answer <http://cmt#readPaper> <http://cmt-instances#paper1726173436972198>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Contribution_co-author> .
Number of matched answers :10
Number of correspondences found: 7
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ExternalReviewer>. 
0.0 <-> ?answer <http://cmt#name> "Anthony Quinlan". 
0.0 <-> ?answer <http://cmt#co-writePaper> ?someObject. 
0.0 <-> ?answer <http://cmt#markConflictOfInterest> ?someObject. 
0.0 <-> <http://cmt-instances#review-20487015603782198_-196548215> <http://cmt#writtenBy> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceMember>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#User>. 

Binary query : ?answer1 a <http://conference#Abstract>. ?answer0 <http://conference#contributes> ?answer1 . ?o2 <http://conference#has_an_abstract> ?answer1. ?o2 a <http://conference#Invited_talk>.
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 a <http://conference#Invited_speaker>. ?answer0 <http://conference#contributes> ?answer1 . ?answer1 a <http://conference#Invited_talk>.
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#contributes> ?o2 .  ?o2 <http://conference#reviews> ?answer1 .  ?answer0 <http://conference#was_a_member_of> ?o3 . ?o3 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :10
Number of correspondences found: 2
0.5 <-> ?answer0 <http://cmt#hasBeenAssigned> ?answer1.  
0.5 <-> ?answer1 <http://cmt#assignedTo> ?answer0.  
creating directory: is_assigned_reviewer_of

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :5
Number of correspondences found: 3
0.7142857142857143 <-> ?x <http://cmt#memberOfProgramCommittee> ?answer. ?x a <http://cmt#ProgramCommitteeChair>. 
0.7142857142857143 <-> ?answer <http://cmt#hasProgramCommitteeMember> ?y. ?y a <http://cmt#ProgramCommitteeChair>. 
0.9411764705882353 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommittee>. 
creating directory: program_committee

Binary query : ?answer0 <http://conference#gives_presentations> ?answer1. ?answer1 a <http://conference#Presentation>.
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer> .  ?answer <http://conference#invited_by> ?o2 .
Number of matched answers :10
Number of correspondences found: 2
1.0 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Reviewer>. 
1.0 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Reviewer>. 
creating directory: external_reviewer

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 . ?o2 <http://conference#is_a_date_of_camera_ready_paper_submission> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#contributes> ?answer1 .   ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Review> .    ?answer0 <http://conference#was_a_member_of> ?o3 . ?o3 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :10
Number of correspondences found: 2
1.5 <-> ?answer1 <http://cmt#writtenBy> ?answer0.  ?answer1 a <http://cmt#Review>.  
1.5 <-> ?answer0 <http://cmt#writeReview> ?answer1.  ?answer1 a <http://cmt#Review>.  
creating directory: assigned_reviewer_author_of_review

Binary query : ?answer1 <http://conference#has_tracks> ?answer0 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#was_a_track-workshop_chair_of> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop> .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_volume> .
Number of matched answers :5
Number of correspondences found: 1
0.0 <-> ?answer <http://cmt#hasConferenceMember> ?someObject. 

Binary query : ?answer0 <http://conference#has_the_first_name> ?o1 .  ?answer0 <http://conference#has_the_last_name> ?o2 .   BIND( concat(STR(?o1)," ", STR(?o2)) as ?answer1)
Number of matched answers :10
Number of correspondences found: 1
0.5 <-> ?answer0 <http://cmt#name> ?answer1.  
creating directory: person_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Person> .
Number of matched answers :10
Number of correspondences found: 3
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person>. 
1.0 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Person>. 
1.0 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Person>. 
creating directory: person

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_proceedings> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#has_a_publisher> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 .   ?answer0 a <http://conference#Conference_volume> . ?o2 <http://conference#is_a_starting_date> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
0.5 <-> ?answer0 <http://cmt#date> ?answer1.  
creating directory: conference_start_date

Binary query : ?answer0 <http://conference#invites_co-reviewers> ?answer1 .
Number of matched answers :10
Number of correspondences found: 2
0.5 <-> ?answer1 <http://cmt#assignedByReviewer> ?answer0.  
0.5 <-> ?answer0 <http://cmt#assignExternalReviewer> ?answer1.  
creating directory: invite_reviewer

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Tutorial> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer a <http://conference#Conference_participant> .   ?answer a <http://conference#Early_paid_applicant> .
Number of matched answers :10
Number of correspondences found: 8
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ExternalReviewer>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#User>. 
0.0 <-> ?someSubject <http://cmt#writtenBy> ?answer. 
0.0 <-> ?answer <http://cmt#co-writePaper> <http://cmt-instances#paper-2070512379231263>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Co-author>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Author>. 
0.0 <-> ?answer <http://cmt#assignedByReviewer> <http://cmt-instances#person63389009>. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference> . ?answer0 <http://conference#has_a_name> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#name> ?answer1.  
creating directory: conference_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Abstract> .   ?answer <http://conference#is_the_1th_part_of> ?o2. ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .
Number of matched answers :10
Number of correspondences found: 3
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Document>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperAbstract>. 
0.0 <-> ?answer <http://cmt#hasSubjectArea> ?someObject. 

Unary query : ?answer <http://conference#was_a_member_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :10
Number of correspondences found: 2
0.9411764705882353 <-> ?x <http://cmt#hasProgramCommitteeMember> ?answer. ?x a <http://cmt#ProgramCommittee>. 
0.9411764705882353 <-> ?answer <http://cmt#memberOfProgramCommittee> ?y. ?y a <http://cmt#ProgramCommittee>. 
creating directory: pc_member

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 . ?o2 <http://conference#is_a_full_paper_submission_date> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer> . ?answer <http://conference#was_a_member_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :10
Number of correspondences found: 5
0.75 <-> ?x <http://cmt#writtenBy> ?answer. ?x a <http://cmt#Review>. 
0.75 <-> ?answer <http://cmt#writeReview> ?y. ?y a <http://cmt#Review>. 
0.9411764705882353 <-> ?x <http://cmt#hasProgramCommitteeMember> ?answer. ?x a <http://cmt#ProgramCommittee>. 
0.9411764705882353 <-> ?answer <http://cmt#memberOfProgramCommittee> ?y. ?y a <http://cmt#ProgramCommittee>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer>. 
creating directory: assigned_reviewer

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Accepted_contribution> .
Number of matched answers :10
Number of correspondences found: 8
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Paper>. 
0.0 <-> ?answer <http://cmt#hasSubjectArea> ?someObject. 
0.0 <-> <http://cmt-instances#person555234723> <http://cmt#markConflictOfInterest> ?answer. 
0.0 <-> <http://cmt-instances#person350310735> <http://cmt#co-writePaper> ?answer. 
0.0 <-> <http://cmt-instances#person-776571692> <http://cmt#readPaper> ?answer. 
0.0 <-> ?answer <http://cmt#readByMeta-Reviewer> <http://cmt-instances#person657957531>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperFullVersion>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Document>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_www> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://conference#was_a_track-workshop_chair_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop> .
Number of matched answers :10
Number of correspondences found: 8
0.0 <-> ?someSubject <http://cmt#assignExternalReviewer> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#User>. 
0.0 <-> ?answer <http://cmt#readPaper> <http://cmt-instances#paper-19311312753072198>. 
0.0 <-> <http://cmt-instances#conference-1409861683> <http://cmt#hasConferenceMember> ?answer. 
0.0 <-> ?answer <http://cmt#hasBeenAssigned> <http://cmt-instances#paper20629177143661114>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ExternalReviewer>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person>. 

Unary query : ?answer <http://conference#is_the_1th_part_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Invited_talk> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Invited_speaker> .
Number of matched answers :10
Number of correspondences found: 6
0.0 <-> <http://cmt-instances#paper17563121268171114> <http://cmt#hasCo-author> ?answer. 
0.0 <-> ?answer <http://cmt#writeReview> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person>. 
0.0 <-> <http://cmt-instances#person8557820> <http://cmt#assignExternalReviewer> ?answer. 
0.0 <-> <http://cmt-instances#paper-15610573324781114> <http://cmt#readByReviewer> ?answer. 
0.0 <-> ?someSubject <http://cmt#hasConferenceMember> ?answer. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Regular_author> . ?answer0 <http://conference#contributes> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .   minus{  ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Abstract> .   }
Number of matched answers :10
Number of correspondences found: 4
0.5 <-> ?answer1 <http://cmt#hasAuthor> ?answer0.  
0.5 <-> ?answer0 <http://cmt#co-writePaper> ?answer1.  
0.5 <-> ?answer1 <http://cmt#hasCo-author> ?answer0.  
0.5 <-> ?answer0 <http://cmt#writePaper> ?answer1.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 . ?o2 <http://conference#is_an_ending_date> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Topic> .
Number of matched answers :10
Number of correspondences found: 2
0.0 <-> ?someSubject <http://cmt#hasSubjectArea> ?answer. 
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> ?someObject. 

Binary query : ?answer0 a <http://conference#Abstract>. ?answer1 <http://conference#has_an_abstract> ?answer0 . ?answer1 a <http://conference#Invited_talk>.
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?o2 a <http://conference#Conference_www>. ?o2 <http://conference#has_a_URL> ?answer .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://conference#was_a_member_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Steering_committee> .
Number of matched answers :10
Number of correspondences found: 10
0.0 <-> ?answer <http://cmt#co-writePaper> <http://cmt-instances#paper16474338158261114>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Author>. 
0.0 <-> <http://cmt-instances#conference1428971814> <http://cmt#hasConferenceMember> ?answer. 
0.0 <-> ?answer <http://cmt#readPaper> <http://cmt-instances#paper-1572844155262198>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer>. 
0.0 <-> <http://cmt-instances#paper-148583572581263> <http://cmt#hasCo-author> ?answer. 
0.0 <-> ?answer <http://cmt#memberOfConference> <http://cmt-instances#conference1428971814>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#User>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceMember>. 
0.0 <-> ?answer <http://cmt#writeReview> <http://cmt-instances#review-1159186596812198_-196548215>. 

Binary query : ?answer1 <http://conference#has_authors> ?answer0 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Review> .
Number of matched answers :10
Number of correspondences found: 6
2.25 <-> ?answer0 a <http://cmt#Reviewer>.  ?answer1 <http://cmt#writtenBy> ?answer0.  ?answer1 a <http://cmt#Review>.  
2.25 <-> ?answer0 <http://cmt#assignedByReviewer> ?v1.  ?v1 a <http://cmt#Reviewer>.  ?v1 <http://cmt#writeReview> ?answer1.  ?answer1 a <http://cmt#Review>.  
2.25 <-> ?answer0 <http://cmt#assignedByReviewer> ?v1.  ?v1 a <http://cmt#Reviewer>.  ?answer1 <http://cmt#writtenBy> ?v1.  ?answer1 a <http://cmt#Review>.  
2.25 <-> ?answer0 a <http://cmt#Reviewer>.  ?answer0 <http://cmt#writeReview> ?answer1.  ?answer1 a <http://cmt#Review>.  
2.25 <-> ?v1 <http://cmt#assignExternalReviewer> ?answer0.  ?v1 a <http://cmt#Reviewer>.  ?answer1 <http://cmt#writtenBy> ?v1.  ?answer1 a <http://cmt#Review>.  
2.25 <-> ?v1 <http://cmt#assignExternalReviewer> ?answer0.  ?v1 a <http://cmt#Reviewer>.  ?v1 <http://cmt#writeReview> ?answer1.  ?answer1 a <http://cmt#Review>.  
creating directory: all_reviewer_author_of_review

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer> .
Number of matched answers :10
Number of correspondences found: 5
0.75 <-> ?x <http://cmt#writtenBy> ?answer. ?x a <http://cmt#Review>. 
0.75 <-> ?answer <http://cmt#writeReview> ?y. ?y a <http://cmt#Review>. 
1.0 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Reviewer>. 
1.0 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Reviewer>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer>. 
creating directory: reviewers_all

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Review> .
Number of matched answers :10
Number of correspondences found: 3
0.75 <-> ?answer <http://cmt#writtenBy> ?y. ?y a <http://cmt#Reviewer>. 
0.75 <-> ?x <http://cmt#writeReview> ?answer. ?x a <http://cmt#Reviewer>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Review>. 
creating directory: review

Binary query : ?answer0 <http://conference#has_the_first_name> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop> .  ?answer0 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer a <http://conference#Invited_talk> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Regular_author> .
Number of matched answers :10
Number of correspondences found: 8
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ExternalReviewer>. 
0.0 <-> ?answer <http://cmt#co-writePaper> ?someObject. 
0.0 <-> <http://cmt-instances#review-14767682836062198_-196548215> <http://cmt#writtenBy> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Co-author>. 
0.0 <-> ?answer <http://cmt#markConflictOfInterest> <http://cmt-instances#paper-20673205111392198>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceMember>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#User>. 

Binary query : ?answer0 a <http://conference#Written_contribution> . ?answer0 <http://conference#has_an_abstract> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 a <http://conference#Presentation>. ?answer0 <http://conference#has_an_abstract> ?o2 . ?answer1 <http://conference#has_an_abstract> ?o2 . ?answer1 a <http://conference#Written_contribution> .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://conference#was_a_committee_chair_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Organizing_committee> .
Number of matched answers :5
Number of correspondences found: 4
0.0 <-> <http://cmt-instances#person-1860172640> <http://cmt#assignExternalReviewer> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceMember>. 
0.0 <-> ?answer <http://cmt#co-writePaper> ?someObject. 
0.0 <-> ?answer <http://cmt#hasBeenAssigned> <http://cmt-instances#paper-1980828754170257>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> . minus{ ?answer a  <http://conference#Abstract> . }
Number of matched answers :10
Number of correspondences found: 6
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperFullVersion>. 
0.0 <-> ?answer <http://cmt#hasSubjectArea> ?someObject. 
0.0 <-> <http://cmt-instances#person350310735> <http://cmt#co-writePaper> ?answer. 
0.0 <-> ?someSubject <http://cmt#hasBeenAssigned> ?answer. 
0.0 <-> <http://cmt-instances#person-901707288> <http://cmt#rejectPaper> ?answer. 
0.0 <-> ?answer <http://cmt#readByMeta-Reviewer> <http://cmt-instances#person657957531>. 

Unary query : ?answer <http://conference#gives_presentations> ?o3. ?o3 <http://conference#has_an_abstract> ?o2 . ?o4 <http://conference#has_an_abstract> ?o2 . ?o4 a <http://conference#Written_contribution> .
Number of matched answers :10
Number of correspondences found: 9
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ExternalReviewer>. 
0.0 <-> <http://cmt-instances#review-14767682836062198_-196548215> <http://cmt#writtenBy> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#User>. 
0.0 <-> <http://cmt-instances#paper2861402034461114> <http://cmt#assignedTo> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Author>. 
0.0 <-> <http://cmt-instances#conference-1409861683> <http://cmt#hasConferenceMember> ?answer. 
0.0 <-> ?answer <http://cmt#name> "Anthony Roberson". 
0.0 <-> ?answer <http://cmt#markConflictOfInterest> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#AuthorNotReviewer>. 

Binary query : ?answer0 <http://conference#is_submitted_at> ?answer1 .
Number of matched answers :10
Number of correspondences found: 10
0.5 <-> ?v1 <http://cmt#acceptPaper> ?answer0.  ?v1 <http://cmt#enterConferenceDetails> ?answer1.  
0.5 <-> ?v1 <http://cmt#co-writePaper> ?answer0.  ?v1 <http://cmt#memberOfConference> ?answer1.  
0.5 <-> ?v1 <http://cmt#acceptPaper> ?answer0.  ?answer1 <http://cmt#detailsEnteredBy> ?v1.  
0.5 <-> ?answer0 <http://cmt#acceptedBy> ?v1.  ?v1 <http://cmt#enterConferenceDetails> ?answer1.  
0.5 <-> ?v1 <http://cmt#rejectPaper> ?answer0.  ?v1 <http://cmt#enterConferenceDetails> ?answer1.  
0.5 <-> ?answer0 <http://cmt#acceptedBy> ?v1.  ?answer1 <http://cmt#detailsEnteredBy> ?v1.  
0.5 <-> ?v1 <http://cmt#hasBeenAssigned> ?answer0.  ?v1 <http://cmt#memberOfConference> ?answer1.  
0.5 <-> ?v1 <http://cmt#rejectPaper> ?answer0.  ?answer1 <http://cmt#detailsEnteredBy> ?v1.  
0.5 <-> ?answer0 <http://cmt#assignedTo> ?v1.  ?answer1 <http://cmt#hasConferenceMember> ?v1.  
0.5 <-> ?answer0 <http://cmt#assignedTo> ?v1.  ?v1 <http://cmt#memberOfConference> ?answer1.  
creating directory: paper_submitted_at_conference

Unary query : ?answer <http://conference#was_a_committee_chair_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :5
Number of correspondences found: 3
0.7142857142857143 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommitteeChair>. 
0.9411764705882353 <-> ?answer <http://cmt#memberOfProgramCommittee> ?y. ?y a <http://cmt#ProgramCommittee>. 
0.9411764705882353 <-> ?x <http://cmt#hasProgramCommitteeMember> ?answer. ?x a <http://cmt#ProgramCommittee>. 
creating directory: pc_chair

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Paper> .
Number of matched answers :10
Number of correspondences found: 2
0.7142857142857143 <-> ?answer <http://cmt#paperID> ?someObject. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Paper>. 
creating directory: regular_paper

Binary query : { ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Paper> .   } UNION { ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .   ?o2 <http://conference#is_the_1th_part_of> ?answer0. ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .   }  ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.
Number of matched answers :10
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://cmt#Paper>.  ?answer0 <http://cmt#title> ?answer1.  
creating directory: paper_title

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Presentation> . minus{ ?answer a <http://conference#Invited_talk> . }
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Abstract> .   ?answer0 <http://conference#is_the_1th_part_of> ?o2. ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .    ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.
Number of matched answers :10
Number of correspondences found: 1
0.5 <-> ?answer0 <http://cmt#title> ?answer1.  
creating directory: paper_abstract_title

Binary query : ?answer0 a <http://conference#Presentation> . ?answer0 <http://conference#has_an_abstract> ?o2 . ?o4 <http://conference#has_an_abstract> ?o2 . ?o4 a <http://conference#Written_contribution> . ?o4 <http://conference#is_submitted_at> ?answer1.
Number of matched answers :10
Number of correspondences found: 0
Matching process ended
