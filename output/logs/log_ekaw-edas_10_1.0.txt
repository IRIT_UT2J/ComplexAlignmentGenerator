===============================================================================
creating directory: ekaw-edas
Running with 10 support instances - 1.0 similarity.
Number of CQAs: 65

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Proceedings_Publisher> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Rejected_Paper> .
Number of matched answers :10
Number of correspondences found: 8
0.0 <-> ?answer <http://edas#isReviewedBy> ?someObject. 
0.0 <-> <http://edas-instances#person364225146> <http://edas#isReviewing> ?answer. 
0.0 <-> ?someSubject <http://edas#hasRelatedPaper> ?answer. 
0.0 <-> ?answer <http://edas#hasTopic> <http://edas-instances#topic1671619607>. 
0.0 <-> <http://edas-instances#conference-1409861683> <http://edas#hasRelatedDocument> ?answer. 
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> "Preparation of carrier free iodine samples for AMS analysis". 
0.0 <-> ?answer <http://edas#isWrittenBy> <http://edas-instances#person-1073906008>. 
0.0 <-> <http://edas-instances#topic515501146> <http://edas#isTopicOf> ?answer. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Track> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Participant> .   {?answer a <http://ekaw#Early-Registered_Participant> . } union{ ?answer a <http://ekaw#Late-Registered_Participant> . }
Number of matched answers :10
Number of correspondences found: 7
0.0 <-> ?answer <http://edas#hasLastName> "Turnbull". 
0.0 <-> ?answer <http://edas#hasFirstName> "Ann". 
0.0 <-> ?answer <http://edas#hasRelatedPaper> ?someObject. 
0.0 <-> ?answer <http://edas#hasRelatedDocument> <http://edas-instances#review20518448391311_-196548215>. 
0.0 <-> ?someSubject <http://edas#isWrittenBy> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 
0.0 <-> ?answer <http://edas#isReviewing> <http://edas-instances#paper-12358590847852198>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#OC_Member> .
Number of matched answers :10
Number of correspondences found: 7
0.0 <-> ?someSubject <http://edas#relatesTo> ?answer. 
0.0 <-> ?answer <http://edas#hasRelatedDocument> <http://edas-instances#review20518448391311_-196548215>. 
0.0 <-> ?someSubject <http://edas#isWrittenBy> ?answer. 
0.0 <-> ?answer <http://edas#isReviewing> <http://edas-instances#paper21059711116211114>. 
0.0 <-> ?answer <http://edas#hasRelatedPaper> <http://edas-instances#paper18165060613902198_v0>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Attendee>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Proceedings> .   ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer1 <http://ekaw#hasEvent> ?answer0 .  ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .  ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop> .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://ekaw#reviewOfPaper> ?answer1 .
Number of matched answers :10
Number of correspondences found: 12
0.5 <-> ?answer0 <http://edas#relatesTo> ?v1.  ?v2 <http://edas#hasMember> ?v1.  ?v2 <http://edas#hasRelatedDocument> ?answer1.  
0.5 <-> ?answer0 <http://edas#relatesTo> ?v1.  ?v1 <http://edas#isMemberOf> ?v2.  ?v2 <http://edas#hasRelatedDocument> ?answer1.  
0.5 <-> ?v1 <http://edas#hasRelatedDocument> ?answer0.  ?v1 <http://edas#isMemberOf> ?v2.  ?v2 <http://edas#hasRelatedDocument> ?answer1.  
0.5 <-> ?v1 <http://edas#hasRelatedDocument> ?answer0.  ?v1 <http://edas#isMemberOf> ?v2.  ?answer1 <http://edas#relatesTo> ?v2.  
0.5 <-> ?v1 <http://edas#hasRelatedDocument> ?answer0.  ?answer1 <http://edas#relatesTo> ?v1.  
0.5 <-> ?answer0 <http://edas#relatesTo> ?v1.  ?v1 <http://edas#hasRelatedDocument> ?answer1.  
0.5 <-> ?v1 <http://edas#hasRelatedDocument> ?answer0.  ?v2 <http://edas#hasMember> ?v1.  ?v2 <http://edas#hasRelatedDocument> ?answer1.  
0.5 <-> ?v1 <http://edas#hasRelatedDocument> ?answer0.  ?v1 <http://edas#hasRelatedDocument> ?answer1.  
0.5 <-> ?v1 <http://edas#hasRelatedDocument> ?answer0.  ?v1 <http://edas#isReviewing> ?answer1.  
0.5 <-> ?v1 <http://edas#hasRelatedDocument> ?answer0.  ?v2 <http://edas#hasMember> ?v1.  ?answer1 <http://edas#relatesTo> ?v2.  
0.5 <-> ?v1 <http://edas#hasRelatedDocument> ?answer0.  ?answer1 <http://edas#isReviewedBy> ?v1.  
0.5 <-> ?answer0 <http://edas#relatesTo> ?v1.  ?v1 <http://edas#isReviewing> ?answer1.  
creating directory: review_of_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Tutorial> .  ?answer0 <http://ekaw#coversTopic> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 a <http://ekaw#Conference_Participant>.      bind( if (exists {?answer0 a <http://ekaw#Early-Registered_Participant>}, "true"^^<http://www.w3.org/2001/XMLSchema#boolean>,        if (exists {?answer0 a <http://ekaw#Late-Registered_Participant>}, "false"^^<http://www.w3.org/2001/XMLSchema#boolean>,"")) as ?answer1) filter(?answer1 != "")
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer1 <http://ekaw#hasEvent> ?answer0 .  ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .  ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Tutorial> .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 a <http://ekaw#Conference> . ?answer0 <http://ekaw#heldIn> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#location> ?answer1.  
creating directory: conference_location

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Late-Registered_Participant> .
Number of matched answers :10
Number of correspondences found: 8
0.0 <-> <http://edas-instances#review-554085786002198_-196548214> <http://edas#relatesTo> ?answer. 
0.0 <-> ?someSubject <http://edas#isWrittenBy> ?answer. 
0.0 <-> ?answer <http://edas#isReviewing> ?someObject. 
0.0 <-> ?answer <http://edas#hasRelatedDocument> <http://edas-instances#review-14810052594411114_-196548215>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reviewer>. 
0.0 <-> <http://edas-instances#conference-104081973> <http://edas#hasMember> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Attendee>. 
0.0 <-> ?answer <http://edas#hasRelatedPaper> <http://edas-instances#paper10080176053721114>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#University> .
Number of matched answers :10
Number of correspondences found: 3
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Organization>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#AcademiaOrganization>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Trip> .
Number of matched answers :5
Number of correspondences found: 4
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceEvent>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#NonAcademicEvent>. 
0.0 <-> ?answer <http://edas#hasStartDateTime> "2008-04-07T18:00:00". 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Excursion>. 

Binary query : ?answer0 a <http://ekaw#Invited_Speaker>. ?answer1 <http://ekaw#writtenBy> ?answer0. ?answer1 a <http://ekaw#Invited_Talk_Abstract>.
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 a <http://ekaw#Invited_Speaker>. ?o2 <http://ekaw#writtenBy> ?answer0. ?o2 <http://ekaw#paperPresentedAs> ?answer1. ?answer1 a <http://ekaw#Invited_Talk>.
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member> .   ?answer0 <http://ekaw#reviewerOfPaper> ?answer1 .
Number of matched answers :10
Number of correspondences found: 6
0.5 <-> ?v1 <http://edas#hasMember> ?answer0.  ?v1 <http://edas#hasRelatedDocument> ?answer1.  
0.5 <-> ?v1 <http://edas#hasMember> ?answer0.  ?answer1 <http://edas#relatesTo> ?v1.  
0.5 <-> ?answer0 <http://edas#isReviewing> ?answer1.  
0.5 <-> ?answer0 <http://edas#isMemberOf> ?v1.  ?answer1 <http://edas#relatesTo> ?v1.  
0.5 <-> ?answer0 <http://edas#isMemberOf> ?v1.  ?v1 <http://edas#hasRelatedDocument> ?answer1.  
0.5 <-> ?answer1 <http://edas#isReviewedBy> ?answer0.  
creating directory: is_assigned_reviewer_of

Unary query : ?o3 <http://ekaw#reviewWrittenBy> ?answer. MINUS{ ?answer <http://ekaw#reviewerOfPaper> ?o2. }
Number of matched answers :10
Number of correspondences found: 6
0.0 <-> ?someSubject <http://edas#isWrittenBy> ?answer. 
0.0 <-> ?answer <http://edas#hasRelatedDocument> ?someObject. 
0.0 <-> <http://edas-instances#conference-1409861683> <http://edas#hasMember> ?answer. 
0.0 <-> ?answer <http://edas#isReviewing> ?someObject. 
0.0 <-> <http://edas-instances#review9456309597651114_-196548212> <http://edas#relatesTo> ?answer. 
0.0 <-> ?answer <http://edas#hasRelatedPaper> <http://edas-instances#paper-10318174732262198>. 

Binary query : ?answer1 <http://ekaw#reviewWrittenBy> ?answer0 .   ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member> .  ?answer0 <http://ekaw#reviewerOfPaper> ?o3.
Number of matched answers :10
Number of correspondences found: 2
0.5 <-> ?answer0 <http://edas#hasRelatedDocument> ?answer1.  
0.5 <-> ?answer1 <http://edas#relatesTo> ?answer0.  
creating directory: assigned_reviewer_author_of_review

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Track> .   ?answer1 <http://ekaw#hasEvent> ?answer0 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop_Chair> .   ?answer0 <http://ekaw#organises> ?answer1 . ?answer1 a <http://ekaw#Workshop> .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .
Number of matched answers :5
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Conference>. 
creating directory: conference

Binary query : ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .   ?answer0 a <http://ekaw#Person> .
Number of matched answers :10
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://edas#Person>.  ?answer0 <http://edas#hasFirstName> ?answer1.  
creating directory: person_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Person> .
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 
creating directory: person

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Banquet> .   ?answer0 <http://ekaw#heldIn> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :5
Number of correspondences found: 2
1.5 <-> ?v1 <http://edas#isLocationOf> ?answer0.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
1.5 <-> ?answer0 <http://edas#hasLocation> ?v1.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: gala_dinner_location

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Proceedings> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer1 <http://ekaw#publisherOf> ?answer0 .  ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Proceedings> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?o3 <http://ekaw#reviewWrittenBy> ?answer0 .   ?o3 <http://ekaw#reviewWrittenBy> ?answer1 . ?answer1 <http://ekaw#reviewerOfPaper> ?o2 . ?o3 <http://ekaw#reviewOfPaper> ?o2 . FILTER(?o!=?answer0)
Number of matched answers :10
Number of correspondences found: 7
0.5 <-> ?answer0 <http://edas#hasRelatedDocument> ?v1.  ?v1 <http://edas#relatesTo> ?answer1.  
0.5 <-> ?v1 <http://edas#relatesTo> ?answer0.  ?answer1 <http://edas#hasRelatedDocument> ?v1.  
0.5 <-> ?v1 <http://edas#isReviewedBy> ?answer0.  ?v1 <http://edas#isReviewedBy> ?answer1.  
0.5 <-> ?v1 <http://edas#relatesTo> ?answer0.  ?v1 <http://edas#relatesTo> ?answer1.  
0.5 <-> ?v1 <http://edas#hasMember> ?answer0.  ?v1 <http://edas#hasMember> ?answer1.  
0.5 <-> ?v1 <http://edas#hasMember> ?answer0.  ?answer1 <http://edas#isMemberOf> ?v1.  
0.5 <-> ?answer0 <http://edas#isMemberOf> ?v1.  ?v1 <http://edas#hasMember> ?answer1.  
creating directory: invite_reviewer

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Tutorial> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Early-Registered_Participant> .
Number of matched answers :10
Number of correspondences found: 7
0.0 <-> ?answer <http://edas#hasLastName> "Turnbull". 
0.0 <-> ?answer <http://edas#hasFirstName> "Ann". 
0.0 <-> ?answer <http://edas#hasRelatedPaper> ?someObject. 
0.0 <-> ?answer <http://edas#hasRelatedDocument> <http://edas-instances#review20518448391311_-196548215>. 
0.0 <-> ?someSubject <http://edas#isWrittenBy> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 
0.0 <-> ?answer <http://edas#isReviewing> <http://edas-instances#paper-12358590847852198>. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .  ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#hasName> ?answer1.  
creating directory: conference_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member> .   ?answer <http://ekaw#reviewerOfPaper> ?o2.
Number of matched answers :10
Number of correspondences found: 5
0.0 <-> ?answer <http://edas#hasLastName> ?someObject. 
0.0 <-> ?answer <http://edas#isReviewing> ?someObject. 
0.0 <-> ?answer <http://edas#hasRelatedPaper> ?someObject. 
0.0 <-> <http://edas-instances#paper-437522444342198_v0> <http://edas#isReviewedBy> ?answer. 
0.0 <-> ?answer <http://edas#hasRelatedDocument> <http://edas-instances#review20518448391311_-196548215>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop> .
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Workshop>. 
creating directory: workshop

Binary query : ?answer0 <http://ekaw#coversTopic> ?answer1 .   ?answer0 a <http://ekaw#Conference> .
Number of matched answers :10
Number of correspondences found: 2
1.5 <-> ?answer0 a <http://edas#Conference>.  ?answer1 <http://edas#isTopicOf> ?answer0.  
1.5 <-> ?answer0 a <http://edas#Conference>.  ?answer0 <http://edas#hasTopic> ?answer1.  
creating directory: conference_topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member> .   ?answer <http://ekaw#reviewerOfPaper> ?o3.
Number of matched answers :10
Number of correspondences found: 5
0.0 <-> ?answer <http://edas#hasLastName> ?someObject. 
0.0 <-> ?answer <http://edas#isReviewing> ?someObject. 
0.0 <-> ?answer <http://edas#hasRelatedPaper> ?someObject. 
0.0 <-> <http://edas-instances#paper-437522444342198_v0> <http://edas#isReviewedBy> ?answer. 
0.0 <-> ?answer <http://edas#hasRelatedDocument> <http://edas-instances#review20518448391311_-196548215>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Accepted_Paper> .
Number of matched answers :10
Number of correspondences found: 6
0.0 <-> ?answer <http://edas#isWrittenBy> ?someObject. 
0.0 <-> ?someSubject <http://edas#isTopicOf> ?answer. 
0.0 <-> ?someSubject <http://edas#hasRelatedPaper> ?answer. 
0.0 <-> <http://edas-instances#person-409841011> <http://edas#isReviewing> ?answer. 
0.0 <-> <http://edas-instances#presentation1425870440169257> <http://edas#relatedToPaper> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#PublishedPaper>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Web_Site> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop_Chair> .
Number of matched answers :10
Number of correspondences found: 7
0.0 <-> ?answer <http://edas#isReviewing> ?someObject. 
0.0 <-> ?answer <http://edas#hasRelatedDocument> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 
0.0 <-> <http://edas-instances#paper-21172179016361114_v0> <http://edas#isReviewedBy> ?answer. 
0.0 <-> ?answer <http://edas#hasRelatedPaper> <http://edas-instances#paper10080176053721114>. 
0.0 <-> <http://edas-instances#paper-15765906475922198_v2> <http://edas#isWrittenBy> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Attendee>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Invited_Talk_Abstract> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Invited_Speaker> .
Number of matched answers :10
Number of correspondences found: 9
0.0 <-> <http://edas-instances#paper366467890101257_v0> <http://edas#isReviewedBy> ?answer. 
0.0 <-> ?answer <http://edas#isReviewing> <http://edas-instances#paper-253329908202257_v0>. 
0.0 <-> <http://edas-instances#paper-17813909028182198_v0> <http://edas#isWrittenBy> ?answer. 
0.0 <-> ?answer <http://edas#hasRelatedPaper> <http://edas-instances#paper15715729506351114_v0>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Author>. 
0.0 <-> ?answer <http://edas#hasRelatedDocument> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 
0.0 <-> ?answer <http://edas#hasLastName> "Amaro". 
0.0 <-> <http://edas-instances#conference-104081973> <http://edas#hasMember> ?answer. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper_Author> . ?answer0 <http://ekaw#authorOf> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .
Number of matched answers :10
Number of correspondences found: 2
1.5 <-> ?answer0 <http://edas#hasRelatedPaper> ?answer1.  ?answer1 a <http://edas#Paper>.  
1.5 <-> ?answer1 <http://edas#isWrittenBy> ?answer0.  ?answer1 a <http://edas#Paper>.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Trip> .   ?answer0 <http://ekaw#heldIn> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :5
Number of correspondences found: 2
1.5 <-> ?v1 <http://edas#isLocationOf> ?answer0.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
1.5 <-> ?answer0 <http://edas#hasLocation> ?v1.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: trip_location

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Research_Topic> .
Number of matched answers :10
Number of correspondences found: 4
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> ?someObject. 
0.0 <-> ?answer <http://edas#isTopicOf> ?someObject. 
0.0 <-> ?someSubject <http://edas#hasTopic> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Topic>. 

Binary query : ?answer0 a <http://ekaw#Invited_Talk_Abstract>. ?answer0 <http://ekaw#paperPresentedAs> ?answer1. ?answer1 a <http://ekaw#Invited_Talk>.
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Student> .
Number of matched answers :10
Number of correspondences found: 7
0.0 <-> ?answer <http://edas#hasRelatedPaper> ?someObject. 
0.0 <-> ?someSubject <http://edas#isWrittenBy> ?answer. 
0.0 <-> <http://edas-instances#conference-104081973> <http://edas#hasMember> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 
0.0 <-> <http://edas-instances#review-9314016512881114_-196548216> <http://edas#relatesTo> ?answer. 
0.0 <-> ?answer <http://edas#hasFirstName> "Zak". 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Attendee>. 

Unary query : ?o2 a <http://ekaw#Web_Site>. ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer. filter(regex(?s,"^http"))
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#SC_Member> .
Number of matched answers :10
Number of correspondences found: 7
0.0 <-> <http://edas-instances#review-985325133921114_-196548212> <http://edas#relatesTo> ?answer. 
0.0 <-> <http://edas-instances#conference-1409861683> <http://edas#hasMember> ?answer. 
0.0 <-> ?answer <http://edas#hasRelatedDocument> <http://edas-instances#review20518448391311_-196548215>. 
0.0 <-> ?answer <http://edas#isReviewing> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 
0.0 <-> ?someSubject <http://edas#isWrittenBy> ?answer. 
0.0 <-> ?answer <http://edas#hasRelatedPaper> <http://edas-instances#paper-236015335212198>. 

Binary query : ?answer1 <http://ekaw#reviewWrittenBy> ?answer0 .
Number of matched answers :10
Number of correspondences found: 2
0.5 <-> ?answer1 <http://edas#relatesTo> ?answer0.  
0.5 <-> ?answer0 <http://edas#hasRelatedDocument> ?answer1.  
creating directory: all_reviewer_author_of_review

Unary query : {?answer <http://ekaw#reviewerOfPaper> ?o2 . } union{ ?o3 <http://ekaw#reviewOfPaper> ?o2 .  ?o3 <http://ekaw#reviewWrittenBy> ?answer .  }
Number of matched answers :10
Number of correspondences found: 7
0.0 <-> ?answer <http://edas#hasLastName> "Turnbull". 
0.0 <-> ?answer <http://edas#isReviewing> ?someObject. 
0.0 <-> ?answer <http://edas#hasRelatedPaper> ?someObject. 
0.0 <-> <http://edas-instances#paper-437522444342198_v0> <http://edas#isReviewedBy> ?answer. 
0.0 <-> <http://edas-instances#conference-1409861683> <http://edas#hasMember> ?answer. 
0.0 <-> ?answer <http://edas#hasRelatedDocument> ?someObject. 
0.0 <-> <http://edas-instances#review9456309597651114_-196548212> <http://edas#relatesTo> ?answer. 

Binary query : ?answer0 <http://ekaw#coversTopic> ?answer1 . ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .
Number of matched answers :10
Number of correspondences found: 2
1.5 <-> ?answer0 a <http://edas#Paper>.  ?answer0 <http://edas#hasTopic> ?answer1.  
1.5 <-> ?answer0 a <http://edas#Paper>.  ?answer1 <http://edas#isTopicOf> ?answer0.  
creating directory: topic_of_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Review> .
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Review>. 
creating directory: review

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop> .  ?answer0 <http://ekaw#coversTopic> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Banquet> .
Number of matched answers :5
Number of correspondences found: 5
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> "Gala Dinner". 
0.0 <-> ?answer <http://edas#hasLocation> <http://edas-instances#location1961263292>. 
0.0 <-> ?answer <http://edas#hasStartDateTime> "2005-05-13T19:00:00". 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#NonAcademicEvent>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceDinner>. 

Binary query : ?answer1 a <http://ekaw#Conference> . ?o2 <http://ekaw#listsEvent> ?answer1 . ?o2 a <http://ekaw#Web_Site> . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer0 . filter(regex(?s,"^http"))
Number of matched answers :5
Number of correspondences found: 0

Unary query : ?answer a <http://ekaw#Invited_Talk> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper_Author> .
Number of matched answers :10
Number of correspondences found: 6
0.0 <-> ?someSubject <http://edas#isWrittenBy> ?answer. 
0.0 <-> <http://edas-instances#paper-437522444342198_v0> <http://edas#isReviewedBy> ?answer. 
0.0 <-> ?answer <http://edas#hasLastName> "Cresques". 
0.0 <-> ?answer <http://edas#hasRelatedPaper> ?someObject. 
0.0 <-> ?answer <http://edas#hasRelatedDocument> <http://edas-instances#review20518448391311_-196548215>. 
0.0 <-> ?answer <http://edas#isReviewing> <http://edas-instances#paper21059711116211114>. 

Binary query : ?answer0 <http://ekaw#presentationOfPaper> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .
Number of matched answers :10
Number of correspondences found: 9
1.5 <-> ?answer1 <http://edas#relatedToEvent> ?answer0.  ?answer1 a <http://edas#Paper>.  
1.5 <-> ?answer0 <http://edas#relatedToPaper> ?answer1.  ?answer1 a <http://edas#Paper>.  
1.5 <-> ?answer0 <http://edas#hasRelatedDocument> ?v1.  ?v1 <http://edas#relatesTo> ?v2.  ?v2 <http://edas#hasRelatedPaper> ?answer1.  ?answer1 a <http://edas#Paper>.  
1.5 <-> ?answer0 <http://edas#hasRelatedDocument> ?v1.  ?v1 <http://edas#relatesTo> ?v2.  ?v2 <http://edas#hasRelatedDocument> ?answer1.  ?answer1 a <http://edas#Paper>.  
2.5 <-> ?v1 <http://edas#relatedToEvent> ?answer0.  ?v1 a <http://edas#Paper>.  ?v1 <http://edas#relatesTo> ?v2.  ?v2 <http://edas#hasRelatedDocument> ?answer1.  ?answer1 a <http://edas#Paper>.  
2.5 <-> ?v1 <http://edas#relatedToEvent> ?answer0.  ?v1 a <http://edas#Paper>.  ?v2 <http://edas#hasRelatedDocument> ?v1.  ?answer1 <http://edas#relatesTo> ?v2.  ?answer1 a <http://edas#Paper>.  
2.5 <-> ?v1 <http://edas#relatedToEvent> ?answer0.  ?v1 a <http://edas#Paper>.  ?v2 <http://edas#hasRelatedPaper> ?v1.  ?v2 <http://edas#hasRelatedPaper> ?answer1.  ?answer1 a <http://edas#Paper>.  
2.5 <-> ?v1 <http://edas#relatedToEvent> ?answer0.  ?v1 a <http://edas#Paper>.  ?v2 <http://edas#isTopicOf> ?v1.  ?v2 <http://edas#isTopicOf> ?answer1.  ?answer1 a <http://edas#Paper>.  
2.5 <-> ?v1 <http://edas#relatedToEvent> ?answer0.  ?v1 a <http://edas#Paper>.  ?v2 <http://edas#isReviewing> ?v1.  ?v2 <http://edas#isReviewing> ?answer1.  ?answer1 a <http://edas#Paper>.  
creating directory: presentation_of_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#OC_Chair> .
Number of matched answers :5
Number of correspondences found: 4
0.0 <-> <http://edas-instances#paper-1921869894163257> <http://edas#isReviewedBy> ?answer. 
0.0 <-> <http://edas-instances#review-554085786002198_-196548214> <http://edas#relatesTo> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Author>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Paper>. 
creating directory: paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Chair> .
Number of matched answers :5
Number of correspondences found: 5
0.0 <-> ?answer <http://edas#isReviewing> <http://edas-instances#paper-253329908202257_v0>. 
0.0 <-> <http://edas-instances#paper14883166801311263_v0> <http://edas#isReviewedBy> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Presenter>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Attendee>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Regular_Paper> .
Number of matched answers :10
Number of correspondences found: 7
0.0 <-> ?answer <http://edas#isReviewedBy> <http://edas-instances#person-883272865>. 
0.0 <-> <http://edas-instances#person364225146> <http://edas#isReviewing> ?answer. 
0.0 <-> ?someSubject <http://edas#hasRelatedPaper> ?answer. 
0.0 <-> <http://edas-instances#conference-1409861683> <http://edas#hasRelatedDocument> ?answer. 
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> "Preparation of carrier free iodine samples for AMS analysis". 
0.0 <-> ?answer <http://edas#isWrittenBy> <http://edas-instances#person-1073906008>. 
0.0 <-> ?answer <http://edas#relatesTo> <http://edas-instances#conference-1409861683>. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .   ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.
Number of matched answers :10
Number of correspondences found: 1
2.5 <-> ?answer0 a <http://edas#Paper>.  ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: paper_title

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Contributed_Talk> .
Number of matched answers :10
Number of correspondences found: 6
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#PaperPresentation>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceEvent>. 
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> "Presentation of A microscopic calculation of potentials and mass parameters for heavy-ion reactions". 
0.0 <-> ?answer <http://edas#relatedToPaper> ?someObject. 
0.0 <-> <http://edas-instances#diapo21291639331961114> <http://edas#relatesTo> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#AcademicEvent>. 

Binary query : ?answer0 a <http://ekaw#Contributed_Talk>. ?answer1 a <http://ekaw#Conference> . ?answer0 <http://ekaw#partOfEvent> ?answer1.
Number of matched answers :10
Number of correspondences found: 3
1.5 <-> ?answer0 <http://edas#hasRelatedDocument> ?v1.  ?v1 <http://edas#relatesTo> ?answer1.  ?answer1 a <http://edas#Conference>.  
1.5 <-> ?v1 <http://edas#relatedToEvent> ?answer0.  ?v1 <http://edas#relatesTo> ?answer1.  ?answer1 a <http://edas#Conference>.  
1.5 <-> ?v1 <http://edas#relatedToEvent> ?answer0.  ?answer1 <http://edas#hasRelatedDocument> ?v1.  ?answer1 a <http://edas#Conference>.  
creating directory: presentation_in_conference
Matching process ended
