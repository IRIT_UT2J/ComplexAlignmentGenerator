===============================================================================
creating directory: cmt-ekaw
Running with 8 support instances - 0.4 similarity.
Number of CQAs: 34

Unary query : ?o2 <http://cmt#hasAuthor> ?answer .
Number of matched answers :8
Number of correspondences found: 1
0.5833333333333333 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper_Author>. 
creating directory: paper_1st_author

Unary query : { ?answer <http://cmt#hasDecision> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Rejection> . } UNION  { ?answer <http://cmt#rejectedBy> ?o2 . }
Number of matched answers :8
Number of correspondences found: 3
0.8205128205128206 <-> ?answer <http://ekaw#writtenBy> ?y. ?y a <http://ekaw#Presenter>. 
0.8545454545454546 <-> ?answer <http://ekaw#hasReviewer> ?y. ?y a <http://ekaw#Presenter>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Rejected_Paper>. 
creating directory: rejected_paper

Binary query : ?answer0 <http://cmt#memberOfProgramCommittee> ?answer1 .
Number of matched answers :8
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceChair> .
Number of matched answers :5
Number of correspondences found: 3
0.5454545454545454 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Participant>. 
0.75 <-> ?x <http://ekaw#hasReviewer> ?answer. ?x a <http://ekaw#Conference_Paper>. 
0.75 <-> ?answer <http://ekaw#reviewerOfPaper> ?y. ?y a <http://ekaw#Conference_Paper>. 
creating directory: conference_chair

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Co-author> .
Number of matched answers :8
Number of correspondences found: 1
0.5 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper_Author>. 
creating directory: paper_co_author

Binary query : ?answer0 <http://cmt#hasBeenAssigned> ?answer1 .
Number of matched answers :8
Number of correspondences found: 6
0.5 <-> ?answer0 <http://ekaw#reviewerOfPaper> ?v1.  ?answer1 <http://ekaw#updatedVersionOf> ?v1.  
0.5 <-> ?answer0 <http://ekaw#reviewerOfPaper> ?v1.  ?v1 <http://ekaw#hasUpdatedVersion> ?answer1.  
0.5 <-> ?answer0 <http://ekaw#reviewerOfPaper> ?answer1.  
0.9 <-> ?v1 <http://ekaw#hasReviewer> ?answer0.  ?v1 <http://ekaw#hasUpdatedVersion> ?answer1.  
0.9 <-> ?v1 <http://ekaw#hasReviewer> ?answer0.  ?answer1 <http://ekaw#updatedVersionOf> ?v1.  
0.9000000000000001 <-> ?answer1 <http://ekaw#hasReviewer> ?answer0.  
creating directory: is_assigned_reviewer_of

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Administrator> .
Number of matched answers :5
Number of correspondences found: 5
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Person>. 
0.0 <-> <http://ekaw-instances#paper-1446402940901263> <http://ekaw#hasReviewer> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Participant>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Student>. 
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> "Alexis Glick". 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommittee> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ExternalReviewer> .
Number of matched answers :8
Number of correspondences found: 4
0.5625 <-> ?x <http://ekaw#writtenBy> ?answer. ?x a <http://ekaw#Neutral_Review>. 
0.5625 <-> ?x <http://ekaw#reviewWrittenBy> ?answer. ?x a <http://ekaw#Neutral_Review>. 
0.5625 <-> ?answer <http://ekaw#authorOf> ?y. ?y a <http://ekaw#Neutral_Review>. 
creating directory: external_reviewer

Binary query : ?answer0 <http://cmt#writeReview> ?answer1 .
Number of matched answers :8
Number of correspondences found: 4
1.0454545454545452 <-> ?answer0 <http://ekaw#authorOf> ?answer1.  ?answer1 a <http://ekaw#Review>.  
1.0454545454545452 <-> ?answer1 <http://ekaw#reviewWrittenBy> ?answer0.  ?answer1 a <http://ekaw#Review>.  
1.1 <-> ?answer1 <http://ekaw#reviewWrittenBy> ?answer0.  ?answer1 a <http://ekaw#Positive_Review>.  
1.1 <-> ?answer0 <http://ekaw#authorOf> ?answer1.  ?answer1 a <http://ekaw#Positive_Review>.  
creating directory: assigned_reviewer_author_of_review

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Conference> .
Number of matched answers :5
Number of correspondences found: 5
0.6666666666666666 <-> ?answer <http://ekaw#hasPart> ?y. ?y a <http://ekaw#Conference_Trip>. 
0.6666666666666667 <-> ?x <http://ekaw#partOfEvent> ?answer. ?x a <http://ekaw#Conference_Trip>. 
0.6666666666666667 <-> ?x <http://ekaw#partOf> ?answer. ?x a <http://ekaw#Conference_Trip>. 
0.6666666666666667 <-> ?answer <http://ekaw#hasEvent> ?y. ?y a <http://ekaw#Conference_Trip>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference>. 
creating directory: conference

Binary query : ?answer0 <http://cmt#name> ?answer1 .   ?answer0 a <http://cmt#Person> .
Number of matched answers :8
Number of correspondences found: 1
1.9000000000000001 <-> ?answer0 a <http://ekaw#Person>.  ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: person_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person> .
Number of matched answers :8
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Person>. 
creating directory: person

Binary query : ?answer0 <http://cmt#date> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://cmt#assignExternalReviewer> ?answer1 .
Number of matched answers :8
Number of correspondences found: 3
0.5 <-> ?v1 <http://ekaw#reviewWrittenBy> ?answer0.  ?answer1 <http://ekaw#authorOf> ?v1.  
0.5 <-> ?v1 <http://ekaw#reviewWrittenBy> ?answer0.  ?v1 <http://ekaw#reviewWrittenBy> ?answer1.  
0.5 <-> ?answer0 <http://ekaw#authorOf> ?v1.  ?v1 <http://ekaw#reviewWrittenBy> ?answer1.  
creating directory: invite_reviewer

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Conference> . ?answer0 <http://cmt#name> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.9 <-> ?answer0 a <http://ekaw#Conference>.  ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: conference_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperAbstract> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommitteeMember> .
Number of matched answers :8
Number of correspondences found: 5
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Possible_Reviewer>. 
0.0 <-> ?answer <http://ekaw#authorOf> ?someObject. 
0.0 <-> <http://ekaw-instances#review-17203840236061114_-196548214> <http://ekaw#reviewWrittenBy> ?answer. 
0.0 <-> ?answer <http://ekaw#reviewerOfPaper> <http://ekaw-instances#paper11139601563192198>. 
0.0 <-> ?someSubject <http://ekaw#writtenBy> ?answer. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer> .
Number of matched answers :8
Number of correspondences found: 4
0.7272727272727288 <-> ?someSubject <http://ekaw#hasReviewer> ?answer. 
0.75 <-> ?answer <http://ekaw#authorOf> ?y. ?y a <http://ekaw#Review>. 
0.7540579710144928 <-> ?x <http://ekaw#writtenBy> ?answer. ?x a <http://ekaw#Review>. 
1.2166666666666668 <-> ?x <http://ekaw#reviewWrittenBy> ?answer. ?x a <http://ekaw#Review>. 
creating directory: assigned_reviewer

Unary query : { ?answer <http://cmt#hasDecision> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Acceptance> . } UNION  { ?answer <http://cmt#acceptedBy> ?o2 . }
Number of matched answers :8
Number of correspondences found: 4
0.9160839160839161 <-> ?answer <http://ekaw#hasReviewer> <http://ekaw-instances#person-203107362>. 
1.0714285714285714 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Accepted_Paper>. 
1.0714285714285714 <-> ?answer <http://ekaw#updatedVersionOf> ?y. ?y a <http://ekaw#Accepted_Paper>. 
1.6008403361344539 <-> ?x <http://ekaw#hasUpdatedVersion> ?answer. ?x a <http://ekaw#Accepted_Paper>. 
creating directory: accepted_paper

Binary query : ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperFullVersion> .   { ?answer0 <http://cmt#writePaper> ?answer1 .   } union{ ?answer0 <http://cmt#co-writePaper> ?answer1 .   }
Number of matched answers :8
Number of correspondences found: 6
0.5 <-> ?answer0 <http://ekaw#authorOf> ?answer1.  
0.9000000000000001 <-> ?answer1 <http://ekaw#writtenBy> ?answer0.  
1.4285714285714286 <-> ?answer0 <http://ekaw#authorOf> ?answer1.  ?answer1 a <http://ekaw#Accepted_Paper>.  
1.5448717948717947 <-> ?answer0 <http://ekaw#authorOf> ?answer1.  ?answer1 a <http://ekaw#Poster_Paper>.  
1.8285714285714287 <-> ?answer1 <http://ekaw#writtenBy> ?answer0.  ?answer1 a <http://ekaw#Accepted_Paper>.  
1.9448717948717946 <-> ?answer1 <http://ekaw#writtenBy> ?answer0.  ?answer1 a <http://ekaw#Poster_Paper>.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://cmt#memberOfConference> ?answer1 .
Number of matched answers :8
Number of correspondences found: 9
1.5370370370370372 <-> ?answer0 <http://ekaw#authorOf> ?v1.  ?v1 <http://ekaw#paperPresentedAs> ?v2.  ?v2 <http://ekaw#partOf> ?answer1.  ?answer1 a <http://ekaw#Conference>.  
1.5370370370370372 <-> ?v1 <http://ekaw#writtenBy> ?answer0.  ?v2 <http://ekaw#presentationOfPaper> ?v1.  ?v2 <http://ekaw#partOf> ?answer1.  ?answer1 a <http://ekaw#Conference>.  
1.5370370370370372 <-> ?v1 <http://ekaw#writtenBy> ?answer0.  ?v1 <http://ekaw#paperPresentedAs> ?v2.  ?v2 <http://ekaw#partOf> ?answer1.  ?answer1 a <http://ekaw#Conference>.  
1.5370370370370372 <-> ?v1 <http://ekaw#hasReviewer> ?answer0.  ?v1 <http://ekaw#hasReviewer> ?v2.  ?v2 <http://ekaw#organises> ?answer1.  ?answer1 a <http://ekaw#Conference>.  
1.5370370370370372 <-> ?v1 <http://ekaw#writtenBy> ?answer0.  ?v2 <http://ekaw#presentationOfPaper> ?v1.  ?answer1 <http://ekaw#hasEvent> ?v2.  ?answer1 a <http://ekaw#Conference>.  
1.5370370370370372 <-> ?v1 <http://ekaw#hasReviewer> ?answer0.  ?v2 <http://ekaw#reviewerOfPaper> ?v1.  ?v2 <http://ekaw#organises> ?answer1.  ?answer1 a <http://ekaw#Conference>.  
1.5370370370370372 <-> ?answer0 <http://ekaw#authorOf> ?v1.  ?v1 <http://ekaw#writtenBy> ?v2.  ?v2 <http://ekaw#organises> ?answer1.  ?answer1 a <http://ekaw#Conference>.  
1.5370370370370372 <-> ?v1 <http://ekaw#writtenBy> ?answer0.  ?v2 <http://ekaw#authorOf> ?v1.  ?answer1 <http://ekaw#organisedBy> ?v2.  ?answer1 a <http://ekaw#Conference>.  
1.5370370370370372 <-> ?v1 <http://ekaw#hasReviewer> ?answer0.  ?v2 <http://ekaw#reviewerOfPaper> ?v1.  ?answer1 <http://ekaw#organisedBy> ?v2.  ?answer1 a <http://ekaw#Conference>.  
creating directory: member_of_conference

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#SubjectArea> .
Number of matched answers :8
Number of correspondences found: 2
0.4 <-> ?x <http://ekaw#coversTopic> ?answer. ?x a <http://ekaw#Submitted_Paper>. 
0.4 <-> ?answer <http://ekaw#topicCoveredBy> ?y. ?y a <http://ekaw#Submitted_Paper>. 
creating directory: topic

Unary query : ?o2 <http://cmt#siteURL> ?answer .
Number of matched answers :0
Number of correspondences found: 0

Unary query : { ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer> .   } union{ ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ExternalReviewer> .   }
Number of matched answers :8
Number of correspondences found: 5
0.75 <-> ?answer <http://ekaw#authorOf> ?y. ?y a <http://ekaw#Review>. 
0.75 <-> ?x <http://ekaw#writtenBy> ?answer. ?x a <http://ekaw#Review>. 
0.9411764705882353 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Possible_Reviewer>. 
1.2005747126436783 <-> ?x <http://ekaw#reviewWrittenBy> ?answer. ?x a <http://ekaw#Review>. 
1.2897727272727268 <-> ?someSubject <http://ekaw#hasReviewer> ?answer. 
creating directory: reviewers_all

Binary query : ?answer0 a <http://cmt#PaperFullVersion> . ?answer0 <http://cmt#hasSubjectArea> ?answer1 .
Number of matched answers :8
Number of correspondences found: 2
0.5 <-> ?answer0 <http://ekaw#coversTopic> ?answer1.  
0.5 <-> ?answer1 <http://ekaw#topicCoveredBy> ?answer0.  
creating directory: topic_of_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Review> .
Number of matched answers :8
Number of correspondences found: 2
0.6666666666666669 <-> ?someSubject <http://ekaw#hasReview> ?answer. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Review>. 
creating directory: review

Binary query : ?answer1 <http://cmt#siteURL> ?answer0 .
Number of matched answers :5
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Author> .
Number of matched answers :8
Number of correspondences found: 1
0.75 <-> ?answer <http://ekaw#authorOf> ?someObject. 
creating directory: author

Binary query : ?answer0 a <http://cmt#PaperFullVersion> .   ?answer1 a <http://cmt#PaperAbstract> . ?answer0 <http://cmt#title> ?o2 . ?answer1 <http://cmt#title> ?o2 .
Number of matched answers :8
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#PaperFullVersion> .
Number of matched answers :8
Number of correspondences found: 7
0.47058823529411764 <-> ?answer <http://ekaw#hasUpdatedVersion> ?someObject. 
0.47058823529411764 <-> ?someSubject <http://ekaw#hasUpdatedVersion> ?answer. 
creating directory: paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ProgramCommitteeChair> .
Number of matched answers :5
Number of correspondences found: 5
0.0 <-> <http://ekaw-instances#paper-8367213188212198> <http://ekaw#hasReviewer> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member>. 
0.0 <-> <http://ekaw-instances#review-1897148521981263_-559246915> <http://ekaw#writtenBy> ?answer. 
0.0 <-> ?answer <http://ekaw#authorOf> <http://ekaw-instances#review19126782906431114_-559246915>. 
0.0 <-> ?answer <http://ekaw#reviewerOfPaper> <http://ekaw-instances#paper459168755124257>. 

Binary query : ?answer0 <http://cmt#title> ?answer1 .   ?answer0 a <http://cmt#PaperFullVersion> .
Number of matched answers :8
Number of correspondences found: 1
0.5 <-> ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: paper_title

Binary query : ?answer0 <http://cmt#title> ?answer1 .   ?answer0 a <http://cmt#PaperAbstract> .
Number of matched answers :0
Number of correspondences found: 0
Matching process ended
