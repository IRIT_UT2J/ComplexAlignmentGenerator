===============================================================================
creating directory: edas-conference
Running with 1 support instances - 0.4 similarity.
Number of CQAs: 52

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#RejectedPaper> .
Number of matched answers :1
Number of correspondences found: 1
0.4285714285714286 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Rejected_contribution>. 
creating directory: rejected_paper

Binary query : ?answer0 <http://edas#hasLastName> ?answer1 .
Number of matched answers :1
Number of correspondences found: 1
1.1470588235294117 <-> ?answer0 <http://conference#has_the_last_name> ?answer1.  
creating directory: person_last_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Attendee> .
Number of matched answers :1
Number of correspondences found: 1
0.0 <-> ?answer <http://conference#has_the_last_name> "Crile". 

Binary query : ?answer0 a <http://edas#Workshop> . ?answer0 <http://edas#hasCall> ?o2 . ?o2 a <http://edas#CallForPapers>. ?o2 <http://edas#hasSubmissionDeadline> ?answer1 .
Number of matched answers :1
Number of correspondences found: 0

Binary query : ?answer0 <http://edas#registrationDueOn> ?answer1 .
Number of matched answers :1
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reception> .   ?answer0 <http://edas#hasEndDateTime> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Excursion> .   ?answer0 <http://edas#hasEndDateTime> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceChair> .
Number of matched answers :1
Number of correspondences found: 1
0.5789473684210527 <-> ?answer <http://conference#contributes> ?y. ?y a <http://conference#Conference_document>. 
creating directory: conference_chair

Binary query : ?answer0 a <http://edas#Workshop> . ?answer0 <http://edas#hasCall> ?o2 . ?o2 a <http://edas#CallForManuscripts>. ?o2 <http://edas#hasSubmissionDeadline> ?answer1 .
Number of matched answers :1
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#AcademiaOrganization> .
Number of matched answers :1
Number of correspondences found: 1
0.6 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Organization>. 
creating directory: university

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Excursion> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reception> .   ?answer0 <http://edas#hasStartDateTime> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer1 a <http://edas#PaperPresentation>. ?o3 <http://edas#relatesTo> ?answer1 . ?o3 a <http://edas#SlideSet> . ?o3 <http://edas#relatesTo> ?answer0 . ?answer0 a <http://edas#Presenter> .
Number of matched answers :1
Number of correspondences found: 2
1.7892156862745097 <-> ?answer1 <http://conference#is_given_by> ?answer0.  ?answer1 a <http://conference#Presentation>.  
2.473426212590299 <-> ?answer0 <http://conference#gives_presentations> ?answer1.  ?answer1 a <http://conference#Presentation>.  
creating directory: presenter_of_paper_presentation

Binary query : ?answer0 <http://edas#paperDueOn> ?answer1 .
Number of matched answers :1
Number of correspondences found: 2
0.5 <-> ?answer0 <http://conference#has_important_dates> ?v1.  ?v1 <http://conference#is_a_date_of_camera_ready_paper_submission> ?answer1.  
0.5 <-> ?v1 <http://conference#belong_to_a_conference_volume> ?answer0.  ?v1 <http://conference#is_a_date_of_camera_ready_paper_submission> ?answer1.  
creating directory: conference_camera_date

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceDinner> .   ?answer0 <http://edas#hasStartDateTime> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Conference> .
Number of matched answers :1
Number of correspondences found: 5
0.6666666666666666 <-> ?answer <http://conference#has_tutorials> ?y. ?y a <http://conference#Conference_part>. 
0.6666666666666667 <-> ?answer <http://conference#has_tracks> ?y. ?y a <http://conference#Conference_part>. 
0.6666666666666667 <-> ?answer <http://conference#has_parts> ?y. ?y a <http://conference#Conference_part>. 
0.6666666666666667 <-> ?x <http://conference#is_part_of_conference_volumes> ?answer. ?x a <http://conference#Conference_part>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference>. 
creating directory: conference

Binary query : ?answer0 <http://edas#hasFirstName> ?o1 .  ?answer0 <http://edas#hasLastName> ?o2 .   BIND( concat(STR(?o1)," ", STR(?o2)) as ?answer1)
Number of matched answers :1
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person> .
Number of matched answers :1
Number of correspondences found: 3
1.0 <-> ?answer <http://conference#invites_co-reviewers> ?y. ?y a <http://conference#Person>. 
1.0 <-> ?x <http://conference#invited_by> ?answer. ?x a <http://conference#Person>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Person>. 
creating directory: person

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceDinner> .   ?answer0 <http://edas#hasLocation> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://edas#startDate> ?answer1 .    ?answer0 a <http://edas#Conference> .
Number of matched answers :1
Number of correspondences found: 2
2.0 <-> ?answer0 a <http://conference#Conference>.  ?v1 <http://conference#belong_to_a_conference_volume> ?answer0.  ?v1 <http://conference#is_a_starting_date> ?answer1.  
2.4210526315789473 <-> ?answer0 a <http://conference#Conference>.  ?answer0 <http://conference#has_important_dates> ?v1.  ?v1 <http://conference#is_a_starting_date> ?answer1.  
creating directory: conference_start_date

Binary query : ?answer0 <http://edas#hasName> ?answer1 .
Number of matched answers :1
Number of correspondences found: 1
1.85 <-> ?answer0 a <http://conference#Conference_volume>.  ?answer0 <http://conference#has_a_name> ?answer1.  
creating directory: conference_name

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Excursion> .   ?answer0 <http://edas#hasStartDateTime> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Workshop> .   ?answer0 <http://edas#hasEndDateTime> ?answer1 .
Number of matched answers :1
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Workshop> .
Number of matched answers :1
Number of correspondences found: 2
0.6153846153846154 <-> ?someSubject <http://conference#has_workshops> ?answer. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop>. 
creating directory: workshop

Binary query : ?answer0 <http://edas#manuscriptDueOn> ?answer1 .
Number of matched answers :1
Number of correspondences found: 2
0.5 <-> ?v1 <http://conference#belong_to_a_conference_volume> ?answer0.  ?v1 <http://conference#is_a_full_paper_submission_date> ?answer1.  
0.5 <-> ?answer0 <http://conference#has_important_dates> ?v1.  ?v1 <http://conference#is_a_full_paper_submission_date> ?answer1.  
creating directory: conference_paper_date

Binary query : ?answer0 <http://edas#hasTopic> ?answer1 .  ?answer0 a <http://edas#Conference> .
Number of matched answers :1
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#AcceptedPaper> .
Number of matched answers :1
Number of correspondences found: 1
0.4285714285714286 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Accepted_contribution>. 
creating directory: accepted_paper

Binary query : ?answer0 <http://edas#hasRelatedPaper> ?answer1 .
Number of matched answers :1
Number of correspondences found: 2
0.5 <-> ?answer0 <http://conference#contributes> ?answer1.  
0.5 <-> ?answer1 <http://conference#has_authors> ?answer0.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://edas#isMemberOf> ?answer1 .
Number of matched answers :1
Number of correspondences found: 3
0.5 <-> ?v1 <http://conference#has_authors> ?answer0.  ?answer1 <http://conference#has_contributions> ?v1.  
0.5 <-> ?v1 <http://conference#has_authors> ?answer0.  ?v1 <http://conference#is_submitted_at> ?answer1.  
0.5 <-> ?answer0 <http://conference#contributes> ?v1.  ?v1 <http://conference#is_submitted_at> ?answer1.  
creating directory: member_of_conference

Binary query : ?answer0 <http://edas#endDate> ?answer1 .
Number of matched answers :1
Number of correspondences found: 2
0.9117647058823529 <-> ?answer0 <http://conference#has_important_dates> ?v1.  ?v1 <http://conference#is_an_ending_date> ?answer1.  
0.9117647058823529 <-> ?v1 <http://conference#belong_to_a_conference_volume> ?answer0.  ?v1 <http://conference#is_an_ending_date> ?answer1.  
creating directory: conference_end_date

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Excursion> .   ?answer0 <http://edas#hasLocation> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Topic> .
Number of matched answers :1
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Topic>. 
creating directory: topic

Binary query : ?answer0 <http://edas#hasRelatedDocument> ?answer1 .  ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reviewer> .   ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Review> .
Number of matched answers :1
Number of correspondences found: 2
4.0 <-> ?answer0 a <http://conference#Reviewer>.  ?answer1 <http://conference#has_authors> ?answer0.  ?answer1 a <http://conference#Review>.  
4.0 <-> ?answer0 a <http://conference#Reviewer>.  ?answer0 <http://conference#contributes> ?answer1.  ?answer1 a <http://conference#Review>.  
creating directory: all_reviewer_author_of_review

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Workshop> .   ?answer0 <http://edas#hasStartDateTime> ?answer1 .
Number of matched answers :1
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceDinner> .   ?answer0 <http://edas#hasEndDateTime> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reviewer> .
Number of matched answers :1
Number of correspondences found: 5
0.75 <-> ?x <http://conference#has_authors> ?answer. ?x a <http://conference#Review>. 
0.75 <-> ?answer <http://conference#contributes> ?y. ?y a <http://conference#Review>. 
1.0 <-> ?x <http://conference#invited_by> ?answer. ?x a <http://conference#Reviewer>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer>. 
1.4 <-> ?answer <http://conference#invites_co-reviewers> ?y. ?y a <http://conference#Reviewer>. 
creating directory: reviewers_all

Binary query : ?answer0 <http://edas#hasTopic> ?answer1 . ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Paper> .
Number of matched answers :1
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Review> .
Number of matched answers :1
Number of correspondences found: 4
0.75 <-> ?x <http://conference#contributes> ?answer. ?x a <http://conference#Reviewer>. 
0.75 <-> ?answer <http://conference#has_authors> ?y. ?y a <http://conference#Reviewer>. 
0.8571428571428572 <-> ?answer <http://conference#reviews> ?someObject. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Review>. 
creating directory: review

Binary query : ?answer0 <http://edas#hasFirstName> ?answer1 .
Number of matched answers :1
Number of correspondences found: 1
1.1666666666666667 <-> ?answer0 <http://conference#has_the_first_name> ?answer1.  
creating directory: person_first_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceDinner> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Author> .
Number of matched answers :1
Number of correspondences found: 1
0.5454545454545454 <-> ?someSubject <http://conference#has_authors> ?answer. 
creating directory: author

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#IndustryOrganization> .
Number of matched answers :1
Number of correspondences found: 1
0.6 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Organization>. 
creating directory: company

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reception> .   ?answer0 <http://edas#hasLocation> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://edas#relatedToPaper> ?answer1 .
Number of matched answers :1
Number of correspondences found: 3
0.5 <-> ?v1 <http://conference#is_the_1th_part_of> ?answer0.  ?v1 <http://conference#is_the_1th_part_of> ?answer1.  
0.5 <-> ?v1 <http://conference#gives_presentations> ?answer0.  ?answer1 <http://conference#has_authors> ?v1.  
0.5 <-> ?answer0 <http://conference#is_given_by> ?v1.  ?v1 <http://conference#contributes> ?answer1.  
creating directory: presentation_of_paper

Binary query : ?answer0 a <http://edas#Workshop> . ?answer0 <http://edas#hasCall> ?o2 . ?o2 a <http://edas#CallForReviews>. ?o2 <http://edas#hasSubmissionDeadline> ?answer1 .
Number of matched answers :1
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Paper> .
Number of matched answers :1
Number of correspondences found: 1
0.4 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> ?someObject. 
creating directory: paper

Unary query : ?answer a <http://edas#Presenter> .
Number of matched answers :1
Number of correspondences found: 1
0.5833333333333333 <-> ?answer <http://conference#gives_presentations> ?y. ?y a <http://conference#Presentation>. 
creating directory: presenter

Binary query : ?answer0 <http://edas#relatesTo> ?answer1 .  ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Paper> .   ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Conference> .
Number of matched answers :1
Number of correspondences found: 2
2.5 <-> ?answer0 a <http://conference#Paper>.  ?answer1 <http://conference#has_contributions> ?answer0.  ?answer1 a <http://conference#Conference>.  
2.5 <-> ?answer0 a <http://conference#Paper>.  ?answer0 <http://conference#is_submitted_at> ?answer1.  ?answer1 a <http://conference#Conference>.  
creating directory: paper_submitted_at_conference

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Paper> .   ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.
Number of matched answers :1
Number of correspondences found: 1
3.3 <-> ?answer0 a <http://conference#Paper>.  ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: paper_title

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#PaperPresentation> .
Number of matched answers :1
Number of correspondences found: 2
0.6842105263157895 <-> ?someSubject <http://conference#gives_presentations> ?answer. 
0.7058823529411764 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Presentation>. 
creating directory: paper_presentation

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reception> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 a <http://edas#PaperPresentation> . ?answer0 <http://edas#relatedToPaper> ?o2 . ?answer1 <http://edas#hasRelatedDocument> ?o2 . ?answer1 a <http://edas#Conference> .
Number of matched answers :1
Number of correspondences found: 4
3.236842105263158 <-> ?answer0 a <http://conference#Conference_document>.  ?v1 <http://conference#gives_presentations> ?answer0.  ?v2 <http://conference#has_members> ?v1.  ?answer1 <http://conference#has_a_commtitee> ?v2.  ?answer1 a <http://conference#Conference>.  
3.605263157894737 <-> ?answer0 a <http://conference#Conference_document>.  ?answer0 <http://conference#is_given_by> ?v1.  ?v1 <http://conference#contributes> ?v2.  ?v2 a <http://conference#Conference_document>.  ?v2 <http://conference#is_submitted_at> ?answer1.  ?answer1 a <http://conference#Conference>.  
4.657894736842106 <-> ?answer0 a <http://conference#Conference_document>.  ?v1 <http://conference#is_the_1th_part_of> ?answer0.  ?v1 a <http://conference#Conference_document>.  ?v2 <http://conference#has_an_abstract> ?v1.  ?v2 a <http://conference#Conference_document>.  ?v2 <http://conference#is_submitted_at> ?answer1.  ?answer1 a <http://conference#Conference>.  
4.657894736842106 <-> ?answer0 a <http://conference#Conference_document>.  ?v1 <http://conference#is_the_1th_part_of> ?answer0.  ?v1 a <http://conference#Conference_document>.  ?v1 <http://conference#is_the_1th_part_of> ?v2.  ?v2 a <http://conference#Conference_document>.  ?v2 <http://conference#is_submitted_at> ?answer1.  ?answer1 a <http://conference#Conference>.  
creating directory: presentation_in_conference
Matching process ended
