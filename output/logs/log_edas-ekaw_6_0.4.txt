===============================================================================
creating directory: edas-ekaw
Running with 6 support instances - 0.4 similarity.
Number of CQAs: 52

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#RejectedPaper> .
Number of matched answers :6
Number of correspondences found: 1
0.9285714285714286 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Rejected_Paper>. 
creating directory: rejected_paper

Binary query : ?answer0 <http://edas#hasLastName> ?answer1 .
Number of matched answers :6
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Attendee> .
Number of matched answers :6
Number of correspondences found: 3
0.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> ?someObject. 
0.0 <-> ?answer <http://ekaw#authorOf> ?someObject. 
0.0 <-> ?someSubject <http://ekaw#writtenBy> ?answer. 

Binary query : ?answer0 a <http://edas#Workshop> . ?answer0 <http://edas#hasCall> ?o2 . ?o2 a <http://edas#CallForPapers>. ?o2 <http://edas#hasSubmissionDeadline> ?answer1 .
Number of matched answers :6
Number of correspondences found: 0

Binary query : ?answer0 <http://edas#registrationDueOn> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reception> .   ?answer0 <http://edas#hasEndDateTime> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Excursion> .   ?answer0 <http://edas#hasEndDateTime> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceChair> .
Number of matched answers :5
Number of correspondences found: 3
0.5454545454545454 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Participant>. 
0.75 <-> ?x <http://ekaw#hasReviewer> ?answer. ?x a <http://ekaw#Conference_Paper>. 
0.75 <-> ?answer <http://ekaw#reviewerOfPaper> ?y. ?y a <http://ekaw#Conference_Paper>. 
creating directory: conference_chair

Binary query : ?answer0 a <http://edas#Workshop> . ?answer0 <http://edas#hasCall> ?o2 . ?o2 a <http://edas#CallForManuscripts>. ?o2 <http://edas#hasSubmissionDeadline> ?answer1 .
Number of matched answers :6
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#AcademiaOrganization> .
Number of matched answers :6
Number of correspondences found: 1
0.6 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Academic_Institution>. 
creating directory: university

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Excursion> .
Number of matched answers :5
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> "Excursion". 
creating directory: trip

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reception> .   ?answer0 <http://edas#hasStartDateTime> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer1 a <http://edas#PaperPresentation>. ?o3 <http://edas#relatesTo> ?answer1 . ?o3 a <http://edas#SlideSet> . ?o3 <http://edas#relatesTo> ?answer0 . ?answer0 a <http://edas#Presenter> .
Number of matched answers :6
Number of correspondences found: 10
2.9071880468434514 <-> ?answer0 a <http://ekaw#Presenter>.  ?v1 <http://ekaw#writtenBy> ?answer0.  ?answer1 <http://ekaw#presentationOfPaper> ?v1.  ?answer1 a <http://ekaw#Individual_Presentation>.  
2.9071880468434514 <-> ?answer0 a <http://ekaw#Presenter>.  ?v1 <http://ekaw#writtenBy> ?answer0.  ?v2 <http://ekaw#updatedVersionOf> ?v1.  ?answer1 <http://ekaw#presentationOfPaper> ?v2.  ?answer1 a <http://ekaw#Individual_Presentation>.  
2.9071880468434514 <-> ?answer0 a <http://ekaw#Presenter>.  ?answer0 <http://ekaw#authorOf> ?v1.  ?answer1 <http://ekaw#presentationOfPaper> ?v1.  ?answer1 a <http://ekaw#Individual_Presentation>.  
2.9071880468434514 <-> ?answer0 a <http://ekaw#Presenter>.  ?v1 <http://ekaw#writtenBy> ?answer0.  ?v1 <http://ekaw#hasUpdatedVersion> ?v2.  ?answer1 <http://ekaw#presentationOfPaper> ?v2.  ?answer1 a <http://ekaw#Individual_Presentation>.  
3.6393861892583117 <-> ?answer0 a <http://ekaw#Presenter>.  ?v1 <http://ekaw#writtenBy> ?answer0.  ?v1 <http://ekaw#paperPresentedAs> ?answer1.  ?answer1 a <http://ekaw#Individual_Presentation>.  
3.6393861892583117 <-> ?answer0 a <http://ekaw#Presenter>.  ?answer0 <http://ekaw#authorOf> ?v1.  ?v1 <http://ekaw#paperPresentedAs> ?answer1.  ?answer1 a <http://ekaw#Individual_Presentation>.  
3.639386189258312 <-> ?answer0 a <http://ekaw#Presenter>.  ?v1 <http://ekaw#writtenBy> ?answer0.  ?v1 <http://ekaw#hasUpdatedVersion> ?v2.  ?v2 <http://ekaw#paperPresentedAs> ?answer1.  ?answer1 a <http://ekaw#Individual_Presentation>.  
3.639386189258312 <-> ?answer0 a <http://ekaw#Presenter>.  ?v1 <http://ekaw#writtenBy> ?answer0.  ?v2 <http://ekaw#updatedVersionOf> ?v1.  ?v2 <http://ekaw#paperPresentedAs> ?answer1.  ?answer1 a <http://ekaw#Individual_Presentation>.  
3.639386189258312 <-> ?answer0 a <http://ekaw#Presenter>.  ?answer0 <http://ekaw#authorOf> ?v1.  ?v1 <http://ekaw#hasUpdatedVersion> ?v2.  ?v2 <http://ekaw#paperPresentedAs> ?answer1.  ?answer1 a <http://ekaw#Individual_Presentation>.  
3.639386189258312 <-> ?answer0 a <http://ekaw#Presenter>.  ?answer0 <http://ekaw#authorOf> ?v1.  ?v2 <http://ekaw#updatedVersionOf> ?v1.  ?v2 <http://ekaw#paperPresentedAs> ?answer1.  ?answer1 a <http://ekaw#Individual_Presentation>.  
creating directory: presenter_of_paper_presentation

Binary query : ?answer0 <http://edas#paperDueOn> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceDinner> .   ?answer0 <http://edas#hasStartDateTime> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Conference> .
Number of matched answers :5
Number of correspondences found: 5
0.6666666666666666 <-> ?answer <http://ekaw#hasPart> ?y. ?y a <http://ekaw#Conference_Trip>. 
0.6666666666666667 <-> ?x <http://ekaw#partOf> ?answer. ?x a <http://ekaw#Conference_Trip>. 
0.6666666666666667 <-> ?x <http://ekaw#partOfEvent> ?answer. ?x a <http://ekaw#Conference_Trip>. 
0.6666666666666667 <-> ?answer <http://ekaw#hasEvent> ?y. ?y a <http://ekaw#Conference_Trip>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference>. 
creating directory: conference

Binary query : ?answer0 <http://edas#hasFirstName> ?o1 .  ?answer0 <http://edas#hasLastName> ?o2 .   BIND( concat(STR(?o1)," ", STR(?o2)) as ?answer1)
Number of matched answers :6
Number of correspondences found: 1
0.5 <-> ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: person_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Person> .
Number of matched answers :6
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Person>. 
creating directory: person

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceDinner> .   ?answer0 <http://edas#hasLocation> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :5
Number of correspondences found: 2
2.893939393939394 <-> ?answer0 a <http://ekaw#Conference_Banquet>.  ?answer0 <http://ekaw#heldIn> ?v1.  ?v1 a <http://ekaw#Location>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
3.439393939393939 <-> ?answer0 a <http://ekaw#Conference_Banquet>.  ?v1 <http://ekaw#locationOf> ?answer0.  ?v1 a <http://ekaw#Location>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: gala_dinner_location

Binary query : ?answer0 <http://edas#startDate> ?answer1 .    ?answer0 a <http://edas#Conference> .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://edas#hasName> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
0.5 <-> ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: conference_name

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Excursion> .   ?answer0 <http://edas#hasStartDateTime> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Workshop> .   ?answer0 <http://edas#hasEndDateTime> ?answer1 .
Number of matched answers :6
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Workshop> .
Number of matched answers :6
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop>. 
creating directory: workshop

Binary query : ?answer0 <http://edas#manuscriptDueOn> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://edas#hasTopic> ?answer1 .  ?answer0 a <http://edas#Conference> .
Number of matched answers :6
Number of correspondences found: 2
1.5 <-> ?answer0 a <http://ekaw#Conference>.  ?answer1 <http://ekaw#topicCoveredBy> ?answer0.  
2.0454545454545454 <-> ?answer0 a <http://ekaw#Conference>.  ?answer0 <http://ekaw#coversTopic> ?answer1.  
creating directory: conference_topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#AcceptedPaper> .
Number of matched answers :6
Number of correspondences found: 3
0.9285714285714286 <-> ?answer <http://ekaw#updatedVersionOf> ?y. ?y a <http://ekaw#Accepted_Paper>. 
0.9285714285714286 <-> ?x <http://ekaw#hasUpdatedVersion> ?answer. ?x a <http://ekaw#Accepted_Paper>. 
0.9285714285714286 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Accepted_Paper>. 
creating directory: accepted_paper

Binary query : ?answer0 <http://edas#hasRelatedPaper> ?answer1 .
Number of matched answers :6
Number of correspondences found: 10
0.5 <-> ?answer0 <http://ekaw#authorOf> ?answer1.  
0.5 <-> ?answer1 <http://ekaw#writtenBy> ?answer0.  
1.0333333333333332 <-> ?answer1 <http://ekaw#writtenBy> ?answer0.  ?answer1 a <http://ekaw#Rejected_Paper>.  
1.0333333333333332 <-> ?answer0 <http://ekaw#authorOf> ?answer1.  ?answer1 a <http://ekaw#Rejected_Paper>.  
1.1 <-> ?v1 <http://ekaw#writtenBy> ?answer0.  ?v1 a <http://ekaw#Accepted_Paper>.  ?answer1 <http://ekaw#updatedVersionOf> ?v1.  
1.1 <-> ?answer1 <http://ekaw#writtenBy> ?answer0.  ?answer1 a <http://ekaw#Accepted_Paper>.  
1.1 <-> ?answer0 <http://ekaw#authorOf> ?answer1.  ?answer1 a <http://ekaw#Accepted_Paper>.  
1.1 <-> ?answer0 <http://ekaw#authorOf> ?v1.  ?v1 a <http://ekaw#Accepted_Paper>.  ?answer1 <http://ekaw#updatedVersionOf> ?v1.  
1.511764705882353 <-> ?answer0 <http://ekaw#authorOf> ?v1.  ?v1 a <http://ekaw#Accepted_Paper>.  ?v1 <http://ekaw#hasUpdatedVersion> ?answer1.  
1.511764705882353 <-> ?v1 <http://ekaw#writtenBy> ?answer0.  ?v1 a <http://ekaw#Accepted_Paper>.  ?v1 <http://ekaw#hasUpdatedVersion> ?answer1.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://edas#isMemberOf> ?answer1 .
Number of matched answers :6
Number of correspondences found: 13
0.5 <-> ?v1 <http://ekaw#hasReviewer> ?answer0.  ?v2 <http://ekaw#reviewerOfPaper> ?v1.  ?answer1 <http://ekaw#organisedBy> ?v2.  
0.5 <-> ?v1 <http://ekaw#hasReviewer> ?answer0.  ?v2 <http://ekaw#reviewerOfPaper> ?v1.  ?v2 <http://ekaw#organises> ?answer1.  
0.5 <-> ?answer0 <http://ekaw#authorOf> ?v1.  ?v1 <http://ekaw#writtenBy> ?v2.  ?v2 <http://ekaw#organises> ?answer1.  
0.5 <-> ?v1 <http://ekaw#writtenBy> ?answer0.  ?v2 <http://ekaw#presentationOfPaper> ?v1.  ?v2 <http://ekaw#partOf> ?answer1.  
0.5 <-> ?v1 <http://ekaw#writtenBy> ?answer0.  ?v2 <http://ekaw#reviewerOfPaper> ?v1.  ?v2 <http://ekaw#organises> ?answer1.  
0.5 <-> ?v1 <http://ekaw#writtenBy> ?answer0.  ?v1 <http://ekaw#paperPresentedAs> ?v2.  ?v2 <http://ekaw#partOf> ?answer1.  
0.5 <-> ?v1 <http://ekaw#hasReviewer> ?answer0.  ?v1 <http://ekaw#hasReviewer> ?v2.  ?v2 <http://ekaw#organises> ?answer1.  
0.5 <-> ?answer0 <http://ekaw#reviewerOfPaper> ?v1.  ?v1 <http://ekaw#hasReviewer> ?v2.  ?v2 <http://ekaw#organises> ?answer1.  
0.5 <-> ?answer0 <http://ekaw#authorOf> ?v1.  ?v1 <http://ekaw#hasReviewer> ?v2.  ?v2 <http://ekaw#organises> ?answer1.  
0.5 <-> ?v1 <http://ekaw#writtenBy> ?answer0.  ?v1 <http://ekaw#writtenBy> ?v2.  ?v2 <http://ekaw#organises> ?answer1.  
0.5 <-> ?v1 <http://ekaw#writtenBy> ?answer0.  ?v2 <http://ekaw#reviewerOfPaper> ?v1.  ?answer1 <http://ekaw#organisedBy> ?v2.  
0.5 <-> ?answer0 <http://ekaw#authorOf> ?v1.  ?v1 <http://ekaw#paperPresentedAs> ?v2.  ?v2 <http://ekaw#partOf> ?answer1.  
0.5 <-> ?v1 <http://ekaw#writtenBy> ?answer0.  ?v1 <http://ekaw#hasReviewer> ?v2.  ?v2 <http://ekaw#organises> ?answer1.  
creating directory: member_of_conference

Binary query : ?answer0 <http://edas#endDate> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Excursion> .   ?answer0 <http://edas#hasLocation> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :5
Number of correspondences found: 2
2.6717171717171717 <-> ?answer0 <http://ekaw#heldIn> ?v1.  ?v1 a <http://ekaw#Location>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
3.217171717171717 <-> ?v1 <http://ekaw#locationOf> ?answer0.  ?v1 a <http://ekaw#Location>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: trip_location

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Topic> .
Number of matched answers :6
Number of correspondences found: 1
0.4545454545454546 <-> ?someSubject <http://ekaw#coversTopic> ?answer. 
creating directory: topic

Binary query : ?answer0 <http://edas#hasRelatedDocument> ?answer1 .  ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reviewer> .   ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Review> .
Number of matched answers :6
Number of correspondences found: 2
2.25 <-> ?answer0 <http://ekaw#authorOf> ?answer1.  ?answer1 a <http://ekaw#Review>.  
3.1166666666666667 <-> ?answer1 <http://ekaw#reviewWrittenBy> ?answer0.  ?answer1 a <http://ekaw#Review>.  
creating directory: all_reviewer_author_of_review

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Workshop> .   ?answer0 <http://edas#hasStartDateTime> ?answer1 .
Number of matched answers :6
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceDinner> .   ?answer0 <http://edas#hasEndDateTime> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reviewer> .
Number of matched answers :6
Number of correspondences found: 4
0.7272727272727273 <-> ?someSubject <http://ekaw#hasReviewer> ?answer. 
0.75 <-> ?x <http://ekaw#writtenBy> ?answer. ?x a <http://ekaw#Review>. 
0.75 <-> ?answer <http://ekaw#authorOf> ?y. ?y a <http://ekaw#Review>. 
1.1233333333333335 <-> ?x <http://ekaw#reviewWrittenBy> ?answer. ?x a <http://ekaw#Review>. 
creating directory: reviewers_all

Binary query : ?answer0 <http://edas#hasTopic> ?answer1 . ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Paper> .
Number of matched answers :6
Number of correspondences found: 2
1.5 <-> ?answer0 a <http://ekaw#Paper>.  ?answer1 <http://ekaw#topicCoveredBy> ?answer0.  
2.045454545454546 <-> ?answer0 a <http://ekaw#Paper>.  ?answer0 <http://ekaw#coversTopic> ?answer1.  
creating directory: topic_of_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Review> .
Number of matched answers :6
Number of correspondences found: 2
0.6666666666666669 <-> ?someSubject <http://ekaw#hasReview> ?answer. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Review>. 
creating directory: review

Binary query : ?answer0 <http://edas#hasFirstName> ?answer1 .
Number of matched answers :6
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#ConferenceDinner> .
Number of matched answers :5
Number of correspondences found: 5
0.625 <-> ?answer <http://ekaw#partOf> ?y. ?y a <http://ekaw#Conference>. 
0.625 <-> ?x <http://ekaw#hasEvent> ?answer. ?x a <http://ekaw#Conference>. 
0.625 <-> ?x <http://ekaw#hasPart> ?answer. ?x a <http://ekaw#Conference>. 
0.625 <-> ?answer <http://ekaw#partOfEvent> ?y. ?y a <http://ekaw#Conference>. 
0.6666666666666667 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Banquet>. 
creating directory: gala_dinner

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Author> .
Number of matched answers :6
Number of correspondences found: 1
0.75 <-> ?answer <http://ekaw#authorOf> ?someObject. 
creating directory: author

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#IndustryOrganization> .
Number of matched answers :6
Number of correspondences found: 1
0.55 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Organisation>. 
creating directory: company

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reception> .   ?answer0 <http://edas#hasLocation> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :5
Number of correspondences found: 2
2.782828282828283 <-> ?answer0 <http://ekaw#heldIn> ?v1.  ?v1 a <http://ekaw#Location>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
3.728282828282828 <-> ?v1 <http://ekaw#locationOf> ?answer0.  ?v1 a <http://ekaw#Location>.  ?v1 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: reception_location

Binary query : ?answer0 <http://edas#relatedToPaper> ?answer1 .
Number of matched answers :6
Number of correspondences found: 9
0.5 <-> ?answer1 <http://ekaw#paperPresentedAs> ?answer0.  
1.0263157894736843 <-> ?answer0 <http://ekaw#presentationOfPaper> ?answer1.  
1.1 <-> ?v1 <http://ekaw#paperPresentedAs> ?answer0.  ?v1 <http://ekaw#updatedVersionOf> ?answer1.  ?answer1 a <http://ekaw#Evaluated_Paper>.  
1.1 <-> ?v1 <http://ekaw#paperPresentedAs> ?answer0.  ?answer1 <http://ekaw#hasUpdatedVersion> ?v1.  ?answer1 a <http://ekaw#Evaluated_Paper>.  
1.1 <-> ?answer0 <http://ekaw#coversTopic> ?v1.  ?v1 <http://ekaw#topicCoveredBy> ?answer1.  ?answer1 a <http://ekaw#Evaluated_Paper>.  
1.1 <-> ?v1 <http://ekaw#topicCoveredBy> ?answer0.  ?v1 <http://ekaw#topicCoveredBy> ?answer1.  ?answer1 a <http://ekaw#Evaluated_Paper>.  
1.1 <-> ?v1 <http://ekaw#topicCoveredBy> ?answer0.  ?answer1 <http://ekaw#coversTopic> ?v1.  ?answer1 a <http://ekaw#Evaluated_Paper>.  
1.6263157894736842 <-> ?answer0 <http://ekaw#presentationOfPaper> ?v1.  ?answer1 <http://ekaw#hasUpdatedVersion> ?v1.  ?answer1 a <http://ekaw#Evaluated_Paper>.  
1.6263157894736842 <-> ?answer0 <http://ekaw#presentationOfPaper> ?v1.  ?v1 <http://ekaw#updatedVersionOf> ?answer1.  ?answer1 a <http://ekaw#Evaluated_Paper>.  
creating directory: presentation_of_paper

Binary query : ?answer0 a <http://edas#Workshop> . ?answer0 <http://edas#hasCall> ?o2 . ?o2 a <http://edas#CallForReviews>. ?o2 <http://edas#hasSubmissionDeadline> ?answer1 .
Number of matched answers :6
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Paper> .
Number of matched answers :6
Number of correspondences found: 5
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper>. 
1.0 <-> ?answer <http://ekaw#updatedVersionOf> ?y. ?y a <http://ekaw#Paper>. 
1.0 <-> ?x <http://ekaw#hasUpdatedVersion> ?answer. ?x a <http://ekaw#Paper>. 
1.0 <-> ?x <http://ekaw#updatedVersionOf> ?answer. ?x a <http://ekaw#Paper>. 
1.0 <-> ?answer <http://ekaw#hasUpdatedVersion> ?y. ?y a <http://ekaw#Paper>. 
creating directory: paper

Unary query : ?answer a <http://edas#Presenter> .
Number of matched answers :6
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Presenter>. 
creating directory: presenter

Binary query : ?answer0 <http://edas#relatesTo> ?answer1 .  ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Paper> .   ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Conference> .
Number of matched answers :6
Number of correspondences found: 14
2.5 <-> ?answer0 a <http://ekaw#Paper>.  ?answer0 <http://ekaw#paperPresentedAs> ?v1.  ?v1 <http://ekaw#partOf> ?answer1.  ?answer1 a <http://ekaw#Conference>.  
2.5 <-> ?answer0 a <http://ekaw#Paper>.  ?v1 <http://ekaw#presentationOfPaper> ?answer0.  ?v1 <http://ekaw#partOf> ?answer1.  ?answer1 a <http://ekaw#Conference>.  
2.5 <-> ?answer0 a <http://ekaw#Paper>.  ?v1 <http://ekaw#authorOf> ?answer0.  ?answer1 <http://ekaw#organisedBy> ?v1.  ?answer1 a <http://ekaw#Conference>.  
2.5 <-> ?answer0 a <http://ekaw#Paper>.  ?v1 <http://ekaw#presentationOfPaper> ?answer0.  ?answer1 <http://ekaw#hasEvent> ?v1.  ?answer1 a <http://ekaw#Conference>.  
2.5 <-> ?answer0 a <http://ekaw#Paper>.  ?answer0 <http://ekaw#coversTopic> ?v1.  ?v1 <http://ekaw#topicCoveredBy> ?v2.  ?v2 <http://ekaw#partOf> ?answer1.  ?answer1 a <http://ekaw#Conference>.  
2.5 <-> ?answer0 a <http://ekaw#Paper>.  ?v1 <http://ekaw#authorOf> ?answer0.  ?v1 <http://ekaw#organises> ?answer1.  ?answer1 a <http://ekaw#Conference>.  
2.5 <-> ?answer0 a <http://ekaw#Paper>.  ?answer0 <http://ekaw#writtenBy> ?v1.  ?v1 <http://ekaw#organises> ?answer1.  ?answer1 a <http://ekaw#Conference>.  
2.5 <-> ?answer0 a <http://ekaw#Paper>.  ?v1 <http://ekaw#topicCoveredBy> ?answer0.  ?v2 <http://ekaw#coversTopic> ?v1.  ?v2 <http://ekaw#partOf> ?answer1.  ?answer1 a <http://ekaw#Conference>.  
2.5 <-> ?answer0 a <http://ekaw#Paper>.  ?v1 <http://ekaw#topicCoveredBy> ?answer0.  ?v1 <http://ekaw#topicCoveredBy> ?v2.  ?v2 <http://ekaw#partOf> ?answer1.  ?answer1 a <http://ekaw#Conference>.  
2.5 <-> ?answer0 a <http://ekaw#Paper>.  ?answer0 <http://ekaw#coversTopic> ?v1.  ?v1 <http://ekaw#topicCoveredBy> ?v2.  ?answer1 <http://ekaw#hasEvent> ?v2.  ?answer1 a <http://ekaw#Conference>.  
2.5 <-> ?answer0 a <http://ekaw#Paper>.  ?v1 <http://ekaw#topicCoveredBy> ?answer0.  ?v2 <http://ekaw#coversTopic> ?v1.  ?answer1 <http://ekaw#hasEvent> ?v2.  ?answer1 a <http://ekaw#Conference>.  
3.5 <-> ?answer0 a <http://ekaw#Paper>.  ?v1 <http://ekaw#updatedVersionOf> ?answer0.  ?v1 a <http://ekaw#Paper>.  ?v2 <http://ekaw#presentationOfPaper> ?v1.  ?v2 <http://ekaw#partOf> ?answer1.  ?answer1 a <http://ekaw#Conference>.  
3.5 <-> ?answer0 a <http://ekaw#Paper>.  ?v1 <http://ekaw#updatedVersionOf> ?answer0.  ?v1 a <http://ekaw#Paper>.  ?v1 <http://ekaw#paperPresentedAs> ?v2.  ?v2 <http://ekaw#partOf> ?answer1.  ?answer1 a <http://ekaw#Conference>.  
3.5 <-> ?answer0 a <http://ekaw#Paper>.  ?v1 <http://ekaw#updatedVersionOf> ?answer0.  ?v1 a <http://ekaw#Paper>.  ?v2 <http://ekaw#presentationOfPaper> ?v1.  ?answer1 <http://ekaw#hasEvent> ?v2.  ?answer1 a <http://ekaw#Conference>.  
creating directory: paper_submitted_at_conference

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Paper> .   ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.
Number of matched answers :6
Number of correspondences found: 1
3.3000000000000003 <-> ?answer0 a <http://ekaw#Paper>.  ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.  
creating directory: paper_title

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#PaperPresentation> .
Number of matched answers :6
Number of correspondences found: 1
0.7058823529411765 <-> ?someSubject <http://ekaw#paperPresentedAs> ?answer. 
creating directory: paper_presentation

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://edas#Reception> .
Number of matched answers :5
Number of correspondences found: 1
0.9555555555555556 <-> ?x <http://ekaw#locationOf> ?answer. ?x a <http://ekaw#Location>. 
creating directory: reception

Binary query : ?answer0 a <http://edas#PaperPresentation> . ?answer0 <http://edas#relatedToPaper> ?o2 . ?answer1 <http://edas#hasRelatedDocument> ?o2 . ?answer1 a <http://edas#Conference> .
Number of matched answers :6
Number of correspondences found: 2
1.5 <-> ?answer0 <http://ekaw#partOf> ?answer1.  ?answer1 a <http://ekaw#Conference>.  
1.5 <-> ?answer1 <http://ekaw#hasEvent> ?answer0.  ?answer1 a <http://ekaw#Conference>.  
creating directory: presentation_in_conference
Matching process ended
