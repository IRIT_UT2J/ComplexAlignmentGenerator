===============================================================================
creating directory: ekaw-cmt
Running with 10 support instances - 0.2 similarity.
Number of CQAs: 65

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Proceedings_Publisher> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Rejected_Paper> .
Number of matched answers :10
Number of correspondences found: 8
0.6203007518796992 <-> ?answer <http://cmt#readByMeta-Reviewer> ?y. ?y a <http://cmt#Reviewer>. 
0.6428571428571428 <-> ?answer <http://cmt#readByReviewer> ?y. ?y a <http://cmt#Reviewer>. 
0.703925420037952 <-> ?x <http://cmt#co-writePaper> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.7678571428571428 <-> ?x <http://cmt#submitPaper> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.8214285714285714 <-> ?answer <http://cmt#rejectedBy> ?y. ?y a <http://cmt#ConferenceMember>. 
0.8363095238095237 <-> ?x <http://cmt#writePaper> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.9285714285714285 <-> ?x <http://cmt#readPaper> ?answer. ?x a <http://cmt#Reviewer>. 
1.0107142857142855 <-> ?someSubject <http://cmt#rejectPaper> ?answer. 
creating directory: rejected_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Track> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Participant> .   {?answer a <http://ekaw#Early-Registered_Participant> . } union{ ?answer a <http://ekaw#Late-Registered_Participant> . }
Number of matched answers :10
Number of correspondences found: 24
0.3318903318903319 <-> ?answer <http://cmt#name> ?someObject. 
0.6637806637806638 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ExternalReviewer>. 
0.7008177008177009 <-> ?x <http://cmt#hasAuthor> ?answer. ?x a <http://cmt#PaperFullVersion>. 
0.7057423331933139 <-> ?someSubject <http://cmt#assignedTo> ?answer. 
0.7095820845820846 <-> ?x <http://cmt#hasCo-author> ?answer. ?x a <http://cmt#PaperFullVersion>. 
0.7272727272727273 <-> <http://cmt-instances#review838955395241311_-196548215> <http://cmt#writtenBy> ?answer. 
0.781986531986532 <-> ?answer ?somePredicate <http://cmt-instances#person-1684927358>. 
0.781986531986532 <-> <http://cmt-instances#person-1684927358> <http://cmt#addProgramCommitteeMember> ?answer. 
0.7917267917267918 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#Administrator>. 
0.8273335767311671 <-> ?answer <http://cmt#memberOfConference> ?someObject. 
0.8438293188293189 <-> ?answer <http://cmt#writeReview> ?y. ?y a <http://cmt#Meta-Review>. 
0.9182426676402581 <-> ?someSubject <http://cmt#hasConferenceMember> ?answer. 
0.9233275801903255 <-> ?x <http://cmt#readByMeta-Reviewer> ?answer. ?x a <http://cmt#PaperFullVersion>. 
0.9463283629950299 <-> ?answer <http://cmt#submitPaper> ?y. ?y a <http://cmt#PaperFullVersion>. 
0.9828042328042328 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Meta-Reviewer>. 
1.0185185185185186 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Meta-Reviewer>. 
1.1002886002886003 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#ExternalReviewer>. 
1.1111498730546352 <-> ?answer <http://cmt#markConflictOfInterest> ?y. ?y a <http://cmt#PaperFullVersion>. 
1.1360028860028861 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#ExternalReviewer>. 
1.1882536365294987 <-> ?x <http://cmt#readByReviewer> ?answer. ?x a <http://cmt#PaperFullVersion>. 
1.3695215459921344 <-> ?answer <http://cmt#readPaper> ?someObject. 
1.3863399164379557 <-> ?answer <http://cmt#hasBeenAssigned> ?y. ?y a <http://cmt#PaperFullVersion>. 
1.4373496873496876 <-> ?answer <http://cmt#writePaper> ?y. ?y a <http://cmt#PaperFullVersion>. 
1.5720806970806973 <-> ?answer <http://cmt#co-writePaper> ?y. ?y a <http://cmt#PaperFullVersion>. 
creating directory: participant

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#OC_Member> .
Number of matched answers :10
Number of correspondences found: 8
0.6060606060606061 <-> ?answer <http://cmt#submitPaper> ?y. ?y a <http://cmt#Document>. 
0.6196969696969697 <-> ?answer <http://cmt#writePaper> ?y. ?y a <http://cmt#Document>. 
0.6410256410256411 <-> ?answer <http://cmt#co-writePaper> ?y. ?y a <http://cmt#Document>. 
0.7142857142857143 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.7222222222222222 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.7222222222222222 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#ConferenceMember>. 
0.7222222222222222 <-> ?answer <http://cmt#addedBy> ?y. ?y a <http://cmt#ConferenceMember>. 
0.7671428571428571 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#ConferenceMember>. 
creating directory: oc_member

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Proceedings> .   ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer1 <http://ekaw#hasEvent> ?answer0 .  ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .  ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop> .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://ekaw#reviewOfPaper> ?answer1 .
Number of matched answers :10
Number of correspondences found: 3
1.1153846153846152 <-> ?v1 <http://cmt#writeReview> ?answer0.  ?v1 a <http://cmt#Reviewer>.  ?v1 <http://cmt#hasBeenAssigned> ?answer1.  
1.1153846153846152 <-> ?answer0 <http://cmt#writtenBy> ?v1.  ?v1 a <http://cmt#Reviewer>.  ?v1 <http://cmt#hasBeenAssigned> ?answer1.  
1.1153846153846152 <-> ?v1 <http://cmt#writeReview> ?answer0.  ?v1 a <http://cmt#Reviewer>.  ?answer1 <http://cmt#assignedTo> ?v1.  
creating directory: review_of_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Tutorial> .  ?answer0 <http://ekaw#coversTopic> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 a <http://ekaw#Conference_Participant>.      bind( if (exists {?answer0 a <http://ekaw#Early-Registered_Participant>}, "true"^^<http://www.w3.org/2001/XMLSchema#boolean>,        if (exists {?answer0 a <http://ekaw#Late-Registered_Participant>}, "false"^^<http://www.w3.org/2001/XMLSchema#boolean>,"")) as ?answer1) filter(?answer1 != "")
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer1 <http://ekaw#hasEvent> ?answer0 .  ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .  ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Tutorial> .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 a <http://ekaw#Conference> . ?answer0 <http://ekaw#heldIn> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :5
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Late-Registered_Participant> .
Number of matched answers :10
Number of correspondences found: 3
0.5185185185185186 <-> ?answer <http://cmt#co-writePaper> ?y. ?y a <http://cmt#PaperFullVersion>. 
creating directory: late_registered_participant

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#University> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Trip> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 a <http://ekaw#Invited_Speaker>. ?answer1 <http://ekaw#writtenBy> ?answer0. ?answer1 a <http://ekaw#Invited_Talk_Abstract>.
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 a <http://ekaw#Invited_Speaker>. ?o2 <http://ekaw#writtenBy> ?answer0. ?o2 <http://ekaw#paperPresentedAs> ?answer1. ?answer1 a <http://ekaw#Invited_Talk>.
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member> .   ?answer0 <http://ekaw#reviewerOfPaper> ?answer1 .
Number of matched answers :10
Number of correspondences found: 2
2.0333333333333328 <-> ?answer0 a <http://cmt#Reviewer>.  ?answer1 <http://cmt#assignedTo> ?answer0.  ?answer1 a <http://cmt#Paper>.  
2.0333333333333328 <-> ?answer0 a <http://cmt#Reviewer>.  ?answer0 <http://cmt#hasBeenAssigned> ?answer1.  ?answer1 a <http://cmt#Paper>.  
creating directory: is_assigned_reviewer_of

Unary query : ?o3 <http://ekaw#reviewWrittenBy> ?answer. MINUS{ ?answer <http://ekaw#reviewerOfPaper> ?o2. }
Number of matched answers :10
Number of correspondences found: 7
0.7 <-> ?answer <http://cmt#submitPaper> ?y. ?y a <http://cmt#Paper>. 
0.7375291375291375 <-> ?answer <http://cmt#co-writePaper> ?y. ?y a <http://cmt#Paper>. 
0.7666666666666667 <-> ?answer <http://cmt#memberOfConference> ?y. ?y a <http://cmt#Conference>. 
0.787878787878788 <-> ?answer <http://cmt#markConflictOfInterest> ?y. ?y a <http://cmt#Paper>. 
0.8 <-> ?answer <http://cmt#writePaper> ?y. ?y a <http://cmt#Paper>. 
1.2727272727272727 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Reviewer>. 
1.5 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Reviewer>. 
creating directory: external_reviewer

Binary query : ?answer1 <http://ekaw#reviewWrittenBy> ?answer0 .   ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member> .  ?answer0 <http://ekaw#reviewerOfPaper> ?o3.
Number of matched answers :10
Number of correspondences found: 2
3.1222222222222227 <-> ?answer0 a <http://cmt#Reviewer>.  ?answer0 <http://cmt#writeReview> ?answer1.  ?answer1 a <http://cmt#Review>.  
3.4555555555555557 <-> ?answer0 a <http://cmt#Reviewer>.  ?answer1 <http://cmt#writtenBy> ?answer0.  ?answer1 a <http://cmt#Review>.  
creating directory: assigned_reviewer_author_of_review

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Track> .   ?answer1 <http://ekaw#hasEvent> ?answer0 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop_Chair> .   ?answer0 <http://ekaw#organises> ?answer1 . ?answer1 a <http://ekaw#Workshop> .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .
Number of matched answers :5
Number of correspondences found: 12
0.9187466451959206 <-> ?answer <http://cmt#detailsEnteredBy> ?y. ?y a <http://cmt#Administrator>. 
0.9788011695906433 <-> ?x <http://cmt#enableVirtualMeeting> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.9831041392182694 <-> ?answer <http://cmt#paperAssignmentFinalizedBy> ?y. ?y a <http://cmt#ConferenceMember>. 
1.0401083681976133 <-> ?x <http://cmt#startReviewerBidding> ?answer. ?x a <http://cmt#ConferenceMember>. 
1.0477004985014138 <-> ?x <http://cmt#runPaperAssignmentTools> ?answer. ?x a <http://cmt#ConferenceMember>. 
1.0530181037427415 <-> ?answer <http://cmt#paperAssignmentToolsRunBy> ?y. ?y a <http://cmt#ConferenceMember>. 
1.1413995022690675 <-> ?answer <http://cmt#virtualMeetingEnabledBy> ?y. ?y a <http://cmt#ConferenceMember>. 
1.211489898989899 <-> ?x <http://cmt#enterConferenceDetails> ?answer. ?x a <http://cmt#ConferenceMember>. 
1.2284987556726688 <-> ?answer <http://cmt#hardcopyMailingManifestsPrintedBy> ?y. ?y a <http://cmt#ConferenceMember>. 
1.3703703703703702 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Conference>. 
1.5957602339181287 <-> ?answer <http://cmt#hasConferenceMember> ?y. ?y a <http://cmt#ConferenceMember>. 
1.8842592592592595 <-> ?x <http://cmt#memberOfConference> ?answer. ?x a <http://cmt#ConferenceMember>. 
creating directory: conference

Binary query : ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .   ?answer0 a <http://ekaw#Person> .
Number of matched answers :10
Number of correspondences found: 1
1.9 <-> ?answer0 a <http://cmt#Person>.  ?answer0 <http://cmt#name> ?answer1.  
creating directory: person_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Person> .
Number of matched answers :10
Number of correspondences found: 9
1.0 <-> ?answer <http://cmt#addedBy> ?y. ?y a <http://cmt#Person>. 
1.0 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Person>. 
1.0 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#Person>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person>. 
1.0 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Person>. 
1.0 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#Person>. 
1.0 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#Person>. 
1.0 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#Person>. 
1.0 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#Person>. 
creating directory: person

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Banquet> .   ?answer0 <http://ekaw#heldIn> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Proceedings> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer1 <http://ekaw#publisherOf> ?answer0 .  ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Proceedings> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?o3 <http://ekaw#reviewWrittenBy> ?answer0 .   ?o3 <http://ekaw#reviewWrittenBy> ?answer1 . ?answer1 <http://ekaw#reviewerOfPaper> ?o2 . ?o3 <http://ekaw#reviewOfPaper> ?o2 . FILTER(?o!=?answer0)
Number of matched answers :10
Number of correspondences found: 2
2.615384615384616 <-> ?answer1 <http://cmt#assignExternalReviewer> ?answer0.  ?answer1 a <http://cmt#Reviewer>.  
2.8376068376068377 <-> ?answer0 <http://cmt#assignedByReviewer> ?answer1.  ?answer1 a <http://cmt#Reviewer>.  
creating directory: invite_reviewer

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Tutorial> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Early-Registered_Participant> .
Number of matched answers :10
Number of correspondences found: 4
0.5106026785714285 <-> ?x <http://cmt#readByReviewer> ?answer. ?x a <http://cmt#PaperFullVersion>. 
creating directory: early_registered_participant

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference> .  ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
2.27037037037037 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#name> ?answer1.  
creating directory: conference_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member> .   ?answer <http://ekaw#reviewerOfPaper> ?o2.
Number of matched answers :10
Number of correspondences found: 22
0.6222222222222222 <-> ?x <http://cmt#writtenBy> ?answer. ?x a <http://cmt#Review>. 
0.6666666666666667 <-> ?x <http://cmt#assignedTo> ?answer. ?x a <http://cmt#Paper>. 
0.6666666666666667 <-> ?answer <http://cmt#hasBeenAssigned> ?y. ?y a <http://cmt#Paper>. 
0.6666666666666667 <-> ?x <http://cmt#hasAuthor> ?answer. ?x a <http://cmt#Paper>. 
0.6666666666666667 <-> ?x <http://cmt#hasCo-author> ?answer. ?x a <http://cmt#Paper>. 
0.6875 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceMember>. 
0.7111111111111111 <-> ?answer <http://cmt#name> "Ali Bader". 
0.7720833333333333 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#ConferenceMember>. 
0.8666666666666667 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer>. 
0.888888888888889 <-> ?answer <http://cmt#writeReview> ?y. ?y a <http://cmt#Review>. 
0.893939393939394 <-> ?answer <http://cmt#markConflictOfInterest> ?y. ?y a <http://cmt#Paper>. 
0.9602272727272727 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#ConferenceMember>. 
0.9993055555555557 <-> ?answer <http://cmt#addedBy> ?y. ?y a <http://cmt#ConferenceMember>. 
1.0373611111111112 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#Reviewer>. 
1.0395464852607712 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#ConferenceMember>. 
1.0956876456876459 <-> ?answer <http://cmt#co-writePaper> ?y. ?y a <http://cmt#Paper>. 
1.1077473958333335 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#ExternalReviewer>. 
1.192982456140351 <-> ?x <http://cmt#readByMeta-Reviewer> ?answer. ?x a <http://cmt#Paper>. 
1.2000000000000002 <-> ?answer <http://cmt#submitPaper> ?y. ?y a <http://cmt#Paper>. 
1.2028769841269842 <-> ?x <http://cmt#readByReviewer> ?answer. ?x a <http://cmt#Paper>. 
1.3290598290598292 <-> ?answer <http://cmt#readPaper> ?y. ?y a <http://cmt#Paper>. 
1.4333333333333336 <-> ?answer <http://cmt#writePaper> ?y. ?y a <http://cmt#Paper>. 
creating directory: pc_member

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://ekaw#coversTopic> ?answer1 .   ?answer0 a <http://ekaw#Conference> .
Number of matched answers :10
Number of correspondences found: 8
3.7125420875420874 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#detailsEnteredBy> ?v1.  ?v1 a <http://cmt#ConferenceMember>.  ?v2 <http://cmt#acceptedBy> ?v1.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
3.7125420875420874 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#detailsEnteredBy> ?v1.  ?v1 a <http://cmt#ConferenceMember>.  ?v2 <http://cmt#rejectedBy> ?v1.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
3.7125420875420874 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#detailsEnteredBy> ?v1.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#acceptPaper> ?v2.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
3.9347643097643097 <-> ?answer0 a <http://cmt#Conference>.  ?answer0 <http://cmt#detailsEnteredBy> ?v1.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#rejectPaper> ?v2.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
4.499579124579125 <-> ?answer0 a <http://cmt#Conference>.  ?v1 <http://cmt#memberOfConference> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v2 <http://cmt#assignedTo> ?v1.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
4.499579124579125 <-> ?answer0 a <http://cmt#Conference>.  ?v1 <http://cmt#memberOfConference> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v2 <http://cmt#hasCo-author> ?v1.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
4.499579124579125 <-> ?answer0 a <http://cmt#Conference>.  ?v1 <http://cmt#memberOfConference> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#hasBeenAssigned> ?v2.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
5.191886816886817 <-> ?answer0 a <http://cmt#Conference>.  ?v1 <http://cmt#memberOfConference> ?answer0.  ?v1 a <http://cmt#ConferenceMember>.  ?v1 <http://cmt#co-writePaper> ?v2.  ?v2 <http://cmt#hasSubjectArea> ?answer1.  
creating directory: conference_topic

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Member> .   ?answer <http://ekaw#reviewerOfPaper> ?o3.
Number of matched answers :10
Number of correspondences found: 22
0.6222222222222222 <-> ?x <http://cmt#writtenBy> ?answer. ?x a <http://cmt#Review>. 
0.6666666666666667 <-> ?x <http://cmt#assignedTo> ?answer. ?x a <http://cmt#Paper>. 
0.6666666666666667 <-> ?answer <http://cmt#hasBeenAssigned> ?y. ?y a <http://cmt#Paper>. 
0.6666666666666667 <-> ?x <http://cmt#hasAuthor> ?answer. ?x a <http://cmt#Paper>. 
0.6666666666666667 <-> ?x <http://cmt#hasCo-author> ?answer. ?x a <http://cmt#Paper>. 
0.6875 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceMember>. 
0.7111111111111111 <-> ?answer <http://cmt#name> "Ali Bader". 
0.7720833333333333 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#ConferenceMember>. 
0.8666666666666667 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer>. 
0.888888888888889 <-> ?answer <http://cmt#writeReview> ?y. ?y a <http://cmt#Review>. 
0.893939393939394 <-> ?answer <http://cmt#markConflictOfInterest> ?y. ?y a <http://cmt#Paper>. 
0.9602272727272727 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#ConferenceMember>. 
0.9993055555555557 <-> ?answer <http://cmt#addedBy> ?y. ?y a <http://cmt#ConferenceMember>. 
1.0373611111111112 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#Reviewer>. 
1.0395464852607712 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#ConferenceMember>. 
1.0956876456876459 <-> ?answer <http://cmt#co-writePaper> ?y. ?y a <http://cmt#Paper>. 
1.1077473958333335 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#ExternalReviewer>. 
1.192982456140351 <-> ?x <http://cmt#readByMeta-Reviewer> ?answer. ?x a <http://cmt#Paper>. 
1.2000000000000002 <-> ?answer <http://cmt#submitPaper> ?y. ?y a <http://cmt#Paper>. 
1.2028769841269842 <-> ?x <http://cmt#readByReviewer> ?answer. ?x a <http://cmt#Paper>. 
1.3290598290598292 <-> ?answer <http://cmt#readPaper> ?y. ?y a <http://cmt#Paper>. 
1.4333333333333336 <-> ?answer <http://cmt#writePaper> ?y. ?y a <http://cmt#Paper>. 
creating directory: assigned_reviewer

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Accepted_Paper> .
Number of matched answers :10
Number of correspondences found: 9
0.790240827134031 <-> ?x <http://cmt#markConflictOfInterest> ?answer. ?x a <http://cmt#ProgramCommitteeMember>. 
0.7949419002050581 <-> ?answer <http://cmt#readByMeta-Reviewer> ?y. ?y a <http://cmt#ProgramCommitteeMember>. 
0.7987012987012987 <-> ?answer <http://cmt#assignedTo> ?y. ?y a <http://cmt#ProgramCommitteeMember>. 
0.8204057380527969 <-> ?x <http://cmt#submitPaper> ?answer. ?x a <http://cmt#AuthorNotReviewer>. 
0.8337843209576591 <-> ?x <http://cmt#co-writePaper> ?answer. ?x a <http://cmt#ExternalReviewer>. 
0.8747326203208555 <-> ?answer <http://cmt#acceptedBy> ?y. ?y a <http://cmt#AuthorNotReviewer>. 
0.8901642475171887 <-> ?x <http://cmt#writePaper> ?answer. ?x a <http://cmt#AuthorNotReviewer>. 
1.079029793735676 <-> ?x <http://cmt#acceptPaper> ?answer. ?x a <http://cmt#ProgramCommitteeMember>. 
1.0844155844155843 <-> ?x <http://cmt#readPaper> ?answer. ?x a <http://cmt#ProgramCommitteeMember>. 
creating directory: accepted_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Web_Site> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop_Chair> .
Number of matched answers :10
Number of correspondences found: 5
0.2857142857142857 <-> ?answer <http://cmt#writePaper> ?someObject. 
creating directory: workshop_chair

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Invited_Talk_Abstract> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Invited_Speaker> .
Number of matched answers :10
Number of correspondences found: 9
0.0 <-> ?answer <http://cmt#name> "Amancio Amaro". 
0.6000000000000001 <-> ?answer <http://cmt#readPaper> ?y. ?y a <http://cmt#Paper>. 
0.6000000000000001 <-> ?answer <http://cmt#co-writePaper> ?y. ?y a <http://cmt#Paper>. 
0.6000000000000001 <-> ?answer <http://cmt#submitPaper> ?y. ?y a <http://cmt#Paper>. 
0.6306818181818182 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#ConferenceMember>. 
0.6458333333333334 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.6515151515151516 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Reviewer>. 
0.6666666666666667 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Reviewer>. 
0.688888888888889 <-> ?answer <http://cmt#writePaper> ?y. ?y a <http://cmt#Paper>. 
creating directory: invited_speaker

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper_Author> . ?answer0 <http://ekaw#authorOf> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .
Number of matched answers :10
Number of correspondences found: 4
3.551282051282051 <-> ?answer0 a <http://cmt#Author>.  ?answer0 <http://cmt#co-writePaper> ?answer1.  ?answer1 a <http://cmt#Paper>.  
3.6666666666666665 <-> ?answer0 a <http://cmt#Author>.  ?answer0 <http://cmt#writePaper> ?answer1.  ?answer1 a <http://cmt#Paper>.  
4.083333333333333 <-> ?answer0 a <http://cmt#Author>.  ?answer1 <http://cmt#hasCo-author> ?answer0.  ?answer1 a <http://cmt#Paper>.  
4.416666666666666 <-> ?answer0 a <http://cmt#Author>.  ?answer1 <http://cmt#hasAuthor> ?answer0.  ?answer1 a <http://cmt#Paper>.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Trip> .   ?answer0 <http://ekaw#heldIn> ?o2 . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Research_Topic> .
Number of matched answers :10
Number of correspondences found: 2
0.2777777777777778 <-> ?answer <http://www.w3.org/2000/01/rdf-schema#label> "graph partitioning". 
creating directory: topic

Binary query : ?answer0 a <http://ekaw#Invited_Talk_Abstract>. ?answer0 <http://ekaw#paperPresentedAs> ?answer1. ?answer1 a <http://ekaw#Invited_Talk>.
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Student> .
Number of matched answers :10
Number of correspondences found: 6
0.0 <-> ?answer <http://cmt#name> "Akeem Adams". 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Person>. 
0.5 <-> ?answer <http://cmt#markConflictOfInterest> ?y. ?y a <http://cmt#Document>. 
0.5 <-> ?x <http://cmt#hasCo-author> ?answer. ?x a <http://cmt#Document>. 
0.5 <-> ?answer <http://cmt#co-writePaper> ?y. ?y a <http://cmt#Document>. 
0.7222222222222222 <-> ?x <http://cmt#hasAuthor> ?answer. ?x a <http://cmt#Document>. 
creating directory: student

Unary query : ?o2 a <http://ekaw#Web_Site>. ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer. filter(regex(?s,"^http"))
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#SC_Member> .
Number of matched answers :10
Number of correspondences found: 7
0.4375 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#ConferenceMember>. 
0.4722222222222222 <-> ?x <http://cmt#hasCo-author> ?answer. ?x a <http://cmt#Paper>. 
0.6597222222222222 <-> ?answer <http://cmt#addedBy> ?y. ?y a <http://cmt#ConferenceMember>. 
0.7152777777777778 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.7152777777777778 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#ConferenceMember>. 
0.7175 <-> ?x <http://cmt#addProgramCommitteeMember> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.7213095238095238 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#ConferenceMember>. 
creating directory: sc_member

Binary query : ?answer1 <http://ekaw#reviewWrittenBy> ?answer0 .
Number of matched answers :10
Number of correspondences found: 6
0.5 <-> ?v1 <http://cmt#assignExternalReviewer> ?answer0.  ?v1 <http://cmt#writeReview> ?answer1.  
0.5 <-> ?answer0 <http://cmt#writeReview> ?answer1.  
0.7222222222222222 <-> ?answer0 <http://cmt#assignedByReviewer> ?v1.  ?v1 <http://cmt#writeReview> ?answer1.  
1.0999999999999999 <-> ?answer1 <http://cmt#writtenBy> ?answer0.  
1.1 <-> ?v1 <http://cmt#assignExternalReviewer> ?answer0.  ?answer1 <http://cmt#writtenBy> ?v1.  
1.3222222222222222 <-> ?answer0 <http://cmt#assignedByReviewer> ?v1.  ?answer1 <http://cmt#writtenBy> ?v1.  
creating directory: all_reviewer_author_of_review

Unary query : {?answer <http://ekaw#reviewerOfPaper> ?o2 . } union{ ?o3 <http://ekaw#reviewOfPaper> ?o2 .  ?o3 <http://ekaw#reviewWrittenBy> ?answer .  }
Number of matched answers :10
Number of correspondences found: 22
0.717948717948718 <-> ?x <http://cmt#assignedTo> ?answer. ?x a <http://cmt#Paper>. 
0.717948717948718 <-> ?answer <http://cmt#hasBeenAssigned> ?y. ?y a <http://cmt#Paper>. 
0.717948717948718 <-> ?x <http://cmt#hasAuthor> ?answer. ?x a <http://cmt#Paper>. 
0.717948717948718 <-> ?x <http://cmt#hasCo-author> ?answer. ?x a <http://cmt#Paper>. 
0.7261446886446887 <-> ?x <http://cmt#assignReviewer> ?answer. ?x a <http://cmt#ConferenceMember>. 
0.7330128205128206 <-> ?answer <http://cmt#name> ?someObject. 
0.75 <-> ?answer <http://cmt#memberOfProgramCommittee> ?someObject. 
0.7528571428571428 <-> ?answer <http://cmt#assignExternalReviewer> ?y. ?y a <http://cmt#ConferenceMember>. 
0.9750793650793651 <-> ?x <http://cmt#assignedByReviewer> ?answer. ?x a <http://cmt#ExternalReviewer>. 
1.1 <-> ?answer <http://cmt#memberOfConference> ?y. ?y a <http://cmt#Conference>. 
1.4490442890442887 <-> ?answer <http://cmt#markConflictOfInterest> ?y. ?y a <http://cmt#Paper>. 
1.4907925407925409 <-> ?answer <http://cmt#submitPaper> ?y. ?y a <http://cmt#Paper>. 
1.5025641025641026 <-> ?answer <http://cmt#co-writePaper> ?y. ?y a <http://cmt#Paper>. 
1.528205128205128 <-> ?answer <http://cmt#writeReview> ?y. ?y a <http://cmt#Review>. 
1.5600539811066128 <-> ?x <http://cmt#readByMeta-Reviewer> ?answer. ?x a <http://cmt#Paper>. 
1.5803547329863121 <-> ?x <http://cmt#readByReviewer> ?answer. ?x a <http://cmt#Paper>. 
1.6153846153846154 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Reviewer>. 
1.646153846153846 <-> ?answer <http://cmt#writePaper> ?y. ?y a <http://cmt#Paper>. 
1.672820512820513 <-> ?answer <http://cmt#readPaper> ?y. ?y a <http://cmt#Paper>. 
1.8615384615384616 <-> ?x <http://cmt#writtenBy> ?answer. ?x a <http://cmt#Review>. 
2.1153846153846154 <-> ?x <http://cmt#assignExternalReviewer> ?answer. ?x a <http://cmt#Reviewer>. 
2.3376068376068377 <-> ?answer <http://cmt#assignedByReviewer> ?y. ?y a <http://cmt#Reviewer>. 
creating directory: reviewers_all

Binary query : ?answer0 <http://ekaw#coversTopic> ?answer1 . ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .
Number of matched answers :10
Number of correspondences found: 1
1.7142857142857149 <-> ?answer0 a <http://cmt#Paper>.  ?answer0 <http://cmt#hasSubjectArea> ?answer1.  
creating directory: topic_of_paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Review> .
Number of matched answers :10
Number of correspondences found: 3
0.9722222222222222 <-> ?answer <http://cmt#writtenBy> ?y. ?y a <http://cmt#Reviewer>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Review>. 
1.2954545454545454 <-> ?x <http://cmt#writeReview> ?answer. ?x a <http://cmt#Reviewer>. 
creating directory: review

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Workshop> .  ?answer0 <http://ekaw#coversTopic> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Conference_Banquet> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer1 a <http://ekaw#Conference> . ?o2 <http://ekaw#listsEvent> ?answer1 . ?o2 a <http://ekaw#Web_Site> . ?o2 <http://www.w3.org/2000/01/rdf-schema#label> ?answer0 . filter(regex(?s,"^http"))
Number of matched answers :5
Number of correspondences found: 0

Unary query : ?answer a <http://ekaw#Invited_Talk> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper_Author> .
Number of matched answers :10
Number of correspondences found: 5
0.6271929824561403 <-> ?x <http://cmt#readByMeta-Reviewer> ?answer. ?x a <http://cmt#Paper>. 
0.6309523809523809 <-> ?x <http://cmt#readByReviewer> ?answer. ?x a <http://cmt#Paper>. 
0.717391304347826 <-> ?answer <http://cmt#assignedByAdministrator> ?y. ?y a <http://cmt#Co-author>. 
0.9999999999999999 <-> ?x <http://cmt#hasCo-author> ?answer. ?x a <http://cmt#Paper>. 
0.9999999999999999 <-> ?x <http://cmt#hasAuthor> ?answer. ?x a <http://cmt#Paper>. 
creating directory: author

Binary query : ?answer0 <http://ekaw#presentationOfPaper> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#OC_Chair> .
Number of matched answers :5
Number of correspondences found: 2
0.3076923076923077 <-> <http://cmt-instances#person-1654457522> <http://cmt#assignedByReviewer> ?answer. 
creating directory: oc_chair

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .
Number of matched answers :10
Number of correspondences found: 11
0.6142857142857143 <-> ?answer <http://cmt#readByReviewer> ?y. ?y a <http://cmt#User>. 
0.6142857142857143 <-> ?answer <http://cmt#hasSubjectArea> <http://cmt-instances#topic3121>. 
0.6222222222222222 <-> ?answer <http://cmt#hasAuthor> ?y. ?y a <http://cmt#User>. 
0.7142857142857143 <-> ?answer <http://cmt#paperID> ?someObject. 
0.7846153846153846 <-> ?x <http://cmt#co-writePaper> ?answer. ?x a <http://cmt#User>. 
0.8545454545454546 <-> ?x <http://cmt#rejectPaper> ?answer. ?x a <http://cmt#User>. 
0.8545454545454546 <-> ?x <http://cmt#acceptPaper> ?answer. ?x a <http://cmt#User>. 
0.8545454545454546 <-> ?x <http://cmt#submitPaper> ?answer. ?x a <http://cmt#User>. 
0.8943181818181818 <-> ?x <http://cmt#writePaper> ?answer. ?x a <http://cmt#User>. 
0.9555555555555556 <-> ?x <http://cmt#readPaper> ?answer. ?x a <http://cmt#User>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://cmt#Paper>. 
creating directory: paper

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#PC_Chair> .
Number of matched answers :5
Number of correspondences found: 3
0.5 <-> ?x <http://cmt#hasCo-author> ?answer. ?x a <http://cmt#Paper>. 
creating directory: pc_chair

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Regular_Paper> .
Number of matched answers :10
Number of correspondences found: 8
0.6153846153846153 <-> ?x <http://cmt#acceptPaper> ?answer. ?x a <http://cmt#User>. 
0.6234817813765182 <-> ?answer <http://cmt#readByMeta-Reviewer> ?y. ?y a <http://cmt#Reviewer>. 
0.6511532105067576 <-> ?x <http://cmt#co-writePaper> ?answer. ?x a <http://cmt#User>. 
0.6602564102564102 <-> ?x <http://cmt#writePaper> ?answer. ?x a <http://cmt#User>. 
0.6689858058608058 <-> ?answer <http://cmt#readByReviewer> ?y. ?y a <http://cmt#Reviewer>. 
0.7216346153846154 <-> ?x <http://cmt#submitPaper> ?answer. ?x a <http://cmt#User>. 
0.7829670329670328 <-> ?x <http://cmt#rejectPaper> ?answer. ?x a <http://cmt#User>. 
0.9279034690799396 <-> ?x <http://cmt#readPaper> ?answer. ?x a <http://cmt#Reviewer>. 
creating directory: regular_paper

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Paper> .   ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.
Number of matched answers :10
Number of correspondences found: 1
1.9 <-> ?answer0 a <http://cmt#Paper>.  ?answer0 <http://cmt#title> ?answer1.  
creating directory: paper_title

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ekaw#Contributed_Talk> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 a <http://ekaw#Contributed_Talk>. ?answer1 a <http://ekaw#Conference> . ?answer0 <http://ekaw#partOfEvent> ?answer1.
Number of matched answers :10
Number of correspondences found: 0
Matching process ended
