===============================================================================
creating directory: conference-confOf
Running with 10 support instances - 0.9 similarity.
Number of CQAs: 73

Unary query : ?answer a <http://conference#Contribution_1th-author>.
Number of matched answers :10
Number of correspondences found: 8
0.0 <-> ?answer <http://confOf#writes> <http://confOf-instances#paper14194386522941114>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://confOf#reviewes> <http://confOf-instances#paper-4969652075681114>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
0.0 <-> <http://confOf-instances#paper-18961490172292198> <http://confOf#writtenBy> ?answer. 
0.0 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Publisher> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Rejected_contribution> .
Number of matched answers :10
Number of correspondences found: 5
0.0 <-> ?answer <http://confOf#writtenBy> <http://confOf-instances#person451441405>. 
0.0 <-> ?answer <http://confOf#dealsWith> ?someObject. 
0.0 <-> <http://confOf-instances#person-1886357074> <http://confOf#writes> ?answer. 
0.0 <-> ?answer <http://confOf#hasKeyword> ?someObject. 
0.0 <-> <http://confOf-instances#person720958559> <http://confOf#reviewes> ?answer. 

Binary query : ?answer0 <http://conference#has_the_last_name> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
0.5 <-> ?answer0 <http://confOf#hasSurname> ?answer1.  
creating directory: person_last_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Track> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_participant> .
Number of matched answers :10
Number of correspondences found: 6
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant>. 
0.0 <-> ?answer <http://confOf#reviewes> ?someObject. 
0.0 <-> ?answer <http://confOf#writes> ?someObject. 
0.0 <-> ?answer <http://confOf#employedBy> <http://confOf-instances#orga1474150858>. 

Unary query : ?answer <http://conference#was_a_member_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Organizing_committee> .
Number of matched answers :10
Number of correspondences found: 7
0.0 <-> ?answer <http://confOf#hasSurname> "Fridman". 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Thing>. 
0.0 <-> ?answer <http://confOf#employedBy> ?someObject. 
0.0 <-> ?answer <http://confOf#hasFirstName> "Alexander". 
0.0 <-> ?answer <http://confOf#reviewes> ?someObject. 
0.0 <-> <http://confOf-instances#paper1850386443322198> <http://confOf#writtenBy> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author>. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_proceedings> .   ?answer0 <http://conference#has_a_name> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer1 <http://conference#has_workshops> ?answer0 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#was_a_member_of> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#reviews> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Tutorial> .  ?answer0 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://confOf#Tutorial>.  ?answer0 <http://confOf#hasTopic> ?answer1.  
creating directory: tutorial_topic

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 . ?o2 a <http://conference#Important_dates>. ?o2 <http://conference#is_a_date_of_acceptance_announcement> ?answer1.
Number of matched answers :5
Number of correspondences found: 1
0.5 <-> ?answer0 <http://confOf#hasAdministrativeEvent> ?v1.  ?v1 <http://confOf#starts_on> ?answer1.  
creating directory: conference_notif_date

Binary query : ?answer0 a <http://conference#Conference_participant>.      bind( if (exists {?answer0 a <http://conference#Early_paid_applicant>}, "true"^^<http://www.w3.org/2001/XMLSchema#boolean>,        if (exists {?answer0 a <http://conference#Late_paid_applicant>}, "false"^^<http://www.w3.org/2001/XMLSchema#boolean>,"")) as ?answer1)
Number of matched answers :10
Number of correspondences found: 1
0.5 <-> ?answer0 <http://confOf#earlyRegistration> ?answer1.  
creating directory: early_registration

Binary query : ?answer1 <http://conference#has_tutorials> ?answer0 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#has_a_location> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
0.5 <-> ?answer0 <http://confOf#location> ?answer1.  
creating directory: conference_location

Unary query : ?answer a <http://conference#Conference_participant> .   ?answer a <http://conference#Late_paid_applicant> .
Number of matched answers :10
Number of correspondences found: 8
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Thing>. 
0.0 <-> ?answer <http://confOf#earlyRegistration> "false". 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://confOf#reviewes> <http://confOf-instances#paper4242506666832198>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Contribution_co-author> .
Number of matched answers :10
Number of correspondences found: 7
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
0.0 <-> ?answer <http://confOf#reviewes> <http://confOf-instances#paper-16677545845022198>. 
0.0 <-> ?answer <http://confOf#hasSurname> "Pease". 
0.0 <-> ?answer <http://confOf#writes> ?someObject. 
0.0 <-> <http://confOf-instances#paper661615269224257> <http://confOf#writtenBy> ?answer. 

Binary query : ?answer1 a <http://conference#Abstract>. ?answer0 <http://conference#contributes> ?answer1 . ?o2 <http://conference#has_an_abstract> ?answer1. ?o2 a <http://conference#Invited_talk>.
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 a <http://conference#Invited_speaker>. ?answer0 <http://conference#contributes> ?answer1 . ?answer1 a <http://conference#Invited_talk>.
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#contributes> ?o2 .  ?o2 <http://conference#reviews> ?answer1 .  ?answer0 <http://conference#was_a_member_of> ?o3 . ?o3 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :10
Number of correspondences found: 1
0.5 <-> ?answer0 <http://confOf#reviewes> ?answer1.  
creating directory: is_assigned_reviewer_of

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#gives_presentations> ?answer1. ?answer1 a <http://conference#Presentation>.
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer> .  ?answer <http://conference#invited_by> ?o2 .
Number of matched answers :10
Number of correspondences found: 6
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
0.0 <-> ?answer <http://confOf#writes> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 
0.0 <-> <http://confOf-instances#paper1850386443322198> <http://confOf#writtenBy> ?answer. 

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 . ?o2 <http://conference#is_a_date_of_camera_ready_paper_submission> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
0.5 <-> ?answer0 <http://confOf#hasAdministrativeEvent> ?v1.  ?v1 <http://confOf#ends_on> ?answer1.  
creating directory: conference_camera_date

Binary query : ?answer0 <http://conference#contributes> ?answer1 .   ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Review> .    ?answer0 <http://conference#was_a_member_of> ?o3 . ?o3 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer1 <http://conference#has_tracks> ?answer0 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#was_a_track-workshop_chair_of> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop> .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_volume> .
Number of matched answers :5
Number of correspondences found: 4
0.0 <-> ?answer <http://confOf#location> "Bernese Alps". 
0.0 <-> ?answer <http://confOf#hasTopic> ?someObject. 
0.0 <-> ?answer <http://confOf#ends_on> "2011-09-25T18:00:00". 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Event>. 

Binary query : ?answer0 <http://conference#has_the_first_name> ?o1 .  ?answer0 <http://conference#has_the_last_name> ?o2 .   BIND( concat(STR(?o1)," ", STR(?o2)) as ?answer1)
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Person> .
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 
creating directory: person

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_proceedings> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#has_a_publisher> ?answer1 .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 .   ?answer0 a <http://conference#Conference_volume> . ?o2 <http://conference#is_a_starting_date> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
0.5 <-> ?answer0 <http://confOf#starts_on> ?answer1.  
creating directory: conference_start_date

Binary query : ?answer0 <http://conference#invites_co-reviewers> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Tutorial> .
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Tutorial>. 
creating directory: tutorial

Unary query : ?answer a <http://conference#Conference_participant> .   ?answer a <http://conference#Early_paid_applicant> .
Number of matched answers :10
Number of correspondences found: 6
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant>. 
0.0 <-> ?answer <http://confOf#reviewes> ?someObject. 
0.0 <-> ?answer <http://confOf#writes> ?someObject. 
0.0 <-> ?answer <http://confOf#employedBy> <http://confOf-instances#orga1474150858>. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference> . ?answer0 <http://conference#has_a_name> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://confOf#Conference>.  ?answer0 <http://confOf#has_title> ?answer1.  
creating directory: conference_name

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Abstract> .   ?answer <http://conference#is_the_1th_part_of> ?o2. ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://conference#was_a_member_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :10
Number of correspondences found: 3
0.0 <-> ?answer <http://confOf#reviewes> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop> .
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Workshop>. 
creating directory: workshop

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 . ?o2 <http://conference#is_a_full_paper_submission_date> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
0.5 <-> ?answer0 <http://confOf#hasAdministrativeEvent> ?v1.  ?v1 <http://confOf#starts_on> ?answer1.  
creating directory: conference_paper_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer> . ?answer <http://conference#was_a_member_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
creating directory: assigned_reviewer

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Accepted_contribution> .
Number of matched answers :10
Number of correspondences found: 6
0.0 <-> ?answer <http://confOf#writtenBy> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Contribution>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Thing>. 
0.0 <-> <http://confOf-instances#person-1122393118> <http://confOf#writes> ?answer. 
0.0 <-> ?answer <http://confOf#hasTitle> ?someObject. 
0.0 <-> ?answer <http://confOf#dealsWith> ?someObject. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Conference_www> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://conference#was_a_track-workshop_chair_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop> .
Number of matched answers :10
Number of correspondences found: 8
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Thing>. 
0.0 <-> ?answer <http://confOf#studyAt> <http://confOf-instances#orga-1021132242>. 
0.0 <-> ?answer <http://confOf#reviewes> <http://confOf-instances#paper7474344568072198>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Scholar>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author>. 

Unary query : ?answer <http://conference#is_the_1th_part_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Invited_talk> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Invited_speaker> .
Number of matched answers :10
Number of correspondences found: 8
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 
0.0 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
0.0 <-> ?answer <http://confOf#hasSurname> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Chair_PC>. 
0.0 <-> ?answer <http://confOf#reviewes> <http://confOf-instances#paper-337047316182257>. 
0.0 <-> <http://confOf-instances#paper11642622845741114> <http://confOf#writtenBy> ?answer. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author>. 

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Regular_author> . ?answer0 <http://conference#contributes> ?answer1 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .   minus{  ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Abstract> .   }
Number of matched answers :10
Number of correspondences found: 2
0.5 <-> ?answer1 <http://confOf#writtenBy> ?answer0.  
0.5 <-> ?answer0 <http://confOf#writes> ?answer1.  
creating directory: is_author_of_paper

Binary query : ?answer0 <http://conference#has_important_dates> ?o2 . ?o2 <http://conference#is_an_ending_date> ?answer1 .
Number of matched answers :5
Number of correspondences found: 1
0.5 <-> ?answer0 <http://confOf#ends_on> ?answer1.  
creating directory: conference_end_date

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Topic> .
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Topic>. 
creating directory: topic

Binary query : ?answer0 a <http://conference#Abstract>. ?answer1 <http://conference#has_an_abstract> ?answer0 . ?answer1 a <http://conference#Invited_talk>.
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?o2 a <http://conference#Conference_www>. ?o2 <http://conference#has_a_URL> ?answer .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://conference#was_a_member_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Steering_committee> .
Number of matched answers :10
Number of correspondences found: 8
0.0 <-> ?answer <http://confOf#writes> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Thing>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 
0.0 <-> ?answer <http://confOf#reviewes> <http://confOf-instances#paper5608555808012198>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author>. 
0.0 <-> <http://confOf-instances#paper-19148055417971114> <http://confOf#writtenBy> ?answer. 
0.0 <-> ?answer <http://confOf#hasFirstName> "Alan". 
0.0 <-> ?answer <http://confOf#hasSurname> "Efrat". 

Binary query : ?answer1 <http://conference#has_authors> ?answer0 . ?answer1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Review> .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Reviewer> .
Number of matched answers :10
Number of correspondences found: 6
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://confOf#hasFirstName> ?someObject. 
0.0 <-> ?answer <http://confOf#writes> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Member_PC>. 
creating directory: reviewers_all

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Review> .
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://conference#has_the_first_name> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
0.5 <-> ?answer0 <http://confOf#hasFirstName> ?answer1.  
creating directory: person_first_name

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Workshop> .  ?answer0 <http://conference#has_a_track-workshop-tutorial_topic> ?answer1 .
Number of matched answers :10
Number of correspondences found: 1
1.5 <-> ?answer0 a <http://confOf#Workshop>.  ?answer0 <http://confOf#hasTopic> ?answer1.  
creating directory: workshop_topic

Unary query : ?answer a <http://conference#Invited_talk> .
Number of matched answers :0
Number of correspondences found: 0

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Regular_author> .
Number of matched answers :10
Number of correspondences found: 7
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant>. 
0.0 <-> ?answer <http://confOf#reviewes> <http://confOf-instances#paper-2159444624562198>. 
0.0 <-> ?answer <http://confOf#hasSurname> "Pease". 
0.0 <-> ?answer <http://confOf#writes> ?someObject. 
0.0 <-> <http://confOf-instances#paper661615269224257> <http://confOf#writtenBy> ?answer. 
0.0 <-> ?answer <http://confOf#hasFirstName> "Marquis". 

Binary query : ?answer0 a <http://conference#Written_contribution> . ?answer0 <http://conference#has_an_abstract> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Binary query : ?answer0 a <http://conference#Presentation>. ?answer0 <http://conference#has_an_abstract> ?o2 . ?answer1 <http://conference#has_an_abstract> ?o2 . ?answer1 a <http://conference#Written_contribution> .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://conference#was_a_committee_chair_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Organizing_committee> .
Number of matched answers :5
Number of correspondences found: 5
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Author>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Thing>. 
0.0 <-> ?answer <http://confOf#hasFirstName> "Beata". 
0.0 <-> ?answer <http://confOf#reviewes> <http://confOf-instances#paper-253329908202257>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> . minus{ ?answer a  <http://conference#Abstract> . }
Number of matched answers :10
Number of correspondences found: 6
0.0 <-> ?answer <http://confOf#writtenBy> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Thing>. 
0.0 <-> ?answer <http://confOf#hasKeyword> ?someObject. 
0.0 <-> <http://confOf-instances#person-1122393118> <http://confOf#writes> ?answer. 
0.0 <-> ?answer <http://confOf#hasTitle> "Electroweak working group report". 
0.0 <-> ?answer <http://confOf#dealsWith> ?someObject. 

Unary query : ?answer <http://conference#gives_presentations> ?o3. ?o3 <http://conference#has_an_abstract> ?o2 . ?o4 <http://conference#has_an_abstract> ?o2 . ?o4 a <http://conference#Written_contribution> .
Number of matched answers :10
Number of correspondences found: 7
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Participant>. 
0.0 <-> ?answer <http://confOf#hasSurname> ?someObject. 
0.0 <-> ?answer <http://confOf#reviewes> ?someObject. 
0.0 <-> ?answer <http://confOf#writes> <http://confOf-instances#paper-9314016512881114>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Person>. 
0.0 <-> ?answer <http://confOf#employedBy> <http://confOf-instances#orga1474150858>. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#NamedIndividual>. 

Binary query : ?answer0 <http://conference#is_submitted_at> ?answer1 .
Number of matched answers :10
Number of correspondences found: 0

Unary query : ?answer <http://conference#was_a_committee_chair_of> ?o2 . ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Program_committee> .
Number of matched answers :5
Number of correspondences found: 2
0.0 <-> ?answer <http://confOf#reviewes> ?someObject. 
0.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Chair_PC>. 

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Paper> .
Number of matched answers :10
Number of correspondences found: 1
1.0 <-> ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://confOf#Paper>. 
creating directory: regular_paper

Binary query : { ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Paper> .   } UNION { ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .   ?o2 <http://conference#is_the_1th_part_of> ?answer0. ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .   }  ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.
Number of matched answers :10
Number of correspondences found: 2
0.5 <-> ?answer0 <http://confOf#hasTitle> ?answer1.  
1.5 <-> ?answer0 a <http://confOf#Paper>.  ?answer0 <http://confOf#hasTitle> ?answer1.  
creating directory: paper_title

Unary query : ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Presentation> . minus{ ?answer a <http://conference#Invited_talk> . }
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Abstract> .   ?answer0 <http://conference#is_the_1th_part_of> ?o2. ?o2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://conference#Written_contribution> .    ?answer0 <http://www.w3.org/2000/01/rdf-schema#label> ?answer1.
Number of matched answers :0
Number of correspondences found: 0

Binary query : ?answer0 a <http://conference#Presentation> . ?answer0 <http://conference#has_an_abstract> ?o2 . ?o4 <http://conference#has_an_abstract> ?o2 . ?o4 a <http://conference#Written_contribution> . ?o4 <http://conference#is_submitted_at> ?answer1.
Number of matched answers :10
Number of correspondences found: 0
Matching process ended
