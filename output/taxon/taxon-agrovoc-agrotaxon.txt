Reading query ...
broader	
preflabel	
literalform	
taxa	หน่วยอนุกรมวิธาน	ტაქსონი	분류군	分類群	نظام التصنيف	takson	ລະດັບການຈັດແບ່ງ	टैक्सा	таксоны	taxón	c_7624	taxon	takson (biologia)	వర్గీకరణ	动物分类	آرایه‌ها	
hastaxonomicrank	
Getting source answers ...
Getting target answers ...
<urn:absolute:ontology.irstea.fr/agronomicTaxon/ressource/goldStandard#triticum_turgidum_subsp_durum>
<urn:absolute:ontology.irstea.fr/agronomicTaxon/ressource/goldStandard#triticum_turgidum_subsp_dicoccoides>
<urn:absolute:ontology.irstea.fr/agronomicTaxon/ressource/goldStandard#plantae>
<urn:absolute:ontology.irstea.fr/agronomicTaxon/ressource/goldStandard#triticum_monococcum_subsp_aegilopoides>
<urn:absolute:ontology.irstea.fr/agronomicTaxon/ressource/goldStandard#triticum_aestivum_subsp_spelta>
<urn:absolute:ontology.irstea.fr/agronomicTaxon/ressource/goldStandard#triticum_monococcum>
<urn:absolute:ontology.irstea.fr/agronomicTaxon/ressource/goldStandard#triticum_turgidum>
<urn:absolute:ontology.irstea.fr/agronomicTaxon/ressource/goldStandard#triticum_turgidum_subsp_turgidum>
<urn:absolute:ontology.irstea.fr/agronomicTaxon/ressource/goldStandard#triticum>
<urn:absolute:ontology.irstea.fr/agronomicTaxon/ressource/goldStandard#triticum_turgidum_subsp_dicoccon>
<urn:absolute:ontology.irstea.fr/agronomicTaxon/ressource/goldStandard#triticum_aestivum>
<urn:absolute:ontology.irstea.fr/agronomicTaxon/ressource/goldStandard#triticum_aestivum_subsp_aestivum>
Sorting triples ...
Intension subgraphs
2.1805555555555554 -- ?answer <http://www.w3.org/2004/02/skos/core#inScheme> ?y. ?y rdf:type <http://ontology.irstea.fr/agronomictaxon/core#Taxonomy>. 
3.0666666666666664 -- ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ontology.irstea.fr/agronomictaxon/core#Taxon>. 
4.066666666666666 -- ?answer <http://ontology.irstea.fr/agronomictaxon/core#hasLowerRank> ?y. ?y rdf:type <http://ontology.irstea.fr/agronomictaxon/core#Taxon>. 
4.066666666666666 -- ?x <http://ontology.irstea.fr/agronomictaxon/core#hasLowerRank> ?answer. ?x rdf:type <http://ontology.irstea.fr/agronomictaxon/core#Taxon>. 

Extension subgraphs
2.1805555555555554 -- ?answer <http://www.w3.org/2004/02/skos/core#inScheme> ?y. ?y rdf:type <http://ontology.irstea.fr/agronomictaxon/core#Taxonomy>. 
3.0666666666666664 -- ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://ontology.irstea.fr/agronomictaxon/core#Taxon>. 
4.066666666666666 -- ?answer <http://ontology.irstea.fr/agronomictaxon/core#hasLowerRank> ?y. ?y rdf:type <http://ontology.irstea.fr/agronomictaxon/core#Taxon>. 
4.066666666666666 -- ?x <http://ontology.irstea.fr/agronomictaxon/core#hasLowerRank> ?answer. ?x rdf:type <http://ontology.irstea.fr/agronomictaxon/core#Taxon>. 
