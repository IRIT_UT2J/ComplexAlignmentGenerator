Reading query ...
species	speiceas	είδος	especies	spezie	種_(分類学)	espèce	soort	
label	
Getting source answers ...
Getting target answers ...
<urn:absolute:ontology.irstea.fr/agronomicTaxon/ressource/goldStandard#triticum_monococcum_subsp_monococcum>
<urn:absolute:ontology.irstea.fr/agronomicTaxon/ressource/goldStandard#triticum_monococcum>
<urn:absolute:ontology.irstea.fr/agronomicTaxon/ressource/goldStandard#triticum_turgidum_subsp_polonicum>
<urn:absolute:ontology.irstea.fr/agronomicTaxon/ressource/goldStandard#triticum>
Sorting triples ...
Intension subgraphs
2.4454545454545453 -- ?x <http://ontology.irstea.fr/agronomictaxon/core#hasHigherRank> ?answer. ?x rdf:type <urn:absolute:ontology.irstea.fr/agronomicTaxon/ressource/goldStandard#SubSpecyRank>. 
4.839285714285714 -- ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?someObject. 
4.839285714285714 -- ?answer <http://ontology.irstea.fr/agronomictaxon/core#hasLowerRank> ?y. ?y rdf:type <http://ontology.irstea.fr/agronomictaxon/core#SpecyRank>. 
4.839285714285714 -- ?x ?somePredicate ?answer. ?x rdf:type <http://ontology.irstea.fr/agronomictaxon/core#SpecyRank>. 

Extension subgraphs
2.4454545454545453 -- ?x <http://ontology.irstea.fr/agronomictaxon/core#hasHigherRank> ?answer. ?x rdf:type <urn:absolute:ontology.irstea.fr/agronomicTaxon/ressource/goldStandard#SubSpecyRank>. 
4.839285714285714 -- ?answer <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> [<http://ontology.irstea.fr/agronomictaxon/core#SpecyRank>, <urn:absolute:ontology.irstea.fr/agronomicTaxon/ressource/goldStandard#SubSpecyRank>]. 
4.839285714285714 -- ?answer <http://ontology.irstea.fr/agronomictaxon/core#hasLowerRank> ?y. ?y rdf:type <http://ontology.irstea.fr/agronomictaxon/core#SpecyRank>. 
4.839285714285714 -- ?x [<http://ontology.irstea.fr/agronomictaxon/core#hasHigherRank>, <http://ontology.irstea.fr/agronomictaxon/core#hasLowerRank>] ?answer. ?x rdf:type <http://ontology.irstea.fr/agronomictaxon/core#SpecyRank>. 
