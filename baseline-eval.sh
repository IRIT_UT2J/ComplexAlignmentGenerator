
#echo "********** Source: AGROVOC - Target: AgronomicTaxon - Query: GENUS ***********"
#java -jar ComplexMatcher.jar http://localhost:3030/AgroVoc/ http://localhost:3030/AgronomicTaxon/ needs/genus-agrovoc.sparql query_templates/AgroVoc/ query_templates/generic/ > output/genus-agrovoc-agrotaxon.txt
#echo "************* Source: AGROVOC - Target: DBpedia- Query: GENUS ****************"
#java -jar ComplexMatcher.jar http://localhost:3030/AgroVoc/ http://dbpedia.org/ needs/genus-agrovoc.sparql query_templates/AgroVoc/ query_templates/DBpedia/ > output/genus-agrovoc-dbpedia.txt
#echo "************** Source: AGROVOC - Target: TaxRef - Query: GENUS ***************"
#java -jar ComplexMatcher.jar http://localhost:3030/AgroVoc/ http://erebe-vm2.i3s.unice.fr:8890/ needs/genus-agrovoc.sparql query_templates/AgroVoc/ query_templates/TaxRef/ > output/genus-agrovoc-taxref.txt


#echo "********** Source: AGROTAXON- Target: Agrovoc - Query: GENUS ***********"
#java -jar ComplexMatcher.jar http://localhost:3030/AgronomicTaxon/ http://localhost:3030/AgroVoc/ needs/genus-agrotaxon.sparql query_templates/generic/ query_templates/AgroVoc/> output/genus-agrotaxon-agrovoc.txt
#echo "************* Source: AGROTAXON - Target: DBpedia- Query: GENUS ****************"
#java -jar ComplexMatcher.jar http://localhost:3030/AgronomicTaxon/ http://dbpedia.org/ needs/genus-agrotaxon.sparql query_templates/generic/ query_templates/DBpedia/ > output/genus-agrotaxon-dbpedia.txt
#echo "************** Source: AGROTAXON - Target: TaxRef - Query: GENUS ***************"
#java -jar ComplexMatcher.jar http://localhost:3030/AgronomicTaxon/ http://erebe-vm2.i3s.unice.fr:8890/ needs/genus-agrotaxon.sparql query_templates/generic/ query_templates/TaxRef/> output/genus-agrotaxon-taxref.txt

#echo "********** Source: DBPEDIA- Target: Agrovoc - Query: GENUS ***********"
#java -jar ComplexMatcher.jar http://dbpedia.org/ http://localhost:3030/AgroVoc/ needs/genus-dbpedia.sparql query_templates/DBpedia/ query_templates/AgroVoc/ > output/genus-dbpedia-agrovoc.txt
#echo "************* Source: DBPEDIA - Target: AgronomicTaxon- Query: GENUS ****************"
#java -jar ComplexMatcher.jar http://dbpedia.org/ http://localhost:3030/AgronomicTaxon/ needs/genus-dbpedia.sparql query_templates/DBpedia/ query_templates/generic/ > output/genus-dbpedia-agrotaxon.txt
#echo "************** Source: DBPEDIA - Target: TaxRef - Query: GENUS ***************"
#java -jar ComplexMatcher.jar http://dbpedia.org/ http://erebe-vm2.i3s.unice.fr:8890/ needs/genus-dbpedia.sparql query_templates/DBpedia/ query_templates/TaxRef/ > output/genus-dbpedia-taxref.txt

echo "********** Source: TAXREF- Target: Agrovoc - Query: GENUS ***********"
java -jar ComplexMatcher.jar http://erebe-vm2.i3s.unice.fr:8890/ http://localhost:3030/AgroVoc/ needs/genus-taxref.sparql query_templates/TaxRef/ query_templates/AgroVoc/ > output/genus-taxref-agrovoc.txt
echo "************* Source: TAXREF - Target: AgronomicTaxon- Query: GENUS ****************"
java -jar ComplexMatcher.jar http://erebe-vm2.i3s.unice.fr:8890/ http://localhost:3030/AgronomicTaxon/ needs/genus-taxref.sparql query_templates/TaxRef/ query_templates/generic/ > output/genus-taxref-agrotaxon.txt
echo "************** Source: TAXREF - Target: Dbpedia - Query: GENUS ***************"
java -jar ComplexMatcher.jar http://erebe-vm2.i3s.unice.fr:8890/ http://dbpedia.org/ needs/genus-taxref.sparql query_templates/TaxRef/ query_templates/DBpedia/ > output/genus-taxref-dbpedia.txt
