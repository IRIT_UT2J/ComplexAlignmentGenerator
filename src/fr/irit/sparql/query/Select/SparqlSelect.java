package fr.irit.sparql.query.Select;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.JsonNode;
import fr.irit.sparql.query.SparqlQuery;


/**
 * A select query. No keyword has to be explicitly written, except for aggregation attributes and filters.
 */
public class SparqlSelect extends SparqlQuery
{
	private String select;
	private ArrayList <String> selectFocus;
	private String aggregate;
	private int limit = -1;

	public SparqlSelect(Set<Map.Entry<String, String>> prefix, String from, String select,
			String where, String aggregate)
	{
		super(prefix, from, where);
		this.setSelect(select);
		this.setAggregate(aggregate);
	}

	public SparqlSelect(Set<Map.Entry<String, String>> prefix, String select, String where)
	{
		super(prefix, "", where);
		this.setSelect(select);
		this.setAggregate("");
	}

	public SparqlSelect(String select,	String where)
	{
		super(new HashSet<Map.Entry<String, String>>(), "", where);
		this.setSelect(select);
		this.setAggregate("");
	}

	public SparqlSelect(String query){
		super(query);
		this.mainQuery = this.mainQuery.trim().replaceAll("SELECT", "select").replaceAll("WHERE", "where").replaceAll("\n", " ");
		this.selectFocus = new ArrayList <String>();
		//System.out.println(mainQuery);
		Pattern pattern = Pattern.compile("select[ \t\ndistinctDISTINCT]+" +
				"(\\?[A-Za-z0-9_-]+)" +
				"[ \t\n]+(\\?*[A-Za-z0-9_-]*[ \t\n]*)" +
				"where[ \t\n]*\\{(.+)\\}[ \t\n]*$");
		Matcher matcher = pattern.matcher(this.mainQuery);
		while (matcher.find()){
			this.selectFocus.add(matcher.group(1).trim());
			if (!matcher.group(2).trim().isEmpty()){
				this.selectFocus.add(matcher.group(2).trim());
			}
			this.where =matcher.group(3).trim();
			this.setAggregate("");
			/*System.out.println("***"+matcher.group(1).trim()+"****");
			System.out.println("***"+matcher.group(2).trim()+"***");
			System.out.println("***"+matcher.group(3).trim()+"***");*/
		}
		Pattern pattern2 = Pattern.compile("select([ \t\ndistinctDISTINCT]+" +
				"\\?[A-Za-z0-9_-]+" +
				"[ \t\n]+\\?*[A-Za-z0-9_-]*[ \t\n]*)" +
				"where");
		Matcher matcher2 = pattern2.matcher(this.mainQuery);
		if (matcher2.find()){
			this.select=matcher2.group(1);
		}
		
		
	}

	public String getSelect()
	{
		return select;
	}

	public void setSelect(String select)
	{
		this.select = select;
	}

	public String getAggregate()
	{
		return aggregate;
	}

	public void setAggregate(String aggregate)
	{
		this.aggregate = aggregate;
	}

	public void setLimit(int i)
	{
		this.limit = i;
	}

//	public String toString()
//	{
//
//		return    this.formatPrefixes()
//				+ "SELECT "+this.select+"\n"
//				+ (this.getFrom().equals("")?"":" FROM "+this.getFrom()+"\n")
//				+ " WHERE {\n"+this.getWhere()+"}\n"
//				+ (this.aggregate.equals("")?"":this.aggregate)
//				+ ((this.limit==-1)?"":" LIMIT "+this.limit);
//	}
	
	public String toString() {
		return this.mainQuery;
	}

	public String toSubgraphForm(){

		String ret = this.where;
		if (this.selectFocus.size()>1){
			int i=0;
			for(String sf: this.selectFocus){
				ret=ret.replaceAll(sf.replaceAll("\\?", "\\\\?")+" ", "\\?answer"+i+" ");
				ret=ret.replaceAll(sf.replaceAll("\\?", "\\\\?")+"\\.", "\\?answer"+i+".");
				ret=ret.replaceAll(sf.replaceAll("\\?", "\\\\?")+"\\}", "\\?answer"+i+"}");
				ret=ret.replaceAll(sf.replaceAll("\\?", "\\\\?")+"\\)", "\\?answer"+i+")");
				i++;
			}
		}
		else{
			ret=ret.replaceAll(this.selectFocus.get(0).replaceAll("\\?", "\\\\?")+" ", "\\?answer ");
			ret=ret.replaceAll(this.selectFocus.get(0).replaceAll("\\?", "\\\\?")+"\\.", "\\?answer.");
			ret=ret.replaceAll(this.selectFocus.get(0).replaceAll("\\?", "\\\\?")+"\\}", "\\?answer}");
			ret=ret.replaceAll(this.selectFocus.get(0).replaceAll("\\?", "\\\\?")+"\\)", "\\?answer)");
		}
		return ret.replaceAll("\n"," ").replaceAll("\\\"", "\\\"");
	}

	public static void displayResult(ArrayList<JsonNode> results)
	{
		for (JsonNode jn : results)
		{
			System.out.println(jn);
		}
	}

	public ArrayList <String> getSelectFocus() {
		return selectFocus;
	}

	public void setSelectFocus(ArrayList <String> selectFocus) {
		this.selectFocus = selectFocus;
	}

	public int getFocusLength(){
		return this.selectFocus.size();
	}
}
