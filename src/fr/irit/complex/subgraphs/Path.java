package fr.irit.complex.subgraphs;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import com.fasterxml.jackson.databind.JsonNode;

import fr.irit.complex.utils.Utils;
import fr.irit.resource.IRI;
import fr.irit.resource.Resource;
import fr.irit.sparql.Proxy.SparqlProxy;
import fr.irit.sparql.query.Exceptions.SparqlEndpointUnreachableException;
import fr.irit.sparql.query.Exceptions.SparqlQueryMalFormedException;

public class Path extends InstantiatedSubgraph {
	ArrayList<IRI> properties; // the list of N properties in the path
	ArrayList<Resource> entities; // the list of N+1 entities from the path 
	ArrayList<IRI> types; // list of N+1 most similar types of the path entities
	double similarity;
	double typeSimilarity;
	ArrayList<Boolean> inverse;

	public Path(ArrayList<IRI> properties, ArrayList<Resource> entities,ArrayList<Boolean> inverse) {
		this.properties = properties;
		this.entities = entities;
		this.types = new ArrayList<IRI>();
		similarity = 0;
		this.inverse = inverse;

	}
	public Path(Resource x, Resource y, String sparqlEndpoint, int length,ArrayList<Boolean> inverse) {
		this.properties= new ArrayList<IRI>();
		this.entities = new ArrayList<Resource>();
		this.types = new ArrayList<IRI>();
		similarity = 0;
		this.inverse = inverse;
		findPathWithLength(x, y, sparqlEndpoint, length);

	}

	public Path(Triple t,Resource x, Resource y) {
		this.properties= new ArrayList<IRI>();
		this.entities = new ArrayList<Resource>();
		this.types = new ArrayList<IRI>();
		this.inverse = new ArrayList<Boolean>();
		properties.add(t.getPredicate());
		if(t.getSubject().equals(t.getObject())) {
			inverse.add(false);
		}
		else {
			if (t.getSubject().equals(x) && t.getObject().equals(y)) {
				inverse.add(false);
			}
			else if (t.getObject().equals(x) && t.getSubject().equals(y)) {
				inverse.add(true);
			}
		}		
		entities.add(x);
		entities.add(y);
		similarity = 0;
		//		System.out.println(this.toString());
		//		System.out.println(inverse);
	}

	private void findPathWithLength(Resource x, Resource y, String sparqlEndpoint, int length) {
		String query = "";
		String queryBody = "";
		ArrayList<String> variables = new ArrayList<String>();
		
		if(!x.isIRI()) {
			variables.add("?x");
		}
		else {
			variables.add(x.toString());
		}
		
		
		for (int i =1; i <= length -1; i++) {
			variables.add("?v"+i);
		}
		if(!y.isIRI()) {
			variables.add("?y");
		}
		else {
			variables.add(y.toString());
		}

		for (int i =1; i <= length; i++) {
			if(inverse.get(i-1)) {
				queryBody += variables.get(i)+ " ?p"+i+ " "+variables.get(i-1)+". \n";
			}
			else {
				queryBody += variables.get(i-1)+ " ?p"+i+ " "+variables.get(i)+". \n";
			}

		}
		
		if(!x.isIRI()) {
			queryBody+="   filter (regex(?x, \"^"+x.toString()+"$\",\"i\"))\n";
		//	queryBody += "FILTER(str(?x)="+x.toValueString()+") \n";
		}
		if(!y.isIRI()) {
			queryBody+="   filter (regex(?y, \"^"+y.toString()+"$\",\"i\"))\n";
		//	queryBody += "FILTER(str(?y)="+y.toValueString()+") \n";
		}

		query = "SELECT DISTINCT * WHERE { " + queryBody + " }  LIMIT 20";

		//System.out.println(query);
		//run query
		SparqlProxy spProx = SparqlProxy.getSparqlProxy(sparqlEndpoint);
		ArrayList<JsonNode> ret;
		try {
			ret = spProx.getResponse(query);
			Iterator<JsonNode> retIteratorTarg = ret.iterator();
		//	System.out.println(query);
			// if more than one result, only take the first one (if instead of while)
			if (retIteratorTarg.hasNext()) {
				JsonNode next = retIteratorTarg.next();
//				System.out.println(next);
				if(next.has("x")) {
					entities.add(new Resource(next.get("x").get("value").toString().replaceAll("\"", "")));
				}
				else {
					entities.add(x);
				}
				int i = 1;
				boolean stop = false;
				while (i <= length && !stop) {
					String p = next.get("p"+i).get("value").toString().replaceAll("\"", "");
					Resource res = new Resource (p);
					if(p.equals("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")) {
						stop = true;
					}
					if(p.equals("http://www.w3.org/2002/07/owl#sameAs")) {
						stop = true;
					}
					if(p.equals("http://www.w3.org/2004/02/skos/core#exactMatch")) {
						stop = true;
					}
					if(p.equals("http://www.w3.org/2004/02/skos/core#closeMatch")) {
						stop = true;
					}
					if(p.equals("http://dbpedia.org/ontology/wikiPageWikiLink")) {
						stop = true;
					}
					if(res.isIRI()){
						this.properties.add(new IRI("<"+p+">"));
					}
					i ++;
				}
				//If a property is rdf:type, remove all properties from list
				if(stop) {
					this.properties = new ArrayList<IRI>();
				}
				if (length>=2 && !stop) {
					for (int j =1 ;j <= length-1;j++) {
						String v = next.get("v"+j).get("value").toString().replaceAll("\"", "");
						Resource res = new Resource (v);
						if(res.isIRI()){
							this.entities.add(new IRI("<"+v+">"));
						}
						else {
							this.entities.add(res);
						}
					}
				}
				if(next.has("y")) {
					entities.add(new Resource(next.get("y").get("value").toString().replaceAll("\"", "")));
				}
				else {
					entities.add(y);
				}
				
			}




		} catch (SparqlQueryMalFormedException e) {
			e.printStackTrace();
		} catch (SparqlEndpointUnreachableException e) {
			e.printStackTrace();
		}
	}

	public double compareLabel(HashSet<String> targetLabels, double threshold, String targetEndpoint, double typeThreshold){
		// compare property path labels to target labels : first similarity of the path
		similarity = 0;
		for(IRI prop:this.properties) {
			try {
				prop.retrieveLabels(targetEndpoint);
				this.similarity += Utils.similarity(prop.getLabels(), targetLabels, threshold);
			} catch (SparqlQueryMalFormedException | SparqlEndpointUnreachableException e) {
				e.printStackTrace();
			}
		}

		//for each entity, compare the similarity of its most similarType to the target label. 
		for (int i =0; i< entities.size();i++) {
			Resource ent = entities.get(i);
			//System.out.println(ent);
			if (ent instanceof IRI) {
				IRI type = types.get(i);
				if (type !=null) {
					double scoreType = Utils.similarity(type.getLabels(), targetLabels, threshold);
					// If the type similarity is higher than the first path sim,
					//System.out.println("prop "+ this.similarity+ " <-> "+type+" "+scoreType);
					if (scoreType > typeThreshold) {
						// keep the type and add the type similarity value to the final score
						this.typeSimilarity+=scoreType;
					}
					else {
						this.types.set(i, null);
					}
				}
			}
		}
		//	System.out.println(this.types.size());
		if(this.pathFound()) {
			this.similarity+=0.5;//if path
		}
		
		return this.getSimilarity();
	}

	public boolean pathFound() {
		return !properties.isEmpty();
	}

	//version all entities
	public String toString() {
		String ret = "";
		for (int i = 0; i < properties.size();i++) {
			ret+= entities.get(i) + " " + properties.get(i) + " " + entities.get(i+1) + ".  ";
		}
		return this.getSimilarity()+" <-> "+ret ;
	}

	public String toSubGraphString() {
		String ret = "";
		ArrayList<String> variables = new ArrayList<String>();
		variables.add("?answer0");
		for (int i =1; i <= properties.size() -1; i++) {
			variables.add("?v"+i);
		}
		variables.add("?answer1");
//		System.out.println(properties);
//		System.out.println(types);
//		System.out.println(inverse);

		for (int i = 0; i < properties.size();i++) {
			String xStr = variables.get(i);
			String yStr = variables.get(i+1);			

			if (this.types.get(i)!= null) {
				ret+= xStr + " a "+this.types.get(i)+ ".  ";
			}
			if(inverse.get(i)) {
				ret+= yStr + " " + properties.get(i) + " " + xStr + ".  ";
			}
			else {
				ret+= xStr + " " + properties.get(i) + " " + yStr + ".  ";
			}
		}
		if (this.types.get(properties.size())!= null) {
			ret+= variables.get(properties.size()) + " a "+this.types.get(properties.size())+ ".  ";
		}
		return ret ;
	}

	public double getSimilarity() {
		return this.similarity+this.typeSimilarity;
	}

	public void getMostSimilarTypes(String endpointUrl,  HashSet<String> targetLabels, double threshold) {
		for (Resource r: entities) {
			if(r instanceof IRI) {
				IRI type = ((IRI) r).findMostSimilarType(endpointUrl, targetLabels, threshold);
				types.add(type);
			}
			else {
				types.add(null);
			}
		}
	}

	public ArrayList<IRI> getProperties(){
		return this.properties;
	}
	public ArrayList<IRI> getTypes(){
		return this.types;
	}
	public ArrayList<Boolean> getInverse(){
		return this.inverse;
	}
}
