package fr.irit.complex.answer;

import java.util.HashSet;

import fr.irit.complex.subgraphs.InstantiatedSubgraph;
import fr.irit.sparql.query.Exceptions.SparqlEndpointUnreachableException;
import fr.irit.sparql.query.Exceptions.SparqlQueryMalFormedException;
import fr.irit.sparql.query.Select.SparqlSelect;

public abstract class Answer {
	HashSet<String> goodTriples ;
	
	public Answer(){
		goodTriples = new HashSet<String>();
		
	}
	
	public void getSimilarIRIs(String targetEndpoint, String sourceEndpoint) throws SparqlQueryMalFormedException, SparqlEndpointUnreachableException{}
	
	public void getExistingMatches(String sourceEndpoint, String targetEndpoint) throws SparqlQueryMalFormedException, SparqlEndpointUnreachableException{}
	
	public void retrieveIRILabels(String endpointURL) throws SparqlQueryMalFormedException, SparqlEndpointUnreachableException{}
	
	public HashSet<InstantiatedSubgraph> findCorrespondingSubGraph(SparqlSelect query, String targetEndpoint, double similarityThreshold, String sourceEndpoint) throws SparqlQueryMalFormedException, SparqlEndpointUnreachableException{
		return new HashSet<InstantiatedSubgraph>();
	}

	public boolean hasMatch(){ 
		return false;
	}
	
	public String printMatchedEquivalents() {
		return "";
	}

}
