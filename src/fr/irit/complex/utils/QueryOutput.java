package fr.irit.complex.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;

import fr.irit.complex.subgraphs.PathSubgraph;
import fr.irit.complex.subgraphs.SubgraphForOutput;
import fr.irit.complex.subgraphs.TripleSubgraph;
import fr.irit.sparql.query.Select.SparqlSelect;

public class QueryOutput extends Output {
	
	private String outputFolder;
	private HashMap<String,String> CQANames;

	public QueryOutput(Parameters params) {
		super(params);
		this.outputFolder = params.outputQUERYfolder;
		this.CQANames = params.CQANames;
	}
	
	public void init(){
	}
	
	public void addToOutput(ArrayList<SubgraphForOutput> output, SparqlSelect sq){
		// create folder with name of cqa
		String cqaName = CQANames.get(sq.toUnchangedString()).replaceAll("\\..*", "");
		File theDir = new File(outputFolder+"/"+cqaName);

		// if the directory does not exist, create it
		if (!theDir.exists()) {
		    System.out.println("creating directory: " + theDir.getName());
		    try{
		        theDir.mkdir();
		    } 
		    catch(SecurityException se){
		        se.printStackTrace();
		    }        
		}
		// for each subgraph in output
		int i =0;
		for (SubgraphForOutput s: output){
			String outputQuery ="SELECT "+sq.getSelect()+ " WHERE {";
			if(s instanceof TripleSubgraph) {
				// If common part is the subject or object, always express the extension
				if (s.toIntensionString().contains("somePredicate")){ 
					outputQuery += ((TripleSubgraph) s).toSPARQLExtension();
				}
				// If common part is the predicate 
				else if (s.toIntensionString().contains("someObject")||s.toIntensionString().contains("someSubject")){
					// and if the predicate similarity is higher than the object/subject similarity --> Intension
					if(((TripleSubgraph)s).predicateHasMaxSim()) {
						outputQuery += ((TripleSubgraph) s).toIntensionString();
					}					
					// else --> extension
					else {
						outputQuery += ((TripleSubgraph) s).toSPARQLExtension();
					}		

				}
				else {
					outputQuery += ((TripleSubgraph) s).toSPARQLExtension();
				}
			}

			// same for PathSubgraph
			else if (s instanceof PathSubgraph) {
				outputQuery += ((PathSubgraph) s).toIntensionString();
			}
			
			
			
			outputQuery += " } ";
			outputQuery = toSubgraphForm(outputQuery, sq.getSelectFocus());
			try {
				PrintWriter writer = new PrintWriter(this.outputFolder+"/"+cqaName+"/"+cqaName+i+".sparql", "UTF-8");
				writer.println(outputQuery);
				writer.close(); 
			} catch (FileNotFoundException | UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
			i++;
			
		}	
		
	}
	
	public String toSubgraphForm(String queryContent, ArrayList<String> selectFocus){

		String ret = queryContent;
		if (selectFocus.size()>1){
			int i=0;
			for(String sf: selectFocus){
				ret=ret.replaceAll( "\\?answer"+i+" ",sf+" ");
				ret=ret.replaceAll( "\\?answer"+i+"\\.",sf+".");
				ret=ret.replaceAll( "\\?answer"+i+"\\}",sf+"}");
				i++;
			}
		}
		else{
			ret=ret.replaceAll( "\\?answer ",selectFocus.get(0)+" ");
			ret=ret.replaceAll( "\\?answer\\.",selectFocus.get(0)+".");
			ret=ret.replaceAll( "\\?answer\\}",selectFocus.get(0)+"}");
		}
		return ret;
	}

}
