package fr.irit.complex.utils;

import java.util.HashMap;

public class SPARQLNode {
	
	String name;
	HashMap<String,String> triples;
	HashMap<String,SPARQLNode> neighbors;
	boolean explored;
	SPARQLNode predecessor ;
	
	public SPARQLNode(String n) {
		this.name = n;
		this.triples = new HashMap<String,String>();
		this.neighbors = new HashMap<String,SPARQLNode>();
		this.explored = false;
	}
	
	public void addNeighbour(SPARQLNode neighbor, String triple) {
		if(this.neighbors.containsKey(neighbor.getName())) {
			//triples.put(neighbor.getName(), triples.get(neighbor.getName())+ " "+triple);
			System.out.println("more than one prop: "+triples.get(neighbor.getName())+ " "+triple );
		}
		else {
			neighbors.put(neighbor.getName(), neighbor);
			triples.put(neighbor.getName(), triple);
		}
	}
	
	public HashMap<String,SPARQLNode> getNeighbors(){
		return this.neighbors;
	}
	
	public boolean hasNeighbor(String n) {
		return this.neighbors.containsKey(n);
	}
	
	public void setPredecessor(SPARQLNode pred) {
		this.predecessor = pred;
	}
	
	public SPARQLNode getPredecessor() {
		return this.predecessor;
	}
	
	public boolean isExplored() {
		return this.explored;
	}
	
	public void explore() {
		this.explored=true;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getTriple(String n) {
		return this.triples.get(n);
	}
	
	public String toString() {
		return  this.name+" : "+this.neighbors.keySet();
	}


}
