package fr.irit.complex.utils;

import java.util.ArrayList;

import fr.irit.complex.subgraphs.SubgraphForOutput;
import fr.irit.sparql.query.Select.SparqlSelect;

public abstract class Output {
	
	protected String sourceEndpoint;
	protected String targetEndpoint;
	
	public Output(Parameters params){
		sourceEndpoint = params.sourceEndpoint;
		targetEndpoint = params.targetEndpoint;
		
	}
	
	public void init(){
	}
	
	public void addToOutput(ArrayList<SubgraphForOutput> output, SparqlSelect sq){
	}
	
	public void end() {}

}
