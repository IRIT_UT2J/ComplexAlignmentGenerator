package fr.irit.complex.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.irit.sparql.exceptions.NotAFolderException;
import fr.irit.sparql.files.FolderManager;
import fr.irit.sparql.query.Select.SparqlSelect;

public class Parameters {
	protected Map<String,FolderManager> queryTemplates ;
	protected boolean outputEDOAL;
	protected boolean outputSPARQL;
	protected boolean outputQUERY;
	protected String outputEndpoint;
	protected String outputEDOALfile;
	protected String outputQUERYfolder;
	protected String sourceEndpoint;
	protected String targetEndpoint;
	protected String outputAlignmentIRI;
	protected ArrayList<SparqlSelect> queries;
	protected HashMap<String,String> CQANames;
	protected String CQAFolder;
	protected boolean startEmbeddedFuseki;
	protected boolean cqaToBeGenerated;

	public Parameters(){

	}

	public boolean init(String filePath) throws JsonProcessingException, IOException, NotAFolderException{
		boolean noProblem = true;
		cqaToBeGenerated = false;
		startEmbeddedFuseki =false;
		ArrayList<JsonNode> outputs = new ArrayList<>();
		queryTemplates = new HashMap<>();
		File fparameter = new File(filePath);
		Scanner paramSc = new Scanner(fparameter);
		this.queries = new ArrayList<SparqlSelect>();
		this.CQANames =new HashMap<String,String>();



		String paramStr = "";
		while(paramSc.hasNextLine()){
			paramStr+=paramSc.nextLine();
		}
		paramSc.close();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode paramRoot = mapper.readTree(paramStr);
		/**Check that all mandatory fields are given in parameter file*/

		//Source ontology
		noProblem =noProblem && ontologyParameter("source_ontology", paramRoot);

		//Target ontology
		noProblem =noProblem && ontologyParameter("target_ontology", paramRoot);

		//CQA folder
		if (!paramRoot.has("CQAs_SPARQL_folder")){
			//System.err.println("No CQAs_SPARQL_folder in parameter file");
			//noProblem=false;
			this.CQAFolder = "generated_CQA";
			cqaToBeGenerated=true;

		}
		else{
			this.CQAFolder= paramRoot.get("CQAs_SPARQL_folder").asText();

		}
		

		//Query templates
		if (noProblem){
			queryTemplates.get(sourceEndpoint).loadQueries();
			queryTemplates.get(targetEndpoint).loadQueries();
		}

		//Outputs
		if (!paramRoot.has("output")){
			System.err.println("No output in parameter file");
			noProblem=false;
		}
		else {
			Iterator<JsonNode> i = paramRoot.get("output").iterator();
			while (i.hasNext())
			{
				outputs.add(i.next());
			}
			if (outputs.size() == 0){
				System.err.println("The output parameter is empty");
				noProblem = false;
			}
			for(JsonNode o : outputs){
				if (!o.has("type")){
					System.err.println("An output has no type field");
					noProblem = false;
				}
				//EDOAL alignment
				else if (o.get("type").asText().equals("edoal")){
					outputEDOAL = true;
					if (o.has("file")){
						outputEDOALfile = o.get("file").asText();
					}
					else{
						outputEDOALfile = "./output.edoal";
					}

				}
				//QUERY outputs
				else if (o.get("type").asText().equals("query")){
					outputQUERY = true;
					if (o.has("folder")){
						outputQUERYfolder = o.get("folder").asText();
					}
					else{
						outputQUERYfolder = "./output_queries/";
					}
					
					File theDir = new File(outputQUERYfolder);

					// if the directory does not exist, create it
					if (!theDir.exists()) {
					    System.out.println("creating directory: " + theDir.getName());
//					    boolean result = false;

					    try{
					        theDir.mkdir();
//					        result = true;
					    } 
					    catch(SecurityException se){
					        se.printStackTrace();
					    }        
//					    if(result) {    
//					        System.out.println("DIR created");  
//					    }
					}
					
				}
				//Output in SPARQL endpoint
				else if (o.get("type").asText().equals("sparql")){
					outputSPARQL = true;
					if (!o.has("endpoint")){
						System.err.println("The SPARQL output has no specified endpoint");
						noProblem = false;
					}
					else{
						outputEndpoint = o.get("endpoint").asText();
						queryTemplates.put(outputEndpoint, new FolderManager("query_templates/Output/"));
						queryTemplates.get(outputEndpoint).loadQueries();						
					}
				}
			}
		}
		if(noProblem && this.startEmbeddedFuseki){
			EmbeddedFuseki fusekiServer = EmbeddedFuseki.getFusekiServer();
			fusekiServer.startServer();
		}
		
		if(noProblem && cqaToBeGenerated) {
			CQAGenerator generator = new CQAGenerator (this.sourceEndpoint, this.CQAFolder);
			generator.cleanCQARepository();
			generator.createCQAs();
		}
		File [] queryFiles = new File(this.CQAFolder).listFiles();
		for(File fquery : queryFiles){
			Scanner squery = new Scanner(fquery);
			//System.out.println(fquery.getName());
			
			String query = squery.useDelimiter("\\Z").next();
			//System.out.println(query);
			SparqlSelect sq = new SparqlSelect(query);
			queries.add(sq);
			CQANames.put(sq.toUnchangedString(),fquery.getName());
			squery.close();
		}

		return noProblem;
	}


	public boolean ontologyParameter(String ontology, JsonNode paramRoot) throws NotAFolderException{
		boolean noProblem = true;
		String endpoint = "";
		if (!paramRoot.has(ontology)){
			System.err.println("No "+ontology+" in parameter file");
			noProblem=false;
		}
		else if (!paramRoot.get(ontology).hasNonNull("file") 
				&& !paramRoot.get(ontology).hasNonNull("sparqlEndpoint")){
			System.err.println("The "+ontology+" field has no file or sparqlEndpoint parameter");
			noProblem=false;
		}
		else {
			if (paramRoot.get(ontology).has("sparqlEndpoint")){
				endpoint = paramRoot.get(ontology).get("sparqlEndpoint").asText();				
			}
			else if (paramRoot.get(ontology).has("file")){
				EmbeddedFuseki fusekiServer = EmbeddedFuseki.getFusekiServer();
				fusekiServer.addDataset(ontology,paramRoot.get(ontology).get("file").asText());
				endpoint = "http://localhost:3031/"+ontology+"/";
				this.startEmbeddedFuseki = true;
			}
			if (paramRoot.get(ontology).has("query_templates")){
				queryTemplates.put(endpoint, new FolderManager(paramRoot.get(ontology).get("query_templates").asText()));
			}
			else{
				queryTemplates.put(endpoint, new FolderManager("query_templates/generic/"));
			}
		}
		if (ontology.equals("source_ontology")){
			this.sourceEndpoint = endpoint;
		}
		else{
			this.targetEndpoint = endpoint;
		}

		return noProblem;

	}

}
