package fr.irit.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.JsonNode;

import fr.irit.complex.utils.Utils;
import fr.irit.sparql.Proxy.SparqlProxy;
import fr.irit.sparql.query.Exceptions.SparqlEndpointUnreachableException;
import fr.irit.sparql.query.Exceptions.SparqlQueryMalFormedException;

public class Resource {

	protected String value;
	protected HashSet<IRI> similarIRIs ;

	public Resource(String val){
		this.value=val.replaceAll("\\\\", "");
		similarIRIs = new HashSet<IRI>();
	}

	public boolean isIRI(){
		Pattern pattern = Pattern.compile("[a-z][/:\\#]");
		Matcher matcher = pattern.matcher(this.value);
		//System.out.println(matcher.find());
		//System.out.println(this.value+ " "+ (!this.value.contains(" ") && matcher.find()));
		return !this.value.contains(" ") && matcher.find() ;
	}

	public void findSimilarResource(String targetEndpoint) throws SparqlQueryMalFormedException, SparqlEndpointUnreachableException{

		/**Label search (heavy on large KBs)*/
		Map<String, String> substitution = new HashMap<>();
		substitution.put("labelValue", this.value);
		if (this.value.length()>1){
			substitution.put("LabelValue", this.value.substring(0, 1).toUpperCase() + this.value.substring(1));
		}
		else{
			substitution.put("LabelValue", "\""+this.value.toUpperCase()+"\"");
		}
		String query = Utils.getInstance().getSimilarQuery(targetEndpoint,substitution);
		//System.out.println(query);
		SparqlProxy spIn = SparqlProxy.getSparqlProxy(targetEndpoint);


		ArrayList<JsonNode> ret = spIn.getResponse(query);

		Iterator<JsonNode> retIterator = ret.iterator();
		while (retIterator.hasNext()) {
			String s = retIterator.next().get("x").get("value").toString().replaceAll("\"", "");
			//System.out.println(s);
			similarIRIs.add(new IRI("<"+s+">"));
		}
		
		substitution.put("labelValue", "\""+this.value.substring(0, 1).toUpperCase() + this.value.substring(1)+"\"@en");
		query = Utils.getInstance().getSimilarQuery(targetEndpoint,substitution);
		ret = spIn.getResponse(query);

		retIterator = ret.iterator();
		while (retIterator.hasNext()) {
			String s = retIterator.next().get("x").get("value").toString().replaceAll("\"", "");
			//System.out.println(s);
			similarIRIs.add(new IRI("<"+s+">"));
		}

		substitution.put("labelValue", "\""+this.value.substring(0, 1).toUpperCase() + this.value.substring(1)+"\"");
		query = Utils.getInstance().getSimilarQuery(targetEndpoint,substitution);
		ret = spIn.getResponse(query);

		retIterator = ret.iterator();
		while (retIterator.hasNext()) {
			String s = retIterator.next().get("x").get("value").toString().replaceAll("\"", "");
			//System.out.println(s);
			similarIRIs.add(new IRI("<"+s+">"));
		}

	}
	
	public String toValueString(){
		if(!(isIRI())){
			return "\""+this.value+"\"";
		}
		else {
			return this.toString();
		}
	}
	
	public String toString(){
		return this.value;
	}
	
	public int hashCode(){
		return this.value.hashCode();
	}
	
	public boolean equals(Object obj){
		if (obj instanceof Resource){
			return this.value.equals(((Resource)obj).value);
		}
		else{
			return false;
		}
	}
	
	public HashSet<IRI> getSimilarIRIs(){
		return this.similarIRIs;
	}


}
