/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oaeicompare;

import com.fasterxml.jackson.databind.JsonNode;
import fr.irit.sparql.Proxy.SparqlProxy;
import fr.irit.sparql.query.Exceptions.SparqlEndpointUnreachableException;
import fr.irit.sparql.query.Exceptions.SparqlQueryMalFormedException;
import fr.irit.sparql.query.Select.SparqlSelect;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrateur
 */
public class OAEICompare {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*if (args.length != 2) {
            System.err.println("Expected arguments : \n"
                    + " arg 1 : URL of an ontology expected\n"
                    + " arg 2 : file with queries\n"
            );
        } else {*/
            try {

            //    String onto_url = args[0];
             //   File fquery = new File("query/" + args[1] + ".txt");
              //  File fgoal = new File("goal/" + args[1] + "_goal.txt");
               // File fSameAs = new File("result/" + args[1] + "_res.txt");
            //File fgoal = new File("goal/qa1_goal.txt");
                //File fquery = new File("query/qa1.txt");
                String onto_url="http://localhost:3030/AgronomicTaxon/";
                File fneed = new File("needs/need1.sparql");

                //Récupération des résultats goal
                ArrayList<String> trueResult = new ArrayList<>();
                /*ArrayList<String> goalResult = new ArrayList<>();
                
                ArrayList<String> trueResultSameAs = new ArrayList<>();
                HashMap<String, String> mapBonResultat = new HashMap<String, String>();
                Scanner sgoal = new Scanner(fgoal);
                while (sgoal.hasNext()) {
                    goalResult.add(sgoal.next());
                }
                sgoal.close();
                Scanner squery = new Scanner(fquery);
                String query = squery.useDelimiter("\\Z").next();*/
                String query = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
                		"SELECT ?x ?y WHERE{?x rdfs:label ?y}";
                //System.out.println(query);
                SparqlProxy spIn = SparqlProxy.getSparqlProxy(onto_url);

                ArrayList<JsonNode> ret = spIn.getResponse(query);

                Iterator<JsonNode> retIterator = ret.iterator();
                while (retIterator.hasNext()) {
                    String s = retIterator.next().get("x").get("value").toString().replaceAll("\"", "");
                    trueResult.add(s);
                    System.out.println(s);
                }
            //Récupérer les sameAs de toutes les URI de trueResult
                //faire boucle
                /*
                FileWriter fw;
                try {
                    fw = new FileWriter(fSameAs);
                    ArrayList<ArrayList<JsonNode>> retSameAs = new ArrayList<ArrayList<JsonNode>>();
                    for (int i = 0; i < trueResult.size(); i++) {
                        SparqlSelect sameAs = new SparqlSelect("?x", "{<" + trueResult.get(i) + ">" + "<http://www.w3.org/2002/07/owl#sameAs> " + "?x}UNION{?x <http://www.w3.org/2002/07/owl#sameAs> <" + trueResult.get(i) + ">}");
                        String querySameAs = sameAs.toString();
                        //System.out.println(sameAs);
                        ArrayList<JsonNode> retSameAsi = spIn.getResponse(sameAs);
                        retSameAs.add(retSameAsi);
                        //System.out.println(retSameAsi);
                    }
                    Iterator<ArrayList<JsonNode>> retSameAsIt = retSameAs.iterator();
                    while (retSameAsIt.hasNext()) {
                        Iterator<JsonNode> retSameAsItI = retSameAsIt.next().iterator();
                        while (retSameAsItI.hasNext()) {
                            String s = retSameAsItI.next().get("x").get("value").toString().replaceAll("\"", "");
                            //System.out.println(s);
                            fw.write(s + "\n");
                            trueResultSameAs.add(s);
                            fw.flush();
                        }
                    }
                    fw.close();
                } catch (IOException ex) {
                    Logger.getLogger(OAEICompare.class.getName()).log(Level.SEVERE, null, ex);
                }
                boolean nonTrouve;
                int j;
                int nbPasBonne = 0;
                for (int i = 0; i < trueResultSameAs.size(); i++) {
                    nonTrouve = true;
                    j = 0;
                    while (nonTrouve && j < goalResult.size()) {
                        if (trueResultSameAs.get(i).equals(goalResult.get(j))) {
                            nonTrouve = false;
                            mapBonResultat.put(trueResultSameAs.get(i), trueResultSameAs.get(i));
                        }
                        j++;
                    }
                    if (nonTrouve) {
                        nbPasBonne++;
                    }
                }
                nbBienAttribues = mapBonResultat.size();
                System.out.println("pas bonnes urls : " + nbPasBonne);
                System.out.println("nb candidates : " + trueResult.size());
                System.out.println("map size : " + nbBienAttribues);
                System.out.println("trueSameAs : " + trueResultSameAs.size());
                precision = (float) (nbBienAttribues) / (float) (trueResultSameAs.size());
                rappel = (float) (nbBienAttribues) / (float) (goalResult.size());
                fMesure = 2 * (precision * rappel) / (precision + rappel);
                System.out.println("Précision : " + precision);
                System.out.println("Rappel : " + rappel);
                System.out.println("F-Mesure : " + fMesure);

            } catch (SparqlQueryMalFormedException | SparqlEndpointUnreachableException | FileNotFoundException ex) {
                Logger.getLogger(OAEICompare.class.getName()).log(Level.SEVERE, null, ex);
            }*/
            }catch (SparqlQueryMalFormedException | SparqlEndpointUnreachableException ex) {
                Logger.getLogger(OAEICompare.class.getName()).log(Level.SEVERE, null, ex);
            }
       // }

    }

}
