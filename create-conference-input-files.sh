#!/bin/sh


ontologies='cmt conference confOf edas ekaw'
ontologies2='ekaw edas confOf conference cmt'
#datasets="eswc 100 80 60 40 20"
datasets="100"

#levenshtein="0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0"

#levenshtein="0.8"

#nbInstSupp="1 2 3 4 5 6 7 8 9 20 100"
nbInstSupp="cqa_reassess"

parameters_folder=`pwd`/input/conference/

#for lev in $levenshtein 
for nsupp in $nbInstSupp
do
mkdir output/conference/test_"$nsupp"
#mkdir output/conference-query/test_"$nsupp"
for set in $datasets
do
  for onto1 in $ontologies
    do
      for onto2 in $ontologies2
       do
         echo $onto1 - $onto2 _ $set
	 if [ $onto1 != $onto2 ]
	  then
	
	content="{\"source_ontology\": 
	{       \"sparqlEndpoint\":\"http://localhost:3030/"$onto1"_"$set"/\",
      \"query_templates\": \"query_templates/generic/\"},
    \"target_ontology\": 
	{       \"sparqlEndpoint\":\"http://localhost:3030/"$onto2"_"$set"/\",
      \"query_templates\": \"query_templates/generic/\"},
\"CQAs_SPARQL_folder\" : \"needs/"$onto1"/\",
    \"output\": [{\"type\":\"edoal\",
                \"file\":\"./output/conference/test_"$nsupp"/"$onto1"-"$onto2".edoal\"},
{\"type\":\"query\", \"folder\":\"./output/conference-query/test_"$nsupp"/"$onto1-$onto2"/\"}]}"
	#echo $content
	echo $content > $parameters_folder/$onto1-$onto2"_"$nsupp.json


	fi	
    done
  done
done
done
